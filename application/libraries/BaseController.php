<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

/**
 * Class : BaseController
 * Base Class to control over all the classes
 * @author : Esther Praveen 
 * @version : 1.1
 * @since : 14 August 2018
 */
class BaseController extends CI_Controller {
	protected $role = '';
	protected $vendorId = '';
	protected $name = '';
	protected $roleText = '';
	protected $global = array ();
	protected $lastLogin = '';


	/*
		1 Administrator
		2 Supervisior
		3 Manager
		4 General Manager
	*/
	
	/**
	 * Takes mixed data and optionally a status code, then creates the response
	 * @access public
	 * @param array|NULL $data
	 *  Data to output to the user
	 *  running the script; otherwise, exit
	 */
	public function response($data = NULL) {
		$this->output->set_status_header ( 200 )->set_content_type ( 'application/json', 'utf-8' )->set_output ( json_encode ( $data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) )->_display ();
		exit ();
	}
	
	//Administrator
	public function isLogin(){
    	if ($this->session->userdata('sub_login_id') && $this->session->userdata('userrole')== 1 ) {
    	}else{
    		redirect(base_url());
    	} 
    }

    // Supervisior
	public function issupervisior(){
    	if ($this->session->userdata('sub_login_id') && $this->session->userdata('userrole')== 2 ) {
    	}else{
    		redirect(base_url());
    	} 
    }

    //Manager
	public function ismanger(){
    	if ($this->session->userdata('sub_login_id') && $this->session->userdata('userrole')== 3 ) {
    	}else{
    		redirect(base_url());
    	} 
    }

    //General Manager
	public function isgernalmanger(){
    	if ($this->session->userdata('sub_login_id') && $this->session->userdata('userrole')== 4 ) {
    	}else{
    		redirect(base_url());
    	} 
    }
	public function isSuperAdmin(){
    	if ($this->session->userdata('admin_login_id') == true ) {
    	}else{
    		redirect(base_url());
    	} 
    }

    function pk($data){
    	echo "<pre/>"; print_r($data);
    }
	
	//This function used to check the user is logged in or not

	public function isLoggedIn() {
		/*$isLoggedIn = $this->session->userdata ( 'isLoggedIn' );*/
		$isLoggedIn = $this->session->userdata ( 'admin_login_id' );
		
		if (! isset ( $isLoggedIn ) || $isLoggedIn != TRUE) {
			redirect ( 'loginMe' );
		} else {
			$this->role = $this->session->userdata('admin_role_id');
			$this->adminId = $this->session->userdata('admin_login_id');
			$this->name = $this->session->userdata('admin_name');
			$this->username = $this->session->userdata('admin_username');
			$this->email = $this->session->userdata('admin_email');
			
			$this->global['name'] = $this->name;
			$this->global['role'] = $this->role;
			$this->global['username'] = $this->username;
			$this->global['email'] = $this->email;
		}
	}
	public function isadminLoggedIn() {
		if (! isset ( $isLoggedIn ) || $isLoggedIn != TRUE) {
			redirect ( 'loginMe' );
		} 
	}
		
	/**
	 * This function is used to load the set of views
	 */
	function loadThis() {
		$this->global ['pageTitle'] = 'wageniCRM : Access Denied';		
		$this->load->view ( 'includes/header', $this->global );
		$this->load->view ( 'access' );
		$this->load->view ( 'includes/footer' );
	}
	
	/**
	 * This function is used to logged out user from system
	 */
	function logout() {
		$this->session->sess_destroy();		
		redirect ( 'loginMe' );
	}

	/**
     * This function used to load views
     * @param {string} $viewName : This is view name
     * @param {mixed} $headerInfo : This is array of header information
     * @param {mixed} $pageInfo : This is array of page information
     * @param {mixed} $footerInfo : This is array of footer information
     * @return {null} $result : null
     */
    function loadViews($viewName = "", $headerInfo = NULL, $pageInfo = NULL, $footerInfo = NULL){

        $this->load->view('includes/header', $headerInfo);
        $this->load->view($viewName, $pageInfo);
        $this->load->view('includes/footer', $footerInfo);
    }
	
	/**
	 * This function used provide the pagination resources
	 * @param {string} $link : This is page link
	 * @param {number} $count : This is page count
	 * @param {number} $perPage : This is records per page limit
	 * @return {mixed} $result : This is array of records and pagination data
	 */
	function paginationCompress($link, $count, $perPage = 10, $segment = SEGMENT) {
		$this->load->library ( 'pagination' );

		$config ['base_url'] = base_url () . $link;
		$config ['total_rows'] = $count;
		$config ['uri_segment'] = $segment;
		$config ['per_page'] = $perPage;
		$config ['num_links'] = 5;
		$config ['full_tag_open'] = '<nav><ul class="pagination">';
		$config ['full_tag_close'] = '</ul></nav>';
		$config ['first_tag_open'] = '<li class="arrow">';
		$config ['first_link'] = 'First';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="arrow">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li class="arrow">';
		$config ['next_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li class="arrow">';
		$config ['last_link'] = 'Last';
		$config ['last_tag_close'] = '</li>';
	
		$this->pagination->initialize ( $config );
		$page = $config ['per_page'];
		$segment = $this->uri->segment ( $segment );
	
		return array (
				"page" => $page,
				"segment" => $segment
		);
	}
}