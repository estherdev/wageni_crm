<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Common {

	/**
	 * Constructor
	 *
	 * Get instance for Database Lib
	 *
	 * @access	public
	 */
	function Common()
	{
		$this->CI =& get_instance();
	}
	/*public function totaluser()
	{
		return $this->CI->db->from('users')->count_all_results();
	}	
	function totalCrewUsers()
	{
		return $this->CI->db->where(['roleId'=>1])->from("users")->count_all_results();		
	}
	function totalOwnerUsers()
	{
		return $this->CI->db->where(['roleId'=>2])->from("users")->count_all_results();		
	}*/
	function totalSubscriber()
	{
		return $this->CI->db->from("subscriberlogin")->count_all_results();		
	}
	function totalCompany()
	{
		return $this->CI->db->from("subscriberpackagemapping")->count_all_results();		
	}
	//Fetch Package Roles
	public function fetchPackages()
	{
		$this->CI->db->select('*');
		$this->CI->db->from('packages');
		$query = $this->CI->db->get();
		if($query){
			return $query->result();
		}else{
			return false;
		}		
	}

	//Fetch Subscriber Roles
	public function fetchSubscriberRoles()
	{
		$this->CI->db->select('*');
		$this->CI->db->from('subscriberroles');
		$query = $this->CI->db->get();
		if($query){
			return $query->result();
		}else{
			return false;
		}		
	}
	//Fetch FAQ
	public function fetchFaq()
	{
		$this->CI->db->select('*');
		//$this->CI->db->order_by('faq_id','DESC');
		$this->CI->db->from('faq');
		$query = $this->CI->db->get();
		if($query){
			return $query->result();
		}else{
			return false;
		}		
	}
	//Fetch Admin Roles
	public function fetchAdminRoles()
	{
		$this->CI->db->select('*');
		$this->CI->db->from('adminemprole');
		$query = $this->CI->db->get();
		if($query){
			return $query->result();
		}else{
			return false;
		}		
	}

	//Fetch Score Name
	public function fetchScore()
	{
		$this->CI->db->select('*');
		$this->CI->db->from('score_name');
		$query = $this->CI->db->get();
		if($query){
			return $query->result();
		}else{
			return false;
		}		
	}
		
	/* For Admin Sub menu  */
	public function fetchadminSubMenu()
	{
		//$this->CI->db->select('*');
		//$this->CI->db->from('sub_menu');
		$this->CI->db->select('sm.*, m.menu_en');
		$this->CI->db->from('sub_menu as sm');
		$this->CI->db->join('menu as m', 'm.id=sm.menu_id');
		$query = $this->CI->db->get();
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}
	
	
}
?>