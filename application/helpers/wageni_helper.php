<?php 
if(! defined('BASEPATH')) exit('no direct script access allowed');

    if(! function_exists('email')){
    	function email($email=null,$msg=null,$sub=null){
    	$ci=& get_instance();

            $mailMsg='<!DOCTYPE html>
            <html>
            <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Wageni Newsletter</title>
            <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
            </head>
            <body style="background-color: #f1f1f1; font-family: raleway, sans-serif; font-size: 15px; margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f1f1f1; font-family: "raleway", sans-serif; font-size: 15px;">
            <tr>
            <td align="center">
            <table border="0" cellpadding="10" cellspacing="0" width="600" align="center">
            <tr>
            <td align="center">
            <table border="0" cellpadding="15" cellspacing="0" width="600" style="background: transparent;">
            <tr>
            <td align="center"><a href=""><img src="http://wageni.us.tempcloudsite.com/globalassets/admin/logo1.png"></a></td>
            </tr>
            </table>
            <table border="0" cellpadding="10" cellspacing="10" width="600" style="background: #fff;">
            <tr>
            <td align="center" style="font-size: 36px; font-weight: bold; color: #555; letter-spacing: 1px;">Welcome to Wageni CRM.</td>
            </tr>
            <tr>
            <td>'.$msg.'</td>
            </tr>
            </table>
            <table border="0" cellpadding="15" cellspacing="0" width="600" style="background: transparent;">
            <tr>
            <td align="center">Copyright &copy; 2018 <a href="http://wageni.us.tempcloudsite.com">wagenicrm.com</a>. All rights reserved.</td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
            </body>
            </html>';
           //$ci->load->library('email');
            $ci->email->set_newline("\r\n");          
            $ci->email->from('praveen@panindia.in', 'Wageni CRM');
            $ci->email->to($email);
            $ci->email->subject($sub);
            $ci->email->message($mailMsg);
            $ci->email->send();
    	}
    }

    function sendEmail($to,$from,$subject,$message,$cc=array(),$bcc=array(),$attachments=array()) {
        if(isset($to) && !empty($subject) && !empty($message)){
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'mastcrew22@gmail.com',
                'smtp_pass' => 'crew@1234',
               // 'smtp_timeout'  => 1, 
                'mailtype'  => 'html', 
                'charset'   => 'UTF-8',
            );

            $CI =& get_instance();
            $CI->load->library('email',$config);
            $CI->email->set_newline("\r\n");
            if(isset($from)){
                $from=array(
                'name'=>'Wageni CRM',
                'email'=>'mastcrew22@gmail.com'
                );
            }
            $CI->email->from($from['email'], $from['name']); 
            //$to='mastcrew22@gmail.com';
            $CI->email->to($to);
            if(isset($cc)){
                $CI->email->cc($cc);
            }            
            if(isset($bcc)){
                $CI->email->bcc($cc);
            }
            if(count($attachments)>0){
                foreach($attachments as $attachment){
                    $CI->email->attach($attachment);
                }
            }
            $CI->email->subject($subject); 
            $CI->email->message($message); 
            $sent=$CI->email->send();
            if(!$sent){
                return $CI->email->print_debugger();
            }else{
                return $sent;
            }
        }
        return false;        
    }
    function makeSlug($str){
        if(!empty($str)){
            $str = preg_replace('~[^\pL\d]+~u', '-', $str);

            $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);

            $str = preg_replace('~[^-\w]+~', '', $str);

            $str = trim($str, '-');

            $str = preg_replace('~-+~', '-', $str);

            $str = strtolower($str);

            if (empty($str)) {
                return 'n-a-'.time();
            }
        }else{
            return 'n-a-'.time();
        }
        return $str;
    }

    function getDateDDMMYY($date){
        if($date!=''){
            $return= date('d-m-Y',strtotime($date));
        }else{
            $return="";
        }
        return $return;
    }

    function DMYTime($date){
        if($date!=''){
            $return= date('d-m-Y m:i A',strtotime($date));
        }else{
            $return="";
        }
        return $return;
    }

    
    function elapsedtime($elapseddate){
        if($elapseddate !=''){
          $post_date = strtotime($elapseddate);
          $now = time();
          $units = 5;
          $return= timespan($post_date, $now, $units); 
         }else{
            $return="";
        }
        return $return;
    }

     function difference_day($difference_day){
        if($difference_day !=''){
            $past_time =  date('Y-m-d H:i',strtotime($difference_day));                                
            $current_time = time();
            $difference = $current_time - strtotime($past_time);
            $difference_minute =  $difference/60;
            $difference_day = intval($difference_minute);
            return $difference_day;
        }
    }


    function complaintsdate($complaintsdate){
        if($complaintsdate !=''){    
          if ($complaintsdate == 1){
            $complaints = '<i class="fa fa-times-circle-o fa-1x text-danger"> Poor </i>';
          }elseif ($complaintsdate == 2){
              $complaints ='<i class="fa fa-times-circle-o fa-1x text-warning"> Fair </i>';
            }elseif ($complaintsdate == 3){
                $complaints ='<i class="fa fa-check-circle-o fa-1x text-primary"> Good </i>';
              }elseif ($complaintsdate == 4){
                  $complaints ='<i class="fa fa-check-circle-o fa-1x text-success"> Excellent</i>';
                }
          else{
             $complaints ='<i class="fa fa-check-circle-o fa-1x text-success"> NA</i>';
          } 
            return $complaints;
        }
    }

    function complaintsstatus($complaintsstatus){ 
        if($complaintsstatus !=''){    
            if ($complaintsstatus ==1){
                  $status= "<span class='btn btn-sm btn-danger'>Pending</span>";
                }elseif ($complaintsstatus ==2){
                      $status= "<span class='btn btn-sm btn-warning'>Ongoing</span>";
                  }elseif ($complaintsstatus ==3){
                      $status= "<span class='btn btn-sm btn-success'>Issue has been addressed</span>";
                }else{
                  $status= "<span class='btn btn-sm btn-danger'>Pending</span>";
              }
            return $status;
        }
    }



      function complaintsassign($complaintsassign){ 
        if($complaintsassign !=''){
            $past_time =  date('Y-m-d H:i',strtotime($complaintsassign));                                
            $current_time = time();
            $difference = $current_time - strtotime($past_time);
            $difference_minute =  $difference/60;
            $day = intval($difference_minute);
          
            if(intval($day) < '1440'){
                $assign =  "Superviser";
              }elseif(intval($day) < '2880') {
                $assign = "Manger";
            }else{
              $assign =  "G Manger";
            }
            return $assign;
        }
    }


    function getStatus($status){
        if($status==1){
            return "<span class='btn btn-sm btn-success'>Active</span>";
        }else{
            return "<span class='btn btn-sm btn-danger'>In-active</span>";
        }
    }





    function socialIcon(){
        $html='';
        $hrml.='<ul class="media-listing clearfix">';
        $hrml.='<li><a href="http://twitter.com" target="_blank" class="fa fa-twitter">&nbsp;</a></li>';
        $hrml.='<li><a href="http://facebook.com" target="_blank" class="fa fa-facebook">&nbsp;</a></li>';
        $hrml.='<li><a href="http://linkedin.com" target="_blank" class="fa fa-linkedin-square">&nbsp;</a></li>';
        $hrml.='<li><a href="http://pinterest.com" target="_blank" class="fa fa-pinterest">&nbsp;</a></li>';
        $hrml.='<li><a href="http://instagram.com" target="_blank" class="fa fa-instagram">&nbsp;</a></li>';
        $hrml.='<li><a href="http://youtube.com" target="_blank" class="fa fa-youtube">&nbsp;</a></li>';
        $hrml.='</ul>';
        return $html;    
    }
    function getCountries(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('countries');   
        $query  =  $CI->db->get();    
        return ($query->num_rows() > 0)?$query->result_array():array();            
    }
   function getScore(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');       
        $CI->db->order_by('score_name','ASC');
        $query = $CI->db->get('score_name');
       return ($query->num_rows() > 0)?$query->result_array():array(); 
    } 
   function getNPSDetractors($id){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select("SUM(npscore) as npscore");       
        $CI->db->where('busid',$id);  
        $CI->db->where('npscore >=', 0);
        $CI->db->where('npscore <=', 6);     
        $query = $CI->db->get('netpscore');
       return ($query->num_rows() > 0)?$query->row_array():array(); 
    } 
   function getNPSPassive($id){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select("SUM(npscore) as npscore");        
        $CI->db->where('busid',$id);  
        $CI->db->where('npscore >=', 7);
        $CI->db->where('npscore <=', 8);     
        $query = $CI->db->get('netpscore');
       return ($query->num_rows() > 0)?$query->row_array():array(); 
    } 
   function getNPSPromoters($id){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select("SUM(npscore) as npscore");          
        $CI->db->where('busid',$id);  
        $CI->db->where('npscore >=', 9);
        $CI->db->where('npscore <=', 10);     
        $query = $CI->db->get('netpscore');
       return ($query->num_rows() > 0)?$query->row_array():array(); 
    } 
   function getBrachById($id){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');       
        $CI->db->from('sub_category as scat');       
        $CI->db->where('scat.main_cat_id',$id);       
        //$CI->db->join('customer_feedback as cf','sl.sub_login_id=cf.busId','inner'); 
        //$CI->db->order_by('sub_login_id','ASC');
        $query = $CI->db->get();
       return ($query->num_rows() > 0)?$query->result_array():array(); 
    } 
   function getCompanyUsers(){
        $CI = & get_instance();
        $CI->load->database();
       // $CI->db->distinct();
        $CI->db->select('sl.*');
        $CI->db->from('subscriberlogin sl');
       // $CI->db->join('customer_feedback as cf','sl.sub_login_id=cf.busId','inner'); 
        $CI->db->where('sl.added_by',0);
        $CI->db->order_by('fname','ASC');
        $query = $CI->db->get();
       return ($query->num_rows() > 0)?$query->result_array():array(); 
    }
   function getComplaintStatus(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->order_by('complaint_status_name','ASC');
        $query = $CI->db->get('complaint_status');
       return ($query->num_rows() > 0)?$query->result_array():array(); 
    }   
    function getCategory(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('category_name,cat_id');
        $CI->db->order_by('category_name','ASC');
        $query = $CI->db->get('category');
       return ($query->num_rows() > 0)?$query->result_array():array(); 
    }   
   function getPackages() {
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
       // $CI->db->where('package_status',1);
        $CI->db->order_by('package_name','ASC');
        $query = $CI->db->get('packages');
       return ($query->num_rows() > 0)?$query->result_array():array(); 
    }   
    function totalSurveyQues($id){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('questionnaire');
        $CI->db->where('businessId',$id);
        $result = $CI->db->get()->num_rows();
        return $result;
    } 
    function guestUserComplaints($id){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('customer_feedback');
        $CI->db->where('busId',$id);
        $result = $CI->db->get()->num_rows();
        return $result;
    }   
    function guestUser($id){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('guest_user');
        $CI->db->where('owner_id',$id);
        $result = $CI->db->get()->num_rows();
        return $result;
    }  
    function totalCompanyUsersForSubAdmin($id){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('subscriberlogin');
        $CI->db->where('added_by',$id);
        $CI->db->where('is_deleted',0);
        $result = $CI->db->get()->num_rows();
        return $result;
    }  
    function totalCompany(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('subscriberlogin');
        $CI->db->where('added_by',0);
        $CI->db->where('is_deleted',0);
        $result = $CI->db->get()->num_rows();
        return $result;
    }  
    function totalActiveCompany(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('subscriberlogin');
        $CI->db->where('added_by',0);
        $CI->db->where('sub_isActive',1);
        $CI->db->where('is_deleted',0);
        $result = $CI->db->get()->num_rows();
        return $result;
    }  
    function totalInctiveCompany(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('subscriberlogin');
        $CI->db->where('added_by',0);
        $CI->db->where('sub_isActive',0);
        $CI->db->where('is_deleted',0);
        $result = $CI->db->get()->num_rows();
        return $result;
    }  
    function totalCompanyUsers(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('subscriberlogin');
        $CI->db->where('added_by<>',0);
        $CI->db->where('is_deleted',0);
        $result = $CI->db->get()->num_rows();
        return $result;
    }
    function totalCategory(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('category');
        $result = $CI->db->get()->num_rows();
        return $result;
    }  
      
    function totalPackage(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('packages');
        $result = $CI->db->get()->num_rows();
        return $result;
    }  
      

    function valueBydate($date ,$status ) {
       // where date(orders.date_time)='".$date."'
        $ci =& get_instance(); 
        $ci->load->database();
        $condition = "DATE(cf.cf_time)=DATE('".$date."')";


        
        //$date=date("%d-%m-%Y",strtotime($date));
        $ci->db->select("
                cf.*,
                gu.*,
                bqa.*,
                c.category_name,
                sc.sub_cat_name,
                ssc.sub_sub_cat_name,
                q.bus_q_que
                ");
        $ci->db->from('customer_feedback as cf');
        $ci->db->order_by('cf_id', 'DESC');
        $ci->db->join('guest_user as gu','cf.cf_user_id=gu.user_id','inner');
        $ci->db->join('questionnaire as q','cf.cf_que_id=q.bus_qid','inner');
        $ci->db->join('business_questionnaire_answer as bqa','cf.cf_score=bqa.bus_que_ans_id','inner');
        $ci->db->join('category as c','cf.cf_cat_id=c.cat_id','inner');
        $ci->db->join('sub_category as sc','cf.cf_cat_sub_id=sc.sub_cat_id','inner');
        $ci->db->join('sub_sub_category as ssc','cf.cf_cat_process_id=ssc.sub_sub_cat_id','inner');
        $ci->db->where('bqa.bus_que_score', $status);
        $ci->db->where($condition);
        if ($ci->session->userdata('sub_login_id')) {
            $ci->db->where('cf.busId', $ci->session->userdata('sub_login_id'));
        }        
        //$group_by = "GROUP BY DATE('cf'.'cf_time') ";
         //$ci->db->group_by(DATE_FORMAT('cf.cf_time','%d-%m-%Y'));
        //$ci->db->distinct('cf.cf_time');
        $ci->db->group_by('cf.cf_time');
        $query = $ci->db->get();
        $result= $query->result_array();
        return count($query->result_array());
        
    }




    /*

    $newdate =  date_format(date_create($date),"Y-m-d");
     $ci=& get_instance();
    //load databse library
    $ci->load->database();
    //echo $newdate; 
    //echo date_create($date);
    if($status == 1){
         $query = $ci->db->query("SELECT *, DATE(order_datetime) FROM `all_tickets` WHERE DATE(order_datetime) = '$newdate' AND `order_status` < 5 ");
         //$query = $ci->db->query("SELECT *  FROM `order_detail` WHERE created = '$date'");
    }
    else {
         $query = $ci->db->query("SELECT *, DATE(order_datetime) FROM `all_tickets` WHERE DATE(order_datetime) = '$newdate' AND `order_status` = $status ");
    }
    */

    function contactdetail(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('contactdetail');
        return $result = $CI->db->get()->row_array();

    }
    function admin_login(){
        $CI = & get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('admin_login');
        return $result = $CI->db->get()->row_array();

    }

    function pr($d, $echo = TRUE){
       if($echo){
           echo '<pre>'.print_r($d, true).'</pre>';
       }else{
          return '<pre>'.print_r($d, true).'</pre>';
       } 
    }

    function prd($d){   
       pr($d);     
       die; 
    }

    function vr($d , $echo = TRUE){ 
       if($echo){
           echo '<pre>';var_dump($d, true);
       }else{
          return '<pre>';var_dump($d, true);
       } 
    }

    function vrd($d){
       vr($d);
       die; 
    }


?>