<?php
/* English */
$lang['site_name'] = "Wageni CRM";
$lang['elglish'] = "English";
$lang['Home'] = "Home";
$lang['contactus']= "contact us";
$lang['SURVEY']= "Survey";
$lang['DASHBOARD'] = "Dashboard";
$lang['ADD'] = "Add";
$lang['CATEGORY']= "Category";
$lang['SUBCATEGORY']= "Sub Category";
$lang['PROCESS']= "Process";
$lang['BRANCH']= "Branch";

/* Display */
$lang['SUCCESSFULLY']= "Successfully";
$lang['INS']= "Insert";


/*	Form Input Field	*/
$lang['LAVELUNAME'] = "Full Name";
$lang['INPUTNAME'] = "uname";
$lang['PLACEHODERUNAME'] = "Enter Name";
$lang['LAVELUPASS'] = "Password";
$lang['INPUTPASS'] = "upass";
$lang['PLACEHODERUPASS'] = "Enter Password";
$lang['LAVELUSERNAME'] = "User Name";
$lang['INPUTUSERNAME'] = "username";
$lang['PLACEHODERUSERNAME'] = "Enter User Name";
$lang['LAVELUEMAIL'] = "Email";
$lang['INPUTEMAIL'] = "uemail";
$lang['PLACEHODERUEMAIL'] = "Enter Email";
$lang['LAVELUADD'] = "Address";
$lang['INPUTEADD'] = "uaddress";
$lang['PLACEHODERUADD'] = "Enter Address";
$lang['LAVELUPHONE'] = "Phone Number";
$lang['INPUTEPHONE'] = "uphonenumber";
$lang['PLACEHODERUPHONE'] = "Enter Phone number";
$lang['Enter_Here'] = "Enter Here";
$lang['Date_of_Birth'] = "Date Of Birth";
$lang['Gender'] = "Gender";
$lang['Male'] = "Male";
$lang['Female'] = "Female";
$lang['Mobile_Number'] = "Mobile_Number";
$lang['Password'] = "Password";
$lang['Email'] = "Email";
$lang['Status'] = "Status";
/* Button */
$lang['Submit'] = "Submit";
$lang['Remove'] = "Remove";
$lang['Reset'] = "Reset";
$lang['Clear'] = "Clear";
$lang['Register'] = "Register";
$lang['Browse'] = "Browse";
$lang['Login'] = "Login";
$lang['DEL'] = "Delete";
$lang['EDIT'] = "Edit";
$lang['UPD'] = "Update";
$lang['SEL'] = "Select";
$lang['OPT'] = "Option";
/* Change Password */
$lang['Change_Password'] = "Change Password";
$lang['Password'] = "Password";
$lang['Old_Password'] = "Old Password";
$lang['New_Password'] = "New Password";
$lang['Comfirm_Password'] = "Comfirm Password";

/* Page Title */

/*Guest User*/
$lang['Hotel_Name'] = "Hotel Name";
$lang['User_Name'] = "User Name";
$lang['User_Mail_Id'] = "User Mail Id";
$lang['No_Of_Adult'] = "No. Of Adult";
$lang['No_Of_Children'] = "No. Of Children";
$lang['No_Of_Room'] = "No. Of Room";
$lang['Check_In'] = "Check In";
$lang['Check_Out'] = "Check Out";

