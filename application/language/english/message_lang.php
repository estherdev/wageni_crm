<?php

/* English */
$lang['site_name'] 			= 	"Wageni CRM";
$lang['elglish']			= 	"English";
$lang['Home'] 				= 	"Home";

/* Form Attribute */
$lang['Date_of_Birth']		= 	"Date Of Birth";
$lang['Gender'] 			= 	"Gender";
$lang['Male'] 				= 	"Male";
$lang['Female'] 			= 	"Female";
$lang['Mobile_Number'] 		= 	"Mobile_Number";
$lang['Password'] 			= 	"Password";
$lang['Email'] 				= 	"Email";
$lang['Change_Password']	= 	"Change Password";
$lang['Old_Password'] 		= 	"Old Password";
$lang['New_Password'] 		= 	"New Password";
$lang['Comfirm_Password']	= 	"Comfirm Password";

/* Button */
$lang['Submit'] 			= 	"Submit";
$lang['Remove'] 			= 	"Remove";
$lang['Reset'] 				= 	"Reset";
$lang['Clear'] 				= 	"Clear";
$lang['Browse'] 			= 	"Browse";
$lang['Register'] 			= 	"Register";
$lang['Login'] 				= 	"Login";


/* Page Title */

