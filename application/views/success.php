<!DOCTYPE html>
<html>
<head>
    <title>Thank You</title>
   <!--  <meta http-equiv="refresh" content="4;url=http://wageni.us.tempcloudsite.com" /> -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <style type="text/css">
      .thankyou-body {position: fixed; top: 20%; left: 50%; transform: translate(-50%,-20%); background: #fff; padding: 35px; box-shadow: 5px 10px 10px -5px #ccc; text-align: center; border-radius: 10px; min-width: 320px;}
      .tIcon {margin: 20px auto; }
      .thankyou {font-size: 30px; text-transform: uppercase; font-weight: bolder; color: #0e2b84; }
      .tranref {font-size: 20px; line-height: 30px; text-align: center; color: #0e2b84; letter-spacing: 1px; }
      .thankyouBtn {margin: 20px 0; font-size: 16px;}
      .postBtn {display: inline-block; padding: 8px 30px; background:#10a1da; color: #fff; margin: 10px auto; border-radius: 50px; text-transform: uppercase; letter-spacing: 1px; box-shadow: 0 0 5px 0 #ccc; transition: all 1s ease;}
      .postBtn:hover, .postBtn:focus {background: #0e2b84; color: #fff; text-decoration: none;}
    </style>
</head>

<body style="background: #f1f1f1;">

  <div class="thankyou-body">
    <div class="tIcon"><img src="<?= base_url();?>globalassets/website/img/thank-you.png"></div>
    <div class="thankyou">Thank you for taking the time to complete this survey.</div>
    <div class="thankyouBtn">Redirecting to WageniCRM after <span id="countdown">5</span> seconds <br />


      <a class="postBtn" href="<?= base_url();?>">Go Back to Home</a><br />
    </div>
  </div> 
  <script type="text/javascript">
    var seconds = 5;    
    function countdown() {
        seconds = seconds - 1;
        if (seconds < 0) {
             window.location = "<?= base_url();?>";
        } else {
             document.getElementById("countdown").innerHTML = seconds;
             window.setTimeout("countdown()", 1000);
        }
    }

    countdown();
    
</script>
</body>
</html>