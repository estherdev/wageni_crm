<!--header Section Start Here -->
<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
   <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        Update User
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Update User</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title">User Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" id="subs" action="<?= base_url('update-subscriber-employee/'.$editsubscriber[0]->sub_login_id);?>" method="post">
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="select-branch">Branch Name</label>
                           <select type="select" class="form-control" id="select-branch"  name="userbranch">
                               <option value="">--Select Branch--</option>
                              <?php foreach ($viewbranch as $key => $value) {?>
                                 <option value="<?= $value->sub_branch_id ?>"  <?php if($value->sub_branch_id == $editsubscriber[0]->branch_id){echo "selected='selected'";}?>  <?php echo set_select('userbranch',$value->sub_branch_id , ( !empty($value->sub_branch_id) && $value->sub_branch_id == "<?= $value->sub_branch_id ?>" ? TRUE : FALSE ));?>><?= ucfirst($value->branch_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('userbranch')?>
                        </div>
                        <div class="col-md-4">
                           <label for="f-name">First Name</label>
                           <input type="text" class="form-control"  name="fname" value="<?= ucfirst($editsubscriber[0]->fname) ;?>" placeholder="First Name">
                           <?= form_error('fname')?>
                        </div>
                        <div class="col-md-4">
                           <label for="l-name">Last Name</label>
                           <input type="text" class="form-control"  name="lname" value="<?= ucfirst($editsubscriber[0]->lname) ;?>" placeholder="First Name">
                           <?= form_error('lname')?>
                        </div>
                     </div>

                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="e-address"><?= $this->lang->line('LAVELUEMAIL');?></label>
                           <input type="email" class="form-control" name="uemail" value="<?= $editsubscriber[0]->sub_email ;?>" placeholder="<?= $this->lang->line('PLACEHODERUEMAIL');?>">
                           <?= form_error('uemail')?>
                        </div>

                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPHONE');?></label>
                           <input type="text" class="form-control" name="uphone" value="<?= $editsubscriber[0]->sub_phone ;?>" placeholder="<?= $this->lang->line('PLACEHODERUPHONE');?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                           <?= form_error('uphone')?>
                        </div>
                      <!--   <div class="col-md-4">
                           <label for="c-name"><?= $this->lang->line('LAVELUSERNAME');?></label>
                           <input type="text" class="form-control" name="username" value="<?= ucfirst($editsubscriber[0]->sub_username);?>" placeholder="<?= $this->lang->line('PLACEHODERUSERNAME');?>">
                           <?= form_error('username')?>
                        </div> -->
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPASS');?></label>
                           <input type="password" class="form-control" name="upass" value="<?= $editsubscriber[0]->sub_pass ;?>" placeholder="<?= $this->lang->line('PLACEHODERUPASS');?>">
                           <?= form_error('upass')?>
                        </div>
                     </div>

                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="select-role">Role</label> 
                           <select type="select" class="form-control" id="select-role"  name="userrole">
                              <option value="">--Select Role--</option>
                              <? foreach ($role as $key => $value) {?>
                              <option value="<?= $value->subscriber_roll_id ?>" <?php if($value->subscriber_roll_id == $editsubscriber[0]->sub_login_role){echo "selected='selected'";}?> <?php echo set_select('userrole', $value->subscriber_roll_id); ?> ><?= ucfirst($value->subscriber_role_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('userrole')?>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                  <button type="submit" class="btn btn-success">Submit</button>
                  <a href="<?= base_url('add-subscriber-employee');?>" class="btn btn-danger" role="button">Back</a>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
</div>
<!-- /.content-wrapper -->
</section><!-- /.content -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->			
<?php include APPPATH . 'views/webadmin/footer.php'; ?>
<!--footer Section End Here