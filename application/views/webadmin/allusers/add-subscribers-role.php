<!--header Section Start Here -->
<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
<?php endif;?>
<?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
   <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
</div>
<?php endif;?>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Add User Role
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Add User Role</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <!-- Info boxes -->
   <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
       <div class="box">
         <div class="box-header">
            <h3 class="box-title">Manage Roles</h3>
 <!--      <div class="pull-right box-tools">
         <button type="button" class="btn btn-info btn-sm"  title=""><i class="fa fa-plus-circle"></i> Create Role</button>
      </div> -->
   </div>
   <form role="form" id="role" action="<?= base_url('add-user-role');?>" method="post">
      <div class="box-body">
         <div class="row">
            <div class="col-md-8 col-md-offset-0">
               <div class="form-group row">
                  <div class="col-sm-1"><label>Name</label></div>
                  <div class="col-sm-9">
                     <input type="text" class="form-control"  name="rolename" placeholder="Enter User Role Name">
                     <?= form_error('rolename')?>
                  </div>
               </div>
<!--             <div class="form-group row">
               <div class="col-sm-3"><label>Discription</label></div>
               <div class="col-sm-9"><textarea class="form-control" type="text" placeholder=""></textarea></div>
            </div>
            <div class="form-group row">
               <div class="col-sm-3"><label>Permissions</label></div>
               <div class="col-sm-9">
                  <table class="table table-striped role-table ">
                     <tr>
                        <td>User</td>
                        <td width="100" align="center" valign="middle">
                           <label class="switch">
                           <input type="checkbox" name="rolepermission[]" value="1">
                           <span class="slider round"></span>
                           </label>
                        </td>
                     </tr>
                     <tr>
                        <td>Branch</td>
                        <td width="100" align="center" valign="middle">
                           <label class="switch">
                           <input type="checkbox" name="rolepermission[]" value="2">
                           <span class="slider round"></span>
                           </label>
                        </td>
                     </tr>
                  </table>
               </div>
            </div> -->
         </div>
      </div>
   </div>
   <!-- /.box-body -->
   <div class="box-footer text-center">
     <button type="submit" class="btn btn-primary">Submit</button>
     <button type="reset" class="btn btn-danger">Reset</button>
  </div>
</form>

</div> 
<!-- /.box -->
</div>
<!-- /.content-wrapper -->
</div>
<!-- /.content-wrapper -->
<div class="row">
   <div class="col-md-12 col-sm-6 col-xs-12">
      <!-- general form elements -->
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">User Role List</h3>
         </div>
         <!-- /.box-header -->
         <div class="box-body">
            <table id="example1" class="table table-bordered table-striped text-center">
               <thead>
                  <tr>
                     <th>S.No</th>
                     <th>Name</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $i= 1; foreach ($subscriberrole as $key => $subrole) { ?>
                     <tr>
                        <td><?= $i++; ?></td>
                        <td><?= ucfirst($subrole->subscriber_role_name);?></td>
                        <td class="text-center">
                           <a class="btn btn-sm btn-info" href="<?php echo base_url().'edit-user-role/'.$subrole->subscriber_roll_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                           <a class="btn btn-sm btn-danger deletesubroles"  data-toggle="modal" data-target="#deleteModal<?= $subrole->subscriber_roll_id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                        <div id="deleteModal<?= $subrole->subscriber_roll_id ; ?>" class="modal fade">
                           <div class="modal-dialog">
                              <div class="modal-content">
                                 <!-- dialog body -->
                                 <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    Are You Sure to <strong><?= $subrole->subscriber_role_name ; ?></strong> Delete  
                                 </div>
                                 <!-- dialog buttons -->
                                 <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('deletesubscribersrole/'.$subrole->subscriber_roll_id ); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                              </div>
                           </div>
                        </div>
                     </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.content-wrapper -->
</div>
</div>
<!-- /.content-wrapper -->
</section><!-- /.content -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/webadmin/footer.php'; ?>
<!--footer Section End Here