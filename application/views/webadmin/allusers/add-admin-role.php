<!--header Section Start Here -->
<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
   <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Admin Role
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Add Admin Role </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title">Admin Role Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="<?= base_url('add-admin-role');?>" method="post">
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-12">
                           <label for="c-name">Role Name</label>
                           <input type="text" class="form-control"  name="rolename" placeholder="Enter Subscribers Role Name">
                           <?= form_error('rolename')?>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                 <div class="box-footer text-center">

                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>

              </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
      <!-- /.content-wrapper -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">Admin Role List</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped text-center">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Name</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php $i= 1; foreach ($adminrole as $key => $role) { ?>
                        <tr>
                           <td><?= $i++; ?></td>
                           <td><?= ucfirst($role->admin_emp_role_name);?></td>
                           <td class="text-center">
                              <a class="btn btn-sm btn-info" href="<?php echo base_url().'editsubrole/'.$role->admin_role_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                              <a class="btn btn-sm btn-danger deleteroles"  data-toggle="modal" data-target="#deleteModal<?= $role->admin_role_id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                           </td>
                           <div id="deleteModal<?= $role->admin_role_id ; ?>" class="modal fade">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <!-- dialog body -->
                                    <div class="modal-body">
                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       Are You Sure to <strong><?= $role->admin_emp_role_name ; ?></strong> Delete  
                                    </div>
                                    <!-- dialog buttons -->
                                    <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('deleteadminrole/'.$role->admin_role_id ); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                                 </div>
                              </div>
                           </div>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
</div>
<!-- /.content-wrapper -->
</section><!-- /.content -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->			
<?php include APPPATH . 'views/webadmin/footer.php'; ?>
<!--footer Section End Here