<?php include APPPATH . 'views/webadmin/header.php';?>
  <?php include APPPATH . 'views/webadmin/main-sidebar.php'; ?>
 <div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
   <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
    <section class="content-header">
      <h1>
         Add User
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Add User </li>
      </ol>
   </section>
    <section class="content">
       <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
             <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title">User Details </h3>
               </div>
               <?php if(count($viewbranch) > 0 ){?>	
              

                <!-- form start -->
              <!--  <form role="form" id="subs" action="<?//= base_url('sub-admin-add-sub-users');?>" method="post"> -->
               <form role="form" id="subs" action="<?= base_url('sub-admin-view-all-users');?>" method="post">
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="select-branch">Branch Name</label>
                           <!-- <select type="select" class="form-control" id="select-branch"  name="userbranch"> -->
                               <select class="form-control required" id="select-branch" name="userbranch">
                               <option value="">--Select Branch--</option>
                              <?php foreach ($viewbranch as $key => $value) {?>
                                 <option value="<?= $value->sub_branch_id ?>" <?php echo set_select('userbranch',$value->sub_branch_id , ( !empty($value->sub_branch_id) && $value->sub_branch_id == "<?= $value->sub_branch_id ?>" ? TRUE : FALSE ));?>><?= ucfirst($value->branch_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('userbranch')?>
                        </div>
                        <div class="col-md-4">
                           <label for="f-name">First Name</label>
                           <input type="text" class="form-control"  name="fname" placeholder="First Name">
                           <?= form_error('fname')?>
                        </div>
                        <div class="col-md-4">
                           <label for="l-name">Last Name</label>
                           <input type="text" class="form-control"  name="lname" placeholder="Last Name">
                           <?= form_error('lname')?>
                        </div>
                     </div>

                      <div class="form-group row">
                        <div class="col-md-4">
                           <label for="e-address"><?= $this->lang->line('LAVELUEMAIL');?></label>
                           <input type="email" class="form-control" name="uemail" placeholder="<?= $this->lang->line('PLACEHODERUEMAIL');?>">
                           <?= form_error('uemail')?>
                        </div>

                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPHONE');?></label>
                           <input type="text" class="form-control" name="uphone" placeholder="<?= $this->lang->line('PLACEHODERUPHONE');?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                           <?= form_error('uphone')?>
                        </div>                        
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPASS');?></label>
                           <input type="password" class="form-control" name="upass" placeholder="<?= $this->lang->line('PLACEHODERUPASS');?>">
                           <?= form_error('upass')?>
                        </div>
                     </div>

                      <div class="form-group row">
                        <div class="col-md-4">
                           <label for="select-role">Role</label>
                          
                           <select type="select" class="form-control" id="select-role"  name="userrole">
                               <option value="">--Select Role--</option>
                              <?php foreach ($role as $key => $value) {?>
                                 <option value="<?= $value->subscriber_roll_id ?>" <?php echo set_select('userrole',$value->subscriber_roll_id , ( !empty($value->subscriber_roll_id) && $value->subscriber_roll_id == "<?= $value->subscriber_roll_id ?>" ? TRUE : FALSE ));?>><?= ucfirst($value->subscriber_role_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('userrole')?>
                        </div>
                     </div>
                  </div>
                   <div class="box-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-danger">Reset</button>
                  </div>
               </form>
                <?php }else{?>
                	<div class="col-sm-12 col-md-12" style="background-color:yellow;">
            <div class="text-center"><h3>Please Add Branch First<a href="<?= base_url('view-branch');?>" target="_blank">Add Branch ...</a></h3></div> 
          </div>
               <?php } ?>
            </div>
          </div>
       </div>
      <style>
/*.scaleBar--1 {
color: #C74C49;
}
.scaleBar--2 {
color: #E07A17;
}
.scaleBar--3 {
color: #C1B71A;

}*/
   </style>
    
</div>
 </section><!-- /.content -->
 			
<?php include APPPATH . 'views/webadmin/footer.php'; ?>
 