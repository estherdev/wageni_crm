header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
   <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Update Employees
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Update Employees</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title">Employees Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="<?= base_url('updateUser/'.$editadmin[0]->admin_login_id);?>" method="post">
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="c-name"><?= $this->lang->line('LAVELUNAME');?></label>
                           <input type="text" class="form-control"  name="uname" value="<?= $editadmin[0]->admin_name ; ?>" placeholder="<?= $this->lang->line('PLACEHODERUNAME');?>">
                           <?= form_error('uname')?>
                        </div>
                        <div class="col-md-4">
                           <label for="c-name"><?= $this->lang->line('LAVELUSERNAME');?></label>
                           <input type="text" class="form-control" name="username" value="<?= $editadmin[0]->admin_username ; ?>" placeholder="<?= $this->lang->line('PLACEHODERUSERNAME');?>">
                           <?= form_error('username')?>
                        </div>
                        <div class="col-md-4">
                           <label for="e-address"><?= $this->lang->line('LAVELUEMAIL');?></label>
                           <input type="email" class="form-control" name="uemail" value="<?= $editadmin[0]->admin_email ; ?>" placeholder="<?= $this->lang->line('PLACEHODERUEMAIL');?>">
                           <?= form_error('uemail')?>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPASS');?></label>
                           <input type="password" class="form-control" name="upass" value="<?= $editadmin[0]->admin_pass ; ?>" placeholder="<?= $this->lang->line('PLACEHODERUPASS');?>">
                           <?= form_error('upass')?>
                        </div>
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPHONE');?></label>
                           <input type="text" class="form-control" name="uphone" value="<?= $editadmin[0]->admin_phone ; ?>" placeholder="<?= $this->lang->line('PLACEHODERUPHONE');?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                           <?= form_error('uphone')?>
                        </div>
                        <div class="col-md-4">
                           <label for="select-role">Role</label>
                           <select type="email" class="form-control" id="select-role" name="userrole">
                              <!--  <option value="none">--Select Role--</option> -->
                              <? foreach ($role as $key => $value) {?>
                              <option value="<?= $value->admin_role_id ?>"  <?php if($value->admin_role_id == $editadmin[0]->admin_login_id){echo "selected='selected'";}?> <?php echo set_select('userrole', $value->admin_role_id); ?> ><?= ucfirst($value->admin_emp_role_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('userrole')?>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">

                <button type="submit" class="btn btn-success">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>

              </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
      <!-- /.content-wrapper -->
</div>
<!-- /.content-wrapper -->
</section><!-- /.content -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->			
<?php include APPPATH . 'views/admin/footer.php'; ?>
<!--footer Section End Here