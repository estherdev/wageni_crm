<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
   <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Employees
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Add Employees</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title">Employees Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="<?= base_url('add-user');?>" method="post">
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="c-name"><?= $this->lang->line('LAVELUNAME');?></label>
                           <input type="text" class="form-control"  name="uname" placeholder="<?= $this->lang->line('PLACEHODERUNAME');?>">
                           <?= form_error('uname')?>
                        </div>
                        <div class="col-md-4">
                           <label for="c-name"><?= $this->lang->line('LAVELUSERNAME');?></label>
                           <input type="text" class="form-control" name="username" placeholder="<?= $this->lang->line('PLACEHODERUSERNAME');?>">
                           <?= form_error('username')?>
                        </div>
                        <div class="col-md-4">
                           <label for="e-address"><?= $this->lang->line('LAVELUEMAIL');?></label>
                           <input type="email" class="form-control" name="uemail" placeholder="<?= $this->lang->line('PLACEHODERUEMAIL');?>">
                           <?= form_error('uemail')?>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPASS');?></label>
                           <input type="password" class="form-control" name="upass" placeholder="<?= $this->lang->line('PLACEHODERUPASS');?>">
                           <?= form_error('upass')?>
                        </div>
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPHONE');?></label>
                           <input type="text" class="form-control" name="uphone" placeholder="<?= $this->lang->line('PLACEHODERUPHONE');?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                           <?= form_error('uphone')?>
                        </div>
                        <div class="col-md-4">
                           <label for="select-role">Role</label>
                           <select type="email" class="form-control" id="select-role"  name="userrole">
                              <!--  <option value="none">--Select Role--</option> -->
                              <? foreach ($role as $key => $value) {?>
                              <option value="<?= $value->admin_role_id ?>"><?= ucfirst($value->admin_emp_role_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('userrole')?>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                 <div class="box-footer text-center">

                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>

              </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
      <!-- /.content-wrapper -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">Employees List</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped text-center">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Name</th>
                           <th>User Name</th>
                           <th>Email</th>
                           <th>Phone</th>
                           <th>Role</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php $i= 1; foreach ($users as $key => $user) { ?>
                        <tr>
                           <td><?= $i++; ?></td>
                           <td><?= ucfirst($user->admin_name);?></td>
                           <td><?= $user->admin_username;?></td>
                           <td><?= $user->admin_email;?></td>
                           <td><?= $user->admin_phone;?></td>
                           <td><?php if ($user->admin_role_id == '1') { 
                           	echo "Employee";
                           }else{ echo "Manager";}?></td>
                           <td><?= $user->admin_isActive;?></td>
                           <td class="text-center">
                              <a class="btn btn-sm btn-info" href="<?php echo base_url().'editUser/'.$user->admin_login_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                              <a class="btn btn-sm btn-danger deleteUsers"  data-toggle="modal" data-target="#deleteModal<?= $user->admin_login_id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                           </td>
                           <div id="deleteModal<?= $user->admin_login_id ; ?>" class="modal fade">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <!-- dialog body -->
                                    <div class="modal-body">
                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       Are You Sure to <strong><?= $user->admin_username ; ?></strong> Delete  
                                    </div>
                                    <!-- dialog buttons -->
                                    <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('deleteusers/'.$user->admin_login_id ); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                                 </div>
                              </div>
                           </div>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
</div>
<!-- /.content-wrapper -->
</section><!-- /.content -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->			
<?php include APPPATH . 'views/admin/footer.php'; ?>
<!--footer Section End Here