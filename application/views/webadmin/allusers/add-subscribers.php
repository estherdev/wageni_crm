<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
   <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add User
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Add User </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title">User Details </h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" id="subs" action="<?= base_url('add-subscriber-employee');?>" method="post">
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="select-branch">Branch Name</label>
                           <!-- <select type="select" class="form-control" id="select-branch"  name="userbranch"> -->
                               <select class="form-control required" id="select-branch" name="userbranch">
                               <option value="">--Select Branch--</option>
                              <?php foreach ($viewbranch as $key => $value) {?>
                                 <option value="<?= $value->sub_branch_id ?>" <?php echo set_select('userbranch',$value->sub_branch_id , ( !empty($value->sub_branch_id) && $value->sub_branch_id == "<?= $value->sub_branch_id ?>" ? TRUE : FALSE ));?>><?= ucfirst($value->branch_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('userbranch')?>
                        </div>
                        <div class="col-md-4">
                           <label for="f-name">First Name</label>
                           <input type="text" class="form-control"  name="fname" placeholder="First Name">
                           <?= form_error('fname')?>
                        </div>
                        <div class="col-md-4">
                           <label for="l-name">Last Name</label>
                           <input type="text" class="form-control"  name="lname" placeholder="Last Name">
                           <?= form_error('lname')?>
                        </div>
                     </div>

                      <div class="form-group row">
                        <div class="col-md-4">
                           <label for="e-address"><?= $this->lang->line('LAVELUEMAIL');?></label>
                           <input type="email" class="form-control" name="uemail" placeholder="<?= $this->lang->line('PLACEHODERUEMAIL');?>">
                           <?= form_error('uemail')?>
                        </div>

                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPHONE');?></label>
                           <input type="text" class="form-control" name="uphone" placeholder="<?= $this->lang->line('PLACEHODERUPHONE');?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                           <?= form_error('uphone')?>
                        </div>
                        <!-- <div class="col-md-4">
                           <label for="c-name"><?= $this->lang->line('LAVELUNAME');?></label>
                           <input type="text" class="form-control"  name="uname" placeholder="<?= $this->lang->line('PLACEHODERUNAME');?>">
                           <?= form_error('uname')?>
                        </div> -->
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPASS');?></label>
                           <input type="password" class="form-control" name="upass" placeholder="<?= $this->lang->line('PLACEHODERUPASS');?>">
                           <?= form_error('upass')?>
                        </div>
                     </div>

                      <div class="form-group row">
                        <div class="col-md-4">
                           <label for="select-role">Role</label>
                          
                           <select type="select" class="form-control" id="select-role"  name="userrole">
                               <option value="">--Select Role--</option>
                              <?php foreach ($role as $key => $value) {?>
                                 <option value="<?= $value->subscriber_roll_id ?>" <?php echo set_select('userrole',$value->subscriber_roll_id , ( !empty($value->subscriber_roll_id) && $value->subscriber_roll_id == "<?= $value->subscriber_roll_id ?>" ? TRUE : FALSE ));?>><?= ucfirst($value->subscriber_role_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('userrole')?>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-danger">Reset</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
      <style>
/*.scaleBar--1 {
color: #C74C49;
}
.scaleBar--2 {
color: #E07A17;
}
.scaleBar--3 {
color: #C1B71A;

}*/
   </style>
      <!-- /.content-wrapper -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">Users List</h3>
               </div>
               <!-- sub_login_role -->
               <!-- /.box-header -->
               <div class="box-body table-responsive">
                  <table id="example1" class="table table-bordered table-striped text-center">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Branch </th>
                           <th>Name</th>
                          <!--  <th>User Name</th> -->
                           <th>Email</th>
                           <th>Phone</th>
                           <th>Role</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php $i= 1; foreach ($subscriber as $key => $user) { ?>
                        <tr>
                           <td><?= $i++; ?></td>
                           <td><?= ucfirst($user->branch_name);?></td>
                           <td><?= ucfirst($user->fname);?> <?= ucfirst($user->lname);?></td>
                           <!-- <td><?= ucfirst($user->sub_username);?></td> -->
                           <td><?= $user->sub_email;?></td>
                           <td><?= $user->sub_phone;?></td>
                           <td class="scaleBar--<?= $user->sub_login_role;?>"><?= ucfirst($user->role); ?></td>
                           <td class="text-center">
                                 <?php if($user->sub_isActive=='1'){?>
                              <a class="btn btn-sm btn-success" href="<?= base_url('company-admin-status/');?><?= $user->sub_login_id;?>/<?= $user->sub_isActive;?>" title="Edit"><i class="fa fa-unlock"></i></a>|
                            <?php }else{ ?>
                              <a class="btn btn-sm btn-warning" href="<?= base_url('company-admin-status/');?><?= $user->sub_login_id;?>/<?= $user->sub_isActive;?>" title="Edit"><i class="fa fa-lock"></i></a>|
                            <?php }?>
                              <a class="btn btn-sm btn-info" href="<?php echo base_url().'edit-subscriber-employee/'.$user->sub_login_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>|
                              <a class="btn btn-sm btn-danger deleteUsers"  data-toggle="modal" data-target="#deleteModal<?= $user->sub_login_id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                           </td>
                           <div id="deleteModal<?= $user->sub_login_id ; ?>" class="modal fade">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <!-- dialog body -->
                                    <div class="modal-body">
                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       Are You Sure to <strong><?= ucfirst($user->sub_username) ; ?></strong> Delete  
                                    </div>
                                    <!-- dialog buttons -->
                                    <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('delete-subscriber-employee/'.$user->sub_login_id ); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                                 </div>
                              </div>
                           </div>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
</div>
<!-- /.content-wrapper -->
</section><!-- /.content -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->			
<?php include APPPATH . 'views/webadmin/footer.php'; ?>
<!--footer Section End Here