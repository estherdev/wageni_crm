 <?php 
 
 include "header.php"; 
 include "main-sidebar.php"; 
 ?>

 <style>
 	.netPromoter table tr td {text-align: center; padding: 2px 5px;}
 	.npNum {width: 40px; height: 40px; margin: auto; font-weight: 600; position: relative; line-height: 38px; text-align: center; color: #ff7273;}
 	.npNum:before {content: ""; position: absolute; top: 0; left: 0; right: 0; bottom: 0; border: 2px solid #ff7273; border-radius: 100%; border-bottom-left-radius: 1px; transform: rotate(-45deg);}
 	.npImg img {width: 100%; vertical-align: bottom;}
 	.npText {position: relative; text-transform: uppercase; font-weight: 700; font-size: 20px; padding-top: 15px; margin-top: 10px; color: #ff7273;}
 	.npText:before {content: ""; position: absolute; top: 0; left: 0; width: 100%; border-top: 7px solid #ff7273; border-radius: 3px; }

 	.npNeutral, .npNeutral:before {color: #ffd40a; border-color: #ffd40a;}
 	.npPromot, .npPromot:before {color: #07c25b; border-color: #07c25b;}
   .npsFlex {display: flex; text-align: center; }
   .npsFlex .detractors {width: 50%; color: #dd4b39;}
   .npsFlex .passive {width: 25%; color: #ffd40a;}
   .npsFlex .promoters {width: 25%; color: #00a65a;}
   .detractors .dFlex {display: flex;}
   .detractors .dFlex > div {flex:1;}
   .npsHead {border-top: 5px solid #000; padding-top: 10px; margin-top: 10px; text-transform: uppercase; font-size: 20px;}
   .npsItem {min-width: 80px; font-size: 24px;}
   .npsItem p {margin: 0;}
   .detractors .npsHead {border-color: #dd4b39; }
   .passive .npsHead {border-color: #ffd40a; position: relative;}
   .passive .npsHead:before, .passive .npsHead:after {
    content: "";
    height: 30px;
    width: 2px;
    background: #ffd40a;
    display: block;
    position: absolute;
    top: -16px;}
    .passive .npsHead:before {left: 0;}
    .passive .npsHead:after {right: 0;}
   .promoters .npsHead {border-color: #00a65a; }
   .npsScore { display: flex;font-size: 30px; align-items: center; height: 130px; justify-content: center; background: #eef3f7; color: #337ab7;}
   .npsScore .dFlex, .npsScore .dFlex > div {display: flex; align-items: center;}
   .npsScore .dFlex > div i, .npsScore .dFlex > div span, .npsScore .dFlex > div strong {padding: 0 5px;}
   .promoterScore i {color: #00a65a; }
   .detractorScore i {color: #dd4b39; }
   .promoterScore i.fa-minus { font-size: 24px; color: #337ab7; margin: 0 10px; }
   .detractorScore .equal {font-weight: 600; margin: 0 10px;}
</style>

<script src="<?= base_url();?>globalassets/admin/dist/js/pages/dashboard.js"></script>
<div class="content-wrapper">
	<section class="content-header">
		<h1> Dashboard <small></small> </h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Company Users</span>
						<span class="info-box-number"><?php echo totalCompanyUsersForSubAdmin($this->session->userdata('sub_login_id'));?></span>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-sitemap"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Total Branch</span>
						<span class="info-box-number"><?php echo totalCompanyUsersForSubAdmin($this->session->userdata('sub_login_id'));?><small></small></span>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-hotel"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Guest User</span>
						<span class="info-box-number"><?php echo guestUser($this->session->userdata('sub_login_id'));?></span>
					</div>
				</div>
			</div>
			<div class="clearfix visible-sm-block"></div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-red"><i class="fa fa-comments"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">User Complaints</span>
						<span class="info-box-number"><?php echo guestUserComplaints($this->session->userdata('sub_login_id'));?></span>
					</div>
				</div>
			</div>

		<!-- 	<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-green"><i class="fa fa-list-alt"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Total Survey Ques.</span>
						<span class="info-box-number"><?php //echo totalSurveyQues($this->session->userdata('sub_login_id'));?></span>
					</div>
				</div>
			</div> -->

		</div>

		<!-- Net Promoter Score -->
		<div class="box box-primary">
			<div class="box-header">
				<div class="box-title">Net Promoter Score (NPS)</div>
			</div>
			<div class="box-body">

				<div class="netPromoter">
					<table cellpadding="5px" cellspacing="0" border="0">
						<tr>
							<td><div class="npNum npdetract">1</div></td>
							<td><div class="npNum npdetract">2</div></td>
							<td><div class="npNum npdetract">3</div></td>
							<td><div class="npNum npdetract">4</div></td>
							<td><div class="npNum npdetract">5</div></td>
							<td><div class="npNum npdetract">6</div></td>
							<td><div class="npNum npNeutral">7</div></td>
							<td><div class="npNum npNeutral">8</div></td>
							<td><div class="npNum npPromot">9</div></td>
							<td><div class="npNum npPromot">10</div></td>
						</tr>
						<tr>
							<?php for ($i=0; $i < 6 ; $i++) { ?>								
							<td valign="bottom"><div class="npImg"><img src="<?= base_url();?>globalassets/website/img/detract.png"></div></td>
							<?php } ?>

							<td valign="bottom"><div class="npImg"><img src="<?= base_url();?>globalassets/website/img/neutral.png"></div></td>
							<td valign="bottom"><div class="npImg"><img src="<?= base_url();?>globalassets/website/img/neutral.png"></div></td>

							<td valign="bottom"><div class="npImg"><img src="<?= base_url();?>globalassets/website/img/promot.png"></div></td>
							<td valign="bottom"><div class="npImg"><img src="<?= base_url();?>globalassets/website/img/promot.png"></div></td>

						</tr>
						<tr>
							<td colspan="6"><div class="npText npdetract">Detractors</div></td>
							<td colspan="2"><div class="npText npNeutral">passive</div></td>
							<td colspan="2"><div class="npText npPromot">Promoters</div></td>
						</tr>
					</table>

				</div>
					<?php 
						$id = $this->session->userdata('sub_login_id');
						$npsDetractors 	= getNPSDetractors($id);
						$npsPassive 	= getNPSPassive($id);
						$npsPromoters 	= getNPSPromoters($id);   


						//$NPS = bcsub($npsDetractors['npscore'], $npsPromoters['npscore']);              
						$NPSPromoters 			= ($npsPromoters['npscore'] * 100)/($npsPromoters['npscore'] + $npsDetractors['npscore']+$npsPassive['npscore']);                 
            $NPSDetractors       = ($npsDetractors['npscore'] * 100)/($npsPromoters['npscore'] + $npsDetractors['npscore']+$npsPassive['npscore']);                 
            //$NPSDetractors      = ($npsDetractors['npscore'] * ($npsDetractors['npscore'] + $npsPromoters['npscore']))/100 ;
            $NPSPromoters =number_format((int)$NPSPromoters, 2, '.', '');
            $NPSDetractors= number_format((int)$NPSDetractors, 2, '.', '');

            $NPS= $NPSPromoters-$NPSDetractors  ;
					?>

           <div class="row">
            <div class="col-md-6">
              <div class="nps npsFlex">
                <div class="detractors">
                  <div class="dFlex">                   
                    <div class="npsItem">
                      <i class="fa fa-thumbs-down"></i>
                      <p><?= $npsDetractors['npscore']?$npsDetractors['npscore']:0  ;?></p>
                    </div>
                  </div>
                  <div class="npsHead">Detractors</div>
                </div>

                <div class="passive">
                  <div class="npsItem">
                    <i class="fa fa-thumbs-up"></i>
                    <p><?= $npsPassive['npscore']?$npsPassive['npscore']:0 ;?></p>
                  </div>
                  <div class="npsHead">Passive</div>
                </div>

                <div class="promoters">
                  <div class="npsItem">
                    <i class="fa fa-thumbs-up"></i>
                    <p><?= $npsPromoters['npscore']?$npsPromoters['npscore']:0  ;?></p>
                  </div>
                  <div class="npsHead">Promoters</div>
                </div>

              </div>

            </div>
            <div class="col-md-6">
              <div class="npsScore">
                <div class="dFlex">
                  <div class="promoterScore">
                    <i class="fa fa-thumbs-up"></i>
                    <span><?= $NPSPromoters?$NPSPromoters:0 ;?> % </span>
                    <i class="fa fa-minus"></i>
                  </div>
                  <div class="detractorScore">
                    <i class="fa fa-thumbs-down"></i>
                    <span><?= $NPSDetractors?$NPSDetractors:0;?> % </span>
                    <span class="equal">=</span>
                  </div>
                  <div class="passiveScore">
                    <strong><?= $NPS; ?></strong>
                  </div>
                </div>
              </div>
            </div>
        </div>

    </div>
</div>

<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">User Feedback</h3>
		<div class="box-tools pull-right">           
		</div>
	</div>
	<div class="box-body">
		<div id="chartContainer" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
		<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	</div>
</div> 

    <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
      
             <div class="box box-primary">
               <div class="box-header">   
              <input type="hidden" name="nps_main_id_"  id="nps_main_id_" value="<?= $this->uri->segment(2);?>" >
                <div class="col-md-2">            
                  <select class="form-control" name="change_feedback" id="change_feedback">
                    <option value=""> Select Feedback</option>    
                        <?php $scores= getScore();                  
                          foreach ($scores as $score) {?> 
                        <option value="<?= $score['score_id'];?>"><?= ucfirst($score['score_name'])?></option>
                     <?php  } ?>
                   </select>
                </div> 

                <div class="col-md-2">            
                  <select class="form-control" name="change_status" id="change_status">
                    <option value=""> Select Status</option>    
                        <?php $ComplaintStatuss= getComplaintStatus();                  
                        foreach ($ComplaintStatuss as $ComplaintStatus) {?> 
                        <option value="<?= $ComplaintStatus['id'];?>"><?= ucfirst($ComplaintStatus['complaint_status_name'])?></option>
                     <?php  } ?>
                   </select>
                </div>  

                <div class="col-md-2">
                <div class="searchBlock">
                  <input id="search" name="search" class="form-control" type="text" placeholder="Search">
                 </div>
                </div>  
                 <div class="col-md-2">
                 	<select class="form-control" id="change_date">
                 		<option value="">Select Date</option>
                 		<option value="0">Today</option>
                 		<option value="1">Yesterday</option>
                 		<option value="2">Last 7 Days</option>
                 		<option value="3">Last 15 Days</option>
                 		<option value="4">This Month</option>
                 		<option value="5">Last Month</option>
                 		<option value="6">Last 6 Months</option>
                 		<option value="7">This Year</option>
                 		<option value="8">Last Year</option>
                 		<option value="9">Select Custom Date</option>

                 	</select>
                 </div>
                 <div id="calender_part"></div> 

               </div>                
            </div>
          </div>
       </div>

    <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
             <div class="box">
             <!--   <div class="box-header">
                  <h3 class="box-title">Guest User Complaints Details</h3>
               </div> -->
                <div class="box-body">  
                  <div class="table-responsive without-toolbar table-striped ">
                        <table class="table table-striped table-hover" id="dashboard_complaints_list" style="width:100%;">
                          <thead class=" text-primary">                   

                            <th class="text-nowrap">S.&nbsp;No.</th>
                            <th class="text-nowrap">Guest User</th>
                            <th class="text-nowrap">Questions</th>
                            <th class="text-nowrap">Answer</th>
                            <th class="text-nowrap">Rating</th>
                            <th class="text-left text-nowrap">Elapsed</th>
                            <th class="text-nowrap">Assign</th>                  
                            <th class="text-nowrap">Current Status</th>
                           </thead>
                          <tbody>
                       </tbody>
                    </table>
                  </div>               
               </div>
             </div>
          </div>
       </div>


         <!--  <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-th"></i>

          

              <div class="box-tools pull-right">
               
              </div>
            </div>
            <div class="box-body">
              <div class="tripAdvisor">
                <img src="<?= base_url();?>/globalassets/admin/images.png">
                <p><strong>Current Rank #798</strong> <span>Hotel Noida</span></p>
              </div>
            </div>
        </div> -->
    </section>
</div>
<?php include "footer.php"; ?>

<script> 
   $(function(){

   
   var dashboard_complaints_list_table = $('#dashboard_complaints_list').DataTable({
      "processing": true,
      "pageLength": <?php echo $this->config->item('record_per_page');?>,
      "serverSide": true,
      "sortable": true,
      "lengthMenu": [[10,20, 30, 50,100], [10,20, 30, 50,100]],
      "language": {
      "emptyTable": "<?php echo $this->config->item('record_not_found_title');?>" 
      },
       "order": [
         [0, "desc"]
      ],
      "ajax":{
         url :"<?php echo base_url('sub-admin-view-all-complaints-ajax')?>", 
         type: "post",   
         data: function (d) {      
            d.feedback_id = $('#change_feedback').val();
            d.status_id = $('#change_status').val();
            d.search = $('#search').val();
            d.change_date=$('#change_date').val();
            if(d.change_date==9){
            	d.from_date=$('#from_date').val();
            	d.to_date=$('#to_date').val();
            }else{
            	d.from_date='';
            	d.to_date='';
            }
           // d.nps_main_id_ = $('#nps_main_id_').val();

             }      
      } ,
      "columns": [ 
      {data: 'id',"orderable":false},
      {data: 'name',class: 'text-nowrap',"orderable":true},
      {data: 'question',class: 'text-nowrap',"orderable":false},
      {data: 'answer',class: 'text-nowrap',"orderable":false},
      {data: 'complaints',class: 'text-nowrap',"orderable":false}, 
      {data: 'complaintstime',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'assign',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'status',class: 'text-center',"orderable":false},         
    ],
    
    "columnDefs": [        
          {
            render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1 +'.';
            },          
            "targets": 0 
          }, 

          {
            "render": function ( data, type, row ) {
            return  data;
            },
            "targets":  2
          }, 

                
       ],
          
 
 
      });


    $('#search').on('keyup', function() {
    dashboard_complaints_list_table.search(this.value).draw();
  });


     $('#change_feedback,#change_status,#to_date').change(function(){
      dashboard_complaints_list_table.ajax.reload();
     });




       $('#change_date').change(function(){
    var id=this.value;

    $('#calender_part').html('');
    var html='';
    if(id==9){
       html +='<div class="col-md-2"><input type="text" class="form-control date" autocomplete="off" id="from_date" placeholder="From Date"></div>';
      html +='<div class="col-md-2"><input type="text" class="form-control date" autocomplete="off" id="to_date" placeholder="To Date"></div>';
      $('#calender_part').html(html);
      
    $('.date').datepicker({
    format: 'dd-mm-yyyy',
    }).on('changeDate', function(e) {
      dashboard_complaints_list_table.ajax.reload();
  });
    }else{
      dashboard_complaints_list_table.ajax.reload();
    }
  })


      });
</script>




<script>





	window.onload = function () {

		var Poor='Poor';
		var Fair='Fair';
		var Good='Good';
		var Excellent='Excellent';


		var chart = new CanvasJS.Chart("chartContainer", {
			animationEnabled: true,



			title: {
				text: "User Feedback"
			},
			axisY: {
				title: "Number of Feedback"
			},
			toolTip: {
				content: "{label} <br/>{name} : {y} Feedback"
			},
			data: [{
				type: "stackedColumn",
				showInLegend: true,
				color: "#dd4b39",
				name: Poor,
				dataPoints: [<?php foreach($complaintsgraph as $row):
					?>
					{ label:"<?php if(!empty($row['cf_time'])){echo date("j M Y", strtotime($row['cf_time']));} ?>",y:<?php echo valueBydate($row['cf_time'],1);?>},
				<?php  endforeach;?>
				]
			},
			{        
				type: "stackedColumn",
				showInLegend: true,
				name: Fair,
				color: "#EDCA93",
				dataPoints: [<?php foreach($complaintsgraph as $row):
					?>
					{ label:"<?php if(!empty($row['cf_time'])){echo date("j M Y", strtotime($row['cf_time']));} ?>",y:<?php echo valueBydate($row['cf_time'],2);?>},
				<?php  endforeach;?>
				]
			},
			{        
				type: "stackedColumn",
				showInLegend: true,
				name: Good,
				color: "#00c0ef",
				dataPoints: [<?php foreach($complaintsgraph as $row):
					?>
					{ label:"<?php if(!empty($row['cf_time'])){echo date("j M Y", strtotime($row['cf_time']));} ?>",y:<?php echo valueBydate($row['cf_time'],3);?>},
				<?php  endforeach;?>
				]
			},
			{        
				type: "stackedColumn",
				showInLegend: true,
				name: Excellent,
				color: "#00a65a",
				dataPoints: [<?php foreach($complaintsgraph as $row):
					?>
					{ label:"<?php if(!empty($row['cf_time'])){echo date("j M Y", strtotime($row['cf_time']));} ?>",y:<?php echo valueBydate($row['cf_time'],4);?>},
				<?php  endforeach;?>
				]
			}]
		});
		chart.render();

		function toolTipContent(e) {
			var str = "";
			var total = 0;
			var str2, str3;
			for (var i = 0; i < e.entries.length; i++){
				var  str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'> "+e.entries[i].dataSeries.name+"</span>: <strong>"+e.entries[i].dataPoint.y+"</strong><br/>";
				total = e.entries[i].dataPoint.y + total;
				str = str.concat(str1);
			}
			str2 = "<span style = 'color:DodgerBlue;'><strong>"+(e.entries[0].dataPoint.x).getFullYear()+"</strong></span><br/>";
			total = Math.round(total * 100) / 100;
			str3 = "<span style = 'color:Tomato'>Total:</span><strong>"+total+"</strong><br/>";
			return (str2.concat(str)).concat(str3);
		}

	}
</script>
<!-- /.content-wrapper -->
<!--footer Section Start Here -->     

<!--footer Section End Here -->
<!--footer Section End Here -->