<!DOCTYPE html>
<html>
   <head>

        <?php $globalassetsadmin =$this->config->item('css')['globalassets'];  ?>
         <?php $admin =$this->config->item('css')['admin'];  ?>
         <?php $plugins =$this->config->item('css')['plugins'];  ?>


      <meta charset="UTF-8">
      <title>wageniCRM | Dashboard</title>
      <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
      <!-- Bootstrap 3.3.2 -->
      <link href="<?= $admin; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <!-- Font Awesome Icons -->
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <!-- Ionicons -->
      <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
      <!-- Morris chart -->
      <link href="<?= $plugins;?>morris/morris.css" rel="stylesheet" type="text/css" />
      <!-- Bootstrap -->
      <link href="<?= $admin; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- jvectormap -->
      <link href="<?= $plugins;?>jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
      <!-- Daterange picker -->
      <link href="<?= $plugins;?>daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />

      <link href="<?= $plugins;?>datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
      <!-- DATA TABLES -->
      <link href="<?= $plugins;?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= $plugins;?>timepicker/bootstrap-timepicker.min.css">
      <!-- Theme style -->
      <link href="<?= $admin; ?>dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
      <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
      <link href="<?= $admin; ?>dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
      <!-- AdminLTE for demo purposes -->
      <!-- Slimscroll -->
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="skin-yellow">
      <div class="wrapper">
      <header class="main-header">
         <!-- Logo -->
         <a href="javascript:void(0)" class="logo"><img src="<?= $admin; ?>logo1.png" ></a>
         <!-- Header Navbar: style can be found in header.less -->
         <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <!--<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
               <span class="sr-only">Toggle navigation</span>
               </a>-->
            <div class="nav-lt">
               <button class="sidebar-toggle"></button>
            </div>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
               <ul class="nav navbar-nav">
                  <!--    <li><i class="fa fa-desktop">Home page</i></li> -->
                  <!-- User Account: style can be found in dropdown.less -->
                  <li class="dropdown user user-menu">
                     <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                     <?php if($this->session->userdata('userfile')== false) {?>
                     <img src="<?= $admin; ?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                     <?php }else { ?>
                     <img src="<?= base_url();?>uploads/<?= $this->session->userdata('userfile')?>" class="user-image" alt="User Image"/>
                     <?php } ?>
                     <span class="hidden-xs">Hi <?= ucfirst($this->session->userdata('fname'));?></span>
                     </a>
                     <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                           <?php if($this->session->userdata('userfile')== false) {?>
                           <img src="<?= $admin; ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                           <?php }else { ?>
                           <img src="<?= base_url();?>uploads/<?= $this->session->userdata('userfile')?>" class="img-circle" alt="User Image"/>
                           <?php } ?>
                           <p><?= ucfirst($this->session->userdata('fname'));?> - <?= ucfirst($this->session->userdata('rolename'));?>
                              <small>Member since <?= date("d-M-Y", strtotime($this->session->userdata('member_since')));?></small>
                           </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                           <div class="pull-left">
                              <a href="<?= base_url('edit-profile')?>" class="btn btn-default btn-flat">Profile</a>
                           </div>
                           <div class="pull-right">
                              <a href="<?= base_url('logout');?>" class="btn btn-default btn-flat">Sign out</a>
                           </div>
                        </li>
                     </ul>
                  </li>
               </ul>
            </div>
         </nav>
      </header>