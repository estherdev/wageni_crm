  <?php
  $globalassetsadmin =  $this->config->item('js')['globalassets'];
  $admin =  $this->config->item('js')['admin'];  
  $plugins =  $this->config->item('js')['plugins'];
?>

<style type="text/css">
td.details-control { background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center 12px; cursor: pointer; }
tr.details td.details-control { background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center 12px; }
.datatable_loader { text-align: center; }
.datatable_loader img { width: 80px; }
.small_loader { position: relative; height: 20px; width: 100%; overflow: hidden; }
.small_loader img { width: 50px; position: absolute; top: -10px; left: 0; }
</style>


      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
      </div>
      <strong>Copyright &copy; <?php echo date('Y');?> <a href="<?= base_url();?> ">wageniCRM</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- jQuery 2.1.3 -->
<script src="<?=  $plugins?>jQuery/jQuery-2.1.3.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?= $admin;?>bootstrap/js/bootstrap.min.js"></script>




<script>

   var form_error_message='<div class="alert alert-danger">There is error in submitting form!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
  // Common Delete Function 
  function deleteRecord(id){
    if(id>0){
      $('#deleteModalPopup').modal('show');
      $('#action_id').val(id);
      $('#confirmation_message').html('Are you sure want to delete this Record?');
    }
   }
   function SetLoader(action){
    if(action==1){
      $('.modalLoader').show();
    }else{
      $('.modalLoader').hide();
    }
   }
  function actionPerform(action_url,table_id){
    $('#deleteModalPopup').modal('hide');
    var action_id=$('#action_id').val();
    $.ajax({
        type: "POST",
        url: action_url,
        data: {'action_id':action_id},
        success: function(ajaxresponse){
            response=JSON.parse(ajaxresponse);
          if(!response['status']){
            $('#success_message').html(form_error_message);
          }else{
            $('#success_message').html('<div class="alert alert-success">'+response['message']+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            table_id.ajax.reload();
           }  
        }
      });
  }
</script>
<script>

var defaltimageurl='<?= base_url()?>globalassets/admin/dist/img/user2-160x160.jpg';
var imageurl='<?= base_url()?>uploads/';
var base_url='<?= base_url()?>';

var datatable_loader='<div class="datatable_loader"><img src="'+base_url+'/globalassets/admin/preloader1.gif"></div>';
var small_loader='<div class="small_loader"><img src="'+base_url+'/globalassets/admin/preloader1.gif"></div>';


 $(document).ready(function(){
 	$(".sidebar-toggle").on('click', function(){
			  //alert('click!');
			  var result=$(".main-sidebar, .content-wrapper, .main-footer").hasClass("activeIn");

			  if(result == false)	{
			  	$(" .main-sidebar, .content-wrapper, .main-footer").addClass("activeIn")
			  }
			  else {
			  	$(" .main-sidebar, .content-wrapper, .main-footer").removeClass("activeIn")
			  }
			});

 	if ($(window).width() <=1020 ) {
 		$(" .main-sidebar, .content-wrapper, .main-footer").addClass("activeIn");
 	}
 	else {
 		$(" .main-sidebar, .content-wrapper, .main-footer").removeClass("activeIn")
 	}
 	$("#sidebarMenu > li").on('click mouseover', function(){
 		$(".main-sidebar, .content-wrapper, .main-footer").removeClass("activeIn")
 	});
});
</script>
<script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.1.3/dist/bootstrap-validate.js" ></script>
<!-- DATA TABES SCRIPT -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.js" ></script>



<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!--flash js-->
<script src="<?= $admin ?>webadmincustom.js"></script>
<script src="<?= $globalassetsadmin ?>validaterule.js"></script>
<script src="<?= $globalassetsadmin ?>flash.js"></script>

<script src="<?=  $plugins?>datatables/jquery.dataTables.js"></script>
<script src="<?=  $plugins?>datatables/dataTables.bootstrap.js" ></script>
<script href="<?=  $plugins?>datepicker/bootstrap-datepicker.js"/></script>
<!-- FastClick -->
<script src='<?=  $plugins?>fastclick/fastclick.min.js'></script>
<!-- AdminLTE App -->
<script src="<?= base_url();?>globalassets/admin/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="<?=  $plugins?>sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?=  $plugins?>jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?=  $plugins?>jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- daterangepicker -->
<script src="<?=  $plugins?>daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?=  $plugins?>datepicker/bootstrap-datepicker.js"></script>
<!-- iCheck -->   
<script src="<?=  $plugins?>iCheck/icheck.min.js" ></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?=  $plugins?>slimScroll/jquery.slimscroll.min.js" ></script>
<!-- ChartJS 1.0.1 -->
<script src="<?=  $plugins?>chartjs/Chart.min.js" ></script>

<script src="<?=  $plugins?>daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?=  $plugins?>timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
 <script src="<?= $admin;?>dist/js/jquery.rating-stars.js"></script>


</body>

</html>