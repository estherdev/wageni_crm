
<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Home Page FAQ</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Home Page FAQ</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">

      <div class="col-md-12 col-sm-6 col-xs-12">

        <!-- general form elements -->

        <div class="box box-primary">

          <div class="box-header">

            <h3 class="box-title">FAQ</h3>

          </div><!-- /.box-header -->

          <!-- form start -->
          <?php echo $this->session->flashdata('successmsg');?>
          <form role="form" action="<?= base_url('admin/masters/Faq/editdata/'.$records[0]['faq_id']);?>" method="post" >

            <div class="box-body">
              <div class="form-group row">
                <div class="col-md-12">
                  <label for="question">Que.</label>
                  <input type="text" class="form-control" value="<?php echo $records[0]['faq_que'];?>" name="question" placeholder="Enter Question">
                  <?= form_error('question');?>
                </div>

                <div class="col-md-12">
                  <label for="answer">Ans.</label>
                  <textarea type="text" class="form-control" name="answer" placeholder="Enter Answer"><?php echo $records[0]['faq_ans'];?></textarea>
                  <?= form_error('answer');?>
                </div>
              </div>

              <div class="box-footer text-center">

                <button type="submit" class="btn btn-success">Update</button>
              </div>

            </form>

          </div><!-- /.box -->

        </div><!-- /.content-wrapper -->
 </section>
      </div><!-- /.content-wrapper -->


      <?php include APPPATH . 'views/admin/footer.php'; ?>
      <!-- page script -->
      <script type="text/javascript">
        $(function () {
          $("#example1").dataTable();
          $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
          });
        });
      </script>
