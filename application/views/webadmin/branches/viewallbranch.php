
<!--header Section Start Here -->
<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Add Branch</h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Add Branch</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
   <?php if($this->session->flashdata("success")):?>
    <div class="alert alert-success"><?= $this->session->flashdata("success");?>
    <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
  </div>
<?php endif;?>
<?php if($this->session->flashdata("danger")):?>
  <div class="alert alert-danger"><?= $this->session->flashdata("danger");?>
  <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
</div>
<?php endif;?>
<div class="row">
  <div class="col-md-12 col-sm-6 col-xs-12">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Branch Details</h3>
      </div><!-- /.box-header -->
      <?php if($sub_branch_limit==false){ ?>
        <!-- form start -->
        <?php echo $this->session->flashdata('successmsg');?>
        <form role="form" id="branch" action="<?= base_url('view-branch');?>" method="post" >
          <div class="box-body">
            <div class="form-group row">
              <div class="col-md-12">
                <label for="branch_name">Branch Name</label>
                <input type="text" class="form-control"  name="branch_name" placeholder="Enter Branch Name">
                <?= form_error('branch_name');?>
              </div>
            </div>
            <div class="box-footer text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button type="reset" class="btn btn-danger">Reset</button>
            </div>
            </div>
          </form>
        <?php } else{ ?>
          <div class="col-sm-12 col-md-12" style="background-color:yellow;">
            <div class="text-center"><h3>With the Current Plan you get only <?php print_r($branchlimi);?> Branches Add and will be able to Add a <?php print_r($branchlimi);?> Branch. Please Upgrade your Current plan For Add More Branches.<a href="<?= base_url('package');?>" target="_blank">More Info...</a></h3></div> 
          </div>
        <?php } ?>
      </div><!-- /.box -->
    </div><!-- /.content-wrapper -->
  </div><!-- /.content-wrapper -->


  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><b>Branch</h3></b>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped text-center">
            <thead>
              <tr>
                <th>S.No.</th>
                <th>Branch Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i= 1; foreach ($viewbranch as $key => $branch) {?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= ucfirst($branch->branch_name); ?></td>
                  <td class="text-center">
                     <a class="btn btn-sm btn-info" href="<?php echo base_url('website/SubsControler/getBranchById/'.$branch->sub_branch_id) ;?>" title="Edit"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-sm btn-danger deleteSlider"  data-toggle="modal" data-target="#deleteModal<?= $branch->sub_branch_id?>" title="Delete"><i class="fa fa-trash"></i></a>
                  </td>
                  <div id="deleteModal<?= $branch->sub_branch_id?>" class="modal fade">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <!-- dialog body -->
                        <div class="modal-body">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          Are You Sure to <strong>Branch</strong> Delete?
                        </div>
                        <!-- dialog buttons -->
                        <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('delete-branch/'.$branch->sub_branch_id); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                      </div>
                    </div>
                  </div>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php include APPPATH . 'views/webadmin/footer.php'; ?>
<!-- page script -->

