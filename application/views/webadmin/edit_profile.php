<!--header Section Start Here -->
<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
   <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        Update Profile
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Update Profile</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title">Profile  Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form method="post" id="editProfile" action="<?= base_url('website/SubsControler/editProfile/'.$profile[0]->sub_login_id);?>"enctype="multipart/form-data">
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="f-name">First Name</label>
                           <input type="text" class="form-control"  name="fname" value="<?= $profile[0]->fname ;?>" placeholder="First Name">
                           <?= form_error('fname')?>
                        </div>
                        <div class="col-md-4">
                           <label for="l-name">Last Name</label>
                           <input type="text" class="form-control"  name="lname" value="<?= $profile[0]->lname ;?>" placeholder="Last Name" >
                           <?= form_error('lname')?>
                        </div>

                        <div class="col-md-4">
                           <label for="e-address"><?= $this->lang->line('LAVELUEMAIL');?></label>
                           <input  class="form-control" name="uemail" value="<?= $profile[0]->sub_email ;?>" placeholder="<?= $this->lang->line('PLACEHODERUEMAIL');?>" disabled >
                           <?= form_error('uemail')?>
                        </div>
                        </div>

                        <div class="form-group row">
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPHONE');?></label>
                           <input type="text" class="form-control" name="uphone" value="<?= $profile[0]->sub_phone ;?>" placeholder="<?= $this->lang->line('PLACEHODERUPHONE');?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                           <?= form_error('uphone')?>
                        </div>
                      
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPASS');?></label>
                           <input type="password" class="form-control" name="upass" value="<?= $profile[0]->sub_pass ;?>" placeholder="<?= $this->lang->line('PLACEHODERUPASS');?>">
                           <?= form_error('upass')?>
                        </div>

                        <div class="col-md-3">
                           <label for="Image">Logo</label>
                            <input type="hidden" name="hiddenimg" value="<?php echo $profile[0]->userfile ;?>">
                           <input type="file" class="form-control"  name="userfile"  />
                            <?= form_error('userfile')?>   
                        </div>
                        <div class="col-md-1">
                           <?php if(empty($profile[0]->userfile)) {?>
                           <img src="<?= base_url();?>globalassets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                           <?php }else {
                              $this->session->set_userdata('userfile',$profile[0]->userfile);
                            ?>
                           <img src="<?= base_url();?>uploads/<?= $profile[0]->userfile;?>" class="img-circle" alt="User Image" />
                           <?php } ?>
                        </div>
                        
                     </div>

                      <div class="box-header">
                        <h3 class="box-title"><strong>Contact Details</strong></h3>
                     </div> 
                     
                     <div class="form-group row">
                        <div class="col-md-4">
                              <label for="">Telephone</label>
                              <input type="text" class="form-control" name="contacttel" value="<?= $profile[0]->contacttel ;?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  placeholder="Telephone" value="<?= set_value('contacttel'); ?>">
                              <?= form_error('contacttel')?>
                        </div> 
                        <div class="col-md-4">
                              <label for="">Contact Number</label>
                              <input type="text" class="form-control" name="contactno" value="<?= $profile[0]->contactno ;?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  placeholder="Contact Number" value="<?= set_value('contactno'); ?>">
                              <?= form_error('contactno')?>
                        </div>
                        <div class="col-md-4">
                              <label for="">Website URL</label>
                              <input type="text" class="form-control" name="weburl" value="<?= $profile[0]->company_url ;?>" placeholder="Website URL" value="<?= set_value('weburl'); ?>">
                              <?= form_error('weburl')?>
                        </div> 
                        </div> 
                         <div class="form-group row">
                        <div class="col-md-3">
                              <label for="">Account No</label>
                              <input type="text" class="form-control" name="accno" value="<?= $profile[0]->accno ;?>" id="adaccno" placeholder="Account No" value="<?= set_value('accno'); ?>">
                              <?= form_error('accno')?>
                        </div> 
                        <div class="col-md-3">
                              <label for="">Generate</label> 
                              <input type="button " class="form-control btn btn-success" value="Generate Account No!" onclick="doFunction(Random);" />
                        </div> 
                        <div class="col-md-6">
                              <label for="">Address</label>
                              <textarea class="form-control" name="contactaddress" placeholder="Address" ><?= set_value('contactaddress'); ?><?= $profile[0]->contactaddress ;?></textarea>
                              <?= form_error('contactaddress')?>
                        </div> 
                     </div> 
                     <div class="box-header">
                        <h3 class="box-title"><strong>Contact Person Details</strong></h3>
                     </div>
                     <div class="form-group row">
                        <div class="col-md-4">
                              <label for="">Name</label>
                              <input type="text" class="form-control" name="cpname" value="<?= $profile[0]->cpname ;?>" placeholder="Name" value="<?= set_value('cpname'); ?>">
                              <?= form_error('cpname')?>
                        </div> 
                        <div class="col-md-4">
                              <label for="">Email</label>
                              <input type="email" class="form-control" name="cpemail" value="<?= $profile[0]->cpemail ;?>" placeholder="Account No" value="<?= set_value('cpemail'); ?>">
                              <?= form_error('cpemail')?>
                        </div> 
                         <div class="col-md-4">
                              <label for="">Title</label>
                              <input type="text" class="form-control" name="cptitle" value="<?= $profile[0]->cptitle ;?>" placeholder="Title" value="<?= set_value('cptitle'); ?>">
                              <?= form_error('cptitle')?>
                        </div> 
                        </div> 
                         <div class="form-group row">
                         <div class="col-md-4">
                              <label for="">Mobile</label>
                              <input type="text" class="form-control" name="cpmobile" value="<?= $profile[0]->cpmobile ;?>" placeholder="Mobile" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  value="<?= set_value('cpmobile'); ?>">
                              <?= form_error('cpmobile')?>
                        </div> 
                         <div class="col-md-4">
                              <label for="">Address</label>
                              <textarea class="form-control" name="cpaddress"  placeholder="Address"><?= set_value('cpaddress'); ?><?= $profile[0]->cpaddress ;?></textarea>
                              <?= form_error('cpaddress')?>
                        </div>
                         <div class="col-md-4">
                              <label for="">Office</label>
                              <textarea class="form-control" name="cpoffice" placeholder="Office"><?= set_value('cpoffice'); ?><?= $profile[0]->cpoffice ;?>
                              <?= form_error('cpoffice')?></textarea>
                        </div> 
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                  <button type="submit" class="btn btn-success">Update</button>
                  <a href="<?= base_url('dashboard');?>" class="btn btn-danger" role="button">Back</a>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
</div>
<!-- <?php echo "<pre/>"; print_r($_SESSION); ?> -->
<script>
  function Random() {    
    return Math.floor(Math.random() * 1000000000);
 }
 function doFunction(){
  document.getElementById('adaccno').value = Random()
}    
</script>
<!-- /.content-wrapper -->
</section><!-- /.content -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->         
<?php include APPPATH . 'views/webadmin/footer.php'; ?>
<!--footer Section End Here