<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

//require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Home (UserController)
 * Home Class to control Home Pages related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 28 August 2018
 */
class Home extends CI_Controller
{
 
    //  This is default constructor of the class
 
    public function __construct()
    {
        parent::__construct();  
    }
    
    // This function used to load the first screen of the user
 
    public function index()
    {
        $this->global['pageTitle'] = 'Home';        
        $this->load->view("website/home", $this->global);
    }
    
   
}

?>