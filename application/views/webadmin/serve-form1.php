<!--header Section Start Here -->
<?php require APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php require APPPATH . 'views/webadmin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Survey
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add Survey</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12 ">
        <!-- general form elements -->
        <div class="box ">
          <div class="box-header">
            <h3 class="box-title">Survey</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <div id="viewAllQue" class="viewAllQue" >
            </div>
          	<div id="messageResponse" class="messageResponse bg-success" >
       		</div>
       		<form id="nextQues" class="nextQues" method="post">
           <div class="qustio-box">
            <div class="row form-group">
              <div class="rate-select">
               <!--  <div class="col-md-4">
                 <label>Category</label>
                  <select class="form-control cat" name="cat" id="cat"  >
                    <option value="none">Select Category</option>
                    <?//= form_error('cat')?>
                     <?php
                  //foreach($category as $cat)
                 // {
                  // echo '<option value="'.$cat->cat_id.'">'.$cat->category_name.'</option>';
                  //}
                  ?>
                  </select>
                </div> -->
                <div class="col-md-6">
                  <label>Sub Category</label>
                  <?= form_error('subcat')?>
                  <select class="form-control scat" name="subcat" id="subcat" >
                  <option value="">Select Sub Category</option>
                  </select>
                </div>

                <div class="col-md-6">
                  <label>Process</label>
                  <?= form_error('subsubcat')?>
                  <select class="form-control scat" name="subsubcat" id="subsubcat" >
                  <option value="">Select Process</option>
                  </select>
                </div>
              </div>
            </div> 
            <hr/>
            <div class="input-dropbox"> 

            <div class="qan">Que</div>
            <div class="input-box">
              	<input type="text" class="form-control surveyQue" name="surveyQue" id="surveyQue" placeholder="Enter your question">
            </div>
            <input type="hidden" name="q_type" id="q_type" class="q_type">
            <div class="dropbox">
            <a><span class="selectedli" id="selectedli">Multiple Choice</span><i class="fa fa-angle-down"></i></a>
            <div class="dropdown-box">
                <ul id="selectdropdownquestion" class="selectdropdownquestion" >
	                <li class="selectli" getlivalue="Multiple Choice">
	                  <a data-action="MultipleChoiceQuestion" class="select-li" id="qmc" data-help="qmc" href="javascript:void(0)"><i class="fa fa-list select-menu-list"></i>Multiple Choice</a>
	                </li>
	                <li class="selectli" getlivalue="Dropdown">
	                  <a data-action="DropdownQuestion" class="select-li" id="qdd" data-help="qdd" href="javascript:void(0)"><i class="fa fa-sort select-menu-list"></i>Dropdown</a>
	                </li>
	                <li class="selectli" getlivalue="Checkboxes">
	                  <a data-action="CheckboxQuestion" class="select-li" id="qchb" data-help="qchb" href="javascript:void(0)"><i class="fa fa-check-square-o select-menu-list"></i>Checkboxes</a>
	                </li>
	                <li class="selectli" getlivalue="Star Rating">
	                  <a data-action="StarRatingQuestion" class="select-li" id="qsr" data-help="qsr" href="javascript:void(0)"><i class="fa fa-star select-menu-list"></i>Star Rating</a>
	                </li>
	                <li class="selectli" getlivalue="Single Textbox">
	                  <a data-action="SingleTextboxQuestion" class="select-li" id="qst" data-help="qst" href="javascript:void(0)"><i class="fa fa-square-o select-menu-list"></i>Single Textbox</a>
	                </li>
	                <li class="selectli" getlivalue="Multiple Textboxes">
	                  <a data-action="MultipleTextboxQuestion" class="select-li" id="qmt" data-help="qmt" href="javascript:void(0)"><i class="fa fa-bars select-menu-list"></i>Multiple Textboxes</a>
	                </li>
                </ul>
            </div>
            </div>
           <div class="help"><i class="fa fa-exclamation-circle"></i></div>
         </div>
       </div> 
       <div id="selectedInputLi" class="selectedInputLi" >
       </div>
       <img src="<?= base_url();?>uploads/loader.gif" id="gif" style="display:none">
       </form>
   </div>
 </div><!-- /.box -->
 <!-- general form elements -->

</div>
</div>   
</section><!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php require APPPATH . 'views/webadmin/footer.php';?>
<!--footer Section End Here -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.min.js"></script>
<script>
  $(document).ready(function() {

   $('#selectdropdownquestion li a').click(function() {
        //Get the data-aaction of list items
        /* to check if other div is alreay open*/
        var len= $('.answer-q').length;

        if(len >=1)
        {
          $('.answer-q').remove();
        }

        var selectInputLi = $(this).attr('data-action');
        var selectQueType = $(this).attr('data-help');
        var Select='-- Select -- ';
        var VP='Very Poor';
        var P='Poor';
        var A='Aaverage';
        var G='Good';
        var VG='Very Good';

        $("#q_type").val(selectQueType);

        switch (selectInputLi) {

       case "MultipleChoiceQuestion":
		        var html='';
		        html +='<div class="answer-q" >';
		        html +='<div class="anwer-body field_wrapperMultipleChoiceQuestion">';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-radio"><input type="radio" id="test4" disabled=""><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" name="qmc[]" class="form-control" placeholder="Enter an answer choice"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleChoiceQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-radio"><input type="radio" id="test4" disabled=""><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" name="qmc[]" class="form-control" placeholder="Enter an answer choice"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleChoiceQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-radio"><input type="radio" id="test4" disabled=""><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" name="qmc[]" class="form-control" placeholder="Enter an answer choice"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleChoiceQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-radio"><input type="radio" id="test4" disabled=""><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" name="qmc[]" class="form-control" placeholder="Enter an answer choice"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleChoiceQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row " >';       
		        html +='<div class="form-radio"><input type="radio" id="test4" disabled=""><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" name="qmc[]" class="form-control" placeholder="Enter an answer choice"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleChoiceQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';


		        $("#selectedInputLi").append(html);

			    var maxField = 5; //Input fields increment limitation  
			    var x = 1; //Initial field counter is 1
			    
			    //Once add button is clicked
			    $('.add_buttonMultipleChoiceQuestion').click(function(){
			        //Check maximum number of input fields
			        if(x < maxField){ 
			        	
			            x++; //Increment field counter
			            $('.field_wrapperMultipleChoiceQuestion').append('<div class="addQ" ><div class="form-group form-row"><div class="form-radio"><input type="radio" id="test4" disabled=""><label for="test4"></label></div><div class="input-box"><input type="text" name="qmc[]" class="form-control" placeholder="Enter an answer choice"></div><div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div><div class="add-remove"><a href="javascript:void(0)" class="remove_buttonMultipleChoiceQuestion"><i class="fa fa-minus-circle"></i></a></div></div></div>'); 
			        }
			    });
			    
			    //Once remove button is clicked
			    $('.field_wrapperMultipleChoiceQuestion').on('click', '.remove_buttonMultipleChoiceQuestion', function(e){
			    	//alert($(this));
			        e.preventDefault();
			        $(this).parent('div').parent('div').parent('div').remove(); //Remove field html
			        x--; //Decrement field counter
			    });

		        $("#surveyQue").focus();

            break;

        case "DropdownQuestion":
		        
		        var html='';
		        html +='<div class="answer-q">';
		        html +='<div class="anwer-body field_wrapperDropdownQuestion">';
		        html +='<div class="form-group form-row">';
		        html +='<div class="input-box"><input type="text" class="form-control" name="qdd[]" placeholder="Enter an answer choice"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonDropdownQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="input-box"><input type="text" class="form-control" name="qdd[]" placeholder="Enter an answer choice"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonDropdownQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="input-box"><input type="text" class="form-control" name="qdd[]" placeholder="Enter an answer choice"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonDropdownQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="input-box"><input type="text" class="form-control" name="qdd[]" placeholder="Enter an answer choice"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonDropdownQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="input-box"><input type="text" class="form-control" name="qdd[]" placeholder="Enter an answer choice"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonDropdownQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);

		        var maxField = 5; //Input fields increment limitation  
			    var x = 1; //Initial field counter is 1
			    
			    //Once add button is clicked
			    $('.add_buttonDropdownQuestion').click(function(){
			        //Check maximum number of input fields
			        if(x < maxField){ 
			        	
			            x++; //Increment field counter
			            $('.field_wrapperDropdownQuestion').append('<div class="addQ" ><div class="form-group form-row"><div class="input-box"><input type="text" class="form-control" name="qdd[]" placeholder="Enter an answer choice"></div><div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div><div class="add-remove"><a href="javascript:void(0)" class="remove_buttonDropdownQuestion"><i class="fa fa-minus-circle"></i></a></div></div>'); 
			        }
			    });
			    
			    //Once remove button is clicked
			    $('.field_wrapperDropdownQuestion').on('click', '.remove_buttonDropdownQuestion', function(e){
			    	//alert($(this));
			        e.preventDefault();
			        $(this).parent('div').parent('div').parent('div').remove(); //Remove field html
			        x--; //Decrement field counter
			    });

		        $("#surveyQue").focus();

            break;
              
          case "CheckboxQuestion":
		        var html='';
		        html +='<div class="answer-q">';
		        html +='<div class="anwer-body field_wrapperCheckboxQuestion">';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-checkbox"><input type="checkbox" id="test4" disabled=""><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qchb[]" ></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)  class="add_buttonCheckboxQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-checkbox"><input type="checkbox" id="test4" disabled=""><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qchb[]" ></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)  class="add_buttonCheckboxQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-checkbox"><input type="checkbox" id="test4" disabled=""><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qchb[]" ></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)  class="add_buttonCheckboxQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-checkbox"><input type="checkbox" id="test4" disabled=""><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qchb[]" ></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)  class="add_buttonCheckboxQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-checkbox"><input type="checkbox" id="test4" disabled=""><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qchb[]" ></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonCheckboxQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);

		          var maxField = 5; //Input fields increment limitation  
			    var x = 1; //Initial field counter is 1
		         $('.add_buttonCheckboxQuestion').click(function(){
			        //Check maximum number of input fields
			        if(x < maxField){ 
			        	
			            x++; //Increment field counter
			            $('.field_wrapperCheckboxQuestion').append('<div class="addQ" ><div class="form-group form-row"><div class="form-checkbox"><input type="checkbox" id="test4" disabled=""><label for="test4"></label></div><div class="input-box"><input type="text" class="form-control" name="qdd[]" placeholder="Enter an answer choice"></div><div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div><div class="add-remove"><a href="javascript:void(0)" class="remove_buttonCheckboxQuestion"><i class="fa fa-minus-circle"></i></a></div></div>'); 
			        }
			    });
			    
			    //Once remove button is clicked
			    $('.field_wrapperCheckboxQuestion').on('click', '.remove_buttonCheckboxQuestion', function(e){
			    	//alert($(this));
			        e.preventDefault();
			        $(this).parent('div').parent('div').parent('div').remove(); //Remove field html
			        x--; //Decrement field counter
			    });

		        $("#surveyQue").focus();

            break;
              
          case "StarRatingQuestion":
		        var html='';
		        html +='<div class="answer-q">';        
		        html +='<div class="rating-box">';        
		        html +='<input type="hidden" class="form-control starno" name="qsr" id="starno" value="5" />';
		        html +='<div class="rating-foot"><div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i> Next Qustion </a></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $("#surveyQue").focus();

            break;
              
          case "SingleTextboxQuestion":

		        var html='';
/*
		         html +='<div class="answer-q" >';
		        html +='<div class="anwer-body field_wrapperMultipleChoiceQuestion">';
		        html +='<div class="form-group form-row">';
		        html +='<div class="input-box"><input type="text" name="qst[]" class="form-control" placeholder="Enter an answer"></div>';
		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleChoiceQuestion"><i class="fa fa-plus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';*/

				html +='<div class="answer-q">';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a>';
		        html +='</div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $("#surveyQue").focus();

            break;
              
        case "MultipleTextboxQuestion":
		        var html='';
				html +='<div class="answer-q">';  
				html +='<div class="anwer-body add-label field_wrapperMultipleTextboxQuestion">';
				html +='<div class="form-group form-row">';
				//html +='<div class="form-radio"><label for="test1">Label</label></div>';
				html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice"  name="qmt[]"></div>';

		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
				html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleTextboxQuestion"><i class="fa fa-plus-circle"></i></a></div>';
				html +='</div>';
				html +='<div class="form-group form-row">';
				//html +='<div class="form-radio"><label for="test2">Label</label></div>';
				html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qmt[]"></div>';

		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
				html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleTextboxQuestion"><i class="fa fa-plus-circle"></i></a></div>';
				html +='</div>';
				html +='<div class="form-group form-row">';
				//html +='<div class="form-radio"><label for="test3">Label</label></div>';
				html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qmt[]"></div>';

		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
				html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleTextboxQuestion"><i class="fa fa-plus-circle"></i></a></div>';
				html +='</div>';
				html +='<div class="form-group form-row">';
				//html +='<div class="form-radio"><label for="test4">Label</label></div>';
				html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qmt[]"></div>';

		        html +='<div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div>';
				html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleTextboxQuestion"><i class="fa fa-plus-circle"></i></a></div>';
				html +='</div>';
				html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a></div>';
		        html +='<div class=""><input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);

  				var maxField = 5; //Input fields increment limitation  
			    var x = 1; //Initial field counter is 1
		                 $('.add_buttonMultipleTextboxQuestion').click(function(){
			        //Check maximum number of input fields
			        if(x < maxField){ 
			        	
			            x++; //Increment field counter
			            $('.field_wrapperMultipleTextboxQuestion').append('<div class="addQ" ><div class="form-group form-row"><div class="input-box"><input type="text" class="form-control" name="qdd[]" value="Enter an answer choice"></div><div class="input-box"><select name="q_score[]" class="form-control"><option>'+Select+' </option><option value="1">'+VP+'</option><option value="2">'+P+'</option><option value="3">'+A+'</option><option value="4">'+G+'</option><option value="5">'+VG+'</option></select></div><div class="add-remove"><a href="javascript:void(0)" class="remove_buttonMultipleTextboxQuestion"><i class="fa fa-minus-circle"></i></a></div></div>'); 
			        }
			    });
			    
			    //Once remove button is clicked
			    $('.field_wrapperMultipleTextboxQuestion').on('click', '.remove_buttonMultipleTextboxQuestion', function(e){
			    	//alert($(this));
			        e.preventDefault();
			        $(this).parent('div').parent('div').parent('div').remove(); //Remove field html
			        x--; //Decrement field counter
			    });
		        $("#surveyQue").focus();

            break;

          default:
              	text = "No value found";  
        //var surveyQue = $('#selectedli').val(); 
  		//alert(surveyQue); 
      }
		/* var str = document.getElementById("selectedli").innerHTML; 
		var res = str.replace("Multiple Choice",selectInputLi);
		document.getElementById("selectedli").innerHTML = res;*/
		//$("#selectedli").html($("#selectedli").html().replace(selectInputLi));
		//$("#selectedli").append('<span class="selectedli">'+selectInputLi+'</span>');
   });
  });


 $(function() {
    $('form').on('submit', function(e) {
        e.preventDefault();       
      	//var json = $(this).serializeJSON();
		//console.log(json);
		$.ajax({
            dataType: 'html',
            type: 'post',
            url: '<?= base_url();?>admin-questionSubmit',
            data: $('#nextQues').serialize(),
            //beforeSubmit: validator,

            success: function(responseData) {
            	
            if (responseData =='1') {
            
                $('#messageResponse').html('<div class="alert alert-success"> Insert Successfully<a class="close" data-dismiss="alert"><i class="icon-remove"></i></a></div>');
               // $('#messageResponse').html('<p>Insert Successfully</p>');
                $('#messageResponse').fadeIn(1000);
                $("form").trigger("reset");

	          		  }else if (responseData == "3") {
	            		alert('Please Select Field are Required')
	            		$('#messageResponse').fadeIn(1000);
            	 		//$('#messageResponse').html('<p>Select </p>');      	
	            }else{
	            		$('#messageResponse').fadeIn(1000);
	            	 	$('#messageResponse').html('<div class="alert alert-danger">Something went wrong<a class="close" data-dismiss="alert"><i class="icon-remove"></i></a></div>');
	            }
            },
            /*error: function(responseData){
                console.log('Ajax request not recieved!');
            }*/
        });
        //$('#gif').show(); 
        //$("#viewAllQue").append(html);
        });
    });
</script>


<script>
    $(document).ready(function(){
    var cat_id = '<?= $this->session->userdata('category');?>';
    //$('#cat').change(function(){
   // var cat_id = $('#cat').val();
    
		    if(cat_id != '')
			{
				$.ajax({
				url:"<?php echo base_url(); ?>website/SubsControler/fetch_subcategory",
				method:"POST",
				data:{cat_id:cat_id},
				success:function(data)
				{
					$('#subcat').html(data);
					$('#subsubcat').html('<option value="">Select Process</option>');
				}
		});
	}
    else
	{
		$('#subcat').html('<option value="">Select Sub category</option>');
		$('#subsubcat').html('<option value="">Select Process</option>');
	}
    //});
   
	$('#subcat').change(function(){
		var sub_cat_id = $('#subcat').val();
			if(sub_cat_id != '')
			{
				$.ajax({
				url:"<?php echo base_url(); ?>website/SubsControler/fetch_subsubcat",
				method:"POST",
				data:{sub_cat_id:sub_cat_id},
				success:function(data)
					{
						$('#subsubcat').html(data);
					}
				});
			}
			else
			{
				$('#subsubcat').html('<option value="">Select Process</option>');
			}
		});

	});
</script>

<script>
	$("ul.selectdropdownquestion").on("click", "li", function() {
	var myText = $(this).attr("getlivalue");         
	$("#selectedli").text( myText );
	 });
</script>

