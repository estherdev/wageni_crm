<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Client Logo
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add Client Logo</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Client Logo Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
            <form method="post" action="<?= base_url('');?>" enctype="multipart/form-data">

              <div class="box-body">
              <div class="form-group row">

                <div class="col-md-6">
                  <label for="title">name</label>
                  <input type="text"  class = "form-control"name="name" />
                  <?= form_error('title')?>            
                </div>

                <div class="col-md-6">
                  <label for="Image">Image</label>
                  <input type="file" class="form-control"  name="userfile"  />
                   <?= form_error('userfile')?>   
                </div>               
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.content-wrapper -->
    </div>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Client Logo</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Name</th>
                    <th>Logo</th>
                    <!-- <th>Action</th> -->
                  </tr>
                </thead>
                <tbody>    
                  <?php $i= 1; foreach ($alldata as $key => $pack) {?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= ucfirst($pack->title); ?></td>
                    <td><img src="<?php echo base_url();?>uploads/<?= $pack->userfile; ?>" height="50px" ></td>
                    <td><?= ucfirst($pack->content); ?></td>
                    <td><?= ($pack->created_date); ?></td>
                    <td><?= ucfirst($pack->created_by); ?></td>
               
                     <td class="text-center">
                        <a class="btn btn-sm btn-info" href="<?= base_url('edit-logo/'.$pack->id)?>" title="Edit"><i class="fa fa-pencil"></i></a> 
                        <a class="btn btn-sm btn-danger deletepack"  data-toggle="modal" data-target="#deleteModal<?= $pack->id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                      </td>
                      <div id="deleteModal<?= $pack->id ; ?>" class="modal fade">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        <!-- dialog body -->
                        <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                         Are You Sure to <strong>Logo</strong> Delete?
                        </div>
                        <!-- dialog buttons -->
                        <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('delete-logo/'.$pack->id);?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                        </div> 
                        </div>
                      </div>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    <!-- /.content-wrapper -->
  </div>

  <!-- /.content-wrapper -->
</section><!-- /.content -->
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>