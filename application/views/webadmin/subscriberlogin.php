<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Wageni| CRM Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?= base_url();?>globalassets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?= base_url();?>globalassets/admin/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?= base_url();?>globalassets/admin/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="#"><b>Wageni</b>CRM</a>
      </div>
      <div class="login-box-body">
        <p class="login-box-msg">Sign in</p>
        <p class="statusMsg"></p>  
         <div id="error"></div> 
        <form id="admin-login-form" method="post">
          <div class="form-group has-feedback">
             <p id="lemailerr"></p>
            <input type="text" class="form-control" name="adminEmail" id="adminEmail" placeholder="Email"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
             <p id="lpasserr"></p>
            <input type="password" class="form-control" name="adminPass" id="adminPass" placeholder="Password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row submitBtn">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat regBtn" name="btn-save" id="btn-submit" >Sign In</button>
            </div>
          </div>
        </form>
      </div>
    </div>

    <!-- jQuery 2.1.3 -->
    <script src="<?= base_url();?>globalassets/admin/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?= base_url();?>globalassets/admin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.1.3/dist/bootstrap-validate.js" ></script>
    <!-- iCheck -->
    <script src="<?= base_url();?>globalassets/admin/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>

<!-- custom script start-->

<script type="text/javascript">

 $( "#admin-login-form" ).submit(function( event ) {
 
  submitLogin();
  event.preventDefault();
});


  function submitLogin() {

    var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var adminEmail = $('#adminEmail').val();   
    var adminPass = $('#adminPass').val(); 

    if(adminEmail.trim() == '' ){
       $errormsg = "<span style='color: red'>Please enter your E-mail</span>";
      $("#lemailerr").html($errormsg)
        $('#adminEmail').focus();
        return false;
    }else if(adminEmail.trim() != '' && !reg.test(adminEmail)){
         $errormsg = "<span style='color: red'>Please enter valid email</span>";
      $("#lemailerr").html($errormsg)
        $('#adminEmail').focus();
        return false;
    }else if(adminPass.trim() == '' ){
        //$(".lpasserr").html('Please enter your Password');
        $errormsg = "<span style='color: red'>Please enter your Password</span>";
      $("#lpasserr").html($errormsg)
        $('#adminPass').focus();
        return false;
    }
    else{
        $.ajax({
            type:'POST',
            url:'<?= base_url();?>subscriberLogin', 
            dataType: "json",
            data: {
                  adminEmail: adminEmail,
                  adminPass: adminPass
              },
            success:function(msg){
                 if(msg.data!='' || msg.status=='true' ){
                   window.location.href='<?= base_url()?>subscriber-dashboard';
                  /*if(msg.data.admin_role_id == 1  ){
                    alert('1');
                    window.location.href='<?//= base_url()?>crew-dashboard/'+msg.data.userId;

                  } else if(msg.data.admin_role_id == 2){
                     window.location.href='<?//= base_url()?>owner-dashboard/'+msg.data.userId;
                  }*/
                }
              else{
                    $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span>'+msg.message+'</div>');
                  }
            }
        });
    }
}
</script>

  </body>
</html>