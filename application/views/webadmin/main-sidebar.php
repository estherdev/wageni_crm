<aside class="main-sidebar">
    <section class="sidebar">
       <div class="user-panel">
         <div class="pull-left image">
            <?php if($this->session->userdata('userfile')== false) {?>
            <img src="<?= base_url();?>globalassets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            <?php }else { ?>
            <img src="<?= base_url();?>uploads/<?= $this->session->userdata('userfile')?>" class="img-circle" alt="User Image"/>
            <?php } ?>
         </div>
         <div class="pull-left info">
            <p><?= ucfirst($this->session->userdata('fname'));?></p>
            <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> <?= ucfirst($this->session->userdata('category_name'));?></a>
         </div>
      </div>
      </form>
       <?php if($this->session->userdata('sub_login_id') && $this->session->userdata('userrole')== 1 ) { ?>
      <ul class="mainmenu sidebar-menu">
         <li class="header">MAIN NAVIGATION</li>
         <li class="treeview">
            <a href="<?= base_url('dashboard')?>">
               <i class="fa fa-dashboard"></i> 
               <span><?= $this->lang->line('DASHBOARD');?></span> 
               <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
         <li class="treeview">
             <a href="<?= base_url('sub-admin-view-feedback');?>">
               <i class="fa fa-comments"></i>
                <span>Feedback </span> 
               <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
         <li class="treeview">
            <a href="<?= base_url('add-user-role');?>">
               <i class="fa fa-user"></i> 
               <span>Add Role</span> 
               <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
         <li class="treeview">
            <a href="<?= base_url('view-branch');?>">
               <i class="fa fa-sitemap"></i> 
               <span>Add Branch</span> 
               <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
         <li class="treeview">
            <a href="<?= base_url('add-subscriber-employee');?>">
               <i class="fa fa-user"></i> <span>Add User</span>
                <i class="fa fa-angle-left pull-right"></i>
             </a>
         </li>
          <li class="treeview">
            <a href="<?= base_url('sub-admin-view-all-questions-list');?>">
               <i class="fa fa-file-text"></i> 
               <span>Add Survey Questions List</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
         <li class="treeview">
            <a href="<?= base_url('guestuser');?>">
               <i class="fa fa-hotel"></i> 
               <span>Add Guest</span> 
               <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
         <li class="treeview">
            <a href="javascript:void(0)"><i class="fa fa-file-text"></i>
             <span>Survey</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
               <li>
                  <a href="<?= base_url('create-survey');?>"><i class="fa fa-circle-o"></i> Add Survey</a>
               </li>
               <li>
                  <a href="<?= base_url('admin-allQueByCat');?>"><i class="fa fa-circle-o"></i> View Survey</a>
               </li>
            </ul>
         </li>
      </ul>

      <?php }elseif($this->session->userdata('sub_login_id') && $this->session->userdata('userrole')== 2 ){ ?>

      <ul class="sidebar-menu">
         <li class="header">MAIN NAVIGATION</li>
         <li class="treeview">
            <a href="javascript:void(0)">
               <i class="fa fa-dashboard"></i> 
                  <span><?= $this->lang->line('DASHBOARD');?></span>
               <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
         <li class="treeview">
            <a href="<?= base_url('gernal-manger-guest-user-feedback');?>">
            <i class="fa fa-comments"></i> 
               <span>Feedback </span> 
               <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
      </ul>

      <?php }elseif($this->session->userdata('sub_login_id') && $this->session->userdata('userrole')== 3 ){ ?>

      <ul class="sidebar-menu">
         <li class="header">MAIN NAVIGATION</li>
         <li class="treeview">
            <a href="javascript:void(0)">
               <i class="fa fa-dashboard"></i> 
               <span><?= $this->lang->line('DASHBOARD');?></span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
         <li class="treeview">
            <a href="<?= base_url('supervisior-guest-user-feedback');?>">
               <i class="fa fa-comments"></i> 
               <span>Feedback </span> 
               <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
      </ul>

      <?php }elseif($this->session->userdata('sub_login_id') && $this->session->userdata('userrole')== 4 ){ ?>

      <ul class="sidebar-menu">
         <li class="header">MAIN NAVIGATION</li>
         <li class="treeview">
            <a href="javascript:void(0)"><i class="fa fa-dashboard"></i>
             <span><?= $this->lang->line('DASHBOARD');?></span> 
             <i class="fa fa-angle-left pull-right"></i>
          </a>
         </li>
         <li class="treeview">
            <a href="<?= base_url('gernal-manger-guest-user-feedback');?>">
               <i class="fa fa-comments"></i> 
               <span>Feedback </span> 
               <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
      </ul>

      <?php }else{ redirect(base_url()); } ?>

   </section>
 </aside>