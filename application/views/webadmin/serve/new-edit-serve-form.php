<!--header Section Start Here -->
<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Survey
      </h1>
      <ol class="breadcrumb">
         <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
         <li class="active">Edit Survey</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
         <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
         <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title">Survey Update</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->                       
               <form method="post" id="editsurvey" action="<?= base_url('website/SubsControler/updateSubQuestionnaire/'.$allquestions[0]['bus_qid']);?>" enctype="multipart/form-data">
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-12">
                           <label for="Question">Question</label>
                           <input type="text" class="form-control surveyQue" value="<?=$allquestions[0]['bus_q_que'];?>" name="surveyQue" id="surveyQue">
                           <?= form_error('surveyQue');?>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-md-12">
                           <div class="answer-q">
                              <div class="field_wrapper">
                        
                                 <?php 
                                   // echo "<pre/>";print_r($allanswer);die;
                                     for($i=0;$i<count($allanswer); $i++) {?>
                                 <!-- <?php //echo"<pre/>";print_r($allanswer[$i]['que_score']);die;?> -->
                                       <div class="input-dropbox">
                                 <div class="input-box">
                                 <input type="text" class="form-control" name="field_name[]" value="<?= $allanswer[$i]['bus_que_type']; ?>"/ placeholder="Enter an answer choice..."></div>
                                 <div class="dropbox"> <!-- <select name="q_score" class="form-control">
                                 <option value="<?= $allanswer[$i]['bus_que_score']; ?>"><?= $allanswer[$i]['bus_que_score']; ?></option>
                              </select> -->
                              <select name="q_score[]" class="form-control">
                            <?php 
                                    //$this->load->helper('wageni');
                            $score = getScore();
                            foreach ($score as $value) {?>
                            <option value="<?= $value['score_id']; ?>" 
                            <?php if($allanswer[$i]['bus_que_score'] == $value['score_id']){echo "selected=selected";}?>><?php echo $value['score_name'];?></option><?php  }?>
                              </select>
                              </div>
                               <div class="help add-remove"><a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus-circle"></i></a></div> </div>
                               <?php } ?>                              
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                     <button type="submit" class="btn btn-success">Update</button>
                     <a href="<?= base_url('admin-QuesAns');?>" class="btn btn-danger" role="button">Back</a>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.content-wrapper -->
   </section>
   <!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/webadmin/footer.php';?>
<script type="text/javascript">
   $(document).ready(function(){

        var Select=' -- Select -- ';
        var Poor='Poor';
        var Fair='Fair';
        var Good='Good';
        var Excellent='Excellent';

        var placeholder='Enter an answer choice...';
        var fafaiconplus='<i class="fa fa-plus-circle"></i>';
        var fafaiconminus='<i class="fa fa-minus-circle"></i>';
 
        var dropdown ='';

          //dropdown +='<option>'+Select+'</option>';
          dropdown +='<option value="1">'+Poor+'</option>';
          dropdown +='<option value="2">'+Fair+'</option>';
          dropdown +='<option value="3">'+Good+'</option>';
          dropdown +='<option value="4">'+Excellent+'</option>';

       var maxField = 5; //Input fields increment limitation
       var addButton = $('.add_button'); //Add button selector
       var wrapper = $('.field_wrapper'); //Input field wrapper
      // var fieldHTML = ' <div class="input-dropbox"> <div class="input-box"><input type="text" name="field_name[]" class="form-control" value=""/></div> <div class="dropbox"><select name="q_score" class="form-control"><option value=""></option></select></div> <div class="help"><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus-circle"></i></a></div></div>'; //New input field html 
       var x = 1; //Initial field counter is 1
       
       //Once add button is clicked
    /*   $(addButton).click(function(){
           //Check maximum number of input fields
           if(x < maxField){ 
               x++; //Increment field counter
               $(wrapper).append(fieldHTML);  
           }
       });*/




          $(addButton).click(function(){
             
              if(x < maxField){ 
                var DOMHTML='';               
                  x++; 


                  DOMHTML +='<div class="input-dropbox"> ';
                  DOMHTML +='<div class="input-box">';
                  DOMHTML +=' <input type="text" name="field_name[]" class="form-control" value="" placeholder="'+placeholder+'"/>';
                  DOMHTML +='</div> ';
                  DOMHTML +=' <div class="dropbox">';
                  DOMHTML +=' <select name="q_score" class="form-control">';
                  DOMHTML += dropdown;    
                  DOMHTML +=' </select>';
                  DOMHTML +=' </div> ';
                  DOMHTML +=' <div class="help add-remove">';
                  DOMHTML +=' <a href="javascript:void(0);" class="remove_button">'+fafaiconminus+'</i></a>';
                  DOMHTML +=' </div>';
                  DOMHTML +=' </div>';
                  $(wrapper).append(DOMHTML); 
              }
          });



       
       //Once remove button is clicked
       $(wrapper).on('click', '.remove_button', function(e){
           e.preventDefault();

           $(this).closest('.input-dropbox').remove(); //Remove field html
           x--; //Decrement field counter
       });
   });
</script>