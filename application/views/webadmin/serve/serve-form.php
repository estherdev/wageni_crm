 <?php require APPPATH . 'views/webadmin/header.php';?>
  <?php require APPPATH . 'views/webadmin/main-sidebar.php';?>
 <div class="content-wrapper">
   <section class="content-header">
    <h1>
     Survey
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add Survey</li>
    </ol>
  </section>
   <section class="content">
    <div class="row">
       <div class="col-md-12 ">
         <div class="box ">
          <div class="box-header">
            <h3 class="box-title">Survey</h3>
          </div> 
          <div class="box-body">
            <div id="viewAllQue" class="viewAllQue" >
            </div>
          	<div id="messageResponse" class="messageResponse bg-success" >
       		</div>
       		<form id="nextQues" class="nextQues" method="post">
           <div class="qustio-box">
            <div class="row form-group">
              <div class="rate-select">
               <!--  <div class="col-md-4">
                 <label>Category</label>
                  <select class="form-control cat" name="cat" id="cat"  >
                    <option value="none">Select Category</option>
                    <?//= form_error('cat')?>
                     <?php
                  //foreach($category as $cat)
                 // {
                  // echo '<option value="'.$cat->cat_id.'">'.$cat->category_name.'</option>';
                  //}
                  ?>
                  </select>
                </div> -->
                <div class="col-md-6">
                  <label>Sub Category</label>
                  <?= form_error('subcat')?>
                  <select class="form-control subcat" name="subcat" id="subcat" >
                  <option value="">Select Sub Category</option>
                  </select><span class="subcatn" style="color:red;"></span>
                </div>

                <div class="col-md-6">
                  <label>Process</label>
                  <?= form_error('subsubcat')?>
                  <select class="form-control subsubcat" name="subsubcat" id="subsubcat" >
                  <option value="">Select Process</option>
                  </select><span class="subsubcatn" style="color:red;"></span>
                </div>
              </div>
            </div> 
            <hr/>
            <div class="input-dropbox"> 

            <div class="qan">Que</div>
            <div class="input-box">
              	<input type="text" class="form-control surveyQue" name="surveyQue" id="surveyQue" placeholder="Enter your question...">
              	<span class="surveyQuen" style="color:red;"></span>
            </div>
            <input type="hidden" name="q_type" id="q_type" class="q_type">
            <div class="dropbox">
            <a><span class="selectedli" id="selectedli">Multiple Choice</span><i class="fa fa-angle-down"></i></a>
            <div class="dropdown-box">
                <ul id="selectdropdownquestion" class="selectdropdownquestion" >
	                <li class="selectli" getlivalue="Multiple Choice">
	                  <a data-action="MultipleChoiceQuestion" class="select-li" id="qmc" data-help="qmc" href="javascript:void(0)"><i class="fa fa-list select-menu-list"></i>Multiple Choice</a>
	                </li>
	                <li class="selectli" getlivalue="Dropdown">
	                  <a data-action="DropdownQuestion" class="select-li" id="qdd" data-help="qdd" href="javascript:void(0)"><i class="fa fa-sort select-menu-list"></i>Dropdown</a>
	                </li>
	                <li class="selectli" getlivalue="Checkboxes">
	                  <a data-action="CheckboxQuestion" class="select-li" id="qchb" data-help="qchb" href="javascript:void(0)"><i class="fa fa-check-square-o select-menu-list"></i>Checkboxes</a>
	                </li>
	                <li class="selectli" getlivalue="Star Rating">
	                  <a data-action="StarRatingQuestion" class="select-li" id="qsr" data-help="qsr" href="javascript:void(0)"><i class="fa fa-star select-menu-list"></i>Star Rating</a>
	                </li>
	         <!--        <li class="selectli" getlivalue="Single Textbox">
	                  <a data-action="SingleTextboxQuestion" class="select-li" id="qst" data-help="qst" href="javascript:void(0)"><i class="fa fa-square-o select-menu-list"></i>Single Textbox</a>
	                </li>
	                <li class="selectli" getlivalue="Multiple Textboxes">
	                  <a data-action="MultipleTextboxQuestion" class="select-li" id="qmt" data-help="qmt" href="javascript:void(0)"><i class="fa fa-bars select-menu-list"></i>Multiple Textboxes</a>
	                </li> -->
                </ul>
            </div>
            </div>
           <div class="help"><i class="fa fa-exclamation-circle"></i></div>
         </div>
       </div> 
       <div id="selectedInputLi" class="selectedInputLi" >
       </div>
       <img src="<?= base_url();?>uploads/loader.gif" id="gif" style="display:none">
       </form>
   </div>
 </div>

</div>
</div>   
</section>
</div>
<?php require APPPATH . 'views/webadmin/footer.php';?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.min.js"></script>
<script>
  $(document).ready(function() {
   $('#selectdropdownquestion li a').click(function() {       
        var len= $('.answer-q').length;

        if(len >=1){
        	$('.answer-q').remove();
        }

        var selectInputLi = $(this).attr('data-action');
        var selectQueType = $(this).attr('data-help');

        var maxField = 5;
		var x = 1; 
        
        var Select=' -- Select -- ';
        var Poor='Poor';
        var Fair='Fair';
        var Good='Good';
        var Excellent='Excellent';

        var placeholder='Enter an answer choice...';
        var fafaiconplus='<i class="fa fa-plus-circle"></i>';
        var fafaiconminus='<i class="fa fa-minus-circle"></i>';
 
    	var dropdown ='';

	        dropdown +='<option>'+Select+'</option>';
	        dropdown +='<option value="1">'+Poor+'</option>';
	        dropdown +='<option value="2">'+Fair+'</option>';
	        dropdown +='<option value="3">'+Good+'</option>';
	        dropdown +='<option value="4">'+Excellent+'</option>';
 
        $("#q_type").val(selectQueType);

        switch (selectInputLi) {

        case "MultipleChoiceQuestion":
		       
		        var html='';
		        var i;	
		        html +='<div class="answer-q" >';
		        html +='<div class="anwer-body field_wrapperMultipleChoiceQuestion">';
		        for (i = 0; i < 4; i++) {		        	
				    html +='<div class="form-group form-row">';
			        html +='<div class="form-radio">';
			        html +='<input type="radio" id="test'+i+'" disabled=""><label for="test'+i+'"></label></div>';
			        html +='<div class="input-box">';
			        html +='<input type="text" name="qmc[]" class="form-control" placeholder="'+placeholder+'"></div>';
			        html +='<div class="input-box">';
			        html +='<select name="q_score[]" class="form-control">';
			        html += dropdown;
			        html +='</select>';
			        html +='</div>';
			        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleChoiceQuestion">'+fafaiconplus+'</a></div>';
			        html +='</div>';
				}
		        html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div> </div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);				    
			    $('.add_buttonMultipleChoiceQuestion').click(function(){			        
			        if(x < maxField){ 
			        	var DOMHTML='';			        	
			            x++; 
					        DOMHTML +='<div class="addQ" >';
					        DOMHTML +='<div class="form-group form-row">';
					        DOMHTML +='<div class="form-radio">';
					        DOMHTML +='<input type="radio" id="test4" disabled="">';
					        DOMHTML +='<label for="test4"></label></div>';
					        DOMHTML +='<div class="input-box">';
					        DOMHTML +='<input type="text" name="qmc[]" class="form-control"  placeholder="'+placeholder+'"></div>';
					        DOMHTML +='<div class="input-box"><select name="q_score[]" class="form-control">';
					        DOMHTML += dropdown;				       
					        DOMHTML +='</select></div>';
					        DOMHTML +='<div class="add-remove">';
				            DOMHTML += '<a href="javascript:void(0)" class="remove_buttonMultipleChoiceQuestion">'+fafaiconminus+'</a>';
				            DOMHTML += '</div>';
				            DOMHTML += '</div>';
				            DOMHTML += '</div>';
			            $('.field_wrapperMultipleChoiceQuestion').append(DOMHTML); 
			        }
			    });	
			    $('.field_wrapperMultipleChoiceQuestion').on('click', '.remove_buttonMultipleChoiceQuestion', function(e){			    	
			        e.preventDefault();
			        $(this).parent('div').parent('div').parent('div').remove(); 
			        x--; 
			    });
		        $("#surveyQue").focus();
            break;

        case "DropdownQuestion":
		        
		        var html='';
		        html +='<div class="answer-q">';
		        html +='<div class="anwer-body field_wrapperDropdownQuestion">';
		         for (i = 0; i < 4; i++) {
			        html +='<div class="form-group form-row">';
			        html +='<div class="input-box"><input type="text" class="form-control" name="qdd[]"  placeholder="'+placeholder+'"></div>';
			        html +='<div class="input-box"><select name="q_score[]" class="form-control">';
			        html += dropdown;
			        html += '</select></div>';
			        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonDropdownQuestion">'+fafaiconplus+'</a></div>';
			        html +='</div>';
		    	}
		        html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $('.add_buttonDropdownQuestion').click(function(){			        
			        if(x < maxField){ 
			        	var DOMHTML='';			        	
			            x++;  
							DOMHTML +='<div class="addQ">';
							DOMHTML +='<div class="form-group form-row">';
							DOMHTML +='<div class="input-box">';
							DOMHTML +='<input type="text" class="form-control" name="qdd[]"  placeholder="'+placeholder+'"></div>';
							DOMHTML +='<div class="input-box"><select name="q_score[]" class="form-control">';
							DOMHTML += dropdown;
							DOMHTML +='</select></div>';
							DOMHTML +='<div class="add-remove">';
							DOMHTML +='<a href="javascript:void(0)" class="remove_buttonDropdownQuestion">'+fafaiconminus+'</a>';
				            DOMHTML += '</div>';
				            DOMHTML += '</div>';
				            $('.field_wrapperDropdownQuestion').append(DOMHTML); 
			        }
			    });			   
			    $('.field_wrapperDropdownQuestion').on('click', '.remove_buttonDropdownQuestion', function(e){			    	
			        e.preventDefault();
			        $(this).parent('div').parent('div').parent('div').remove(); 
			        x--; 
			    });
		        $("#surveyQue").focus();
            break;
              
          case "CheckboxQuestion":
		        var html='';
		        html +='<div class="answer-q">';
		        html +='<div class="anwer-body field_wrapperCheckboxQuestion">';		        
		        for (i = 0; i < 4; i++) {
			        html +='<div class="form-group form-row">';
			        html +='<div class="form-checkbox"><input type="checkbox" id="test4" disabled=""><label for="test4"></label></div>';
			        html +='<div class="input-box"><input type="text" class="form-control" placeholder="'+placeholder+'" name="qchb[]" ></div>';
			        html +='<div class="input-box"><select name="q_score[]" class="form-control">';
			        html += dropdown;
			        html +='</select></div>';
			        html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonCheckboxQuestion">'+fafaiconplus+'</a></div>';
			        html +='</div>';
				}
		        html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div></div>';
		        html +='<div class=""><input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);

		        $('.add_buttonCheckboxQuestion').click(function(){			        
			        if(x < maxField){ 
			        	var DOMHTML='';
			            x++; 				        	 
				        	DOMHTML += '<div class="addQ">';
				        	DOMHTML += '<div class="form-group form-row">';
				        	DOMHTML += '<div class="form-checkbox">';
				        	DOMHTML += '<input type="checkbox" id="test'+x+'" disabled="">';
				        	DOMHTML += '<label for="test'+x+'"></label></div>';
				        	DOMHTML += '<div class="input-box">';
				        	DOMHTML += '<input type="text" class="form-control" name="qdd[]"  placeholder="'+placeholder+'"></div>';
				        	DOMHTML += '<div class="input-box"><select name="q_score[]" class="form-control">';
				        	DOMHTML += dropdown;
				        	DOMHTML += '</select></div>';
				        	DOMHTML += '<div class="add-remove">';
				            DOMHTML += '<a href="javascript:void(0)" class="remove_buttonCheckboxQuestion">'+fafaiconminus+'</a>';
				            DOMHTML += '</div>';
				            DOMHTML += '</div>';			
			            $('.field_wrapperCheckboxQuestion').append(DOMHTML); 
			        }
			    });
			    
			   
			    $('.field_wrapperCheckboxQuestion').on('click', '.remove_buttonCheckboxQuestion', function(e){			    	
			        e.preventDefault();
			        $(this).parent('div').parent('div').parent('div').remove(); 
			        x--; 
			    });

		        $("#surveyQue").focus();

            break;
              
          case "StarRatingQuestion":
		        var html='';
		        html +='<div class="answer-q">';        
		        html +='<div class="rating-box">';        
		        html +='<input type="hidden" class="form-control starno" name="qsr" id="starno" value="5" />';
		        html +='<div class="rating-foot"><div></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $("#surveyQue").focus();

            break;
              
          case "SingleTextboxQuestion":

		        var html='';
				html +='<div class="answer-q">';
		        html +='<div class="answer-foot">';
		        html +='<div></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $("#surveyQue").focus();

            break;
              
        case "MultipleTextboxQuestion":
		        var html='';
				html +='<div class="answer-q">';  
				html +='<div class="anwer-body add-label field_wrapperMultipleTextboxQuestion">';
				for (i = 0; i < 4; i++) {
					html +='<div class="form-group form-row">'; 
					html +='<div class="input-box"><input type="text" class="form-control"  placeholder="'+placeholder+'"  name="qmt[]"></div>';
			        html +='<div class="input-box"><select name="q_score[]" class="form-control">';
			        html += dropdown;
			        html +='</select></div>';
					html +='<div class="add-remove"><a href="javascript:void(0)" class="add_buttonMultipleTextboxQuestion">'+fafaiconplus+'</a></div>';
					html +='</div>';
				}
				html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div></div>';
		        html +='<div class=""><input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        
		        $("#selectedInputLi").append(html);
  				
		            $('.add_buttonMultipleTextboxQuestion').click(function(){			        
			        if(x < maxField){ 			        	
			         	var DOMHTML='';
			            x++; 
				            DOMHTML +='<div class="addQ" >';
				            DOMHTML +='<div class="form-group form-row"><div class="input-box">';
				            DOMHTML +='<input type="text" class="form-control" name="qdd[]" value="Enter an answer choice"></div>';
				            DOMHTML +='<div class="input-box"><select name="q_score[]" class="form-control">';
				        	DOMHTML += dropdown;
				        	DOMHTML += '</select></div>';
				            DOMHTML += '<div class="add-remove">';
				            DOMHTML += '<a href="javascript:void(0)" class="remove_buttonMultipleTextboxQuestion">'+fafaiconminus+'</a>';
				            DOMHTML += '</div>';
				            DOMHTML += '</div>';
			            $('.field_wrapperMultipleTextboxQuestion').append(DOMHTML); 			          
			        }
			    });
			    
			   
			    $('.field_wrapperMultipleTextboxQuestion').on('click', '.remove_buttonMultipleTextboxQuestion', function(e){			    	
			        e.preventDefault();
			        $(this).parent('div').parent('div').parent('div').remove(); 
			        x--; 
			    });
		        $("#surveyQue").focus();

            break;

          default:
              	text = "No value found";  
        //var surveyQue = $('#selectedli').val(); 
      }
		/* var str = document.getElementById("selectedli").innerHTML; 
		var res = str.replace("Multiple Choice",selectInputLi);
		document.getElementById("selectedli").innerHTML = res;*/
		//$("#selectedli").html($("#selectedli").html().replace(selectInputLi));
		//$("#selectedli").append('<span class="selectedli">'+selectInputLi+'</span>');
   });
  });
</script>
<script>
    $('form').on('submit', function(e) {
        e.preventDefault();       
      	//var json = $(this).serializeJSON();
		//console.log(json);
		$.ajax({
            dataType: 'html',
            type: 'post',
            url: '<?= base_url('admin-questionSubmit');?>',
            data: $('#nextQues').serialize(),
            //beforeSubmit: validator,
            success: function(responseData) {
            		var obj = $.parseJSON(responseData);
        		  if(obj.status == '1'){
		              flash( obj.message,{
		                'bgColor' : 'Green'
		              });
			              $("form").trigger("reset");
		            }else{
		            	if(obj.message == 'error'){
	            			flash( obj.message,{
			                  'bgColor' : 'Red'
			                });
	            		 	var errorlist = obj.values;
			                $.each(errorlist, function (index, value) {
			                   	$("."+index).css('border', '1px solid red');
    							$("."+index+"n").text(value);
			                });
		            	}else{
        	 				flash( obj.message,{
			                  'bgColor' : 'Red'
			                });
		            	}
		            }       
            	},          
        	});        
        });

	// $(document).ready(function(){
	var cat_id = '<?= $this->session->userdata('category');?>';
	//$('#cat').change(function(){
	// var cat_id = $('#cat').val();
    
    if(cat_id>0){
		$.ajax({
		url:"<?php echo base_url('website/SubsControler/fetch_subcategory');?>",
		method:"POST",
		data:{cat_id:cat_id},
		success:function(data){
				$('#subcat').html(data);
				$('#subsubcat').html('<option value="">Select Process</option>');
			}
		});
	} else	{
		$('#subcat').html('<option value="">Select Sub category</option>');
		$('#subsubcat').html('<option value="">Select Process</option>');
	}
   // });
   
	$('#subcat').change(function(){
		var sub_cat_id = $('#subcat').val();
			if(sub_cat_id>0){
				$.ajax({
				url:"<?php echo base_url('website/SubsControler/fetch_subsubcat');?>",
				method:"POST",
				data:{sub_cat_id:sub_cat_id},
				success:function(data){
						$('#subsubcat').html(data);
					}
				});
			}else{
				$('#subsubcat').html('<option value="">Select Process</option>');
			}
		});
 
		$("ul.selectdropdownquestion").on("click", "li", function() {
		var getlivalue = $(this).attr("getlivalue");         
			$("#selectedli").text( getlivalue );
		});

</script>