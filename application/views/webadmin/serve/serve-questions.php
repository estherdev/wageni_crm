<!--header Section Start Here -->
<?php require APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php require APPPATH . 'views/webadmin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page --><head>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">  
 </head>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     All Question
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">All Question</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12 ">
        <!-- general form elements -->
        <div class="box ">
          <div class="box-header">
            <h3 class="box-title">Question</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form action="<?= base_url('admin-QuesAns')?>" method="post">
          <div class="box-body">
             
            <div class="rating-box">
            <div class="rate-select">
           <!--  <div class="col-md-4">
           <label>Category</label>
           <select class="form-control cat" name="cat" id="cat"  >
                    <option value="none">Select Category</option>
                     <?php
                  //foreach($category as $cat){//echo '<option value="'.$cat->cat_id.'">'.$cat->category_name.'</option>';  }
                  ?>
            </select>
            </div> -->
            <div class="col-md-6">
            <label>Sub Category</label>
            <select class="form-control scat" name="subcat" id="subcat" >
            <option value="">Select Sub Category</option>
            </select>
            </div>  

            <div class="col-md-6">
            <label>Process</label>
             <select class="form-control scat" name="subsubcat" id="subsubcat" >
             <option value="">Select Process</option>
            </select>
            </div>
            </div>
            <div class="rating-foot">
            <div class="text-center">
                <input type="submit" class="btn btn-success searchQues" value="Submit"></div>
            </div>
            </div>
        
          </div>
          </form>
       </div> 
   </div>
 </div><!-- /.box -->
 <!-- general form elements -->

</div>
</div>   
</section><!-- /.content -->
</div>
      <!-- <script type="text/javascript">
         $(document).ready(function() {
             $('select[name="category"]').on('change', function() {
                 var categoryId = $(this).val();
                 //alert(categoryId);
                 if(categoryId) {
                     $.ajax({
                         url: 'subcatid/'+categoryId,
                         type: "GET",
                         dataType: "json",
                         success:function(data) {
                             $('select[name="subcategory"]').empty();
                             $.each(data, function(key, value) {
                                 $('select[name="subcategory"]').append('<option value="'+ value.sub_cat_id +'">'+ value.sub_cat_name +'</option>');
                             });
                         }
                     });
                 }else{
                     $('select[name="subcategory"]').empty();
                 }
             });
         });
      </script> -->


      <script>
   $(document).ready(function(){

    
     var cat_id = '<?= $this->session->userdata('category');?>';
    
     if(cat_id != '')
     {
      $.ajax({
       url:"<?php echo base_url(); ?>website/SubsControler/fetch_subcategory",
       method:"POST",
       data:{cat_id:cat_id},
       success:function(data)
       {
        $('#subcat').html(data);
        $('#subsubcat').html('<option value="">Select Process</option>');
       }
      });
     }
     else
     {
      $('#subcat').html('<option value="">Select Sub Category</option>');
      $('#subsubcat').html('<option value="">Select Process</option>');
     }
   

   
    $('#subcat').change(function(){
     var sub_cat_id = $('#subcat').val();
     if(sub_cat_id != '')
     {
      $.ajax({
       url:"<?php echo base_url(); ?>website/SubsControler/fetch_subsubcat",
       method:"POST",
       data:{sub_cat_id:sub_cat_id},
       success:function(data)
       {
        $('#subsubcat').html(data);
       }
      });
     }
     else
     {
      $('#subsubcat').html('<option value="">Select Process</option>');
     }
    });
    
   });
</script>
<!--footer Section Start Here -->     
<?php require APPPATH . 'views/webadmin/footer.php';?>
<!--footer Section End Here -->
