<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Company
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add Company</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Company Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="<?= base_url('add-company');?>" method="post">
            <div class="box-body">
              <div class="form-group row">

                <div class="col-md-6">
                  <label for="select-subscriber">Subscriber</label>
                  <select type="email" class="form-control" id="select-subscriber"  name="subscribername" >
                    <option value="none">--Select Subscriber--</option>
                    <?php foreach ($company as $key => $value) {?>
                       <option value="<?= $value->sub_login_id;?>"><?= ucfirst($value->sub_name);?></option>
                    <?php } ?>    
                  </select>
                  <?= form_error('subscribername')?>            
                </div>
                <div class="col-md-6">
                  <label for="select-package">Package</label>
                  <select type="email" class="form-control" id="select-package"  name="packagename" >
                    <option value="none">--Select Package--</option>
                    <?php foreach ($package as $key => $value) {?>
                       <option value="<?= $value->package_id;?>"><?= ucfirst($value->package_name);?></option>
                    <?php } ?> 
                  </select>
                   <?= form_error('packagename')?>   
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.content-wrapper -->
    </div>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Packages</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Company Name</th>
                    <th>Package</th>
                    <th>Price</th>
                    <th>Registration Date</th>
                    <th>Validity Expire</th>
                    <!-- <th>Action</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php $i= 1; foreach ($viewcompany as $key => $pack) {?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= ucfirst($pack->sub_name); ?></td>
                    <td><?= ucfirst($pack->package_name); ?></td>
                    <td><?= ucfirst($pack->package_price); ?>.00</td>
                    <td><?= $pack->subscriber_start_date; ?></td>
                    <td><?= $pack->subscriber_end_date; ?></td>
                     <td class="text-center">
                       <!--  <a class="btn btn-sm btn-info" href="#" title="Edit"><i class="fa fa-pencil"></i></a> -->
                        <!-- <a class="btn btn-sm btn-danger deletepack"  data-toggle="modal" data-target="#deleteModal<?//= $pack->spm_id ; ?>" title="Delete"><i class="fa fa-trash"></i></a> -->
                      </td>
                      <div id="deleteModal<?= $pack->spm_id ; ?>" class="modal fade">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        <!-- dialog body -->
                        <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                         Are You Sure to <strong>Company</strong> Delete?
                        </div>
                        <!-- dialog buttons -->
                        <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='#';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                        </div>
                        </div>
                      </div>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    <!-- /.content-wrapper -->
  </div>

  <!-- /.content-wrapper -->
</section><!-- /.content -->
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>