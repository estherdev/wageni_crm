<!--header Section Start Here -->
<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add  Score
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add Score </li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Score Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
            <form method="post" action="<?= base_url('website/Score');?>">

              <div class="box-body">
              <div class="form-group row">

                <div class="col-md-4">
                  <label for="title">Score Name</label>
                  <input type="text"  class = "form-control" autofocus ="" name="score_name" />
                  <?= form_error('score_name')?>            
               </div>

              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.content-wrapper -->
    </div>
    <!-- /.content-wrapper -->
  
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><b>Score Details</h3></b>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Hotel Name</th>
                
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i= 1; foreach ($allscore as $key => $score) {?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= ucfirst($score->score_name); ?></td>                         
                    <td class="text-center">
                            <a class="btn btn-sm btn-info" href="<?php echo base_url("website/Score/editScoreData/".$score->score_id);?>" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-sm btn-danger deleteSlider"  data-toggle="modal" data-target="#deleteModal<?= $score->score_id?>" title="Delete"><i class="fa fa-trash"></i></a>
                    </td>
                            <div id="deleteModal<?= $score->score_id?>" class="modal fade">
                            <div class="modal-dialog">
                            <div class="modal-content">
                            <!-- dialog body -->
                              <div class="modal-body">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              Are You Sure to <strong>Score</strong> Delete?
                              </div>
                            <!-- dialog buttons -->
                            <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('website/Score/deleteScore/'.$score->score_id); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                            </div>
                            </div>
                            </div>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->

  <!-- /.content-wrapper -->
</section><!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/webadmin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>