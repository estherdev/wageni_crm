<!--header Section Start Here -->
<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit Guest User
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Edit Guest User </li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Edit User Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
            <form method="post" id="guest" action="<?= base_url('update-guestuser/');?><?=$records[0]['user_id'];?>">

              <div class="box-body">
              <div class="form-group row">


                  <div class="col-md-4">
                  <label>Hotel Name</label>
                   <select class="form-control subcat" value="<?= $records[0]['user_id'];?>"name="subcat" id="subcat" >
                    
                  </select>
                </div>  
            

                <div class="col-md-4">
                  <label for="user"><?= $this->lang->line('User_Name')?></label>
                  <input type="text" class="form-control"  value="<?= $records[0]['user_name'];?>" name="user_name"  />
                   <?= form_error('user_name')?>   
                </div> 

                <div class="col-md-4">
                  <label for="user"><?= $this->lang->line('User_Mail_Id')?></label>
                  <input type="text" class="form-control"  value="<?= $records[0]['user_email_id'];?>" disabled=""  />
                   <?= form_error('user_id')?>   
                </div>

                 <div class="col-md-4">
                  <label for="content"><?= $this->lang->line('No_Of_Adult')?></label>
                  <input type="text"  class = "form-control" value="<?= $records[0]['no_of_adult'];?>" name="no_of_adult" />
                  <?= form_error('no_of_adult')?>            
                </div>

                <div class="col-md-4">
                  <label for="content"><?= $this->lang->line('No_Of_Children')?></label>
                  <input type="text"  class = "form-control" value="<?= $records[0]['no_of_children'];?>" name="no_of_children" />
                  <?= form_error('no_of_children')?>            
                </div>

                <div class="col-md-4">
                  <label for="content"><?= $this->lang->line('No_Of_Room')?> </label>
                  <input type="text"  class = "form-control" value="<?= $records[0]['no_of_room'];?>" name="no_of_room" />
                  <?= form_error('no_of_room')?>            
                </div>

                <div class="col-md-4">
                  <label for="content"><?= $this->lang->line('Check_In')?></label>
                  <input type="date"  class = "form-control" value="<?= $records[0]['check_in'];?>" name="check_in" />
                  <?= form_error('check_in')?>            
                </div>

                 <div class="col-md-4">
                  <label for="content"><?= $this->lang->line('Check_Out')?></label>
                  <input type="date"  class = "form-control" value="<?= $records[0]['check_out'];?>" name="check_out" />
                  <?= form_error('check_out')?>            
                </div>


              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-success">Update</button>
                 <a href="<?= base_url('website/Feedback');?>" class= "btn btn-danger" role="button">Back</a>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.content-wrapper -->
    </div>
    <!-- /.content-wrapper -->
  </div>
  <!-- /.content-wrapper -->
</section><!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/webadmin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>
  <script>
    $(document).ready(function(){

    var cat_id = '<?= $this->session->userdata('category')?>';
 
        if(cat_id != '')
      {
        $.ajax({
        url:"<?php echo base_url(); ?>website/SubsControler/fetch_subcategory",
        method:"POST",
        data:{cat_id:cat_id},
        success:function(data)
        { 
          $('#subcat').html(data);
        }
    });
  }
    else
  {
    $('#subcat').html('<option value="">Select Sub Category</option>');
  }


  });
</script>
