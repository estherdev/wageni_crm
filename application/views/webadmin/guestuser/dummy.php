
  <!--header Section Start Here -->
<?php include "header.php";?>
<!--header Section End Here -->
<!--slider Section Start Here -->
<div class="contact-page">
<section id="slider" class="banner-two">
   <div class="about-banner">
      <div class="container banner-text">
         <div class="row">
            <div class="col-xs-12">
               <h1>Contact Us</h1>
            </div>
         </div>
      </div>
   </div>
</section>
<!--slider Section End Here -->
<!--content Section Start Here -->
<div id="content">
   <!--contact-form-form Section Start Here -->
   <section class="contact-form-form">
      <div class="container">
         <div id="success" >
            <div role="alert" class="alert alert-success"><strong>Thanks</strong> for using our template. Your message has been sent.</div>
         </div>
         <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-9">
               <h2>Fill form here</h2>
               <form action="<?= base_url('contact-us');?>" method="post">
                 <fieldset>
                  <legend>Name of Your City</legend>
                      <div>
                      <input type="radio" id="agra"name="city" value="agra"  />
                      <label for="agra">Agra</label>
                  </div>
                  <div>
                      <input type="radio" id="mathura" name="city" value="mathura" />
                      <label for="mathura">Mathura</label>
                  </div>
                  <div>
                      <input type="radio" id="aligarh" name="city" value="aligarh" checked/>
                      <label for="aligarh">Aligarh</label>
                  </div>
                              </fieldset>

                               <fieldset>
                  <legend>Howz The Sevice Is</legend>
                  <div>
                      <input type="radio" id="vgood"name="drone" value="vgood"  />
                      <label for="vgood">V.Good</label>
                  </div>

                  <div>
                      <input type="radio" id="Good" name="drone" value="Good" checked/>
                      <label for="Good">Good</label>
                  </div>

                  <div>
                      <input type="radio" id="Average" name="drone" value="Average" />
                      <label for="Average">Average</label>
                  </div>

                     <div>
                      <input type="radio" id="Poor" name="drone" value="Poor" />
                      <label for="Poor">Poor</label>
                  </div>

                   </fieldset>


                  <fieldset>
                  <legend>Name of Your Country</legend>
                      <div>
                      <input type="radio" id="india"name="country" value="india" checked />
                      <label for="india">INDIA</label>
                  </div>
                  <div>
                      <input type="radio" id="usa" name="country" value="usa" />
                      <label for="usa">USA</label>
                  </div>
                  <div>
                      <input type="radio" id="brazil" name="country" value="brazil" />
                      <label for="brazil">BRAZIL</label>
                  </div>
                  </fieldset>              
                
                  <div class="submit-btn">
                     <input type="submit" name="submit" value="submit" class="detail-submit"/>
                  </div>
               </form>
            </div>
          
         </div>
      </div>
   </section>

</div>
</div>
<!--content Section End Here -->
<!--footer Section Start Here -->
<?php include "footer.php";?>
<!--footer Section End Here -->