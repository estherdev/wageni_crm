 <?php include APPPATH . 'views/webadmin/header.php';?>
 
 <?php include APPPATH . 'views/webadmin/main-sidebar.php';?>
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>   Add Guest User   </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add Guest User </li>
    </ol>
  </section>
   <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?> 

    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Guest User Details</h3>
          </div> 
          <div id="message_box"></div>
          <div id="success_message" > </div>
            <form method="post" id="add_guestuser" class="form-content">

              <div class="box-body">
              <div class="form-group row">

                <div class="col-md-3">
                  <label>Branch Name</label><span class="mandatory"> *</span>
                   <select class="form-control subcat" name="subcat" id="subcat" >
                    
                  </select>
                   <span id="subcat_error" class="error"></span>
                </div>               
                <!-- <div class="col-md-4">
                  <label for="hotelname"><?//= $this->lang->line('Hotel_Name')?></label>
                  <input type="text"  class = "form-control" autofocus ="" name="hotel_name" />
                  <?//= form_error('hotel_name')?>            
                </div> -->

                <div class="col-md-3">
                  <label for="username"><?= $this->lang->line('User_Name')?></label><span class="mandatory"> *</span>
                  <input type="text" class="form-control"  name="user_name" id="user_name"   value="<?php echo set_value('user_name');?>"/>
                  <span id="user_name_error" class="error"></span>
                  <!--  <?//= form_error('user_name')?>    -->
                </div>

                <div class="col-md-3">
                  <label for="usermail"><?= $this->lang->line('User_Mail_Id')?></label><span class="mandatory"> *</span>
                  <input type="text" class="form-control"  name="user_id" id="user_id"  value="<?php echo set_value('user_id');?>"/>
                   <span id="user_id_error" class="error"></span>
                 </div>

                <div class="col-md-3">
                  <label for="usermail">Phone</label><span class="mandatory"> *</span>
                  <input type="text" class="form-control"  name="user_phone" id="user_phone"  value="<?php echo set_value('user_phone');?>" />
                   <span id="user_phone_error" class="error"></span>
                 </div>

                </div>

                <div class="form-group row">
                 <div class="col-md-2">
                  <label for="adult"><?= $this->lang->line('No_Of_Adult')?></label><span class="mandatory"> *</span>
                  <input type="text"  class = "form-control" name="no_of_adult" id="no_of_adult" value="<?php echo set_value('no_of_adult');?>" />
                   <span id="no_of_adult_error" class="error"></span>
                 </div>

                <div class="col-md-2">
                  <label for="content"><?= $this->lang->line('No_Of_Children')?></label><span class="mandatory"> *</span>
                  <input type="text"  class = "form-control" name="no_of_children" id="no_of_children" value="<?php echo set_value('no_of_children');?>"/>
                   <span id="no_of_children_error" class="error"></span>
                 </div>

                <div class="col-md-2">
                  <label for="content"><?= $this->lang->line('No_Of_Room')?></label><span class="mandatory"> *</span>
                  <input type="text"  class = "form-control" name="no_of_room" id="no_of_room" value="<?php echo set_value('no_of_room');?>"/>
                   <span id="no_of_room_error" class="error"></span>
                 </div>
                <div class="col-md-2">
                  <label for="content">Room No</label><span class="mandatory"> *</span>
                  <input type="text"  class = "form-control" name="room_no"  id="user_room" value="<?php echo set_value('user_phone');?>" />
                   <span id="user_room_error" class="error"></span>
                 </div>

                <div class="col-md-4">
                  <label>Nationality</label><span class="mandatory"> *</span>
                   <select class="form-control user_nationality" name="user_nationality" id="user_nationality" >
                      <option value="">Select Nationality</option>
                      <?php $countries=getCountries();
                      		foreach ($countries as $key => $countrie) {?>
                     <option value="<?= $countrie['id']?>"><?= $countrie['name']?></option>
                      <?php } ?>
                  </select>
                   <span id="user_nationality_error" class="error"></span>
                </div>  
                </div>
                <div class="form-group row">
                  <div class='col-md-3'>
                    <div class="form-group">
                      <label>Check In:</label><span class="mandatory"> *</span>
                      <input type='text' class="form-control" name="check_in"  id='datetimepicker6'  value="<?php echo set_value('check_in');?>" >
                       <span id="datetimepicker6_error" class="error"></span>

                     </div>
                  </div>              
                  <div class='col-md-3'>
                    <div class="form-group">
                      <label>Check Out:</label><span class="mandatory"> *</span>
                      <input type='text' class="form-control" name="check_out" id='datetimepicker7'  value="<?php echo set_value('check_out');?>" >
                       <span id="datetimepicker7_error" class="error"></span>
                      
                     </div>
                  </div>

<!--                  <div class='col-md-3'>
                    <div class="form-group">
                      <label>Check In Time:</label><span class="mandatory"> *</span>
                      <input type='text' class="form-control timepicker" name="check_in_time" id="check_in_time"  value="<?php echo set_value('check_in_time');?>" >
                       <span id="check_in_time_error" class="error"></span>

                     </div>
                  </div>           
                  <div class='col-md-3'>
                    <div class="form-group">
                      <label>Check Out Time:</label><span class="mandatory"> *</span>
                      <input type='text' class="form-control timepicker" name="check_out_time" id="check_out_time"  value="<?php echo set_value('check_out_time');?>"  >
                       <span id="check_out_time_error" class="error"></span>
                     </div>
                  </div> -->

                   <div class='col-md-3'>
                  <div class="bootstrap-timepicker">
                  	<div class="form-group">
                  		<label>Check In Time:</label>
                  		<div class="input-group">
                  			<input type="text" name="check_in_time" id="check_in_time"  class="form-control timepicker">

                  			<div class="input-group-addon">
                  				<i class="fa fa-clock-o"></i>
                  			</div>
                  		</div>
						<span id="check_in_time_error" class="error"></span>
                  	</div>
                  	</div>                  	
                  </div>

                  
                   <div class='col-md-3'>
                  <div class="bootstrap-timepicker">
                  	<div class="form-group">
                  		<label>Check Out Time:</label>
                  		<div class="input-group">
                  			<input type="text"  name="check_out_time" id="check_out_time"  class="form-control timepicker">

                  			<div class="input-group-addon">
                  				<i class="fa fa-clock-o"></i>
                  			</div>
                  		</div>
                  		 <span id="check_out_time_error" class="error"></span>
                  	</div>
                  	</div>                  	
                  </div> 
                  </div>
                   <div class="form-group row">

                  <div class='col-md-12'>
                    <div class="form-group">
                      <label>Address:</label><span class="mandatory"> *</span>
                      <textarea type="text" class="form-control" name="user_address" id="address"  rows="4" cols="50">  <?php echo set_value('user_address');?> </textarea> 
                       <span id="address_error" class="error"></span>
                     </div>
                  </div>  
              </div>
            </div>
             <div class="box-footer text-center">
                <button type="button" class="btn btn-primary" id="addguest">Submit</button> 
                <button type="reset" class="btn btn-danger">Reset</button>
            </div> 
          </form>
        </div>
       </div>
     </div>
 
       <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
      
             <div class="box box-primary">
               <div class="box-header">   

                <div class="col-md-3">            
                  <select class="form-control" name="change_user" id="change_user">
                    <option value=""> Select User</option>    
                        <?php $scores= getScore();                  
                    foreach ($alluserdata as $score) {?> 
                        <option value="<?= $score['user_id'];?>"><?= ucfirst($score['user_name'])?></option>
                     <?php  } ?>
                   </select>
                </div> 
       <!--          <div class="col-md-3">            
                  <select class="form-control" name="change_status" id="change_status">
                    <option value=""> Select Branch</option>    
                        <?php $ComplaintStatuss= getComplaintStatus(); 
                                  
                    foreach ($alluserdata as $ComplaintStatus) {
                       ?> 
                        <option value="<?=  ($ComplaintStatus['hotel_name']);?>"><?= ucfirst($ComplaintStatus['sub_cat_name'])?></option>
                     <?php  } ?>
                   </select>
                </div> -->  

               </div>                
            </div>
          </div>
       </div>
  
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title"><b>Guest User Details</h3></b>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive">

            <table id="guestuser_list" class="table table-bordered table-striped table-hover table-sm text-center text-nowrap">
              <thead>
                <tr>
                  <th>S.No.</th>
                  <th><?= $this->lang->line('User_Name')?></th>
                  <th><?= $this->lang->line('User_Mail_Id')?></th>
                  <th><?= $this->lang->line('No_Of_Adult')?></th>
                  <th><?= $this->lang->line('No_Of_Children')?></th>
                  <th><?= $this->lang->line('No_Of_Room')?></th>
                  <th><?= ('Room No')?></th>
                  <th><?= $this->lang->line('Check_In')?></th>
                  <th><?= $this->lang->line('Check_Out')?></th>
                  <!-- <th>Status</th> -->
                  <th>Pre Stay</th>
                  <th>On Stay</th>
                  <th>Post Stay</th> 
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div> 
        </div> 
      </div> 
    </div>  
 </section> 
</div>
 <?php include APPPATH . 'views/webadmin/footer.php';?> 


 <style type="text/css">
td.details-control { background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center 12px; cursor: pointer; }
tr.details td.details-control { background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center 12px; }
.datatable_loader { text-align: center; }
.datatable_loader img { width: 80px; }
.small_loader { position: relative; height: 20px; width: 100%; overflow: hidden; }
.small_loader img { width: 50px; position: absolute; top: -10px; left: 0; }
</style>

 
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>


<script>
 //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })

  var defaltimageurl='<?= base_url()?>globalassets/admin/dist/img/user2-160x160.jpg';
  var imageurl='<?= base_url()?>uploads/';
  var base_url='<?= base_url()?>';

   var datatable_loader='<div class="datatable_loader"><img src="'+base_url+'/globalassets/admin/preloader1.gif"></div>';
   var small_loader='<div class="small_loader"><img src="'+base_url+'/globalassets/admin/preloader1.gif"></div>';

   $(function(){
   
   var guestuser_list_table = $('#guestuser_list').DataTable({
      "processing": true,
      "pageLength": <?php echo $this->config->item('record_per_page');?>,
      "serverSide": true,
      "sortable": true,
      "lengthMenu": [[10,20, 30, 50,100], [10,20, 30, 50,100]],
      "language": {
      "emptyTable": "<?php echo $this->config->item('record_not_found_title');?>" 
      },
       "order": [
         [1, "desc"]
      ],
      "ajax":{
         url :"<?php echo base_url('sub-admin-view-all-guest-user-ajax')?>", 
         type: "post",   
         data: function (d) {      
              d.user_id = $('#change_user').val();
               d.status_id = $('#change_status').val();
             }      
      } ,
      "columns": [ 
      {data: 'id',"orderable":false},
      {data: 'name',class: 'text-nowrap',"orderable":false},
      {data: 'email',class: 'text-nowrap',"orderable":false},
      {data: 'no_of_adult',class: 'text-nowrap',"orderable":false}, 
      {data: 'no_of_children',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'no_of_room',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'room_no',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'check_in',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'check_out',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'pre_stay',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'on_stay',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'post_stay',class: 'text-center text-nowrap',"orderable":false},       
        
    ],
    
    "columnDefs": [        
          {
              render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1 +'.';
          },          
           "targets": 0 
          }, 
          
          {
              "render": function ( data, type, row ) {
              var html="";
           
              if(row['pre_stay']==1 ){
                   html +=' <span id="loader'+row['id']+row['on_stay_key']+'"></span><a href="javascript:void(0)"  class="btn btn-danger btn-sm mt-1">Done</a>';
              }else{
                  html +=' <span id="loader'+row['id']+'"></span><a href="javascript:void(0)" onClick="sendNotification('+row['id']+','+row['pre_stay_key']+')" id="notification'+row['id']+row['pre_stay_key']+'" class="btn btn-success btn-sm mt-1">Send</a>';
              }
               return html;
 
            },
          
          "targets": 9
         },
          
          {
              "render": function ( data, type, row ) {
              var html="";
           
              if(row['on_stay']==1 ){
                   html +=' <span id="loader'+row['id']+row['on_stay_key']+'"></span><a href="javascript:void(0)"  class="btn btn-danger btn-sm mt-1">Done</a>';
              }else{
                  html +=' <span id="loader'+row['id']+'"></span><a href="javascript:void(0)" onClick="sendNotification('+row['id']+','+row['on_stay_key']+')" id="notification'+row['id']+row['on_stay_key']+'" class="btn btn-success btn-sm mt-1">Send</a>';
              }
               return html;
 
            },
          
          "targets": 10
         },
          
          {
              "render": function ( data, type, row ) {
              var html="";
           
              if(row['post_stay']==1 ){
                   html +=' <span id="loader'+row['id']+row['on_stay_key']+'"></span><a href="javascript:void(0)"  class="btn btn-danger btn-sm mt-1">Done</a>';
              }else{
                  html +=' <span id="loader'+row['id']+'"></span><a href="javascript:void(0)" onClick="sendNotification('+row['id']+','+row['post_stay_key']+')" id="notification'+row['id']+row['post_stay_key']+'" class="btn btn-success btn-sm mt-1">Send</a>';
              }
               return html;
 
            },
          
          "targets": 11
         },
      
               
    
       
          ],
          
 
 
      });

   /*=========================    FILETR    ==================================*/


  $('#change_user,#change_status').change(function(){
      guestuser_list_table.ajax.reload();
     });

 
    

   });
   
/*===========================================================*/
 

function sendNotification(id,status){
  if(id>0){
    $('#notification'+id).hide();
    $('#loader'+id).html(small_loader);
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('sub-admin-send-notification-to-guest')?>',
      data: {'id':id,'status':status},
      success: function(ajaxresponse){
        response=JSON.parse(ajaxresponse);
        if(response['status']){
          $('#success_message').html('<div class="alert alert-success">'+response['message']+'!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        }else{
          $('#success_message').html('<div class="alert alert-danger">'+response['message']+'!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
         }
        $('#notification'+id).show();
        $('#loader'+id).html('');
        setTimeout(function () {
             location.reload()
        }, 100);
        // $('#change_order_status').trigger('change');
      }
    }); 
  }
} 
/*===========================================================*/

  function getComplaintsStatus(status){
    if(status==1){
      return "<span class='btn btn-sm btn-danger'>Pending</span>";
   }else if(status==2){
     return "<span class='btn btn-sm btn-warning'>Ongoing</span>";
   }else if(status==3){
    return "<span class='btn btn-sm btn-success'>Issue has been addressed</span>";
   }else{
  return "<span class='btn btn-sm btn-danger'>Pending</span>";
   }
 }
   
/*===========================================================*/   
  function changeCompanyUsersStatus(id,status){
   if(id>0){
    if(status==1){
      var set_status=0;
    }else{
      var set_status=1;
    }
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-change-company-users-status')?>',
      data: {'id':id,'status':set_status},
      success: function(ajaxresponse){
        response=JSON.parse(ajaxresponse);
        if(response['status']){ 
          if(set_status){
            var html='<a href="javascript:void(0)" onClick="changeCompanyUsersStatus('+id+','+set_status+')"><span class="btn btn-sm btn-success">Active</span></a>';
          }else{
            var html='<a href="javascript:void(0)" onClick="changeCompanyUsersStatus('+id+','+set_status+')"><span class="btn btn-sm btn-danger">In-active</span></a>';
          }
          $('#status'+id).html(html);
          company_list_table.ajax.reload();
        }
      }
    }); 
  }
}
 
 
</script>







 
   <script type="text/javascript">
  function makeTrim(x) {
    if (x) {
      return x.replace(/^\s+|\s+$/gm, '');
    } else {
      return x;
    }
  }


    function validEmail(email) {
    var re = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return re.test(email);
  }
   
 
   
  </script>


<script>


  $('#addguest').click(function(){
   
     $('#message_box').html('');
    $('#success_message').html('');
    $('.form-control').removeClass('error');
    $('.error').html('');
    var errorMessage=[];
    var branch=makeTrim($('#subcat').val());
    var name=makeTrim($('#user_name').val());
    var email=makeTrim($('#user_id').val());
    var address=makeTrim($('#address').val());
    var contact_no=makeTrim($('#user_phone').val());
    var room=makeTrim($('#no_of_room').val());
    var children=makeTrim($('#no_of_children').val());
    var adult=makeTrim($('#no_of_adult').val());
    var user_room=makeTrim($('#user_room').val());
    var nationality=makeTrim($('#user_nationality').val());
    var datein=makeTrim($('#datetimepicker6').val());
    var dateout=makeTrim($('#datetimepicker7').val());
    var timein=makeTrim($('#check_in_time').val());
    var timeout=makeTrim($('#check_out_time').val());


    if(name==''){
      errorMessage['user_name']="User Name is Required!";
    }else{
      if(name.length<3){
        errorMessage['user_name']="User Name should be greater than 2 characters in length!";
      }
    }
    if(email==''){
          errorMessage['user_id']="Email is Required!";
    }else{
        if(!validEmail(email)){
            errorMessage['user_id']="Invilid Email Address!";
        }
    }
    if(contact_no==''){
          errorMessage['user_phone']="Contact No. is Required!";
    }else if(isNaN(contact_no) || (contact_no.length>12) || (contact_no.length<6)){
        errorMessage['user_phone']="Invilid Contact No!";
    } 
    if(children==''){
          errorMessage['no_of_children']="children is Required!";
    }else if(isNaN(children) || (children.length>5) || (children.length<0)){
        errorMessage['no_of_children']="Invilid children No!";
    }
    if(adult==''){
          errorMessage['no_of_adult']="Adult is Required!";
    }else if(isNaN(adult) || (adult.length>5) || (adult.length<0)){
        errorMessage['no_of_adult']="Invilid Adult No!";
    }
    if(room==''){
          errorMessage['no_of_room']="Room No. is Required!";
    }else if(isNaN(room) || (room.length>5) || (room.length<0)){
        errorMessage['no_of_room']="Invilid Room No!";
    }
    if(user_room==''){
          errorMessage['user_room']="Room No. is Required!";
    }else if(isNaN(user_room) || (user_room.length>5) || (user_room.length<0)){
        errorMessage['user_room']="Invilid Room No!";
    }
    if(branch==''){
      errorMessage['subcat']="Branch is Required!";
    } 
    if(address==''){
      errorMessage['address']="Address is Required!";
    }
   if(nationality ==''){
      errorMessage['user_nationality']="nationality is Required!";
    }
   if(datein ==''){
      errorMessage['datetimepicker6']="Check in Date is Required!";
    }
   if(dateout ==''){
      errorMessage['datetimepicker7']="Check out Date is Required!";
    }
   if(timein ==''){
      errorMessage['check_in_time']="Check in time is Required!";
    }
   if(timeout ==''){
      errorMessage['check_out_time']="Check out time is Required!";
    }
 

    var errorLength=Object.keys(errorMessage).length;
    if(errorLength>0){
      for (var key in errorMessage) {
        if (errorMessage.hasOwnProperty(key)) {           
          $('#'+key).addClass('error');
          $('#'+key+'_error').html(errorMessage[key]);
        }
      }
      
     $('#success_message').html('<div class="alert alert-danger">There is error in submitting form!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
      

    }else{
      var form = $("#add_guestuser")[0];
      var formData = new FormData(form);
      $.ajax({
        type: "POST",
        url: '<?php echo base_url('save-guest-user')?>',
        data: formData,
        async : false,
        cache : false,
        contentType : false,
        processData : false,
        success: function(ajaxresponse){
          response=JSON.parse(ajaxresponse);
          if(!response['status']){
            $.each(response, function(key, value) {
              $('#'+key).addClass('error');
              $('#'+key+'_error').html(value);
            });
            $('#success_message').html('<div class="alert alert-danger">There is error in submitting form!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
             
          }else{
            $("#add_guestuser")[0].reset();                
            
            $('#success_message').html('<div class="alert alert-success">'+response['message']+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            setTimeout(function(){ location.reload(); }, 1000);           

          }  
        }
      });   
    }

  });
  /*  time */

  $(document).ready(function(){



    var cat_id = '<?= $this->session->userdata('category')?>';

    if(cat_id != '')
    {
      $.ajax({
        url:"<?php echo base_url(); ?>website/SubsControler/fetch_subcategory",
        method:"POST",
        data:{cat_id:cat_id},
        success:function(data)
        {
          $('#subcat').html(data);
        }
      });
    }
    else
    {
      $('#subcat').html('<option value="">Select Hotel</option>');
    }

  });
</script>
 <script type="text/javascript">
       /*$(function () {
            $('#datetimepicker6').datetimepicker();
            $('#datetimepicker7').datetimepicker();
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });
        });*/
    </script>



  <script type="text/javascript">
 var vals = [];
$(document).ready(function(){

 
 $("#datetimepicker6").datepicker({
            dateFormat: "dd-mm-yy",
            maxDate: 0,
            onSelect: function () {
                var dt2 = $('#datetimepicker7');
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                var dt2Date = dt2.datepicker('getDate');
                //difference in days. 86400 seconds in day, 1000 ms in second
                var dateDiff = (dt2Date - minDate)/(86400 * 1000);
                
                startDate.setDate(startDate.getDate() + 30);
                if (dt2Date == null || dateDiff < 0) {
                    dt2.datepicker('setDate', minDate);
                }
                else if (dateDiff > 30){
                    dt2.datepicker('setDate', startDate);
                }
                //sets dt2 maxDate to the last day of 30 days window
                dt2.datepicker('option', 'minDate', startDate);
                dt2.datepicker('option', 'minDate', minDate);
            }
        });
        $('#datetimepicker7').datepicker({
            dateFormat: "dd-mm-yy",
            maxDate: 0
        });
       });
    </script>
