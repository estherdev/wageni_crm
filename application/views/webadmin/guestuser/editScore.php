<!--header Section Start Here -->
<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit  Score
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add Score </li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Edit Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
           <form method="post" action="<?= base_url('website/Score/updateScore/'.$records[0]['score_id']);?>">

              <div class="box-body">
              <div class="form-group row">

                <div class="col-md-4">
                  <label for="title">Edit Name</label>
                  <input type="text"  class = "form-control" autofocus ="" value="<?= $records[0]['score_name'];?>" name="score_name" />
                  <?= form_error('score_name')?>            
               </div>

              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-success">Update</button>
                <a href="<?= base_url('website/Score');?>" class= "btn btn-danger" role="button">Back</a>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.content-wrapper -->
    </div>
    <!-- /.content-wrapper -->

  <!-- /.content-wrapper -->
</section><!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/webadmin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>