<?php include "header.php";?>
<div class="contact-page">
<section id="slider" class="banner-two">
   <div class="about-banner">
      <div class="container banner-text">
         <div class="row">
            <div class="col-xs-12">
               <h1>Contact Us</h1>
            </div>
         </div>
      </div>
   </div>
</section>
<div id="content">
   <section class="contact-form-form">
      <div class="container">
         <div id="success" >
            <div role="alert" class="alert alert-success"><strong>Thanks</strong> for using our template. Your message has been sent.</div>
         </div>
         <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-9">
               <h2>Fill form here</h2>
               <form action="<?= base_url('contact-us');?>" method="post">
                  <div class="form-block clearfix">
                     <input type="text" placeholder="name*" name="uname" />
                     <input type="text" placeholder="email*" name="uemail" />
                  </div>
                  <div class="form-block">
                     <input type="text" placeholder="subject*" name="subject" />
                  </div>
                  <div class="form-block">
                     <textarea cols="1" rows="1" placeholder="Comment*" name="message" ></textarea>
                  </div>
                  <div class="submit-btn">
                     <input type="submit" name="submit" value="submit" class="detail-submit"/>
                  </div>
               </form>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3 contact-details">
               <h2>Contact Details</h2>
               <address class="address">
                  <strong>Office address</strong>
                  <span class="about-us-paragraph"> <?= $contact[0]->address; ?> </span>
               </address>
               <div class="address phone animate-effect ">
                  <h3>phone</h3>
                  <a href="tel: <?= $contact[0]->phone1; ?>"><?= $contact[0]->phone1; ?> <span>OR</span> </a><a href="tel: <?= $contact[0]->phone2; ?>"> <?= $contact[0]->phone2; ?></a>
                  <a href="tel: <?= $contact[0]->phone3; ?>"><?= $contact[0]->phone3; ?></a>
               </div>
               <div class="address phone email animate-effect">
                  <h3>email</h3>
                  <a href="mailto:info@attorney.com"> <?= $contact[0]->contact_email; ?></a>
               </div>
            </div>
         </div>
      </div>
   </section>
<!--    <section class="map embed-responsive embed-responsive-16by9">
      <div id="map"></div>
      <div class="container-fluid animate-effect">
         <div class="row">
         	<img src="assets/img/google-map.jpg" alt="" title=""/>
         </div>         
         </div> 
   </section> -->
   <section class="consulation">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-2 col-md-2 col-md-offset-1 animate-effect">
               <span class="consult-info">Get a free legal
               consulation</span>
            </div>
            <div class="col-xs-12 col-md-6 col-sm-6 animate-effect">
               <a href="tel:+8758657443" class="contact-consult"><i class="icon-consult fa fa-mobile"></i>+8758 657 443</a>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 animate-effect">
               <span class="consult-info week-time"><span>7 day a week </span> from 8 am to 8 pm</span>
            </div>
         </div>
      </div>
   </section>
</div>
</div>
<?php include "footer.php";?>
