		<!--slider Section Start Here -->
			<section id="slider">
				<div class="container-fluid">
					<div class="row">
						<!--slider Start Here -->
						<div class="banner-slider">
							<div class="tp-banner">
								<ul>
									<?php foreach ($homeslider as $key => $slider) {?>
										
									
									<li class="item-1" data-transition="zoomin" data-slotamount="1" data-masterspeed="1000" data-thumb="<?= base_url().$slider->slider_image?>"  data-saveperformance="off"  data-title="Slide">
										<img src="<?= base_url().$slider->slider_image?>"  alt="fullslide1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
										<div  class="slide-content">
											<h2 class="tp-caption white_heavy_60 lfb ltl tp-resizeme h1 layer-a"
											data-x="0"
											data-y="center"
											data-speed="2000"
											data-start="1100"
											data-easing="Power4.easeInOut"
											data-splitin="none"
											data-splitout="none"
											data-elementdelay="0.1"
											data-endelementdelay="0.1"
											data-endspeed="1000"
											data-endeasing="Power4.easeIn"
											style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><?= ucfirst($slider->title); ?></h2>
											<div class="tp-caption black_thin_blackbg_30 layer-b customin ltl tp-resizeme"
											data-x="100"
											data-y="350"
											data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
											data-speed="1500"
											data-start="2400"
											data-easing="Power4.easeInOut"
											data-splitin="none"
											data-splitout="none"
											data-elementdelay="0.01"
											data-endelementdelay="0.1"
											data-endspeed="1000"
											data-endeasing="Power4.easeIn"
											style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">
												<span><?= ucfirst($slider->sub_title); ?></span>
											</div>
											<div class="tp-caption light_heavy_60 layer-c customin ltl tp-resizeme"
											data-x="left" data-hoffset="100"
											data-y="center" data-voffset="100"
											data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
											data-speed="1000"
											data-start="2600"
											data-easing="Power4.easeInOut"
											data-splitin="none"
											data-splitout="none"
											data-elementdelay="0.01"
											data-endelementdelay="0.1"
											data-endspeed="1000"
											data-endeasing="Power4.easeIn"
											style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">
												<a href="#"  data-toggle="modal" data-target="#requst"  class="btn btn-default btn-effect"> <i class="svg-shape"> <img src="<?= base_url();?>globalassets/website/svg/FreeConsultation.svg" alt="" class="svg" /> </i><?= $slider->url_link_text; ?>  › </a>

											</div>
										</div>
									</li>
									<?php } ?>									
								</ul>
							<div class="tp-bannertimer">
							&nbsp;
							</div>
						</div>
					</div>
					<!--slider End Here -->
				</div>
			</div>
		</section>

			<!--slider Section End Here -->