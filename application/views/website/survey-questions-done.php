<?php include "header.php";?>
<div class="contact-page">
 <section id="<?= $this->config->item('id')['slider'];?>" class="banner-two">
  <div class="<?= $this->config->item('class')['about-banner'];?>">
   <div class="container banner-text">
    <div class="row">
     <div class="col-xs-12">
      <h1>Survey Feedback Form</h1>
    </div>
  </div>
</div>
</div>
</section>
<div id=" " class="feedback-group" style="background-image: url(<?= base_url();?>/globalassets/website/img/bg-feed.jpg);">
  <div class="container">
   <div class="feedback-form">
     <div class="ffHead text-center">
      <h2> <?= $titlehead;?> </h2>
      <div class="doneSurvey"><?= $title;?></div>
      <a href="<?= base_url();?>" class="btn surveyBtn">Go to home page</a>
    </div>
</div>
</div>
</div>
</div>

<?php include "footer.php";?>
 