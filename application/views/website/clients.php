<?php include "header.php";?> 
<section id="slider" class="banner-one">
   <div class="about-banner">
      <div class="container banner-text">
         <div class="row">
            <div class="col-xs-12">
               <h1>clients</h1>
            </div>
         </div>
      </div>
   </div>
</section> 
<div id="content">
    <section class="clients-content">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <h2>Clients</h2>
            </div>
         </div>
         <div class="row">
            <ul class="clients-listing clearfix">
               <?php foreach($alldata as $key => $logo) {?>
               <li class="col-xs-12 col-sm-3 col-md-2 ">
                  <span>
                  <?php if($logo['client_logo'] == true){?>
                  <img src="<?= base_url();?>uploads/<?=$logo['client_logo'];?>" alt="" title=""/>
                  <?php } else {?>
                  <img src="<?= base_url();?>globalassets/website/img/client-1.png" alt="" title=""/>
                  <?php } ?>
                  </span>
               </li>
               <?php }?>		
            </ul>
         </div>
      </div>
   </section> 
   <section class="happy-clients" >
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <h2>happy clients</h2>
            </div>
         </div>
         <div class="row">
            <div class="happy-client-list clearfix">
               <?php foreach ($alltest as $key => $value) { ?>
               <figure class="col-xs-12 col-sm-3 col-md-3  animate-effect">
                  <?php if ($value->image==true) {?>
                  <img src="<?= base_url();?>uploads/<?= $value->image; ?>" alt="" title=""/>
                  <?php }else {?>
                  <img src="<?= base_url();?>globalassets/website/img/happy-client1.png" alt="" title=""/>
                  <?php } ?>
               </figure>
               <div class="col-xs-12 col-sm-9 col-md-9 happy-client-state animate-effect">
                  <p class="about-us-paragraph"><?= $value->content;?></p>
                  <span class="taging-client">
                  <span class="posting-by">By</span>
                  <a href="javascript:void(0)"><?= $value->title;?></a>
                  </span>
               </div>
               <?php } ?>
            </div>
            <div class="pagination"><?php echo $links; ?></div>
         </div>
      </div>
   </section>
 </div> 		
<?php include "footer.php";?> 