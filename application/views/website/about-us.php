<?php include "header.php";?>
			<section id="slider" class="banner-three">
				<div class="about-banner">
					<div class="container banner-text">
						<div class="row">
							<div class="col-xs-12">
								<h1>about us</h1>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div id="content">	
				<section class="our-attorney">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-5 col-md-5 our-attorney-pics">
							<?php if ($about[0]['image']== true) {?>
								<img src="<?= base_url();?>uploads/<?php echo $about[0]['image'];?>" alt="" title=""/>
								<?php }else { ?>
								<img src="<?= base_url();?>globalassets/website/img/about-us-img-1.jpg" alt="" title=""/>
							<?php } ?>
							</div>
							<div class="col-xs-12 col-sm-7 col-md-7 attorney-mob">
								<h2><?= ucfirst($about[0]['title']);?></h2>
								<p><?= ucfirst($about[0]['content']);?></p>
							</div>
						</div>
					</div>
				</section>
				<section class="our-principles">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<h2>Our Principles</h2>
								<span class="heading-details">- Lorem ipsum dolor sit amet, consectetur adipiscing eli</span>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 clearfix principles-box animate-effect">
								<div class="index-box animate-effect">
									<span>1</span>
								</div>
								<div class="principles-detail animate-effect">
									<h3 class="underline-label">heading here</h3>
									<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 principles-box animate-effect">
								<div class="index-box animate-effect">
									<span>2</span>
								</div>
								<div class="principles-detail animate-effect">
									<h3 class="underline-label">heading here</h3>
									<p>	Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 principles-box animate-effect">
								<div class="index-box animate-effect">
									<span>3</span>
								</div>
								<div class="principles-detail animate-effect">
									<h3 class="underline-label">heading here</h3>
									<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
								</div>
							</div>
						</div>
					</div>
				</section>
			<section class="our-practice-area">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-md-9">
								<h2>Our Practice area</h2>
								<span class="heading-details underline-label">- Lorem ipsum dolor sit amet ctetur.</span>
								<div class="panel-group animate-effect" id="accordion" role="tablist" aria-multiselectable="true">
									<?php foreach($practice as $key => $value) { ?>
									<div class="panel panel-default animate-effect">
										<div class="panel-heading" role="tab" id="headingOne">
											<h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?= $value->id;?>" aria-expanded="true" aria-controls="collapseOne"> <?= $value->title;?> <i class="fa fa-angle-up"></i> <i class="fa fa-angle-down"></i></a></h3>
										</div>
										<div id="collapseOne<?= $value->id;?>" class="panel-collapse collapse clearfix" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body about-us-paragraph animate-effect">
												<p> <?= $value->content; ?></p>
											</div>
											<div class="panel-pics animate-effect">
												<?php if ($value->image==true) { ?>
												<img src ="<?= base_url();?>uploads/<?= $value->image; ?>" alt="" title="" />
											<?php }else { ?>
											<i class="family-law-svg"> <img src="<?= base_url();?>globalassets/website/svg/family-law.svg" alt=""  title="" class="svg"/> </i>
												<?php } ?>
											</div>
										</div>
									</div>
									<?php } ?>							
								</div>
							</div>
							<div class="col-xs-12 col-md-3 our-services">
								<h2>Our Service</h2>
								<span class="heading-details underline-label">- Lorem ipsum dolor sit amet ctetur.</span>
								<ul class="practice-listing animate-effect">
									<li><a href="javascript:void(0)">SERIOUS CAR CRASH <i class="fa fa-chevron-right"></i></a></li>
									<li><a href="javascript:void(0)">BRAIN INJURY <i class="fa fa-chevron-right"></i></a></li>
									<li><a href="javascript:void(0)">TRUCK ACCIDENTS <i class="fa fa-chevron-right"></i></a></li>
									<li><a href="javascript:void(0)">SEMI-TRUCK COLLISION <i class="fa fa-chevron-right"></i></a></li>
									<li><a href="javascript:void(0)">MOTORCYCLE ACCIDENTS <i class="fa fa-chevron-right"></i></a></li>
									<li><a href="javascript:void(0)">WRONGFUL DEATH <i class="fa fa-chevron-right"></i></a></li>
									<li><a href="javascript:void(0)">SEMI-TRUCK COLLISION <i class="fa fa-chevron-right"></i></a></li>
									<li><a href="javascript:void(0)">MOTORCYCLE ACCIDENTS <i class="fa fa-chevron-right"></i></a></li>
									<li><a href="javascript:void(0)">WRONGFUL DEATH <i class="fa fa-chevron-right"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</section>
			</div>
<?php include "footer.php";?>