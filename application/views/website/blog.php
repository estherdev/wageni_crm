<?php include "header.php";?>
<section id="slider" class="banner-two">
   <div class="about-banner">
      <div class="container banner-text">
         <div class="row">
            <div class="col-xs-12">
               <h1>blog</h1>
            </div>
         </div>
      </div>
   </div>
</section>
<div id="content">
   <section class="blog-content">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-9">
               <h2>Blog Page</h2>
               <?php foreach ($results as $blog) {?>                  
               <span class="heading-details"></span>
               <div class="blog-listing clearfix zoom">
                  <div class="blog-listing-pics">
                     <figure>
                        <?php if ($blog->userfile == true) { ?>
                           <img src="<?= base_url();?>uploads/<?= $blog->userfile ;?>" alt="" title=""/>
                           
                        <?php }else{ ?>
                        <img src="<?= base_url();?>globalassets/website/img/blog-img-1.jpg" alt="" title=""/>
                    <?php } ?>
                     </figure>
                  </div>
                  <div class="blog-information">
                     <h3><?= $blog->title;?></h3>
                     <div class="blog-admin-info underline-label">
                        <span class="admin">- By :<span><?= $blog->created_by;?></span></span>
                        <span>Date :<span><?= $blog->created_date;?></span></span>
                     </div>
                     <p>
                       <?= $blog->content;?>
                     </p>
                     <a class="more-btn" href="<?= base_url('website/Pages/blogDetails/'.$blog->id);?>"> Read More</a>
                  </div>
               </div>
           <?php } ?>
          <div class="pagination"><?php echo $links; ?></div>     
            </div>
            <div class="col-xs-12 col-sm-5 col-md-3">              
               <div class="featured-blog">
                  <h2>Latest Blog</h2>
                  <ul class="featured-blog-list">
                   <?php foreach ($sideblog as $blog) {?>
                     <li class="clearfix zoom">                  
                        <figure>
                      <?php if ($blog->userfile == true) { ?>
                     <a href="<?= base_url('website/Pages/blogDetails/'.$blog->id);?>"><img src="<?= base_url();?>uploads/<?= $blog->userfile ;?>" alt="" title=""/>
                     <?php } else {?>
                           <a href="<?= base_url('website/Pages/blogDetails/'.$blog->id);?>"><img src="<?= base_url();?>globalassets/website/img/feature-blog-1.jpg" alt="" title=""/></a>
                     <?php } ?>
                  </figure>
                        <div class="featured-blog-descpt">
                           <h5><a href="<?= base_url('website/Pages/blogDetails/'.$blog->id);?>"><?= $blog->title;?></a></h5>
                           <span>By : <span><?= $blog->created_by;?> </span></span>
                           <p> 
                              <?php $this->load->helper('text');?>
                              <?= word_limiter($blog->content,10);?>
                           </p>
                        </div>                  
                     </li> 
                     <?php } ?>                          
                 </ul>
               </div>
            </div>
         </div>
      </div>
   </section>
 </div>
 <?php include "footer.php";?>
 