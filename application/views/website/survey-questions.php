<!--  <?php //echo "<pre/>"; print_r($allquestions); die; ?>   -->

<?php include "header.php";?>

<div class="contact-page">
   <section id="slider" class="banner-two">
      <div class="about-banner">
         <div class="container banner-text">
            <div class="row">
               <div class="col-xs-12">
                  <h1>Survey Feedback Form</h1>
               </div>
            </div>
         </div>
      </div>
   </section>

   <div id="content" class="feedback-group">
      <div class="container">
         <div class="feedback-form">
            <div class="row">
               <div class="col-xs-12">
                  <h2>Feedback Form</h2>
                  <span class="heading-details underline-label">If u have any suggestion or any feedback for us.We't love to hear it!</span>
               </div>
            </div>


            <?php if(count($allquestions)>0 ){ ?>
            <form class="submitsurveyguestuser" id="submitsurveyguestuser" method="post">
              <input type="hidden" name="semail" value="<?= $this->uri->segment(2); ?>">
              <input type="hidden" name="cat" value="<?= $allquestions[0][5]; ?>">
              <input type="hidden" name="subcat" value="<?= $allquestions[0][6]; ?>">
              <input type="hidden" name="process" value="<?= $allquestions[0][7]; ?>">
              <input type="hidden" name="uid" value="<?= $allquestions[0][8]; ?>">
              <input type="hidden" name="busid" value="<?= $allquestions[0][0]; ?>">
            <div class="feedback-head">
               <h3>Evalution Critiria <?//= $ip = $this->input->ip_address();?></h3>
            </div>
             

    <?php  
    
     $quelength= count($allquestions); 
       
      for($que=0; $que < count($allquestions);  $que++) {

            $allquestions[$que][0]; 
            $allquestions[$que][1]; 
            $allquestions[$que][2];            
            $allquestions[$que][3];             
            $allquestions[$que][7];             
             $this->session->set_userdata('cat',$allquestions[$que][5]); 
            $this->session->set_userdata('subcat',$allquestions[$que][6]); 
             $this->session->set_userdata('process',$allquestions[$que][7]); 
             $this->session->set_userdata('uid',$allquestions[$que][8]); 

            switch ($allquestions[$que][3]) {

            /**********************************************    Radio    **************************************************/    
            
            case 'qmc':                       
            ?>

            <div class="feedbacksec">
               <div class="feed-back-qus">
                 <h2> <b>Que.<?= $que+1;?> </b><span class="questions-servey-feedback"><?= $allquestions[$que][2] ?></span></h2>
               </div>
                <?php for($qmc = 0; $qmc<count($allquestions[$que][4]); $qmc++) {   ?>
                   <div class="feed-back-option">
                    <input class="form-check-input" id="radio-<?= $allquestions[$que][0]?>-<?= $allquestions[$que][4][$qmc]->bus_que_ans_id;?>" name="que-ans[<?= $allquestions[$que][1]?>][]" type="radio" value="<?= $allquestions[$que][4][$qmc]->bus_que_ans_id; ?>"> 
                    <label for="radio-<?=$allquestions[$que][0];?>-<?= $allquestions[$que][4][$qmc]->bus_que_ans_id; ?>" class="radio-inline"><?= $allquestions[$que][4][$qmc]->bus_que_type ?></label>
                  </div>
                <?php } ?>     
   
            </div>            
            <?php 
            break;

            /**********************************************    Dropdown   **************************************************/

            case 'qdd':                       
            ?>

            <div class="feedbacksec">
               <div class="feed-back-qus">
                 <h2><b>Que.<?= $que+1;?> </b><span class="questions-servey-feedback"><?= $allquestions[$que][2] ?></span></h2>
               </div>
               <div class="feed-back-option">
                <div>
               <?php for($qchb = 0; $qchb<count($allquestions[$que][4]); $qchb++) {?>

                    <input type="hidden" name="cat" value="<?= $allquestions[$que][5];?>">
                    <input type="hidden" name="subcat" value="<?= $allquestions[$que][6];?>">
                    <input type="hidden" name="process" value="<?= $allquestions[$que][7];?>">
                    <input type="checkbox" id="checkbox-<?=$allquestions[$que][0]?>-<?= $allquestions[$que][4][$qchb]->bus_que_ans_id ?>" name="que-ans[<?= $allquestions[$que][0]?>][]" value="<?= $allquestions[$que][4][$qchb]->bus_que_ans_id ?>">
                    <label for="checkbox-<?=$allquestions[$que][0]?>-<?= $allquestions[$que][4][$qchb]->bus_que_ans_id ?>"><?= $allquestions[$que][4][$qchb]->bus_que_type ?></label>
                  <?php } ?>     
                </div>

                </div>             
                              
               </div>

            <?php 
            break;

            /**********************************************    Dropdown   **************************************************/

            case 'qdd':                       
            ?>
             <div class="feedbacksec">
                <div class="feed-back-qus">
                 <h2> <b>Que.<?= $que+1;?> </b><span class="questions-servey-feedback"><?= $allquestions[$que][2] ?></span></h2>
                </div>
                <div class="feed-back-option">
                   <div>
                    <select class="form-control" name="que-ans[<?= $allquestions[$que][0]?>][]">
                     <?php for($qdd = 0; $qdd<count($allquestions[$que][4]); $qdd++) {?>
                              
                    <input type="hidden" name="cat" value="<?= $allquestions[$que][4];?>">
                    <input type="hidden" name="subcat" value="<?= $allquestions[$que][5];?>">
                    <input type="hidden" name="process" value="<?= $allquestions[$que][6];?>">
                    <option value="<?= $allquestions[$que][3][$qdd]->bus_que_ans_id ?>"><?= $allquestions[$que][3][$qdd]->bus_que_type ?></option>
                    <?php } ?>
                    </select> 
                   </div>
                </div>
             </div>

            <?php 
            break;  

            /**********************************************    Star   **************************************************/

            case 'qsr':                       
            ?>  
             <div class="feedbacksec">
                <div class="feed-back-qus">
                   <h2><b>Que.<?= $que+1;?> </b><span class="questions-servey-feedback"><?= $allquestions[$que][2] ?></span></h2>
                </div>
                <div class="feed-back-option">
                    <div class="reting-row rating-stars" id="rating-star"> 
                      <input type="number" readonly class="rating-value hidden" name="que-ans[<?= $allquestions[$que][1]?>][]" id="que-ans[<?= $allquestions[$que][1]?>][]" value="">
                       <ul>
                         <li class="rating-star"><i class="fa fa-star"></i></li>
                         <li class="rating-star"><i class="fa fa-star"></i></li>
                         <li class="rating-star"><i class="fa fa-star"></i></li>
                         <li class="rating-star"><i class="fa fa-star"></i></li>
                         <li class="rating-star"><i class="fa fa-star"></i></li>
                      </ul>
                   </div>





                   <!-- <div class="rating-stars">
                      <ul>
                         <li class="rating-star"><i class="fa fa-star-o"></i></li>
                         <li class="rating-star"><i class="fa fa-star-o"></i></li>
                         <li class="rating-star"><i class="fa fa-star-o"></i></li>
                         <li class="rating-star"><i class="fa fa-star-o"></i></li>
                         <li class="rating-star"><i class="fa fa-star-o"></i></li>
                      </ul>
                   </div> -->
                </div>
             </div>

             <?php 
            break;   

            /**********************************************    Default   **************************************************/
            default:
                
            break;
   }   }     ?>
      

           <div class="feed-sbmit text-center">
              <input type="submit" class="btn btn-success feed-btn" value="SUBMIT" id='btnsubmit'></input>
              <p id="sent"></p>
           </div>
 <?php } else { echo "<div class='no-result text-center'><p>If u have any suggestion or any feedback for us.We't love to hear it!</p></div>";  } ?>   

      </div>
   </div>
</div>
</div>
<!--content Section End Here -->

<?php include "footer.php";?>


<script>/*
$(document).ready(function() {

  var data = $("#submitsurveyguestuser").serialize();
      alert(data);

      $.ajax({
        type : 'POST',
        url  : '<?//= base_url();?>website/Pages/guestSurveyQuestion',
        data : data,
         success :  function(data)
          {

    $("#submitsurveyguestuser").submit(function() {
        $.post("<?//= base_url();?>website/Pages/guestSurveyQuestion", $("#submitsurveyguestuser").serialize())
        .done(function(data) {
            if (data=='1'){
                $("#sent").text("Success");  

                window.location = '<?//= base_url();?>success'; 

            }
            else{
                $("#sent").text("Error");
            }
        });
        return false;
    })
});*/
 </script>

 <script>
      $(function () {
        $('form').bind('submit', function () {
        // alert($('form').serialize());
         //return false;
          $.ajax({
            type: 'post',
            url: '<?= base_url();?>website/Pages/guestSurveyQuestion',
            data: $('form').serialize(),
            success: function (data) {            
               if (data=='1'){
                $("#sent").text("Success"); 
                window.location = '<?= base_url();?>success'; 
                }
                else{
                    $("#sent").text("Error");
                }
            }
          });
          return false;
        });

    var ratingOptions = {
        selectors: {
            starsSelector: '.rating-stars',
            starSelector: '.rating-star',
            starActiveClass: 'is--active',
            starHoverClass: 'is--hover',
            starNoHoverClass: 'is--no-hover',
            targetFormElementSelector: '.rating-value'
        }
    };

    $(".rating-stars").ratingStars(ratingOptions);
        
      });
    </script>