<!--header Section Start Here -->
<?php include "header.php";?>
<!--header Section End Here -->
<!--slider Section Start Here -->

<!--slider Section End Here -->
<!--content Section Start Here -->
<div id="content">
<div class="dashboard">
		<div class="listing-lt dash-lt">
			<ul class="leftNav">
				<li><a href="">Profile</a></li>
				<li class="dropdown">
					<a class="navLink" data-toggle="collapse" href="#booking">Bookings</a>
					<ul id="booking" class="dropList collapse in">
						<li><a href="">Current</a></li>
						<li><a href="">Past</a></li>
					</ul>
				</li>
				<li class="active"><a href="">Travel Notes</a></li>
				<li>
					<a class="navLink" data-toggle="collapse" href="#acSetting">Account Settings</a>
					<ul id="acSetting" class="dropList collapse in">
						<li><a href="">Notification</a></li>
						<li><a href="">Login & Security</a></li>
						<li><a href="">Help</a></li>
					</ul>
				</li>
				<li><a href="">Stats</a></li>
			</ul>
		</div>
		<div class="dashboard-rt">
			<div class="container-fluid">
				<div class="crewListing-container">
					<ul class="gotoLink">
						<li><a href="">Home</a></li>
						<li class="active"><a href="#">Dashboard</a></li>
					</ul>
					<div class="crew-heading">
						<h2>Dashboard</h2>
						<hr class="heading-rule" />
					</div>
					<div class="card">
						<div class="card-body">
							<div>
								<table id="example" class="table" >
        <thead>
            <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
                <th>Extn.</th>
                <th>E-mail</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tiger</td>
                <td>Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
                <td>5421</td>
                <td>t.nixon@datatables.net</td>
            </tr>
            <tr>
                <td>Garrett</td>
                <td>Winters</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>63</td>
                <td>2011/07/25</td>
                <td>$170,750</td>
                <td>8422</td>
                <td>g.winters@datatables.net</td>
            </tr>
            <tr>
                <td>Ashton</td>
                <td>Cox</td>
                <td>Junior Technical Author</td>
                <td>San Francisco</td>
                <td>66</td>
                <td>2009/01/12</td>
                <td>$86,000</td>
                <td>1562</td>
                <td>a.cox@datatables.net</td>
            </tr>
            <tr>
                <td>Cedric</td>
                <td>Kelly</td>
                <td>Senior Javascript Developer</td>
                <td>Edinburgh</td>
                <td>22</td>
                <td>2012/03/29</td>
                <td>$433,060</td>
                <td>6224</td>
                <td>c.kelly@datatables.net</td>
            </tr>
            <tr>
                <td>Airi</td>
                <td>Satou</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>33</td>
                <td>2008/11/28</td>
                <td>$162,700</td>
                <td>5407</td>
                <td>a.satou@datatables.net</td>
            </tr>
           
        </tbody>
    </table>
							</div>
						</div>
					</div>
					<div class="noteBtn"><button class="addNote">Add New Note</button></div>
					<ul class="tripNote">
						<li class="wow fadeInUp">
							<span class="noteEdit"><a href=""><i class="fa fa-edit"></i></a><a href=""><i class="fa fa-trash"></i></a></span>
							<h5>Trip C</h5>
							<span class="tripDate">Date goes here</span>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
						</li>
						<li class="wow fadeInUp">
							<h5>Trip C</h5>
							<span class="tripDate">Date goes here</span>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
						</li>
						<li class="wow fadeInUp">
							<h5>Trip C</h5>
							<span class="tripDate">Date goes here</span>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!--content Section End Here -->
<!--footer Section Start Here -->			
<?php include "footer.php";?>
<!--footer Section End Here -->