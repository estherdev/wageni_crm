 <?php 
 include "header.php";
  ?>	
  <style type="text/css">
  	.login .login-form-wrapper {
    background: rgba(255,255,255,0.85);
    padding: 25px;
    box-shadow: 5px 10px 10px -5px #ccc;
    text-align: center;
    border-radius: 5px;
    min-width: 320px;
    max-width: 400px;
    margin: 0px auto;
}

.login {
    padding: 50px;
}
  </style>
<div id="content"> 
<div class="login" style="background-image: url(<?= base_url()?>globalassets/admin/wc_bg.jpg );">
  <div class="login-form-wrapper">
  	<div class="login-box-body">
        
        <h3><strong>Sign in</strong></h3>
        <p class="statusMsg"></p>  
         <div id="error"></div> 
        <p id="err" class="text-center"></p>
        <form id="wageni-login-form"  method="post" >
          <div class="form-group has-feedback">        
          <input id="login_username" class="form-control" name="login_username" placeholder="Email" type="email">   
          </div>
          <p id="lemailerr"></p>
          <div class="form-group has-feedback">             
           <input id="login_password" class="form-control" name="login_password" placeholder="Password" type="password">            
          </div>
          <p id="lpasserr"></p>
          <div class="row submitBtn">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat regBtn" name="btn-save" id="btn-submit">Sign In</button>
             <!--  <a href="<?//= base_url('user-forget-password');?>"><button class="btn btn-link">forget password ?</a></button> -->
            </div>
          </div>
        </form>
      </div>
          </div>
      </div>
</div>			
<?php include "footer.php";?>


<script type="text/javascript">
   $( "#wageni-login-form" ).submit(function( event ) {
   	submitLogin();
   	event.preventDefault();
   });
   
  
   function submitLogin() {
     
   var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
   //var utype = $('#userType').val();
   var lemail = $('#login_username').val();   
   var lpass = $('#login_password').val(); 
 //  alert('Email =>'+lemail+' password =>' +lpass);
   
   if(lemail.trim() == '' ){
   	$errormsg = "<span style='color: red'>Please enter your E-mail</span>";
   	$("#lemailerr").html($errormsg)
   	$('#lemail').focus();
   	return false;
   }else if(lemail.trim() != '' && !reg.test(lemail)){
   	$errormsg = "<span style='color: red'>Please enter valid email</span>";
   	$("#lemailerr").html($errormsg)
   	$('#lemail').focus();
   	return false;
   }else if(lpass.trim() == '' ){
          
            $errormsg = "<span style='color: red'>Please enter your Password</span>";
            $("#lpasserr").html($errormsg)
            $('#lpass').focus();
            return false;
        } else{
        	$.ajax({
        		type:'POST',
        		url:'<?= base_url();?>Home/login', 
        		dataType: "json",
        		data: {lemail: lemail,lpass: lpass},
                success:function(msg){
                	if(msg.data!='' || msg.status=='true' ){
                		$('#login').modal('hide'); 
                			window.location.href='<?= base_url()?>dashboard';
                      }else{
                       	$("#err").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span>'+msg.message+'</div>');
                      }
                   }
              });
          }
    }
</script>