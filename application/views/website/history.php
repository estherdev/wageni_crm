<?php include "header.php";?>
<section id="slider" class="banner-one">
   <div class="about-banner">
      <div class="container banner-text">
         <div class="row">
            <div class="col-xs-12">
               <h1>history</h1>
            </div>
         </div>
      </div>
   </div>
</section>
<div id="content">
   <section class="history-content">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <h2>History</h2>
               <?php $i= 1; foreach ($historycontent as $key => $history) {?>
               <span class="heading-details"></span>
               <?php } ?>
            </div>
         </div>
         <div class="row">
            <div class="video-container clearfix">
               <div class="col-xs-12 col-sm-5 video-section">
                  <?php if($history['image']==true){ ?>
                  <img src="<?= base_url();?>uploads/<?=$history['image'];?>" alt="" title=""/>
                  <?php } else {?>
                  <img src="<?= base_url();?>globalassets/website/img/video-back.jpg" alt="" title=""/>
                  <?php } ?>
                  <a href="javascript:void(0)" class="play-icon"><i class="play-btn"></i></a>
               </div>
               <div class="col-xs-12 col-sm-7 history-declaration">
                  <p><?= ($history['content']);?></p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="our-history-year">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <h2>Our years</h2>
               <span class="heading-details"></span>
            </div>
         </div>
         <div class="row">
            <div class="yearwise-listing clearfix">
               <?php foreach ($alldata as $key => $pack) { ?>
               <div class="col-xs-12 col-sm-6 year-listing-block spacer-right animate-effect">
                  <h3 class="underline-label"><span><?= date("Y", strtotime($pack->history_date)) ;?></span> - YEARS</h3>
                  <p><?= $pack->content;?></p>
               </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </section>
</div>     
<?php include "footer.php";?>