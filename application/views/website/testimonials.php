<!-- header Section Start Here -->
<?php include "header.php";?>
<!-- header Section End Here -->
<!-- slider Section Start Here -->
<section id="slider">
   <div class="about-banner">
      <div class="container banner-text">
         <div class="row">
            <div class="col-xs-12">
               <h1>Testimonials</h1>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- slider Section End Here -->
<!-- content Section Start Here -->
<div id="content">
   <!-- happy-clients and testimonial Section start Here -->
   <section class="happy-clients testimonial">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <h2>happy clients</h2>
               <span class="heading-details underline-label"></span>
            </div>
         </div>
         <div class="row">
            <?php foreach ($alltestimonials as $key => $value) { ?>
            <div class="happy-client-list clearfix">
               <figure class="col-xs-12 col-sm-3 col-md-3">
                  <?php if ($value->image==true) { ?>
                  <img src="<?= base_url();?>uploads/<?= $value->image; ?>" alt="" title=""/>
                  <?php }else {?>
                  <img src="<?= base_url();?>globalassets/website/img/happy-client1.png" alt="" title=""/>
                  <?php } ?>
               </figure>
               <div class="col-xs-12 col-sm-9 col-md-9 happy-client-state animate-effect">
                  <p class="about-us-paragraph">
                     <?= $value->content; ?>
                  </p>
                  <span class="taging-client"> <span class="posting-by">By</span> <a href="javascript::"><?= $value->title; ?></a> </span>
               </div>
            </div>
            <?php } ?>
         <div class="pagination"><?php echo $links; ?></div>     
         </div>
      </div>
   </section>
   <!--happy-clients Section End Here -->
</div>
<!-- content Section End Here -->
<!-- footer Section Start Here -->        
<?php include "footer.php";?>
<!-- footer Section End Here -->