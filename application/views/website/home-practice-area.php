<section class="practice-area animate-effect">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 section-header">
				<h2> PRACTICE AREA </h2>
				<span class="practice-desp">- When an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>
			</div>
			<div class="col-xs-12 practice-law-list">
				<div class="row">
					<?php foreach($practice as $key => $value) { ?>
						<div class="col-xs-12 col-sm-3 col-md-3 family-law">
							<h3 class="h3">  <?= $value->title;?>  </h3>
							<div class="family-group">
								<?php if ($value->image==true) { ?>
									<img src="<?= base_url();?>uploads/<?= $value->image; ?>" alt="" title="" />
								<?php }else { ?>
									<i class="family-law-svg"> <img src="<?= base_url();?>globalassets/website/svg/family-law.svg" alt=""  title="" class="svg"/> </i>
								<?php } ?>
							</div>
							<p>
								<?php $this->load->helper('text');?>
								<?= word_limiter($value->content,20);?>
							</p>
							<a href="javascript:void(0)" class="more-btn"> Know More <i class="fa fa-chevron-right"></i></a>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

