<?php include "header.php"?>
<body>	
	<div id="wrapper" class="blog-page blog-details-page">
		<section id="slider" class="banner-two">
			<div class="about-banner">
				<div class="container banner-text">
					<div class="row">
						<div class="col-xs-12">
							<h1>Blog</h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div id="content">
			<section class="blog-content">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-7 col-md-9">
							<h2>Blog Details</h2>
							<div class="blog-listing clearfix">
								<div class="blog-listing-pics">
									<figure>
										<?php if($singleBlog[0]['userfile']==true){?>
											<img src="<?=base_url();?>uploads/<?=$singleBlog[0]['userfile'];?>" alt="" title="" />
										<?php } else {?>
											<img src="<?= base_url();?>globalassets/website/img/blog-details-img1.jpg" alt="" title=""/>
										<?php } ?>										
									</figure>
								</div>
								<div class="blog-information animate-effect">
									<h3><?= $singleBlog[0]['title']; ?></h3>
									<div class="blog-admin-info underline-label">
										<span class="admin">- By :<span> <?= $singleBlog[0]['created_by']; ?> </span></span>
										<span>Date :<span> 10-feb-05</span></span>
									</div>
									<p><?= $singleBlog[0]['content']; ?></p>
									<p><?= $singleBlog[0]['content']; ?></p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-5 col-md-3">
							<div class="featured-blog">
								<h2>Latest Blog</h2>
								<span class="heading-details"></span>
								<ul class="featured-blog-list">
									<?php foreach ($sideblog as $blog) { ?>
										<li class="clearfix zoom">											
											<figure>
												<?php if ($blog->userfile == true) { ?>
													<a href="<?= base_url('website/Pages/blogDetails/'.$blog->id);?>"><img src="<?= base_url();?>uploads/<?= $blog->userfile ;?>" alt="" title=""/>
													<?php } else {?>
														<a href="blog-details.html"><img src="assets/img/feature-blog-1.jpg" alt="" title=""/></a>
													<?php } ?>
												</figure>
												<div class="featured-blog-descpt">
													<h5><a href="<?= base_url('website/Pages/blogDetails/'.$blog->id);?>"><?= $blog->title;?></a></h5>
													<span>By : <span><?= $blog->created_by;?> </span></span>
													<p><?php $this->load->helper('text');?><?= word_limiter($blog->content,10);?></p>
												</div>											
											</li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
<?php include "footer.php"?>