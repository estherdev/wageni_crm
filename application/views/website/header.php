<!DOCTYPE html>
<html lang="en">
  <head>

    <?php $css =$this->config->item('css')['websitecss'];  ?>
    <?php $website =$this->config->item('css')['website'];  ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>WageniCRM</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Exo:400,300,300italic,500,400italic,500italic,600,600italic,700,700italic%7CMerriweather:400,300,300italic,400italic,700,700italic%7COpen+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800%7CMontserrat:400,700%7CLibre+Baskerville:400,700' rel='stylesheet' type='text/css'>
    <link href="<?= $css;?>bootstrap.min.css" rel="stylesheet">
    <link href="<?= $css;?>font-awesome.min.css" rel="stylesheet">
    <link href="<?= $css;?>glyphicons.css" rel="stylesheet">
    <link href="<?= $css;?>owl.carousel.css" rel="stylesheet">
    <link href="<?= $css;?>global.css" rel="stylesheet">
    <link href="<?= $css;?>settings.css" rel="stylesheet">
    <link href="<?= $css;?>style.css" rel="stylesheet">
    <link href="<?= $css;?>responsive.css" rel="stylesheet">
  </head>
  <body class="homepage">
<!--     <div class="modal fade modal-login modal-border-transparent" id="login" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <button type="button" class="btn btn-close close" data-dismiss="modal" aria-label="Close">
          <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
          </button>
          <div class="clear"></div>
          <div id="modal-login-form-wrapper">
            <form id="login-form" method="post">
              <div class="modal-body pb-5">
                <h4 class="text-center heading mt-10 mb-20">Sign-in</h4>
                <p id="err" class="text-center"></p>
                <div class="modal-seperator"></div>
                <div class="form-group"> 
                  <input id="login_username" class="form-control" name="login_username" placeholder="Email" type="text"> 
                </div>
                <p id="lemailerr"></p>
                <div class="form-group"> 
                  <input id="login_password" class="form-control" name="login_password" placeholder="Password" type="password"> 
                </div>
                <p id="lpasserr"></p>
                <div class="form-group">
                  <div class="row gap-5">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                      <div class="checkbox-block fa-checkbox"> 
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 text-right"> 
                      <button id="login_lost_btn" type="submit" class="btn btn-link">forget password ?</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <div class="row gap-10">
                  <div class="col-xs-6 col-sm-6 mb-10">
                    <button type="submit" class="btn btn-primary btn-block">Sign-in</button>
                  </div>
                  <div class="col-xs-6 col-sm-6 mb-10">
                    <button type="submit" class="btn btn-primary btn-block btn-inverse" data-dismiss="modal" aria-label="Close">Cancel</button>
                  </div>
                </div>
              </div>
            </form>
            <form role="form" class="form-signin" method="post" id="forgot-form">
              <div id="lost-form" style="display:none;">
                <div class="modal-body pb-5">
                  <h3 class="text-center heading mt-10 mb-20">Forgot password</h3>
                  <div class="form-group mb-10"> 
                    <input type="email" name="lost_email" id="lost_email" class="form-control" type="text" placeholder="Enter Your Email">
                  </div>
                  <div class="text-center">
                    <button id="lost_login_btn" type="button" class="btn btn-link">Sign-in</button> 
                  </div>
                </div>
                <div id="forgoterror"></div>
                <div class="modal-footer mt-10">
                  <div class="row gap-10">
                    <div class="col-xs-6 col-sm-6">
                      <button type="submit" onclick="submitforgetpass();" name="forgot_password" class="btn btn-primary btn-block" id="btn-forgot">Submit</button>
                    </div>
                    <div class="col-xs-6 col-sm-6">
                      <button type="submit" class="btn btn-primary btn-inverse btn-block" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div> -->
    <div id="wrapper">
    <header id="header" class="normal">
      <div class="primary-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-2 media-header">
              <ul class="media-listing clearfix">
                <li><a href="http://twitter.com" target="_blank" class="fa fa-twitter">&nbsp;</a></li>
                <li><a href="http://facebook.com" target="_blank" class="fa fa-facebook">&nbsp;</a></li>
                <li><a href="http://linkedin.com" target="_blank" class="fa fa-linkedin-square">&nbsp;</a></li>
                <li><a href="http://pinterest.com" target="_blank" class="fa fa-pinterest">&nbsp;</a></li>
                <li><a href="http://instagram.com" target="_blank" class="fa fa-instagram">&nbsp;</a></li>
                <li><a href="http://youtube.com" target="_blank" class="fa fa-youtube">&nbsp;</a></li>
              </ul>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-10">
              <div class="primary-right-block">
                <div class="consultation">
                  <a href="javascript:void(0)" data-toggle="modal" data-target="#requst" ><i class="svg-shape icon-enevlope"> <img src="<?= $website;?>svg/FreeConsultation.svg" alt="" class="svg" /> </i><span>Request a  Free Consultation</i></span></a>
                </div>
                <div class="consultation">
                  <?php if ($this->session->userdata('username')==true) {?>                                	
                  <span class="user-login">
                    <?php  echo ucfirst($this->session->userdata('username')); ?>
                    <div class="drop-down-user">
                      <ul>
                        <li><a href="<?= base_url('dashboard');?>">Dashboard</a></li>
                        <li><a href="<?= base_url('logout');?>">Logout</a></li>
                      </ul>
                    </div>
                  </span>
                  <?php } else{?>
                  <a href="<?= base_url('user-login');?>"  class="login-btn"><span>Login</span></a>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="navigation-header">
        <div class="container">
          <div class="row">
            <a href="<?= base_url();?>" class="col-xs-10 col-sm-3 col-md-4 logo"><img src="<?= $website;?>img/logo.jpg" alt="logo" title="logo"/></a>
            <div class="col-xs-2 col-sm-9 col-md-6 navigation">
              <nav class="navbar navbar-default">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                  <ul class="nav navbar-nav">
                    <li class="<?php if($this->uri->uri_string() == base_url()) { echo 'active'; } ?>"><a href="<?= base_url();?>">Home</a></li>
                    <li class="dropdown <?php if($this->uri->uri_string() =='about-us' || $this->uri->uri_string() =='clients' || $this->uri->uri_string() =='history'  ) { echo 'active'; } ?> ">
                      <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About<span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                        <li class="<?php if($this->uri->uri_string() =='about-us') { echo 'active'; } ?>"><a href="<?= base_url('about-us');?>">About-Us</a></li>
                        <li class="<?php if($this->uri->uri_string() == 'clients') { echo 'active'; } ?>"><a href="<?= base_url('clients');?>">clients</a></li>
                        <li class="<?php if($this->uri->uri_string() == 'history') { echo 'active'; } ?>"><a href="<?= base_url('history');?>">Milestone</a></li>
                      </ul>
                    </li>
                    <li class="<?php if($this->uri->uri_string() =='testimonials') { echo 'active'; } ?>"><a href="<?= base_url('testimonials');?>">testimonials</a></li>
                    <li class="<?php if($this->uri->uri_string() =='contact-us') { echo 'active'; } ?>"><a href="<?= base_url('contact-us');?>">Contacts</a></li>
                    <li class="<?php if($this->uri->uri_string() =='package') { echo 'active'; } ?>"><a href="<?= base_url('package');?>">Packages</a></li>
                    <li class="<?php if($this->uri->uri_string() =='faq') { echo 'active'; } ?>"><a href="<?= base_url('faq');?>">Faq</a></li>
                    <li class="<?php if($this->uri->uri_string() =='blog') { echo 'active'; } ?>"><a href="<?= base_url('blog');?>">blog</a></li>
                  </ul>
                </div>
              </nav>
            </div>
            <?php $records = contactdetail();               ?>
            <a href="tel:<?= $records['fphone'];?>" class="col-xs-12 col-md-2 contact-number clearfix"> <i class="icon-call svg-shape"> <img src="<?= $website;?>svg/phone.svg" alt="" class="svg" /></i><span><?= $records['fphone'];?></span> </a>
          </div>
        </div>
      </div>
    </header>