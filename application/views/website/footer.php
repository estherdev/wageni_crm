<?php $website =$this->config->item('js')['website'];  ?>
<?php $admin =$this->config->item('js')['admin'];  ?>
<footer id="footer">
   <div class="top-footer">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-8">
               <nav class="navbar navbar-default">
               </nav>
            </div>
            <div class="consultation">
               <a href="javascript:void(0)" data-toggle="modal" data-target="#requst" ><i class="svg-shape icon-enevlope"> <img src="<?= $website;?>svg/FreeConsultation.svg" alt="logo" class="svg" /> </i><span>Request a  Free Consultation <i class="fa fa-chevron-right"></i></span> </a>
            </div>
         </div>
      </div>
   </div>
   <div class="main-footer">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-md-6 block-right"	>
               <div class="row">
                  <div class="col-xs-12 col-sm-6  practice-footer tablet-mode">
                     <h3> Quik Links </h3>
                     <ul class="practice-listing">
                        <li> <a href="<?= base_url();?>">Home <i class="fa fa-chevron-right"></i></a></li>
                        <li> <a href="<?= base_url('about-us');?>">About-Us <i class="fa fa-chevron-right"></i></a></li>
                        <li> <a href="<?= base_url('clients');?>">clients <i class="fa fa-chevron-right"></i></a></li>
                        <li> <a href="<?= base_url('testimonials');?>">testimonials <i class="fa fa-chevron-right"></i></a></li>
                        <li> <a href="<?= base_url('history');?>">Milestone <i class="fa fa-chevron-right"></i></a></li>
                     </ul>
                  </div>
                  <div class="col-xs-12 col-sm-6 tablet-mode">
                     <ul class="practice-listing listing-continue">
                        <li> <a href="<?= base_url('contact-us');?>"> Contacts <i class="fa fa-chevron-right"></i></a></li>
                        <li><a href="<?= base_url('faq');?>">Faq <i class="fa fa-chevron-right"></i></a></li>
                        <li><a href="<?= base_url('blog');?>">blog <i class="fa fa-chevron-right"></i></a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-md-6"	>
               <div class="row">
                  <div class="col-xs-12 col-sm-6  spacer-mobile">
                     <?php $records =contactdetail(); ?>
                     <a href="<?= base_url();?>" class="footer-logo"><img src="<?= $website;?>img/logo.jpg" alt="" title=""/></a>
                     <p> <?= $records['fdescription'];?> </p>
                     <span class="copy-right">&copy; <?= date('Y')?> Wageni Hospitality Solutions Ltd.</span>
                  </div>
                  <div class="col-xs-12 col-sm-6  spacer-mobile">
                     <address class="location clearfix">
                        <a href="javascript:void(0)" class="icon-location"> 
                        <i class="svg-shape"><img src="<?= $website;?>svg/pin.svg" alt="" class="svg" /></i>
                        </a>
                        <div class="address-info">
                           <span class="attorney-theme">Wageni</span>
                           <span><?= $records['faddress']; ?></span>
                        </div>
                     </address>
                     <a href="tel:<?= $records['fphone']; ?>" class="contact-number clearfix">
                     <i class="icon-call svg-shape"><img src="<?= $website;?>svg/phone.svg" alt="" class="svg" /></i>
                     <span><?= $records['fphone']; ?></span>
                     </a>
                     <a href="mailto:<?= $records['femail']; ?>" class="contact-number mail-info clearfix">
                     <i class="icon-call icon-msg svg-shape"><img src="<?= $website;?>svg/mail.svg" alt="" class="svg" /> </i>
                     <span><?= $records['femail']; ?></span> 
                     </a>
                     <ul class="media-listing">
                        <li><a href="http://twitter.com" target="_blank" class="fa fa-twitter">&nbsp;</a></li>
                        <li><a href="http://facebook.com" target="_blank" class="fa fa-facebook">&nbsp;</a></li>
                        <li><a href="http://linkedin.com" target="_blank" class="fa fa-linkedin-square">&nbsp;</a></li>
                        <li><a href="http://pinterest.com" target="_blank" class="fa fa-pinterest">&nbsp;</a></li>
                        <li><a href="http://instagram.com" target="_blank" class="fa fa-instagram">&nbsp;</a></li>
                        <li><a href="http://youtube.com" target="_blank" class="fa fa-youtube">&nbsp;</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="requst" tabindex="-1" role="dialog" aria-labelledby="myDemo">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myDemo">Schedule a live demo with us</h4>
            </div>
            <p class="statusMsg"></p>
            <div id="error"></div>
            <form class="form-horizontal" id="company_demo" method="post" novalidate>
               <div class="modal-body">
                  <div class="row form-group">
                     <div class="col-sm-6">
                        <label>First Name</label>
                        <input type="text" class=" form-control" name="fname" class="fname" id="fname">
                     </div>
                     <div class="col-sm-6">
                        <label>Last Name</label>
                        <input type="text" class=" form-control" name="lname" class="lname" id="lname">
                     </div>
                  </div>
                  <div class="row form-group">
                     <div class="col-sm-6">
                        <label>Email</label>
                        <input type="text" class=" form-control" name="company_email" id="company_email">
                     </div>
                     <div class="col-sm-6">
                        <label>Phone</label>
                        <input type="text" class=" form-control" name="company_phone" id="company_phone" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                     </div>
                  </div>
                  <div class="row form-group">
                     <div class="col-sm-12">
                        <label>Company Name</label>
                        <input type="text" class=" form-control" name="company_name" id="company_name">
                     </div>
                  </div>
                  <div class="row form-group">
                     <div class="col-sm-12">
                        <label>Company Website URL</label>
                        <input type="text" class=" form-control" name="company_url" id="company_url">
                     </div>
                  </div>
                  <div class="row form-group">
                     <div class="col-sm-6">
                        <label>Country</label>
                        <select class=" form-control" name="country" id="country">
                           <option value="" >Please Select</option>
                           <option value="1">India</option>
                           <option value="2">Canada</option>
                           <option value="3">US</option>
                        </select>
                     </div>
                     <div class="col-sm-6">
                        <label>Package</label>
                        <select class=" form-control" name="package" id="package">
                           <option value="" >Please Select</option>
                           <?php   $packge_demo = getPackages();
                              foreach ($packge_demo as $key => $value) {?>
                           <option value="<?= $value['package_id'] ;?>"><?= $value['package_name'] ;?></option>
                           <?php } ?>
                        </select>
                     </div>
                  </div>
                  <div class="row form-group">
                     <div class="col-sm-6">
                        <label>How many rooms does your property have?</label>
                        <input type="text" class=" form-control" name="hvroom" id="hvroom"  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="">
                     </div>
                     <div class="col-sm-6">
                        <label>Category </label>
                        <select class=" form-control" name="category" id="category">
                           <option value="" >Please Select</option>
                           <?php  $categories = getCategory(); 
                              foreach ($categories as $key => $value) { ?>
                           <option value="<?= $value['cat_id'];?>"><?= $value['category_name'];?></option>
                           <?php } ?>
                        </select>
                     </div>
                  </div>
                  <div class="row form-group">
                     <div class="col-sm-12">
                        <label>Do you use a property management system (PMS)?</label>
                        <div class="radio-btn">
                           <input class="form-check-input" id="pmsno" name="pms" type="radio" value="0" checked="">
                           <label for="pmsno">No</label>
                           <input class="form-check-input" id="pmsyes" name="pms" type="radio" value="1" ><label  for="pmsyes">Yes</label> 
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <input type="submit" class="btn btn-default"  id="btn-submit" value="Submit"></input>
               </div>
            </form>
         </div>
      </div>
   </div>
   <a id="back-to-top" href="javascript:void(0)" class="back-to-top" role="button" ><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
</footer>
</div>
<!-- <?php //pr($_SESSION);?> -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script src="<?= $admin;?>dist/js/jquery.rating-stars.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
<script src="<?= $website; ?>js/less.js"></script>
<!-- revolution Js -->
<script src="<?= $website; ?>js/jquery.themepunch.tools.min.js"></script>
<script src="<?= $website; ?>js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= $website; ?>js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="<?= $website; ?>js/owl.carousel.min.js"></script>
<!-- <script src="<?//= $website; ?>js/gmap.js"></script> -->
<!-- <script src="<?//= $website; ?>js/vailidation.js"></script> -->
<script src="<?= $website; ?>js/site.js"></script>
<script>
   $('document').ready(function()	{
   
   $(window).scroll(function () {
   	if ($(this).scrollTop()) { $('#back-to-top').fadeIn(); }
   	else { $('#back-to-top').fadeOut(); }
   });
   
   $('#back-to-top').click(function () {
   	$('body,html').animate({ scrollTop: 0 }, 800);
   });
   
   $("#clints-list").owlCarousel({
   	loop:true,
   	autoPlay : 2500,
   	stopOnHover : true,
   	nav : false, 
   	pagination:false,
   	slideSpeed : 1000,
   
   	responsive:{ 
   		0:{items:1},
   		600:{items:3},
   		1000:{items:5}
   	}
   });
   
   
   	/******************************************** Start form validation  ************************************************/
   	$("#company_demo").validate({
   		rules:
   		{
   			fname:{
   				required: true,
   				minlength: 3,
   				maxlength:15
              },
              lname:{
              	required: true,
              	minlength: 3,
              	maxlength:15
              },
              company_name:{
              	required: true,
              	minlength: 3
              },
              company_phone:{
              	required:true,
              	minlength:9,
              	maxlength:13,
   			//customphone:true,
   			number: true
   		},
   		hvroom:{
   			required: true,
   			number: true,
   			minlength:1,
   			maxlength:3,
               //alphanumeric : true,
               //noSpace: true,
               maxlength: 50
           },
           company_email:{required: true,email: true},
           country:{ required: true },
           package:{ required: true },
           category:{ required: true },
           pms:{ required: true },
           company_url:{ required: true },
       },
       messages:
       {
       	fname:{
       		required: "Please enter First Name",
       		minlength: "First Name Needs To Be Minimum of 3 Characters",
       		maxlength: "First Name Needs To Be Maximum of 15 Characters"
       	},
       	lname:{
       		required: "Please enter Last Name",
       		minlength: "Last Name Needs To Be Minimum of 3 Characters",
       		maxlength: "Last Name Needs To Be Maximum of 15 Characters"
       	},
       	company_name:{
       		required: "Please enter Company Name",
       		minlength: "Company Name Needs To Be Minimum of 3 Characters"
       	},
       	company_phone:{
       		required: "Please enter Mobile No.",
       		minlength: "Mobile No. Needs To Be Minimum of 9 digits",
       		maxlength: "Please enter no more than 13 digits."
       	},
       	company_email: "Please enter a Valid Email",
       	hvroom:{
       		required: "Please enter Room No.",
       		minlength: "Room No. Needs To Be Minimum of 1 digits",
       		maxlength: "Please enter no more than 3 digits."
       	}, 
       	country: "Please Select Country",     
       	package: "Please Select Package",     
       	category: "Please Select Category",     
       	pms: "Please Select Pms",     
       	company_url: "Please Website Url"
       },
       submitHandler: submitdemoForm
   });
   
   
   
   
   	function submitdemoForm(){
   		var data = $("#company_demo").serialize();
   		$.ajax({
   			type : 'POST',
   			url  : '<?= base_url();?>Home/company_register',
   			data : data,
              success :  function(data)
              {
              	var trimresult = $.trim(data) 
              	if(trimresult==1){
              		$("#error").fadeIn(1000, function(){
              			$("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Sorry email already taken !</div>');
              			$("#btn-submit").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Create Account');
              		});
              	}else if(trimresult=="registered"){                	
              		$("#btn-submit").html('Signing Up');                            
              		$("#statusMsg").html('Registered Successfully');  
                      $('#myDemo').modal('hide');   
                      window.location.href="<?= base_url();?>";
                  }else{
                  	$("#error").fadeIn(1000, function(){
              		$("#error").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+trimresult+' !</div>');
              		$("#btn-submit").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Create Account');
                  	});
                  }
              }
          });
   		return false;
   
   	}
   	/******************************************** End submit after validation true ***********************************/
   
   });
</script>
<!--  **************************** script for forgot password for admin module **************************** -->
<script type="text/javascript">
   $('document').ready(function(){
     $("#forgot-form").validate({
         rules:{
         	lost_email:{required: true,email: true},
         },
         messages:{user_email: "Please enter a Valid Email"  },
         submitHandler: forgotForm
     });
   
     function forgotForm() {
         var data = $("#forgot-form").serialize();
         $.ajax({
   
             type : 'POST',
             url  : '<?= base_url();?>Home/ForgotPassword',
             data : data,
             beforeSend: function(){
                 $("#forgoterror").fadeOut();
                 $("#btn-forgot").html('<span class="fa fa-refresh fa-spin"></span> &nbsp; waiting ...');
             },
             success :  function(data){
               var trimforgotresult = $.trim(data) 
                 if(trimforgotresult== "err"){
                     $("#forgoterror").fadeIn(1000, function(){
                         $("#forgoterror").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Sorry Email not found !</div>');
                         //$("#btn-forgot").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Create Account');
                     });
                 }
                 else if(trimforgotresult=="sent"){
                     $("#btn-forgot").html('Forgot');                            
                     $("#statusFrogotMsg").html('Password Sent Successfully');           
   
                 }else{
                     $("#err").fadeIn(1000, function(){
                         $("#err").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+trimforgotresult+' !</div>');
                         $("#btn-forgot").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Forgot Password');
                     });
                 }
             }
         });
         return false;
     }
     /* form submit */
   });
</script>
</body>
</html>