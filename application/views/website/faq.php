<?php include "header.php";?>
<section id="slider" class="banner-three">
   <div class="about-banner">
      <div class="container banner-text">
         <div class="row">
            <div class="col-xs-12">
               <h1><?php print_r($pageTitle);?></h1>
            </div>
         </div>
      </div>
   </div>
</section>
<div id="content">
   <section class="faq-listing">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <h2><?php print_r($pageHeading);?></h2>
            </div>
         </div>
         <div class="row faq-group">
            <?php foreach ($allfaq as $key => $faq) { ?>
            <div class="col-xs-12 col-sm-6 faq-listing-list">
               <span class="question-label ">Q</span>
               <div class="question-block ">
                  <h3><?= $faq->faq_que;?></h3>
                  <p><?= $faq->faq_ans;?></p>
               </div>
            </div>
            <?php } ?>
         </div>
      </div>
   </section>
   <section class="consulation faq-comment">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-3 animate-effect">
               <span class="consult-info">Ask your Questions :</span>
            </div>
            <div class="col-xs-12 col-md-9 col-sm-8  animate-effect">
               <input type="text" placeholder="Type your question here :" value="" class="question-input  animate-effect" />
               <input type="submit" value="go" class="go-btn animate-effect"/>
            </div>
         </div>
      </div>
   </section>
</div>		
<?php include "footer.php";?>