<section class="client animate-effect">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2> Clients </h2>
				<div class="clints-list" >
					<ul class="client-listing owl-carousel" id="clints-list" class="owl-carousel">
						<?php foreach($alldata as $key => $logo) {?>
							<li >
								<span>
									<?php if($logo['client_logo'] == true){?>
										<img src="<?= base_url();?>uploads/<?=$logo['client_logo'];?>" alt="" title=""/>
									<?php } else {?>
										<img src="<?= base_url();?>globalassets/website/img/client-1.png" alt="" title=""/>
									<?php } ?>
								</span>
							</li>
						<?php }?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>				