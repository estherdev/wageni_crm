<!--  <?php //echo "<pre/>"; print_r($allquestions); //die; ?>   -->
<?php include "header.php";?>
<style>
  .no-result { width: 100%; min-height: 400px; text-align: center;}

  .netPromoter2 table {width: 100%; text-align: center;}
  .netPromoter2 table tr td {padding: 2px;}
  .npNum2 {width: 100%; padding: 5px; background-color: #333; color: #fff;}
  .npNum2 label {width: 100%; display: block; color: #fff !important; font-size: 18px;}
  .npText2 {
    position: relative;
    text-transform: uppercase;
    font-weight: 700;
    font-size: 18px;
    padding: 5px;
    color: #ff7273;
}
  .npNum2.npdetract, .npNum2.npdetract:before, .npText2.npdetract, .npText2.npdetract:before { color: #fff; border-color: #ff7273; background-color: #ff7273;}
  .npNum2.npNeutral, .npNum2.npNeutral:before, .npText2.npNeutral, .npText2.npNeutral:before { color: #fff; border-color: #ffd40a;background-color: #ffd40a;}
  .npNum2.npPromot, .npNum2.npPromot:before, .npText2.npPromot, .npText2.npPromot:before {  color: #fff; border-color: #07c25b;background-color: #07c25b;}
</style>
<div class="contact-page">
   <section id="slider" class="banner-two">
      <div class="about-banner">
         <div class="container banner-text">
            <div class="row">
               <div class="col-xs-12">
                  <h1>Survey Feedback Form</h1>
               </div>
            </div>
         </div>
      </div>
   </section>
   <div id="content" class="feedback-group" style="background-image: url(<?= base_url();?>/globalassets/website/img/bg-feed.jpg);">
      <div class="container">
         <div class="feedback-form">
            <div class="ffHead text-center">
               <h2>Feedback Form</h2>
               <span class="heading-details"><?= $title;?></span>
            </div>
            <?php if(count($allquestions) > 0 ){ ?>
            <form class="submitsurveyguestuser" id="submitsurveyguestuser" method="post" name="submitsurveyguestuser">
               <input type="hidden" name="semail" value="<?= $this->uri->segment(2); ?>">
               <input type="hidden" name="staytype" value="<?= $this->uri->segment(3); ?>">
               <input type="hidden" name="cat" value="<?= $allquestions[0]['cat_id']; ?>">
               <input type="hidden" name="uid" value="<?= $allquestions[0]['user_id']; ?>">
               <input type="hidden" name="busid" value="<?= $allquestions[0]['owner_id']; ?>">
               <input type="hidden" name="subcat" value="<?= $allquestions[0]['sub_cat_id'];?>">
               <input type="hidden" name="process" value="<?= $allquestions[0]['cat_process'];?>">
               <div class="feedback-head">
                  <h3>Evalution Critiria <?//= $ip = $this->input->ip_address();?></h3>
               </div>
               <?php
                  $i=1;
                  foreach ($allquestions as $que => $questions) {
                    $allresult=  $questions['allresult'];
                    switch ($questions['q_type']) {
                  
                     /**********************************************    Radio   **************************************************/
                  
                     case 'qmc':
                     ?>
               <div class="feedbacksec">
                  <div class="feed-back-qus">
                     <h2> <b>Que.<?= $i++;?> </b><span class="questions-servey-feedback"><?= $questions['que']; ?></span></h2>
                  </div>
                  <div class="feed-back-option">
                     <div>
                        <div class="fbo-ans"><strong>Ans:</strong></div>
                        <?php foreach ($allresult as $qmc=> $qmcquestions) {  ?>
                        <?php if(!empty($qmcquestions['bus_que_type'])){?>
                        <div class="form-group">
                           <input class="form-check-input" id="radio-<?= $questions['owner_id'].'-'.$questions['qid'].'-'.  $questions['cat_id'].'-'. $qmcquestions['bus_que_ans_id'] ;?>" name="que-ans[<?= $questions['qid']?>][]"   type="radio"  value="<?= $qmcquestions['bus_que_ans_id']; ?>"> 
                           <label for="radio-<?= $questions['owner_id'].'-'.$questions['qid'].'-'.  $questions['cat_id'].'-'. $qmcquestions['bus_que_ans_id'] ;?>" class="radio-inline">
                           <?php } ?>
                           <?php if(!empty($qmcquestions['bus_que_type'])){ echo ucfirst(trim($qmcquestions['bus_que_type']) ); } ?>
                           </label> 
                        </div>
                        <?php } ?>
                     </div>
                  </div>
               </div>
               <?php 
                  break;
                  
                  /**********************************************    Checkbox   **************************************************/
                  
                  case 'qchb': 
                  
                  ?>
               <div class="feedbacksec">
                  <div class="feed-back-qus">
                     <h2> <b>Que.<?= $i++;?> </b><span class="questions-servey-feedback"><?= $questions['que']; ?></span></h2>
                  </div>
                  <div class="feed-back-option">
                     <div>
                        <div class="fbo-ans"><strong>Ans:</strong></div>
                        <?php foreach ($allresult as $qchb=> $qchbquestions) {     ?> 
                        <?php if(!empty($qchbquestions['bus_que_type'])){?>
                        <input type="checkbox" id="checkbox-<?= $questions['owner_id'].'-'.$questions['qid'].'-'.  $questions['cat_id'].'-'. $qchbquestions['bus_que_ans_id'] ;?>"  name="que-ans[<?= $questions['qid']?>][]"  value="<?= $qchbquestions['bus_que_ans_id']; ?>">  
                        <label for="checkbox-<?= $questions['owner_id'].'-'.$questions['qid'].'-'.  $questions['cat_id'].'-'. $qchbquestions['bus_que_ans_id'] ;?>" class="radio-inline"> 
                        <?php } ?>
                        <?php if(!empty($qchbquestions['bus_que_type'])){ echo ucfirst(trim($qchbquestions['bus_que_type'])) ; } ?></label>   
                        <?php } ?>     
                     </div>
                  </div>
               </div>
               <?php 
                  /**********************************************    Dropdown   **************************************************/
                  
                  case 'qdd':                       
                  ?>
               <div class="feedbacksec">
                  <div class="feed-back-qus">
                     <h2> <b>Que.<?= $i++;?> </b><span class="questions-servey-feedback"><?= $questions['que']; ?></span></h2>
                  </div>
                  <div class="feed-back-option">
                     <div>
                        <div class="fbo-ans"><strong>Ans:</strong></div>
                        <select class="form-control" name="que-ans[<?= $questions['qid']?>][]">
                           <?php foreach ($allresult as $qchb=> $qddquestions) {     ?> 
                           <?php if(!empty($qddquestions['bus_que_type'])){?>
                           <option value="<?= $qddquestions['bus_que_ans_id'] ?>"> <?php if(!empty($qddquestions['bus_que_type'])){ echo ucfirst(trim($qddquestions['bus_que_type'])) ; } ?></option>
                           <?php } ?>
                           <?php } ?>
                        </select>
                     </div>
                  </div>
               </div>
               <?php 
                  break;  
                  
                  /**********************************************    Star   **************************************************/
                  
                  case 'qsr':                       
                  ?>  
               <div class="feedbacksec">
                  <div class="feed-back-qus">
                     <h2> <b>Que.<?= $i++;?> </b><span class="questions-servey-feedback"><?= $questions['que']; ?></span></h2>
                  </div>
                  <div class="feed-back-option">
                     <div class="reting-row rating-stars" id="rating-star<?= $allresult[0]['bus_que_ans_id']?>">
                        <input type="hidden" readonly class="rating-value" name="que-ans[<?= $allresult[0]['bus_que_ans_id'];?>][]" id="que-ans[<?= $allresult[0]['bus_que_ans_id'];?>][]" value="0">
                        <ul>
                           <li class="rating-star"><i class="fa fa-star"></i></li>
                           <li class="rating-star"><i class="fa fa-star"></i></li>
                           <li class="rating-star"><i class="fa fa-star"></i></li>
                           <li class="rating-star"><i class="fa fa-star"></i></li>
                           <li class="rating-star"><i class="fa fa-star"></i></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <?php 
                  break;
                  
                  /**********************************************    Single text Box   **************************************************/
                  /*
                              case 'qst':                       
                              ?>
               <div class="feedbacksec">
                  <div class="feed-back-qus">
                     <h2> <b>Que.<?= $i++;?> </b><span class="questions-servey-feedback"><?= $questions['que']; ?></span></h2>
                  </div>
                  <div class="feed-back-option">
                     <div>
                     </div>
                  </div>
               </div>
               <?php 
                  break;*/
                  
                  /**********************************************    Multiple text Box    **************************************************/
                  
                  /*   case 'qmt':                       
                  ?>  
               <div class="feedbacksec">
                  <div class="feed-back-qus">
                     <h2> <b>Que.<?= $i++;?> </b><span class="questions-servey-feedback"><?= $questions['que']; ?></span></h2>
                  </div>
                  <div class="row">
                     <?php foreach ($allresult as $qchb=> $qmtquestions) { ?> 
                     <?php if(!empty($qddquestions['bus_que_type'])){?>
                     <div class="col-md-6 form-group">
                        <label style="color: #000;"><?php if(!empty($qmtquestions['bus_que_type'])){ echo ucfirst(trim($qmtquestions['bus_que_type'])) ; } ?></label>
                        <input type="text" name="que-ans[<?= $qmtquestions['bus_que_id'] ?>][]"  value="" class="form-control" placeholder=" Please enter text..." >
                        <input type="hidden" name="que-ans[<?= $qmtquestions['bus_que_ans_id'] ?>][]"  value="<?= $qmtquestions['bus_que_ans_id']?>" class="form-control" placeholder=" Please enter text..." >
                     </div>
                     <?php } ?>
                     <?php } ?>
                  </div>
               </div>
               <?php 
                  break;
                  
                  
                  */
                  
                  default:
                  
                  break;
                  
                  }
                  }
                  ?>
               <div class="card">
                  <div class="card-body">
                     <div class="feed-back-qus">
                        <h2> <b>Que. </b><span class="questions-servey-feedback">HOW LIKELY ARE YOU TO RECOMMEND US TO YOUR FAMILY AND FRIENDS ? </span></h2>
                     </div>
                     <div class="feed-back-option">

                        <div>
                         <!--   <div class="fbo-ans"><strong>Ans:</strong></div> -->

                          <div class="netPromoter2">
                            <table cellpadding="" cellspacing="0" border="0">
                              <tbody><tr>
                                <td><div class="npNum2 npdetract"><input type="radio" name="npscore" value="0" id="nps0"> <label for="nps0">0</label></div></td>
                                <td><div class="npNum2 npdetract"><input type="radio" name="npscore" value="1" id="nps1"> <label for="nps1">1</label></div></td>
                                <td><div class="npNum2 npdetract"><input type="radio" name="npscore" value="2" id="nps2"> <label for="nps2">2</label></div></td>
                                <td><div class="npNum2 npdetract"><input type="radio" name="npscore" value="3" id="nps3"> <label for="nps3">3</label></div></td>
                                <td><div class="npNum2 npdetract"><input type="radio" name="npscore" value="4" id="nps4"> <label for="nps4">4</label></div></td>
                                <td><div class="npNum2 npdetract"><input type="radio" name="npscore" value="5" id="nps5"> <label for="nps5">5</label></div></td>
                                <td><div class="npNum2 npdetract"><input type="radio" name="npscore" value="6" id="nps6"> <label for="nps6">6</label></div></td>
                                <td><div class="npNum2 npNeutral"><input type="radio" name="npscore" value="7" id="nps7"> <label for="nps7">7</label></div></td>
                                <td><div class="npNum2 npNeutral"><input type="radio" name="npscore" value="8" id="nps8"> <label for="nps8">8</label></div></td>
                                <td><div class="npNum2 npPromot"><input type="radio" name="npscore" value="9" id="nps9"> <label for="nps9">9</label></div></td>
                                <td><div class="npNum2 npPromot"><input type="radio" name="npscore" value="10" id="nps10"> <label for="nps10">10</label></div></td>
                              </tr>
                           <!--    
                              <tr>
                                <td colspan="6"><div class="npText2 npdetract">Detractors</div></td>
                                <td colspan="2"><div class="npText2 npNeutral">Neutrals</div></td>
                                <td colspan="2"><div class="npText2 npPromot">Promoters</div></td>
                              </tr> -->
                            </tbody></table>
                            
                          </div>

                           <!-- <div class="form-group">
                              <input class="form-check-input" id="cr1" name="npscore" type="radio" value="1"> 
                              <label for="cr1" class="radio-inline"><strong>Will not recommend</strong></label>
                           </div>
                           <div class="form-group">
                              <input class="form-check-input" id="cr2" name="npscore" type="radio" value="2"> 
                              <label for="cr2" class="radio-inline"><strong>Not sure</strong></label>
                           </div>
                           <div class="form-group">
                              <input class="form-check-input" id="cr3" name="npscore" type="radio" value="3"> 
                              <label for="cr3" class="radio-inline"><strong>Very likely</strong></label>
                           </div>
                           <div>
                              <input class="form-check-input" id="cr4" name="npscore" type="radio" value="4"> 
                              <label for="cr4" class="radio-inline"><strong>Will most definitely recommend</strong></label>
                           </div> -->
                        </div>
                     </div>
                  </div>
               </div>
               <div class="feed-sbmit text-center">
                  <button type="button" class="btn feed-btn" id="btnsubmit">SUBMIT</button>
                  <div class="success_message" id="success_message" > </div>
               </div>
            </form>
            <?php } else { ?>              
            <div class="no-result"><img src="<?= base_url();?>/globalassets/admin/dist/img/no-record.jpg" alt="No Record Found"></div>
            <?php  } ?>   
         </div>
      </div>
   </div>
</div>
<?php include "footer.php";?>
<script>
   var ratingOptions = {
     selectors: {
       starsSelector: '.rating-stars',
       starSelector: '.rating-star',
       starActiveClass: 'is--active',
       starHoverClass: 'is--hover',
       starNoHoverClass: 'is--no-hover',
       targetFormElementSelector: '.rating-value'
     }
   };
   
   $(".rating-stars").ratingStars(ratingOptions);
   
   
   $(function () {
   
     $('#btnsubmit').click(function(){
       $('#message_box').html('');
       $('#success_message').html('');
       $('.error').html('');
       var errorMessage=[];
   
       if (typeof $("input[name='npscore']:checked").val() === "undefined") {
          errorMessage['npscore']='required';
       }
      // return false;
   
     //var errorLength=Object.keys(errorMessage).length;
    // if(errorLength>0){
      /* for (var key in errorMessage) {
         if (errorMessage.hasOwnProperty(key)) {           
           $('#'+key).addClass('error');
           $('#'+key+'_error').html(errorMessage[key]);
         }
       }
       
       $('#success_message').html('<div class="alert alert-danger">There is error in submitting form!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');*/
       
   
    // }else{
   
     var form = $("#submitsurveyguestuser")[0];
     var formData = new FormData(form);
   
     $.ajax({
       type: "POST",
       url: '<?php echo base_url('guest-save-survey-question')?>',
       data: formData,
       async : false,
       cache : false,
       contentType : false,
       processData : false,
       success: function(ajaxresponse){
         response=JSON.parse(ajaxresponse);
         if(!response['status']){
           $.each(response, function(key, value) {
             $('#'+key).addClass('error');
             $('#'+key+'_error').html(value);
           });
           $('#success_message').html('<div class="alert alert-danger">There is error in submitting form!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
   
         }else{
           $("#submitsurveyguestuser")[0].reset();                
   
             $('#success_message').html('<div class="alert alert-success">'+response['message']+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            setTimeout(function(){  window.location = '<?= base_url('success');?>';  }, 1500);           
   
          }  
        }
      });  
     //}
   });
   
   });
    
</script>