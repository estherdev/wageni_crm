<section class="about-us animate-effect">
   <div class="container">
      <div class="row">
         <div class="col-xs-12 section-header">
            <h2> about us </h2>
            <span class="practice-desp">- When an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>
         </div>
      </div>
   </div>
   <div role="tabpanel">
      <div class="tabing-section">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                   <ul class="nav nav-tabs" role="tablist">
                     <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Milestone</a></li>
                     <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Team</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="home">
                     <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-5 block-box ">
                           <h3 class="h3"> qualified attorneys </h3>
                           <p >Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                           <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                           <div class="moment zoom">
                              <span> January 2015 </span>
                              <h2> at present moment </h2>
                              <figure class="">
                                 <img src="<?= base_url();?>globalassets/website/img/moment-img.jpg" alt="" title=""/>
                              </figure>
                              <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
                              <a class="more-btn" href="javascript:void(0)"> Know More <i class="fa fa-chevron-right"></i></a>
                           </div>
                        </div>
                        <div class="seprater col-xs-12 col-sm-2 col-md-2 ">
                           <span>present</span>
                           <span class="small-circle"></span>
                           <span class="large-circle"></span>
                           <a href="javascript:void(0)" class="arrow-down"> <i class="fa fa-chevron-down"></i> <i class="fa fa-chevron-down"></i> </a>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 attroney-base ">
                           <h3 class="h3"> a long line of attorneys </h3>
                           <p>Excepteur sint occaecat cupidatat non proident, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                           <div class="moment moment-right  zoom">
                              <span> Deember 2014 </span>
                              <h2> Heading Title here </h2>
                              <figure class=""><img src="<?= base_url();?>globalassets/website/img/heading-about-us.jpg" alt="" title=""/></figure>
                              <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
                              <a class="more-btn" href="#"> Know More <i class="fa fa-chevron-right"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="profile">
                     <div class="row">
                        <?php foreach($teamdata as $key => $value) { ?>
                        <div class="col-xs-12 col-sm-6 col-md-3 attorney-listing  zoom">
                           <figure class="">
                              <?php if ($value->image==true) { ?>
                              <img src ="<?= base_url();?>uploads/<?= $value->image; ?>" alt="" title="" />
                              <?php } else {?>
                              <img src="<?= base_url();?>globalassets/website/img/Attorney-1-img.jpg" alt="" title=""/>
                              <?php } ?>
                           </figure>
                           <h2 class="at-h2"><?= $value->name;?> <span><?= $value->designation;?></span></h2>
                           <p><?= $value->content;?></p>
                           <a class="more-btn" href="javascript:void(0)"> Know More <i class="fa fa-chevron-right"></i></a>
                           <ul class="media-listing ">
                              <li><a href="http://twitter.com" target="_blank" class="fa fa-twitter">&nbsp;</a></li>
                              <li><a href="http://facebook.com" target="_blank" class="fa fa-facebook">&nbsp;</a></li>
                              <li><a href="http://linkedin.com" target="_blank" class="fa fa-linkedin-square">&nbsp;</a></li>
                              <li><a href="http://pinterest.com" target="_blank" class="fa fa-pinterest">&nbsp;</a></li>
                              <li><a href="http://instagram.com" target="_blank" class="fa fa-instagram">&nbsp;</a></li>
                              <li><a href="http://youtube.com" target="_blank" class="fa fa-youtube">&nbsp;</a></li>
                           </ul>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>