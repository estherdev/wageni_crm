 <?php include "header.php";?>
  <div class="contact-page">
<section id="slider" class="banner-two">
   <div class="about-banner">
      <div class="container banner-text">
         <div class="row">
            <div class="col-xs-12">
               <h1>Package Details</h1>
            </div>
         </div>
      </div>
   </div>
</section>
  <div id="content">

    <section class="contact-form-form">
      <div class="container">
         <div id="success" >
            <div role="alert" class="alert alert-success">
            </div>
         </div>
         <div class="">
               <h3>Package Details</h3>

               <div class="card card-pricing text-center px-3 mb-4 blue">
                  <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm"><?= ucfirst($packages['package_name']) ?></span>
                  <div class="bg-transparent card-header pt-4 border-0">
                     <div class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="<?= $packages['package_price'];?>">$<span class="price"><?= $packages['package_price'];?></span><span class="h6 text-muted ml-2">/ per month</span></div>
                  </div>
                  <div class="card-body pt-0">
                     <ul class="list-unstyled mb-4">
                        <li>Up to 5 users</li>
                        <li>Basic support on Github</li>
                        <li>Monthly updates</li>
                        <li>Free cancelation</li>
                     </ul>
                     
                  </div>
               </div>
         </div>
         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">
               <h2>Fill form here</h2>
               <form action="" class="form-horizontal" id="packagebuy" method="post" novalidate>
               <div class="modal-body">
                  <input type="hidden" name="packageid" value="<?= $packages['package_id'] ?>" />
                  <div class="row form-group">
                     <div class="col-sm-3">
                        <label>First Name</label>
                        <input type="text" class=" form-control" name="fname" class="fname" id="fname">
                        <?php echo "<b style='color:red;'>" .form_error('fname') ."</b>" ;?>
                     </div>
                     <div class="col-sm-3">
                        <label>Last Name</label>
                        <input type="text" class=" form-control" name="lname" class="lname" id="lname">
                        <?php echo "<b style='color:red;'>" .form_error('lname') ."</b>" ;?>
                     </div>
                     <div class="col-sm-3">
                        <label>Email</label>
                        <input type="email" class=" form-control" name="company_email" id="company_email">
                        <?php echo "<b style='color:red;'>" .form_error('company_email') ."</b>" ;?>
                     </div>
                     <div class="col-sm-3">
                        <label>Phone</label>
                        <input type="text" class=" form-control" name="company_phone" id="company_phone" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                        <?php echo "<b style='color:red;'>" .form_error('company_phone') ."</b>" ;?>
                     </div>
                  </div>
                  <div class="row form-group">
                     <div class="col-sm-3">
                        <label>Company Name</label>
                        <input type="text" class=" form-control" name="company_name" id="company_name">
                        <?php echo "<b style='color:red;'>" .form_error('company_name') ."</b>" ;?>
                     </div>
                     <div class="col-sm-3">
                        <label>Company Website URL</label>
                        <input type="text" class=" form-control" name="company_url" id="company_url">
                        <?php echo "<b style='color:red;'>" .form_error('company_url') ."</b>" ;?>
                     </div>
                     <div class="col-sm-3">
                        <label>Country</label>
                        <select class=" form-control" name="country" id="country">
                           <option value="" >Please Select</option>
                           <?php $getCountries= getCountries();
                           foreach ($getCountries as $key => $countries) {  ?>
                           <option value="<?= $countries['id']?>"><?= $countries['name']?></option>
                           <?php } ?>
                         </select>
                        <?php echo "<b style='color:red;'>" .form_error('country') ."</b>" ;?>
                     </div>
                 <!--     <div class="col-sm-3">
                        <label>Package</label>
                        <select class=" form-control" name="package" id="package">
                           <option value="" >Please Select</option>
                           <?php   $packge_demo = $this->db->get("packages")->result();
                           foreach ($packge_demo as $key => $value) {?>
                              <option value="<?= $value->package_id ;?>"><?= $value->package_name ;?></option>
                           <?php } ?>
                        </select>
                     </div> -->
                  </div>
                  <div class="row form-group">
                     <div class="col-sm-3">
                        <label>How many rooms does your property have?</label>
                        <input type="text" class=" form-control" name="hvroom" id="hvroom"  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="">
                        <?php echo "<b style='color:red;'>" .form_error('hvroom') ."</b>" ;?>
                     </div>
                     <div class="col-sm-3">
                        <label>Category </label>
                        <select class=" form-control" name="category" id="category">
                           <option value="" >Please Select</option>
                           <?php  $categories = $this->db->get("category")->result(); 
                           foreach ($categories as $key => $value) {                         
                              ?>
                              <option value="<?= $value->cat_id ;?>"><?= $value->category_name ;?></option> 
                           <?php } ?>
                        </select>
                        <?php echo "<b style='color:red;'>" .form_error('category') ."</b>" ;?>
                     </div>
                     <div class="col-sm-6">
                        <label>Do you use a property management system (PMS)?</label>
                        <div class="custom-control custom-radio radio-btn">
                           <input class="custom-control-input" id="pmsno" name="pms" type="radio" value="0" >
                           <label class="custom-control-label" for="pmsno">No</label>
                           <input class="custom-control-input" id="pmsyes" name="pms" type="radio" value="1" >
                           <label  class="custom-control-label" for="pmsyes">Yes</label> 
                        </div>
                     </div>
                  </div>
                
               </div>
               <div class="modal-footer">
                  <input type="reset" class="btn btn-danger"  value="Clear"></input>
                  <input type="submit" class="btn btn-success"  name="submitpackageorder" id="btn-submit" value="Submit"></input>
               </div>
            </form>
            </div>
        
         </div>
      </div>
   </section>
   <!--contact-form-form Section End Here -->

   <!-- consulation Section Start Here -->
   <section class="consulation">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-2 col-md-2 col-md-offset-1 animate-effect">
               <span class="consult-info">Get a free legal
               consulation</span>
            </div>
            <div class="col-xs-12 col-md-6 col-sm-4 animate-effect">
               <a href="tel:+8758657443" class="contact-consult"><i class="icon-consult fa fa-mobile"></i>+8758 657 443</a>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 animate-effect">
               <span class="consult-info week-time"><span>7 day a week </span> from 8 am to 8 pm</span>
            </div>
         </div>
      </div>
   </section>
   <!-- consulation Section Start Here -->
</div>
</div>
<!--content Section End Here -->
<!--footer Section Start Here -->
<?php include "footer.php";?>
<!--footer Section End Here -->