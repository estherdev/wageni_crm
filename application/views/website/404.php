<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Wageni</title>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<!-- google fonts -->
		<link href='http://fonts.googleapis.com/css?family=Exo:400,300,300italic,500,400italic,500italic,600,600italic,700,700italic%7CMerriweather:400,300,300italic,400italic,700,700italic%7COpen+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800%7CMontserrat:400,700%7CLibre+Baskerville:400,700' rel='stylesheet' type='text/css'>
		<!-- Bootstrap -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
		<link href="assets/css/global.css" rel="stylesheet">

		<link href="assets/css/settings.css" rel="stylesheet">

		<link href="assets/css/style.css" rel="stylesheet">
		<link href="assets/css/responsive.css" rel="stylesheet">


		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body>
		<!--Wrapper Section Start Here -->
		<div id="wrapper" class="blog-page">

			<!--header Section Start Here -->
			<header id="header" class="normal">
				<div class="primary-header">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-2 media-header">
								<ul class="media-listing clearfix">
									<li>
										<a href="http://twitter.com" target="_blank" class="fa fa-twitter">&nbsp;</a>
									</li>
									<li>
										<a href="http://facebook.com" target="_blank" class="fa fa-facebook">&nbsp;</a>
									</li>
									<li>
										<a href="http://linkedin.com" target="_blank" class="fa fa-linkedin-square">&nbsp;</a>
									</li>
									<li>
										<a href="http://pinterest.com" target="_blank" class="fa fa-pinterest">&nbsp;</a>
									</li>
									<li>
										<a href="http://instagram.com" target="_blank" class="fa fa-instagram">&nbsp;</a>
									</li>
									<li>
										<a href="http://youtube.com" target="_blank" class="fa fa-youtube">&nbsp;</a>
									</li>
								</ul>

							</div>

							<div class="col-xs-12 col-sm-8 col-md-10">
								<div class="primary-right-block">
									<div class="consultation">
										<a href="#" data-toggle="modal" data-target="#requst" ><i class="svg-shape icon-enevlope"> <img src="assets/svg/FreeConsultation.svg" alt="" class="svg" /> </i><span>Request a  Free Consultation <i class="fa fa-chevron-right"></i></span> </a>

									</div>
									<button class="search">
										<i class="fa fa-search"></i>
									</button>
									<div class="header-search-box">
										<input type="text">
									</div>
								</div>

							</div>

						</div>

					</div>
				</div>

				<div class="navigation-header">
					<div class="container">
						<div class="row">
							<a href="index.html" class="col-xs-12 col-sm-4 col-md-4 logo"><img src="assets/img/logo.jpg" alt="" title=""/></a>
							<div class="col-xs-12 col-sm-10 col-md-6 navigation">
								<nav class="navbar navbar-default">

									<!-- Brand and toggle get grouped for better mobile display -->
									<div class="navbar-header">
										<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
											<span class="sr-only">Toggle navigation</span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
										</button>
									</div>

									<!-- Collect the nav links, forms, and other content for toggling -->
									<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
										<ul class="nav navbar-nav">
											<li>
												<a href="index.html">Home </a>
											</li>

											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> About <span class="caret"></span></a>
												<ul class="dropdown-menu" role="menu">
													<li>
														<a href="about-us.html">About-Us</a>
													</li>
													<li>
														<a href="clients.html">clients</a>
													</li>
													 <li>
														<a href="history.html">Milestone</a>
													</li>
											

													
												</ul>

											</li>

											
													

											
                                            <li>
														<a href="testimonials.html">testimonials</a>
													</li>
                                                   
                                            

											<li>
												<a href="contact-us.html"> Contacts</a>
											</li>
                                             <li>
														<a href="package.html">Packages</a>
													</li>
                                            <li>
														<a href="faq.html">Faq</a>
													</li>
                                                    <li>
														<a href="blog.html">blog</a>
													</li>

										</ul>

									</div><!-- /.navbar-collapse -->

								</nav>

							</div>
							<a href="tel:18881234567" class="col-xs-12 col-md-2 contact-number clearfix"> <i class="icon-call svg-shape"> <img src="assets/svg/phone.svg" alt="" class="svg" /></i><span>1 (888) 123 4567</span> </a>

						</div>

					</div>

				</div>

			</header>
			<!--header Section End Here -->

			<!--slider Section Start Here -->
			<section id="slider" class="banner-one">
				<div class="about-banner">
					<div class="container banner-text">
						<div class="row">
							<div class="col-xs-12">
								<h1>404</h1>
							</div>
						</div>
					</div>

				</div>

			</section>
			<!--slider Section End Here -->

			<!--content Section Start Here -->
			<div id="content">
				<!--page-not-found Section Start Here -->
				<section class="page-not-found">
					<div class="container">
						<div class="row">
							<span class="animate-effect bin"> <img src="assets/img/delete-box.png" alt="" title=""/> <i class="fa fa-times"></i> </span>
							<span class="error-value animate-effect">404</span>
							<span class="error-msg animate-effect"> <span > oops </span> this page cannot be found </span>

						</div>

					</div>

				</section>

				<!--page-not-found Section End Here -->

			</div>
			<!--content Section End Here -->

			<!--footer Section Start Here -->
			<footer id="footer">
				<div class="top-footer">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-9 col-md-8">
								<nav class="navbar navbar-default">
									<!-- Collect the nav links, forms, and other content for toggling -->
									
								</nav>

							</div>
							<div class="consultation">
								<a href="#"  data-toggle="modal" data-target="#requst" ><i class="svg-shape icon-enevlope"> <img src="assets/svg/FreeConsultation.svg" alt="" class="svg" /> </i><span>Request a  Free Consultation <i class="fa fa-chevron-right"></i></span> </a>

							</div>

						</div>

					</div>

				</div>
				<div class="main-footer">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-md-6 block-right"	>
								<div class="row">
									<div class="col-xs-12 col-sm-6  practice-footer tablet-mode">
										<h3> Quik Links </h3>
										<ul class="practice-listing">
											<li>
												<a href="index.html">Home <i class="fa fa-chevron-right"></i></a>
											</li>

											
													<li>
														<a href="about-us.html">About-Us <i class="fa fa-chevron-right"></i></a>
													</li>
													<li>
														<a href="clients.html">clients <i class="fa fa-chevron-right"></i></a>
													</li>
													

											
											
													

											
                                            <li>
														<a href="testimonials.html">testimonials <i class="fa fa-chevron-right"></i></a>
													</li>
                                                    <li>
														<a href="history.html">Milestone <i class="fa fa-chevron-right"></i></a>
													</li>
											
                                            

											

										</ul>

									</div>

									<div class="col-xs-12 col-sm-6 tablet-mode">
										<ul class="practice-listing listing-continue">
											<li>
												<a href="contact-us.html"> Contacts <i class="fa fa-chevron-right"></i></a>
											</li>
                                            <li>
														<a href="faq.html">Faq <i class="fa fa-chevron-right"></i></a>
													</li>
                                                    <li>
														<a href="blog.html">blog <i class="fa fa-chevron-right"></i></a>
													</li>
										</ul>

									</div>

								</div>
							</div>
							<div class="col-xs-12 col-md-6"	>
								<div class="row">
									<div class="col-xs-12 col-sm-6  spacer-mobile">
										<a href="index.html" class="footer-logo"><img src="assets/img/logo.jpg" alt="" title=""/></a>
										<p>
											Attrney is . Excepteur sint occaecat cupidatat non proident.
										</p>
										<p>
											Ut enim ad minim veniam, quis nostrud exercitation ex ullamco laboris nisi ut aliquip ex ea commodo consequat.
										</p>
										
										<span class="copy-right">&copy; 2018 Wageni Hospitality Solutions Ltd.</span>

									</div>

									<div class="col-xs-12 col-sm-6  spacer-mobile">
										<address class="location clearfix">
											<a href="#" class="icon-location"> <i class="svg-shape"> <img src="assets/svg/pin.svg" alt="" class="svg" /></i></a>
											<div class="address-info">
												<span class="attorney-theme">Wageni</span>
												<span> A-2, Sector-63,</span>
												<span>Noida, 201301,</span>
												<span>India</span>
											</div>

										</address>
										<a href="tel:17079217269" class="contact-number clearfix"><i class="icon-call svg-shape"> <img src="assets/svg/phone.svg" alt="" class="svg" /></i><span> +1 707 921 7269</span> </a>
										<a href="mailto:info@wageni.com" class="contact-number mail-info clearfix"><i class="icon-call icon-msg svg-shape"><img src="assets/svg/mail.svg" alt="" class="svg" /> </i><span>info@wageni.com</span> </a>
										<ul class="media-listing">
											<li>
												<a href="http://twitter.com" target="_blank" class="fa fa-twitter">&nbsp;</a>
											</li>
											<li>
												<a href="http://facebook.com" target="_blank" class="fa fa-facebook">&nbsp;</a>
											</li>
											<li>
												<a href="http://linkedin.com" target="_blank" class="fa fa-linkedin-square">&nbsp;</a>
											</li>
											<li>
												<a href="http://pinterest.com" target="_blank" class="fa fa-pinterest">&nbsp;</a>
											</li>
											<li>
												<a href="http://instagram.com" target="_blank" class="fa fa-instagram">&nbsp;</a>
											</li>
											<li>
												<a href="http://youtube.com" target="_blank" class="fa fa-youtube">&nbsp;</a>
											</li>
										</ul>
									</div>

								</div>
							</div>

						</div>
					</div>

				</div>

			</footer>
			<!--footer Section End Here -->

		</div>
		<!--Wrapper Section End Here -->

		<script src="assets/js/jquery-1.11.2.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/less.js"></script>

		<!-- revolution Js -->
		<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
		<!-- revolution Js -->
		<script type="text/javascript" src="assets/js/site.js"></script>
        <div class="modal fade" id="requst" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Schedule a live demo with us</h4>
      </div>
      <div class="modal-body">
        <div class="row  form-group">
         <div class="col-sm-6">
           <label>First Name</label>
           <input type="text" class=" form-control">
         </div>
          <div class="col-sm-6">
           <label>Last Name</label>
           <input type="text" class=" form-control">
         </div>
         </div>
         <div class="row  form-group">
         <div class="col-sm-12">
           <label>Email</label>
           <input type="text" class=" form-control">
         </div>
         </div>
          <div class="row  form-group">
         <div class="col-sm-12">
           <label>Company Name</label>
           <input type="text" class=" form-control">
         </div>
         </div>
          <div class="row  form-group">
         <div class="col-sm-12">
           <label>Company Website URL</label>
           <input type="text" class=" form-control">
         </div>
         </div>
          <div class="row  form-group">
         <div class="col-sm-12">
           <label>Country</label>
           <select  class=" form-control" >
              <option >Please Select</option>
           </select>
         </div>
         </div>
         
          <div class="row  form-group">
         <div class="col-sm-12">
           <label>How many rooms does your property have?</label>
           <textarea  class=" form-control"  rows="5"></textarea>
         </div>
         </div>
          <div class="row  form-group">
         <div class="col-sm-12">
           <label>Do you use a property management system (PMS)?</label>
           <div class="radio-btn">
                    <label class="radio-inline"><input class="form-check-input" id="Loan" name="Loan" type="radio" value="1">Yes </label>
                    <label class="radio-inline"> <input class="form-check-input" id="Loan" name="Loan" type="radio" value="0">No</label>
                </div>
         </div>
         </div>
         
          <div class="row  form-group">
         <div class="col-sm-12">
           <label>Is your property independent, or a member of a group?</label>
           <div class="radio-btn">
                    <label class="radio-inline"><input class="form-check-input" id="Loan" name="Loan" type="radio" value="1">Independent property </label>
                    <label class="radio-inline"> <input class="form-check-input" id="Loan" name="Loan" type="radio" value="0">Member of a group</label>
                </div>
         </div>
         </div>
          <div class="row  form-group">
         <div class="col-sm-12">
           <label>Preferred contact method</label>
           <div class="radio-btn">
                    <label class="radio-inline"><input class="form-check-input" id="Loan" name="Loan" type="radio" value="1">Phone</label>
                    <label class="radio-inline"> <input class="form-check-input" id="Loan" name="Loan" type="radio" value="0">Email</label>
                </div>
         </div>
         </div>
          <div class="row  form-group">
         <div class="col-sm-12">
           <label>How did you hear about GuestRevu?</label>
            <select  class=" form-control" >
              <option >Please Select</option>
           </select>
         </div>
         </div>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary">I want a demo!</button>
      </div>
    </div>
  </div>
</div>
	</body>
</html>