 <?php 
 include "header.php";
  ?>	
  <style type="text/css">
    .login .login-form-wrapper {
    background: rgba(255,255,255,0.85);
    padding: 25px;
    box-shadow: 5px 10px 10px -5px #ccc;
    text-align: center;
    border-radius: 5px;
    min-width: 320px;
    max-width: 400px;
    margin: 0px auto;
}

.login {
    padding: 50px;
}
.login.forget{ padding: 82px 50px; }
.forget .sign{ margin-top:20px; display: block; }
  </style>
}
<div id="content"> 
 <div class="login forget" style="background-image: url(<?= base_url()?>globalassets/admin/wc_bg.jpg);">
       
      <div class="login-form-wrapper">
        <p class="login-box-msg">Forgot Password</p>
        <p class="statusMsg"></p>  
         <div id="error"></div> 
          <form role="form" class="form-signin" method="post" id="forgot-form">
      <!--   <form id="admin-login-form" method="post" action="<?//= base_url()?>/admin/AdminLogin/forgotPassword"> -->
          <div class="form-group has-feedback">
             <p id="lemailerr"></p>
            <input type="email" class="form-control" name="adminEmail" id="adminEmail" placeholder="Enter Email ID"/>
           
          </div>
          <div class="row submitBtn">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat regBtn" name="forgotPassword" id="btn-submit" >Send</button>
              <a class="sign" href="<?= base_url('user-login');?>">Sign In</a>
            </div> 
          </div>
        </form>
      </div> 
    </div> 
</div>			
<?php include "footer.php";?>