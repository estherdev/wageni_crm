<section class="news-testimonial  animate-effect">
   <div class="container">
      <div class="row">
         <div class="col-xs-12 col-md-12 testimonial">
            <h2> TESTIMONIALS </h2>
            <span class="practice-desp"></span>
            <div class="row" id="owl-demo-home">
               <?php foreach($alltestimonials as $key => $value) {?>
               <div class="col-xs-12 user-testimonial ">
                  <div class="testimonial-inner ">
                     <h2 class="h2"> Best attorneys ever! </h2>
                     <p class="user-description">
					      <?php $this->load->helper('text');?>
                     <?php echo word_limiter($value->content, 100);?> 
                     </p>
                     <span class="user-name"> <?=$value->title;?> </span>
                     <div class="fk-stars clearfix" title="5-star">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        <i class="fa fa-star star-bg"></i>
                        <i class="fa fa-star star-bg"></i>
                     </div>
                     <figure class="user-image">
                        <?php if($value->image == true){?>
                        <img src="<?= base_url();?>uploads/<?=$value->image;?>" width="50px" alt="" title=""/>
                        <?php } else {?>
                        <img src="<?= base_url();?>globalassets/website/img/testmonial-user.png" alt="" title=""/>
                        <?php } ?>
                     </figure>
                  </div>
               </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</section>