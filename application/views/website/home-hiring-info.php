<section class="hiring-info animate-effect">
	<div class="container ">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8">
				<h2 class="long-heading"> We are hiring ! Some long heading title will here! </h2>
				<p class="hiring-detail">
					Excepteur sint occaecat cupidatat non proident, sunt in deserunt mollit anim id est laborum.
					Voluptate velit esse cillum dolore eu fugiat nulla pariatur.
				</p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-1">
				<a href="javascript:void(0)" class="more-btn btn-footer"> Know More <i class="fa fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
</section>