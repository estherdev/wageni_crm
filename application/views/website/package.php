<!--header Section Start Here -->
<?php include "header.php";?>
<?php if ($this->session->flashdata('success')) {  ?>
  <hr>  <div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>  <hr>
  <?php }  if ($this->session->flashdata('error')) {  ?>
  <hr>  <div class="alert alert-danger"><?= $this->session->flashdata('error') ?></div>  <hr>
    <?php } ?>
<section id="slider" class="banner-three">
   <div class="about-banner">
      <div class="container banner-text">
         <div class="row">
            <div class="col-xs-12">
               <h1>Packages</h1>
            </div>
         </div>
      </div>
   </div>
</section> 
<div id="content">
    <section class="our-attorney">
      <div class="container mb-5 mt-5">
         <div class="row">
            <div class="col-xs-12">
               <h2>Our Packages</h2>
               <span class="heading-details underline-label"></span>
            </div>
         </div>
         <div class="pricing card-deck flex-column flex-md-row mb-3">
         	<?php foreach ($packages as $key => $value) { ?>
            <div class="card card-pricing text-center px-3 mb-4 blue">
               <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm"><?= ucfirst($value->package_name);?></span>
               <div class="bg-transparent card-header pt-4 border-0">
                  <div class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="<?= $value->package_price;?>">$<span class="price"><?= $value->package_price;?></span><span class="h6 text-muted ml-2">/ per month</span></div>
               </div>
               <div class="card-body pt-0">
                  <ul class="list-unstyled mb-4">
                     <li>Up to 5 users</li>
                     <li>Basic support on Github</li>
                     <li>Monthly updates</li>
                     <li>Free cancelation</li>
                  </ul>
                  <a href="<?php echo base_url('Cart/buypackage/'.$value->package_id); ?>" data-packageid="<?= $value->package_id;?>" class="btn btn-success btn-outline mb-3 packagebuynow">Buy now</a>
                    <!--  <button type="button" data-packageid="<?= $value->package_id;?>" class="btn btn-success btn-outline mb-3 packagebuynow">Buy now</button> -->
               </div>
            </div>
 <?php } ?>
         </div>
      </div>
   </section>
   <section class="our-principles">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <h2>Our Principles</h2>
               <span class="heading-details">- Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 clearfix principles-box animate-effect">
               <div class="index-box animate-effect">
                  <span>1</span>
               </div>
               <div class="principles-detail animate-effect">
                  <h3 class="underline-label">heading here</h3>
                  <p>
                     Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                  </p>
               </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 principles-box animate-effect">
               <div class="index-box animate-effect">
                  <span>2</span>
               </div>
               <div class="principles-detail animate-effect">
                  <h3 class="underline-label">heading here</h3>
                  <p>
                     Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                  </p>
               </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 principles-box animate-effect">
               <div class="index-box animate-effect">
                  <span>3</span>
               </div>
               <div class="principles-detail animate-effect">
                  <h3 class="underline-label">heading here</h3>
                  <p>
                     Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </section> 
   <section class="our-practice-area">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-md-9">
               <h2>Our Practice area</h2>
               <span class="heading-details underline-label">- Lorem ipsum dolor sit amet ctetur.</span>
                <div class="panel-group animate-effect" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default animate-effect">
                     <div class="panel-heading" role="tab" id="headingOne">
                        <h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Family law <i class="fa fa-angle-up"></i> <i class="fa fa-angle-down"></i></a></h3>
                     </div>
                     <div id="collapseOne" class="panel-collapse collapse in clearfix" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body about-us-paragraph animate-effect">
                           <p>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing.
                           </p>
                        </div>
                        <div class="panel-pics animate-effect">
                           <i class="family-law-svg"> <img src="<?= base_url();?>globalassets/website/svg/family-law.svg" alt=""  title="" class="svg"/> </i>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default animate-effect">
                     <div class="panel-heading" role="tab" id="headingTwo">
                        <h3 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Business Law <i class="fa fa-angle-up"></i> <i class="fa fa-angle-down"></i></a></h3>
                     </div>
                     <div id="collapseTwo" class="panel-collapse collapse clearfix" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body about-us-paragraph animate-effect">
                           <p>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing.
                           </p>
                        </div>
                        <div class="panel-pics animate-effect">
                           <i class="bussiness-law-svg"> <img src="<?= base_url();?>globalassets/website/svg/business-law.svg" alt="" title="" class="svg"/> </i>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default animate-effect">
                     <div class="panel-heading" role="tab" id="headingThree">
                        <h3 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> Civil Litigation <i class="fa fa-angle-up"></i> <i class="fa fa-angle-down"></i></a></h3>
                     </div>
                     <div id="collapseThree" class="panel-collapse collapse clearfix" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body about-us-paragraph animate-effect">
                           <p>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing.
                           </p>
                        </div>
                        <div class="panel-pics animate-effect">
                           <i class="civilean-law-svg"> <img src="<?= base_url();?>globalassets/website/svg/civil-litigation.svg" alt=""  title="" class="svg"/> </i>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default animate-effect">
                     <div class="panel-heading" role="tab" id="headingFour">
                        <h3 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Trust &amp; Estates <i class="fa fa-angle-up"></i> <i class="fa fa-angle-down"></i></a></h3>
                     </div>
                     <div id="collapseFour" class="panel-collapse collapse clearfix" role="tabpanel" aria-labelledby="headingFour">
                        <div class="panel-body about-us-paragraph animate-effect">
                           <p>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing.
                           </p>
                        </div>
                        <div class="panel-pics animate-effect">
                           <i class="trust-law-svg"> <img src="<?= base_url();?>globalassets/website/svg/trust-estates.svg" alt=""  title="" class="svg"/> </i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-md-3 our-services">
               <h2>Our Service</h2>
               <span class="heading-details underline-label">- Lorem ipsum dolor sit amet ctetur.</span>
               <ul class="practice-listing animate-effect">
                  <li>
                     <a href="#">SERIOUS CAR CRASH <i class="fa fa-chevron-right"></i></a>
                  </li>
                  <li>
                     <a href="#">BRAIN INJURY <i class="fa fa-chevron-right"></i></a>
                  </li>
                  <li>
                     <a href="#">TRUCK ACCIDENTS <i class="fa fa-chevron-right"></i></a>
                  </li>
                  <li>
                     <a href="#">SEMI-TRUCK COLLISION <i class="fa fa-chevron-right"></i></a>
                  </li>
                  <li>
                     <a href="#">MOTORCYCLE ACCIDENTS <i class="fa fa-chevron-right"></i></a>
                  </li>
                  <li>
                     <a href="#">WRONGFUL DEATH <i class="fa fa-chevron-right"></i></a>
                  </li>
                  <li>
                     <a href="#">SEMI-TRUCK COLLISION <i class="fa fa-chevron-right"></i></a>
                  </li>
                  <li>
                     <a href="#">MOTORCYCLE ACCIDENTS <i class="fa fa-chevron-right"></i></a>
                  </li>
                  <li>
                     <a href="#">WRONGFUL DEATH <i class="fa fa-chevron-right"></i></a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </section>
 </div> 			
<?php include "footer.php";?>
 