
<?php $this->load->view('email_templates/header');?>
<table border="0" cellpadding="10" cellspacing="10" width="600" style="background: #fff;">
    <tr>
        <td align="center" style="font-size: 36px; font-weight: bold; color: #555; letter-spacing: 1px;">Thank you</td>
    </tr>
    <tr><td> <strong><?= ucfirst($titlehead);?>, </strong></td></tr>
   <tr><td> <strong><?= ucfirst($title);?>, </strong></td></tr> 
     <tr><td> Thanks </td></tr>
    <tr><td>The Wageni CRM Team </td></tr>
</table>
<?php $this->load->view('email_templates/footer');?>