
<?php $this->load->view('email_templates/header');?>
<table border="0" cellpadding="10" cellspacing="10" width="600" style="background: #fff;">
    <tr>
        <td align="center" style="font-size: 36px; font-weight: bold; color: #555; letter-spacing: 1px;">Welcome to Wageni CRM.</td>
    </tr>
    <tr><td> Hi <?= ucfirst($name);?>, </td></tr>
    <tr>
        <td> Thanks for using Wageni CRM . Please tell us about your experience in this 2-Minutes survey. Your feedback help us to create a better experience for you and for all of our customers. <a href="<?= base_url('survey-form/'.trim($email1).'/'.trim($quetypename));?>" target="_blank">Click Here</a>
        </td>
    </tr>
    <tr><td> Thanks </td></tr>
    <tr><td>The Wageni CRM Team </td></tr>
</table>
<?php $this->load->view('email_templates/footer');?>