
<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Packages</h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url('view-packages');?>"><i class="fa fa-eye"></i> Back</a></li>
      <li class="active">Update Package</li>
    </ol>
  </section>
     

  <!-- Main content -->
  <section class="content">
     <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <div class="row">

      <div class="col-md-12 col-sm-6 col-xs-12">

        <!-- general form elements -->

        <div class="box box-primary">

          <div class="box-header">

            <h3 class="box-title">Update Package</h3>

          </div><!-- /.box-header -->

          <!-- form start -->

          <form role="form" id="editpack" action="<?= base_url('updatePackages/'.$editpackage[0]->package_id);?>" method="post" >
            <div class="box-body">
              <div class="form-group row">
                <div class="col-md-6">
                  <label for="package_name">Name</label>
                  <input type="text" id="package_name" name="package_name" value="<?= $editpackage[0]->package_name; ?>" class="form-control">
                    <?= form_error('package_name');?>
                </div>

                <div class="col-md-6">
                   <label for="package_price">Price</label>
                  <input type="text" id="package_price" name="package_price" value="<?= $editpackage[0]->package_price; ?>" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                  <?= form_error('package_price');?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6">
                  <label for="allowed_user">Allowed Branch</label>
                  <input type="text" class="form-control" name="allowed_user" value="<?= $editpackage[0]->package_unit; ?>" placeholder="" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"> 
                  <?= form_error('allowed_user');?>
                </div>  

                 
                <div class="col-md-6">
                  <label for="package_validity">Validity (Month)</label>
                     <select class="form-control" id="package_validity" name="package_validity">
                       <option value="">--Select Months--</option>
                       <?php 
                       for($i=1; $i<=12; $i++){ ?>
                              <option value="<?php echo $i ;?>" <?php if($i == $editpackage[0]->package_valid){echo "selected='selected'";} ?>><?php echo $i ;?> Month</option>
                         <?php }
                       ?>
                 </select>
                 <?= form_error('package_validity');?>
                </div>   
            </div>
                 <!--  <input type="text" class="form-control" name="package_validity" value="<?//= $editpackage[0]->package_valid; ?>" placeholder="" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">  -->
              <div class="box-footer text-center">
                <button type="submit" class="btn btn-success">Update</button>
                <a href="<?= base_url('view-packages');?>" class="btn btn-danger" role="button">Back</a>              

               
              </div>
            </form>
          </div><!-- /.box -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <?php include APPPATH . 'views/admin/footer.php'; ?>

