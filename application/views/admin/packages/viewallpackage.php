
<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Packages</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Package</li>
    </ol>
  </section>


  <!-- Main content -->
  <section class="content">
   <?php if($this->session->flashdata("success")):?>
    <div class="alert alert-success"><?= $this->session->flashdata("success");?>
    <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
  </div>
<?php endif;?>
<?php if($this->session->flashdata("failed")):?>
  <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
  <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
</div>
<?php endif;?>
<div class="row">

  <div class="col-md-12 col-sm-6 col-xs-12">

    <!-- general form elements -->

    <div class="box box-primary">

      <div class="box-header">

        <h3 class="box-title">Add Package</h3>

      </div><!-- /.box-header -->

      <!-- form start -->

      <form role="form" id="package" action="<?= base_url('view-packages');?>" method="post" >
        <div class="box-body">
          <div class="form-group row">
            <div class="col-md-6">
              <label for="package_name">Name</label>
              <input type="text" id="package_name" name="package_name" class="form-control" onkeypress="return onKeyValidate(event,alpha);">
              <?= form_error('package_name');?>
            </div>

            <div class="col-md-6">
             <label for="package_price">Price</label>
             <input type="text" id="package_price" name="package_price" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
             <?= form_error('package_price');?>
           </div>
         </div>
         <div class="form-group row">
          <div class="col-md-6">
            <label for="allowed_user">Allowed Branch</label>
            <input type="text" class="form-control" name="allowed_user" placeholder="" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"> 
            <?= form_error('allowed_user');?>
          </div>    
          <div class="col-md-6">
            <label for="package_validity">Validity (Month)</label>
            <select class="form-control" id="package_validity" name="package_validity">
             <option value="">--Select Months--</option>
             <?php 
             for($i=1; $i<=12; $i++){
              echo '<option value="'.$i.'">'.$i.' Month</option>'."\n";
            }
            ?>
          </select>
          <?= form_error('package_validity');?>
        </div> 

      </div><!-- /.box-body -->
      <div class="box-footer text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-danger">Reset</button>              
      </div>
    </form>
  </div><!-- /.box -->
</div><!-- /.content-wrapper -->
</section><!-- /.content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12 ">
      <!-- general form elements -->
      <div class="box ">
        <div class="box-header">
          <h3 class="box-title">Package</h3>
        </div><!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">

         <div class="row">  

          <!-- item -->
          <?php $i= 1; foreach ($Package as $key => $pack) {?>
            <div class="col-md-3 text-center">
              <div class="panel panel-primary panel-pricing ">
                <div class="panel-heading">
                  <i class="fa fa-desktop"></i>
                  <h3><?= ucfirst($pack->package_name); ?></h3>
                </div>
                <div class="panel-body text-center">
                  <p><strong>$<?= $pack->package_price; ?>.00</strong></p>
                </div>
                <ul class="list-group text-center">
                  <li class="list-group-item"><i class="fa fa-check"></i><h4>Only <?php if($pack->package_unit == 1){
                    echo "<strong> ".$pack->package_unit."</strong>  Branch";
                  }else {
                    echo "<strong> ".$pack->package_unit."</strong>  Branches";
                  } ?></h4></li>
                </ul>
                <div class="panel-footer">
                  <a class="btn btn-sm btn-info" href="<?php echo base_url().'editPackages/'.$pack->package_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                  <a class="btn btn-sm btn-danger deletepack"  data-toggle="modal" data-target="#deleteModal<?= $pack->package_id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                </div>
              <div id="deleteModal<?= $pack->package_id ; ?>" class="modal fade">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <!-- dialog body -->
                    <div class="modal-body">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      Are You Sure to <strong>Package</strong> Delete?
                    </div>
                    <!-- dialog buttons -->

                    <div class="modal-footer"><button type="button" class="btn btn-success"  onclick="window.location='<?php echo base_url('deletepackages/'.$pack->package_id ); ?>';">Confirm</button><button type="button" class="btn btn-danger" data-dismiss="modal">Close</button></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /item -->
        <?php } ?>
      </div>
    </div>                
  </div><!-- /.box -->
</div>
</div>                
</section>  
</div><!-- /.content-wrapper -->
<?php include APPPATH . 'views/admin/footer.php'; ?>
<!-- page script -->
<script>
  var alpha = "[ A-Za-z]";
var numeric = "[0-9]"; 
var alphanumeric = "[ A-Za-z0-9]"; 

function onKeyValidate(e,charVal){
    var keynum;
    var keyChars = /[\x00\x08]/;
    var validChars = new RegExp(charVal);
    if(window.event)
    {
        keynum = e.keyCode;
    }
    else if(e.which)
    {
        keynum = e.which;
    }
    var keychar = String.fromCharCode(keynum);
    if (!validChars.test(keychar) && !keyChars.test(keychar))   {
        return false
    } else{
        return keychar;
    }
}
</script>
