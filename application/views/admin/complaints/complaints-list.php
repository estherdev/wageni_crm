<?php include APPPATH . 'views/admin/header.php';?>

<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<div class="content-wrapper">
  <section class="content-header">
    <h1> All Feedback</h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">All Feedback </li>
    </ol>
  </section>
  <section class="content">

  <!--      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
      
             <div class="box box-primary">
               <div class="box-header">   
                  <?php //prd($this->session->all_userdata()); ?>
                 <div class="col-md-3">            
                  <select class="form-control" name="change_category" id="change_category">
                    <option value=""> Select Category</option>    
                        <?php $scores= getBrachById($this->session->userdata('category'));                  
                          foreach ($scores as $score) {?> 
                        <option value="<?= $score['sub_cat_id'];?>"><?= ucfirst($score['sub_cat_name'])?></option>
                     <?php  } ?>
                   </select>
                </div> 
                <div class="col-md-3">            
                  <select class="form-control" name="change_stay" id="change_stay">
                    <option value=""> Select Stay</option>                                           
                         <option value="<?= 'Pre Stay';?>"><?= ucfirst('Pre Stay')?></option>
                         <option value="<?= 'On Stay';?>"><?= ucfirst('On Stay')?></option>
                         <option value="<?= 'Post Stay';?>"><?= ucfirst('Post Stay')?></option>                    
                   </select>
                </div>
                <div class="col-md-3">            
                  <select class="form-control" name="change_type" id="change_type">
                    <option value=""> Select Stay</option>                                           
                         <option value="<?= 'qmc';?>"><?= ucfirst('Multiple Choice')?></option>
                         <option value="<?= 'qdd';?>"><?= ucfirst('Dropdown')?></option>
                         <option value="<?= 'qchb';?>"><?= ucfirst('Checkboxes')?></option>                    
                         <option value="<?= 'qsr';?>"><?= ucfirst('Star Rating')?></option>                    
                   </select>
                </div>
                <div class="col-md-2">
                <div class="searchBlock">
                  <input id="search" name="search" class="form-control" type="text" placeholder="Search">
                 </div>
                </div>
               </div>                
            </div>
          </div>
       </div> -->
  
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title"><b> All Feedback</h3></b>
          </div>
          <div class="box-body table-responsive">

            <table id="feedback_list" class="table table-bordered table-striped table-hover table-sm text-center text-nowrap">
              <thead>
                <tr>
                  <th>S No.</th>                  
                  <th>Guest User</th>                 
                  <th>Category</th>                  
                  <th>Sub Category</th>                  
                  <th>Process</th>                  
                  <th>NPS</th>                  
                  <th>Time</th>                  
                  <th>View</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div> 
        </div> 
      </div> 
    </div> 

 </section> 
</div>
 <?php include APPPATH . 'views/admin/footer.php';?>



<script>

 

   $(function(){   
   var feedback_list_table = $('#feedback_list').DataTable({
      "processing": true,
      "pageLength": <?php echo $this->config->item('record_per_page');?>,
      "serverSide": true,
      "sortable": true,
      "lengthMenu": [[10,20, 30, 50,100], [10,20, 30, 50,100]],
      "language": {
      "emptyTable": "<?php echo $this->config->item('record_not_found_title');?>" 
      },
       "order": [
         [1, "desc"]
      ],
      "ajax":{
         url :"<?php echo base_url('sub-admin-view-net-promter-score-survey-ajax')?>", 
         type: "post",   
         data: function (d) {      
                d.category_id = $('#change_category').val();
                d.stay_id = $('#change_stay').val();
                d.type_id = $('#change_type').val();
                d.nps_main_id_ = $('#nps_main_id_').val();
             }      
      } ,
      "columns": [ 
      {data: 'id',"orderable":false},
      {data: 'user_name',class: 'text-nowrap',"orderable":true},
      {data: 'category_name',class: 'text-nowrap',"orderable":true},
      {data: 'sub_cat_name',class: 'text-left text-nowrap',"orderable":true},
      {data: 'sub_sub_cat_name',class: 'text-nowrap',"orderable":false},
   	  {data: 'npscore',class: 'text-nowrap',"orderable":false},
      {data: 'added_time',class: 'text-nowrap',"orderable":false},
      {data: 'id',"orderable":false},       
        
    ],
    
    "columnDefs": [        
              
              {
                  render: function (data, type, row, meta) {
                  return meta.row + meta.settings._iDisplayStart + 1 +'.';
              },          
               "targets": 0 
              },

              {
                "render": function ( data, type, row ) {
                  var html="";
                  //var redurl= base_url+'sub-admin-view-survey-user/'+data
                  var redurl= base_url+'admin-view-all-feedback/'+data
                   html +=' <span></span> <a href="'+redurl+'" class="btn btn-success btn-sm mt-1"><i class="fa fa-eye"></i> </a>';              
                  return html;
              },

              "targets": 7
            },
                        
          ],
 
      });

   /*=========================    FILETR    ==================================*/

  $('#search').on('keyup', function() {
    feedback_list_table.search(this.value).draw();
  });

  $('#change_category,#change_stay,#change_type').change(function(){
      feedback_list_table.ajax.reload();
  });
  

});






</script>