<?php include APPPATH . 'views/admin/header.php';?>
 <?php include APPPATH . 'views/admin/main-sidebar.php'; ?>

<style>
  .content .form-inline .form-group {margin-bottom: 2px;}
  #calender_part .form-group {margin-right: 2px;}
</style>

 <div class="content-wrapper">
      <div class="success_message" id="success_message"></div>
    <section class="content-header">
       <h1>Guest User Feedback Details</h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Guest User Feedback Details</li>
      </ol>
   </section>
    <section class="content">
      
       <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
      
             <div class="box box-primary">
               <div class="box-header">   
                <div class="form-inline">
                  <div class="form-group">
                    <select class="form-control" name="change_users" id="change_users">
                     <option value=""> Select Company</option>
                    <?php $company = getCompanyUsers();                  
                     foreach ($company as $users) {?>                     
                      <option value="<?= $users['sub_login_id'];?>"><?= ucfirst($users['fname'].' '.$users['lname'] )?></option>
                   <?php } ?>
                   </select>
                  </div>
                  <div class="form-group">
                    <select class="form-control" name="change_category" id="change_category">
                     <option value=""> Select Category</option>
                    <?php $category= getCategory();                  
                     foreach ($category as $cat) {?>                     
                      <option value="<?= $cat['cat_id'];?>"><?= ucfirst($cat['category_name'])?></option>
                   <?php } ?>
                   </select>
                  </div>
                  <div class="form-group">
                    <select class="form-control" name="change_feedback" id="change_feedback">
                    <option value=""> Select Feedback</option>    
                        <?php $scores= getScore();                  
                    foreach ($scores as $score) {?> 
                        <option value="<?= $score['score_id'];?>"><?= ucfirst($score['score_name'])?></option>
                     <?php } ?>
                   </select>
                  </div>
                  <div class="form-group">
                    <select class="form-control" name="change_status" id="change_status">
                    <option value=""> Select Status</option>    
                        <?php $ComplaintStatuss= getComplaintStatus();                  
                    foreach ($ComplaintStatuss as $ComplaintStatus) {?> 
                        <option value="<?= $ComplaintStatus['id'];?>"><?= ucfirst($ComplaintStatus['complaint_status_name'])?></option>
                     <?php } ?>
                   </select>
                  </div>
                  <div class="form-group">
                    <div class="searchBlock">
                      <input id="search" name="search" class="form-control" type="text" placeholder="Search">
                     </div>
                  </div>
                  <div class="form-group">
                    <select class="form-control" id="change_date">
                      <option value="">Select Date</option>
                      <option value="0">Today</option>
                      <option value="1">Yesterday</option>
                      <option value="2">Last 7 Days</option>
                      <option value="3">Last 15 Days</option>
                      <option value="4">This Month</option>
                      <option value="5">Last Month</option>
                      <option value="6">Last 6 Months</option>
                      <option value="7">This Year</option>
                      <option value="8">Last Year</option>
                      <option value="9">Select Custom Date</option>
                    </select>
                  </div>
                  <div class="form-group" id="calender_part">
                  </div>
                </div>

               </div>                
            </div>
          </div>
       </div>
       <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
             <div class="box">
                <div class="box-body">  
                  <div class="table-responsive without-toolbar table-striped ">
                        <table class="table table-striped table-hover" id="company_list" style="width:100%;">
                          <thead class="text-primary"> 
                            <th class="text-nowrap">S.&nbsp;No.</th>
                            <th class="text-nowrap">Company Name</th>
                            <th class="text-nowrap">Name</th>
                            <th class="text-nowrap">Category</th>
                            <th class="text-nowrap">Questions</th>
                            <th class="text-nowrap">Answer</th>
                            <th class="text-nowrap">Rating</th>
                            <th class="text-nowrap">Elapsed Time</th>
                            <th class="text-nowrap">Assign</th>                  
                            <th class="text-nowrap">Current Status</th>
                            <th class="text-nowrap">Notify</th>
                           </thead>
                          <tbody>
                       </tbody>
                    </table>
                  </div>               
               </div>
             </div>
          </div>
       </div>
</div>
 </section> 
 <?php include APPPATH . 'views/admin/footer.php'; ?>



<script>


  var defaltimageurl='<?= base_url()?>globalassets/admin/dist/img/user2-160x160.jpg';
  var imageurl='<?= base_url()?>uploads/';
  var base_url='<?= base_url()?>';

   var datatable_loader='<div class="datatable_loader"><img src="'+base_url+'/globalassets/admin/preloader1.gif"></div>';
   var small_loader='<div class="small_loader"><img src="'+base_url+'/globalassets/admin/preloader1.gif"></div>';

    $(function(){
   
   var all_feedback_list_table = $('#company_list').DataTable({
      "processing": true,
      "pageLength": <?php echo $this->config->item('record_per_page');?>,
      "serverSide": true,
      "sortable": true,
      "lengthMenu": [[10,20, 30, 50,100], [10,20, 30, 50,100]],
      "language": {
      "emptyTable": "<?php echo $this->config->item('record_not_found_title');?>" 
      },
       "order": [
         [0, "desc"]
      ],
      "ajax":{
         url :"<?php echo base_url('admin-view-all-complaints-ajax')?>", 
         type: "post",   
         data: function (d) {      
              d.feedback_id = $('#change_feedback').val();
              d.category_id = $('#change_category').val();
              d.status_id = $('#change_status').val();
              d.bus_id = $('#change_users').val();
              d.search = $('#search').val();
              d.change_date=$('#change_date').val();
              if(d.change_date==9){
                d.from_date=$('#from_date').val();
                d.to_date=$('#to_date').val();
              }else{
                d.from_date='';
                d.to_date='';
              }
            }      
      } ,
      "columns": [ 
      {data: 'id',"orderable":false},
      {data: 'busid',class: 'text-nowrap',"orderable":false},
      {data: 'name',class: 'text-nowrap',"orderable":false},
      {data: 'category_name',class: 'text-nowrap',"orderable":false},
      {data: 'question',class: 'text-nowrap',"orderable":false},
      {data: 'answer',class: 'text-nowrap',"orderable":false},
      {data: 'complaints',class: 'text-nowrap',"orderable":false}, 
      {data: 'complaintstime',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'assign',class: 'text-center text-nowrap',"orderable":false}, 
      {data: 'status',class: 'text-center',"orderable":false}, 
      {data: 'cf_status',class: 'text-center text-nowrap',"orderable":false}, 
        
    ],
    
    "columnDefs": [        
                  {
                    render: function (data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1 +'.';
                    },          
                    "targets": 0 
                  }, 

                  {
                    "render": function ( data, type, row ) {            
                      return  data;            
                    },
                    "targets":  2
                  },

                  {
                    "render": function ( data, type, row ) {
                      var html="";

                      if(row['cf_status']==3 ){
                        html +=' <span id="loader'+row['id']+'"></span><a href="javascript:void(0)"  class="btn btn-danger btn-sm mt-1">Done</a>';
                      }else{
                        html +=' <span id="loader'+row['id']+'"></span><a href="javascript:void(0)" onClick="sendNotification('+row['id']+','+row['cf_status']+')" id="notification'+row['id']+'" class="btn btn-success btn-sm mt-1">Send</a>';
                      }
              //return getComplaintsStatus(row['cf_status'])+html;
              return html; 
              },
              "targets": 10
              },

       
          ],
          
 
 
      });

   /*=========================    FILETR    ==================================*/


  $('#search').on('keyup', function() {
    all_feedback_list_table.search(this.value).draw();
  });


         $('#change_date').change(function(){
    var id=this.value;

    $('#calender_part').html('');
    var html='';
    if(id==9){
      html +='<div class="form-group"><input type="text" class="form-control date" autocomplete="off" id="from_date" placeholder="From Date"></div>';
      html +='<div class="form-group"><input type="text" class="form-control date" autocomplete="off" id="to_date" placeholder="To Date"></div>';
      $('#calender_part').html(html);
      
    $('.date').datepicker({
    format: 'dd-mm-yyyy',
    }).on('changeDate', function(e) {
      all_feedback_list_table.ajax.reload();
  });
    }else{
      all_feedback_list_table.ajax.reload();
    }
  })

  $('#change_category,#change_feedback,#change_status,#change_users,#to_date').change(function(){
      all_feedback_list_table.ajax.reload();
     });

  /*===========================================================*/
  
  $('#company_list tbody').on( 'click', 'tr td.details-control', function () {
 
    var tr  =  $(this).closest('tr');
    var row =  all_feedback_list_table.row(tr);
    if ( row.child.isShown() ) {
      tr.removeClass( 'details' );
      row.child.hide();
    } else {
      tr.addClass( 'details' );
            companyDetails(row);
            var id = row.data().id; 
            $(tr).attr('row_id', id);
            close_existing_module(id,all_feedback_list_table);
      }
    }); 
    /*===========================================================*/

   });
   
/*===========================================================*/


function sendNotification(id,status){
  if(id>0){
    $('#notification'+id).hide();
    $('#loader'+id).html(small_loader);
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-send-notification-to-customer')?>',
      data: {'id':id,'status':status},
      success: function(ajaxresponse){
        response=JSON.parse(ajaxresponse);
        if(response['status']){
          $('#success_message').html('<div class="alert alert-success">'+response['message']+'!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        }else{
          $('#success_message').html('<div class="alert alert-danger">'+response['message']+'!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
         }
        $('#notification'+id).show();
        $('#loader'+id).html('');
       	setTimeout(function () {
       			 location.reload()
    		}, 100);
        // $('#change_order_status').trigger('change');
      }
    }); 
  }
} 
/*===========================================================*/

  function getComplaintsStatus(status){
    if(status==1){
      return "<span class='btn btn-sm btn-danger'>Pending</span>";
   }else if(status==2){
     return "<span class='btn btn-sm btn-warning'>Ongoing</span>";
   }else if(status==3){
    return "<span class='btn btn-sm btn-success'>Issue has been addressed</span>";
   }else{
  return "<span class='btn btn-sm btn-danger'>Pending</span>";
   }
 }
/*===========================================================*/
 function companyDetails(row){
  var id = row.data().id; 
  row.child(datatable_loader).show();
  $.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-company-users-details-ajax')?>',
      data: {'id':id},
      success: function(ajaxresponse){
      response=JSON.parse(ajaxresponse);
      var res=response['data'];
         row.child(res).show();
       }
    }); 
 } 

/*===========================================================*/
 function close_existing_module(main_id,order_list_table){
   $('#company_list >tbody >tr').each(function() {
    var tr = $(this).closest('tr');
    var row = order_list_table.row( tr );
    var id=$(tr).attr("row_id");
    if (typeof(id) == "undefined") {
        id=0;
    }
    if ($(this).hasClass('details') && main_id!=id) {
      row.child.hide();
      tr.removeClass('details');
    }
  }); 
} 

   
/*===========================================================*/   
  function changeCompanyUsersStatus(id,status){
   if(id>0){
    if(status==1){
      var set_status=0;
    }else{
      var set_status=1;
    }
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-change-company-users-status')?>',
      data: {'id':id,'status':set_status},
      success: function(ajaxresponse){
        response=JSON.parse(ajaxresponse);
        if(response['status']){ 
          if(set_status){
            var html='<a href="javascript:void(0)" onClick="changeCompanyUsersStatus('+id+','+set_status+')"><span class="btn btn-sm btn-success">Active</span></a>';
          }else{
            var html='<a href="javascript:void(0)" onClick="changeCompanyUsersStatus('+id+','+set_status+')"><span class="btn btn-sm btn-danger">In-active</span></a>';
          }
          $('#status'+id).html(html);
          
        }
      }
    }); 
  }
}

function claculateReview(rating){

  var fullStar = new Array(Math.floor(rating + 1)).join('<i class="fa fa-star"></i>');

  var halfStar = ((rating%1) !== 0) ? '<i class="fa fa-star-half-o"></i>': '';

  var noStar = new Array(Math.floor(5 + 1 - rating)).join('<i class="fa fa-star-o"></i>');

  var html=fullStar + halfStar + noStar;
  return html;
            
}

/*===========================================================*/


function updateCompany(id){
$.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-update-company-ajax')?>',
      data: {'id':id},
    }); 

}


/*===========================================================*/

 $( document ).ready(function() {

       $('.starDisplay').each(function() {

            var rating = $(this).data("rating");            
           

            var numStars = 5;//$(this).data("numStars");

            var fullStar = new Array(Math.floor(rating + 1)).join('<i class="fa fa-star"></i>');

            var halfStar = ((rating%1) !== 0) ? '<i class="fa fa-star-half-o"></i>': '';

            var noStar = new Array(Math.floor(numStars + 1 - rating)).join('<i class="fa fa-star-o"></i>');

            $(this).html(fullStar + halfStar + noStar);

        });
       });

 

    //$('.starDisplay').stars();
</script>