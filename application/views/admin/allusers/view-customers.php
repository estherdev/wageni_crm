<!-- <?php// echo "<pre/>";print_r($subscriber); die;?> -->

<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>View customer</h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">View customer</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <!--  <div class="box-header">
                  <h3 class="box-title"><strong>customer Details</strong></h3>
               </div> -->
               <!-- /.box-header -->
               <!-- form start -->
               <?php if($this->session->flashdata("success")):?>
                  <div class="alert alert-success"><?= $this->session->flashdata("success");?>
                  <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
               </div>
            <?php endif;?>
            <?php if($this->session->flashdata("failed")):?>
               <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
               <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
            </div>
         <?php endif;?>
         
      </div>
      <!-- /.box -->
   </div>
   <!-- /.content-wrapper -->
</div>
<!-- /.content-wrapper -->
<div class="row">
   <div class="col-xs-6 col-md-12 col-sm-6">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Customer Details List</h3>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example1" class="dataTables_wrapper form-inline table table-bordered table-striped text-center text-nowrap" role="grid">
               <thead>
                  <tr>
                     <th class="text-center">S.No</th>
                     <th class="text-center">Company Name</th>
                     <th class="text-center">Package</th>
                     <th class="text-center">Amount</th>
                     <th class="text-center">Category</th>
                     <th class="text-center">Logo</th>
                     <th class="text-center">Expired</th>
                     <th class="text-center">Email</th>
                     <th class="text-center">Phone</th>
                     <th class="text-center">Url</th>
                     <th class="text-center">Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $i= 1; foreach ($subscriber as $key => $user) { ?>
                     <tr>
                        <td><?= $i++; ?></td>
                        <td><?= ucfirst($user->sub_name);?> </td>
                        <!-- <td><?= ucfirst($user->fname." ".$user->lname);?> </td> -->
                        <td><?= ucfirst($user->package_name);?></td>
                        <td><?= $user->package_price;?></td>
                        <td><?= ucfirst($user->category_name);?></td>
                        <td><?php if(empty($user->userfile)) {?>
                           <img src="<?= base_url();?>globalassets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" height="30px;" width="30px;"/>
                        <?php }else { ?>
                           <img src="<?= base_url();?>uploads/<?= $user->userfile;?>" class="img-circle" alt="User Image"    height="30px;" width="30px;" />
                        <?php } ?>
                     </td>
                     <td><?= $user->subscriber_end_date;?></td>
                     <td><?= $user->sub_email;?></td>
                     <td><?= $user->sub_phone;?></td>
                     <td><a href="http://<?= $user->company_url ;?>"><?= $user->company_url ;?></a></td>
                     <td class="text-center" style="white-space:nowrap">
                        <?php if($user->sub_isActive=='1'){?>
                           <a class="btn btn-sm btn-success" href="<?= base_url('company-status/');?><?= $user->sub_login_id;?>/<?= $user->sub_isActive;?>/<?= $user->category;?>" title="Edit"><i class="fa fa-unlock"></i></a>
                        <?php } else { ?>
                           <a class="btn btn-sm btn-danger" href="<?= base_url('company-status/');?><?= $user->sub_login_id;?>/<?= $user->sub_isActive;?>/<?= $user->category;?>" title="Edit"><i class="fa fa-lock"></i></a>
                        <?php } ?>
                        <a class="btn btn-sm btn-info" href="<?php echo base_url('editSubscriber/'.$user->sub_login_id) ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-sm btn-danger deleteUsers"  data-toggle="modal" data-target="#deleteModal<?= $user->sub_login_id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                     </td>
                     <div id="deleteModal<?= $user->sub_login_id ; ?>" class="modal fade">
                        <div class="modal-dialog">
                           <div class="modal-content">
                              <!-- dialog body -->
                              <div class="modal-body">
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 Are You Sure to <strong><?= $user->sub_username ; ?></strong> Delete  
                              </div>
                              <!-- dialog buttons -->
                              <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('admin-delete-company/'.$user->sub_login_id ); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                           </div>
                        </div>
                     </div>
                  </tr>
               <?php } ?>
            </tbody>
         </table>
      </div>
      <!-- /.box-body -->
   </div>
   <!-- /.box -->
</div>
<!-- /.content-wrapper -->
</div>
</section>
</div>
<!-- /.content-wrapper -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->         
<?php include APPPATH . 'views/admin/footer.php'; ?>
<!--footer Section End Here