<table class="table table-dark table-striped table-hover">
    <thead class="success">
        <tr>
            <th class="text-center text-nowrap">S.No.</th>
        <!--     <th class="text-center text-nowrap">Company</th> -->
            <th class="text-center text-nowrap">Branch</th>
            <!-- <th class="text-center text-nowrap">Review</th> -->
            <th class="text-center text-nowrap">Name</th>
            <th class="text-center text-nowrap">Sub name</th>
            <th class="text-center text-nowrap">Contact No.</th>
            <th class="text-center text-nowrap">Role</th>
            <th class="text-center text-nowrap">Email</th>
            <th class="text-center text-nowrap">status</th>
         </tr>
    </thead>
    <tbody>
        <?php if(count($companyListAdmin)>0){
            $sr=1; foreach($companyListAdmin as $company){ ?>
                <tr>
                    <td class="text-center text-nowrap"><?php echo $sr;?></td> 
                   <!--  <td class="text-center text-nowrap"><?php echo $company['company_id'];?></td>  -->
                    <td class="text-center text-nowrap"><?php echo $company['branch_id'];?></td> 
           <!--          <td class="text-center text-nowrap"><?php echo $company['total_review'];?></td>  -->
                    <td class="text-center text-nowrap"><?php echo $company['first_name'].' '.$company['first_name'];?></td>
                    <td class="text-center text-nowrap"><?php echo $company['sub_name'];?></td> 
                    <td class="text-center text-nowrap"><?php echo $company['phone'];?></td> 
                    <td class="text-center text-nowrap"><?php echo $company['role'];?></td> 
                    <td class="text-center text-nowrap"><?php echo $company['email'];?></td>    
                    <td class="text-center text-nowrap"><?php echo getStatus($company['status']);?></td>    
                </tr>
        <?php $sr++; }
        } else { ?>
        <tr><td class="text-center" colspan="9"><?php echo ($this->config->item('record_not_found_title'));?></td></tr>
        <?php  } ?>
    </tbody>
</table>
