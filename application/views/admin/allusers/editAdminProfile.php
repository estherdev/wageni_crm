<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <?php if($this->session->flashdata("success")):?>
  <div class="alert alert-success"><?= $this->session->flashdata("success");?>
    <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
  </div>
  <?php endif;?>
  <?php if($this->session->flashdata("failed")):?>
  <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
    <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
  </div>
  <?php endif;?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Update Profile
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Update Profile</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Profile  Details</h3>
          </div>
          <?php // for temperary use only
            $this->db->select('*');
            $query = $this->db->get('admin_login');
            $Aprofile = $query->result_array(); 
            
            ?>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" id="profile" action="<?= base_url('admin/allusers/Auth/editAdminProfile/'.$Aprofile[0]['admin_login_id']);?>" method="post"  enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group row">
                <div class="col-md-4">
                  <label for="f-name">Name</label>
                  <input type="text" class="form-control"  name="name" value="<?= $Aprofile[0]['admin_name']; ?>" placeholder="Name">
                  <?= form_error('name')?>
                </div>
                <div class="col-md-4">
                  <label for="l-name">User Name</label>
                  <input type="text" class="form-control"  name="username" value="<?= $Aprofile[0]['admin_username'];?>" placeholder="User Name" >
                  <?= form_error('username')?>
                </div>
                <div class="col-md-4">
                  <label for="e-address"><?= $this->lang->line('LAVELUEMAIL');?></label>
                  <input  class="form-control" name="uemail" value="<?= $Aprofile[0]['admin_email']; ?>" placeholder="<?= $this->lang->line('PLACEHODERUEMAIL');?>" disabled >
                  <?= form_error('uemail')?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-4">
                  <label for="m-number"><?= $this->lang->line('LAVELUPHONE');?></label>
                  <input type="text" class="form-control" name="uphone" value="<?= $Aprofile[0]['admin_phone'];?>" placeholder="<?= $this->lang->line('PLACEHODERUPHONE');?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                  <?= form_error('uphone')?>
                </div>
                <div class="col-md-4">
                  <label for="m-number"><?= $this->lang->line('LAVELUPASS');?></label>
                  <input type="password" class="form-control" name="upass" value="<?= $Aprofile[0]['admin_pass']; ?>" placeholder="<?= $this->lang->line('PLACEHODERUPASS');?>">
                  <?= form_error('upass')?>
                </div>
                <div class="col-md-3">
                  <label for="Image">Image</label>
                  <input type="hidden" name="hiddenimg" value="<?= $Aprofile[0]['userfile'];?>">
                  <input type="file" value="<?= $Aprofile[0]['userfile'];?>" class="form-control"  name="userfile"  />
                  <?= form_error('userfile')?>   
                </div>
                <div class="col-md-1">
                  <?php if($Aprofile[0]['userfile']== false) {?>
                  <img src="<?= base_url();?>globalassets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" height="30px;" width="30px;"/>
                  <?php }else { ?>
                  <img src="<?= base_url();?>uploads/<?= $Aprofile[0]['userfile'];?>" class="img-circle" alt="User Image"    height="30px;" width="30px;" />
                  <?php } ?>
                </div>
              </div>
              <div class="form-group row">
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <button type="submit" class="btn btn-success">Update</button>
              <a href="<?= base_url('admin/dashboard');?>" class="btn btn-danger" role="button">Back</a>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.content-wrapper -->
    </div>
</div>
<!-- /.content-wrapper -->
</section><!-- /.content -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->         
<?php include APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here