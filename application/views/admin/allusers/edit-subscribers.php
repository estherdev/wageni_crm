<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
   <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        Update Company 
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Update Company </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title"><strong>Company  Details</strong></h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" id="editsubs" action="<?= base_url('updateSubscriber/'.$editsubscriber[0]->sub_login_id);?>" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-4">
                           <label for="f-name">First Name</label>
                           <input type="text" class="form-control"  name="fname" value="<?= $editsubscriber[0]->fname ;?>" placeholder="First Name">
                           <?= form_error('fname')?>
                        </div>

                        <div class="col-md-4">
                           <label for="l-name">Last Name</label>
                           <input type="text" class="form-control"  name="lname" value="<?= $editsubscriber[0]->lname ;?>" placeholder="Last Name" >
                           <?= form_error('lname')?>
                        </div>

                        <div class="col-md-4">
                           <label for="e-address"><?= $this->lang->line('LAVELUEMAIL');?></label>
                           <input  class="form-control" name="uemail" value="<?= $editsubscriber[0]->sub_email ;?>" placeholder="<?= $this->lang->line('PLACEHODERUEMAIL');?>" disabled >
                           <?= form_error('uemail')?>
                        </div>
                     </div>
                        <div class="form-group row">
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPHONE');?></label>
                           <input type="text" class="form-control" name="uphone" value="<?= $editsubscriber[0]->sub_phone ;?>" placeholder="<?= $this->lang->line('PLACEHODERUPHONE');?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                           <?= form_error('uphone')?>
                        </div>
                       <!--  <div class="col-md-4">
                           <label for="c-name"><?= $this->lang->line('LAVELUNAME');?></label>
                           <input type="text" class="form-control"  name="uname" value="<?= $editsubscriber[0]->sub_name ;?>" placeholder="<?= $this->lang->line('PLACEHODERUNAME');?>">
                           <?= form_error('uname')?>
                        </div> -->
                        <div class="col-md-4">
                           <label for="m-number"><?= $this->lang->line('LAVELUPASS');?></label>
                           <input type="password" class="form-control" name="upass" value="<?= $editsubscriber[0]->sub_pass ;?>" placeholder="<?= $this->lang->line('PLACEHODERUPASS');?>">
                           <?= form_error('upass')?>
                        </div>

                        <div class="col-md-4">
                           <label for="select-role">Role</label>
                           <select type="text" class="form-control" id="select-role"  name="userrole">
                               <option value="">--Select Role--</option>
                              <?php foreach ($role as $key => $value) {?>
                              <option value="<?= $value->subscriber_roll_id ?>" <?php if($value->subscriber_roll_id == $editsubscriber[0]->sub_login_role){echo "selected='selected'";}?> <?php echo set_select('userrole', $value->subscriber_roll_id); ?> ><?= ucfirst($value->subscriber_role_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('userrole')?>
                        </div>
                     </div>
                     
                        <div class="form-group row">
                        <div class="col-md-4">
                           <label for="select-package">Package</label>
                           <select class="form-control" id="select-package"  name="userpackage">
                               <option value="">--Select Package--</option>
                              <?php foreach ($package as $key => $value) {?>
                                 <option value="<?= $value->package_id ?>" <?php if($value->package_id == $editsubscriber[0]->subscriber_package_id){echo "selected='selected'";}?> <?php echo set_select('userpackage', $value->package_id); ?> ><?= ucfirst($value->package_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('userpackage')?>
                        </div>
                        <div class="col-md-4">
                           <label for="select-category">Category</label>
                           <select class="form-control" id="select-category"  name="usercategory">
                               <option value="">--Select Category--</option>
                                 <?php foreach ($category as $key => $categoryvalue) {?>
                                 <option value="<?= $categoryvalue->cat_id ?>" <?php if($categoryvalue->cat_id == $editsubscriber[0]->category){echo "selected='selected'";}?> <?php echo set_select('usercategory', $categoryvalue->cat_id); ?> ><?= ucfirst($categoryvalue->category_name) ?></option>
                              <?php } ?>
                           </select>
                           <?= form_error('usercategory')?>
                        </div>
                        <div class="col-md-3">
                           <label for="logo">Logo</label>
                           <input type="file" name="userfile" >
                           <input type="hidden" name="hiddenimg" value="<?= $editsubscriber[0]->userfile ;?>">
                           <?= form_error('userfile')?>
                        </div>
                         <div class="col-md-1">
                           <?php if(empty($editsubscriber[0]->userfile)) {?>
                           <img src="<?= base_url();?>globalassets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                           <?php }else { ?>
                           <img src="<?= base_url();?>uploads/<?= $editsubscriber[0]->userfile;?>" class="img-circle" alt="User Image" />
                           <?php } ?>
                        </div>
                     </div>

                     <div class="box-header">
                        <h3 class="box-title"><strong>Contact Details</strong></h3>
                     </div> 
                     
                     <div class="form-group row">
                     <div class="col-md-4">
                        <label for="">Company Name</label>
                        <input type="text" class="form-control" name="cname" value="<?= $editsubscriber[0]->sub_name ;?>" placeholder="Company Name" value="<?= set_value('cname'); ?>">
                        <?= form_error('cname')?>
                     </div>

                        <div class="col-md-4">
                              <label for="">Account No</label>
                              <input type="text" class="form-control" value="<?= $editsubscriber[0]->accno ;?>" id="accno" placeholder="Account No"  disabled>
                              <?= form_error('accno')?>
                        </div> 
                       <!--  <div class="col-md-3">
                              <label for="">Generate</label> 
                              <input type="button " class="form-control btn btn-success"  value="Generate Account No!" onclick="doFunction(Random);" />
                        </div> 
 -->
                        <div class="col-md-4">
                              <label for="">Telephone</label>
                              <input type="text" class="form-control" name="contacttel" value="<?= $editsubscriber[0]->contacttel ;?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  placeholder="Telephone" value="<?= set_value('contacttel'); ?>">
                              <?= form_error('contacttel')?>
                        </div> 
                        </div> 
                        <div class="form-group row">

                        <div class="col-md-4">
                              <label for="">Contact Number</label>
                              <input type="text" class="form-control" name="contactno" value="<?= $editsubscriber[0]->contactno ;?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  placeholder="Contact Number" value="<?= set_value('contactno'); ?>">
                              <?= form_error('contactno')?>
                        </div>
                        <div class="col-md-8">
                              <label for="">Website URL</label>
                              <input type="text" class="form-control" value="<?= $editsubscriber[0]->company_url ;?>" placeholder="Website URL" disabled>
                              <?= form_error('weburl')?>
                        </div> 
                        </div> 
                         <div class="form-group row">
                        <div class="col-md-12">
                              <label for="">Address</label>
                              <textarea class="form-control" name="contactaddress" placeholder="Address" ><?= set_value('contactaddress'); ?><?= $editsubscriber[0]->contactaddress ;?></textarea>
                              <?= form_error('contactaddress')?>
                        </div> 
                     </div> 
                     <div class="box-header">
                        <h3 class="box-title"><strong>Contact Person Details</strong></h3>
                     </div>
                     <div class="form-group row">

                        <div class="col-md-3">
                              <label for="">Name</label>
                              <input type="text" class="form-control" name="cpname" value="<?= $editsubscriber[0]->cpname ;?>" placeholder="Name" value="<?= set_value('cpname'); ?>">
                              <?= form_error('cpname')?>
                        </div> 
                        <div class="col-md-3">
                              <label for="">Email</label>
                              <input type="email" class="form-control" name="cpemail" value="<?= $editsubscriber[0]->cpemail ;?>" placeholder="Contact Email" value="<?= set_value('cpemail'); ?>">
                              <?= form_error('cpemail')?>
                        </div> 
                         <div class="col-md-3">
                              <label for="">Title</label>
                              <input type="text" class="form-control" name="cptitle" value="<?= $editsubscriber[0]->cptitle ;?>" placeholder="Contact Title" value="<?= set_value('cptitle'); ?>">
                              <?= form_error('cptitle')?>
                        </div> 

                         <div class="col-md-3">
                              <label for="">Mobile</label>
                              <input type="text" class="form-control" name="cpmobile" value="<?= $editsubscriber[0]->cpmobile ;?>" placeholder="Contact Mobile" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  value="<?= set_value('cpmobile'); ?>">
                              <?= form_error('cpmobile')?>
                        </div>
                        </div> 
                         <div class="form-group row"> 
                         <div class="col-md-6">
                              <label for="">Address</label>
                              <textarea class="form-control" name="cpaddress"  placeholder="Contact Address"><?= set_value('cpaddress'); ?><?= $editsubscriber[0]->cpaddress ;?></textarea>
                              <?= form_error('cpaddress')?>
                        </div>
                         <div class="col-md-6">
                              <label for="">Office</label>
                              <textarea class="form-control" name="cpoffice" placeholder="Contact Office"><?= set_value('cpoffice'); ?><?= $editsubscriber[0]->cpoffice ;?>
                              <?= form_error('cpoffice')?></textarea>
                        </div> 
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                  <button type="submit" class="btn btn-success">Update</button>
                  <a href="<?= base_url('admin-add-company');?>" class="btn btn-danger" role="button">Back</a>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
</div>
<!-- /.content-wrapper -->
</section><!-- /.content -->
  <script>
    function Random() {    
        return Math.floor(Math.random() * 1000000000);
    }
      function doFunction(){
    document.getElementById('accno').value = Random()
    }    
</script>
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->			
<?php include APPPATH . 'views/admin/footer.php'; ?>
<!--footer Section End Here