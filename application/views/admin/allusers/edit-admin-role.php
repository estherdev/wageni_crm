<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
   <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Admin Role
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Edit Admin Role</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title">Admin Role Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" id="role" action="<?= base_url('update-admin-role/'.$records[0]['admin_role_id']);?>" method="post" >
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-12">
                           <label for="c-name">Role Name</label>                        
                           <input type="text" value="<?php echo $records[0]['admin_emp_role_name'];?>" class = "form-control" name="rolename" />
                           <?= form_error('rolename')?>
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                 <div class="box-footer text-center">

                <button type="submit" class="btn btn-success">Update</button>
                <button type="reset" class="btn btn-danger">Reset</button>

              </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
      <!-- /.content-wrapper -->
</div>
<!-- /.content-wrapper -->
</section><!-- /.content -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->			
<?php include APPPATH . 'views/admin/footer.php'; ?>
<!--footer Section End Here