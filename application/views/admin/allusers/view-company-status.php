<!-- <?php// echo "<pre/>";print_r($subscriber); die;?> -->

<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>View Company Status</h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">View Company Status</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <!--  <div class="box-header">
                  <h3 class="box-title"><strong>customer Details</strong></h3>
               </div> -->
               <!-- /.box-header -->
               <!-- form start -->
               <?php if($this->session->flashdata("success")):?>
                  <div class="alert alert-success"><?= $this->session->flashdata("success");?>
                  <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
               </div>
            <?php endif;?>
            <?php if($this->session->flashdata("failed")):?>
               <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
               <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
            </div>
         <?php endif;?>
         
      </div>
      <!-- /.box -->
   </div>
   <!-- /.content-wrapper -->
</div>
<!-- /.content-wrapper -->
<div class="row">
   <div class="col-xs-6 col-md-12 col-sm-6">
      <div class="box">
         <div class="box-header">
            <h3 class="box-title">Company Status Details List</h3>
         </div>
         <!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example1" class="dataTables_wrapper form-inline table table-bordered table-striped text-center text-nowrap" role="grid">
               <thead>
                  <tr>
                     <th class="text-center">S.No</th>
                     <th class="text-center">Company Logo</th>
                     <th class="text-center">Company Name</th>
                     <th class="text-center">Amount Paid</th>
                     <th class="text-center">Registerted</th>
                     <th class="text-center">Expired</th>
                     <th class="text-center">Status</th>
                  </tr>
               </thead>
               <tbody>
                  <?php $i= 1; foreach ($subscriber as $key => $user) { ?>
                     <tr>
                        <td><?= $i++; ?></td>
                        <td><?php if(empty($user->userfile)) {?>
                           <img src="<?= base_url();?>globalassets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" height="30px;" width="30px;"/>
                        <?php }else { ?>
                           <img src="<?= base_url();?>uploads/<?= $user->userfile;?>" class="img-circle" alt="User Image" height="30px;" width="30px;" />
                        <?php } ?>
                     </td>
                        <td><?= ucfirst($user->sub_name);?> </td>
                        <!-- <td><?= ucfirst($user->fname." ".$user->lname);?> </td> -->
                        <td>$<?= $user->package_price;?></td>
                        <td><?= getDateDDMMYY($user->package_createddate);?></td>
                        <td><?= $user->subscriber_end_date;?></td>
                             <td class="text-center" style="white-space:nowrap">
                        <?php if($user->sub_isActive=='1'){?>
                           <a class="btn btn-sm btn-success" href="<?= base_url('company-status/');?><?= $user->sub_login_id;?>/<?= $user->sub_isActive;?>/<?= $user->category;?>" title="Edit"><i class="fa fa-unlock"></i> Active</a>
                        <?php } else { ?>
                           <a class="btn btn-sm btn-danger" href="<?= base_url('company-status/');?><?= $user->sub_login_id;?>/<?= $user->sub_isActive;?>/<?= $user->category;?>" title="Edit"><i class="fa fa-lock"></i>  In-Active</a>
                        <?php } ?>
                     </td>
                  </tr>
               <?php } ?>
            </tbody>
         </table>
      </div>
      <!-- /.box-body -->
   </div>
   <!-- /.box -->
</div>
<!-- /.content-wrapper -->
</div>
</section>
</div>
<!-- /.content-wrapper -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->         
<?php include APPPATH . 'views/admin/footer.php'; ?>
<!--footer Section End Here