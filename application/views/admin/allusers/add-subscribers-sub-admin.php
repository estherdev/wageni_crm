<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <?php if($this->session->flashdata("success")):?>
   <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <?php if($this->session->flashdata("failed")):?>
   <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
   </div>
   <?php endif;?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        List All Company Sub Admin 
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">List All Company Sub Admin </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Info boxes -->
  
      <!-- /.content-wrapper -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">List All Company Sub Admin</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped text-center">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Phone</th>
                           <th>Role</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php $i= 1; foreach ($subscriberSubAdmin as $key => $user) { ?>
                        <tr>
                           <td><?= $i++; ?></td>
                           <td><?= ucfirst($user->sub_name);?></td>
                           <td><?= $user->sub_email;?></td>
                           <td><?= $user->sub_phone;?></td>
                           <td><?php if ($user->sub_login_role == '1') { 
                              echo "Employee";
                           }else{ echo "Manager";}?></td>
                           <td class="text-center">
                              <a class="btn btn-sm btn-info" href="<?php echo base_url().'editSubscriber/'.$user->sub_login_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                              <a class="btn btn-sm btn-danger deleteUsers"  data-toggle="modal" data-target="#deleteModal<?= $user->sub_login_id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                           </td>
                           <div id="deleteModal<?= $user->sub_login_id ; ?>" class="modal fade">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <!-- dialog body -->
                                    <div class="modal-body">
                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       Are You Sure to <strong><?= $user->sub_username ; ?></strong> Delete  
                                    </div>
                                    <!-- dialog buttons -->
                                    <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('deletesubscribers/'.$user->sub_login_id ); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                                 </div>
                              </div>
                           </div>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
</div>
<!-- /.content-wrapper -->
</section><!-- /.content -->
<!-- Main content -->
<!--  /.content -->
<!--footer Section Start Here -->			
<?php include APPPATH . 'views/admin/footer.php'; ?>
<!--footer Section End Here