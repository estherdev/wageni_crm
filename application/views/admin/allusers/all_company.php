<?php include APPPATH . 'views/admin/header.php'; include APPPATH . 'views/admin/main-sidebar.php'; ?>

<div class="content-wrapper">
<?php if($this->session->flashdata("success")):?>
<div class="alert alert-success"><?= $this->session->flashdata("success");?> <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a></div><?php endif;?>
   
<?php if($this->session->flashdata("failed")):?>
<div class="alert alert-danger"><?= $this->session->flashdata("failed");?><a class="close" data-dismiss="alert"><i class="icon-remove"></i></a></div><?php endif;?>
   <section class="content-header">
      <h1>View company</h1>

  <div class="successmsg"></div>
      <ol class="breadcrumb"><li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li class="active">View company</li></ol>
   </section>
    <section class="content">
       <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
             <div class="box box-primary">
               <div class="box-header">     
                <div class="col-md-2">            
                  <select class="form-control" name="change_category" id="change_category">
                     <option value=""> Select Category</option>
                    <?php $category= getCategory(); foreach ($category as $cat) {?>                     
                      <option value="<?= $cat['cat_id'];?>"><?= ucfirst($cat['category_name'])?></option>
                   <?php } ?>
                   </select>
                   </div>   
                <div class="col-md-2">            
                  <select class="form-control" name="change_package" id="change_package">
                    <option value=""> Select Package</option>
                    <?php $packages= getPackages(); foreach ($packages as $package) {?>                     
                      <option value="<?= $package['package_id'];?>"><?= ucfirst($package['package_name'])?></option>
                   <?php } ?>
                   </select>
                   </div>  
               </div>                
            </div>
          </div>
       </div>
       <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
             <div class="box">
               <div class="box-header">
                  <h3 class="box-title">company List</h3>
               </div>
                <div class="box-body">  
                  <div class="table-responsive without-toolbar table-striped ">
                        <table class="table table-striped table-hover" id="company_list" style="width:100%;">
                          <thead class="text-primary">
                            <th class="text-nowrap">S.&nbsp;No.</th>
                            <th class="text-nowrap">#</th>
                            <th class="text-nowrap">Image</th>
                            <th class="text-nowrap">Name</th>
                            <th class="text-nowrap">Review</th>
                            <th class="text-nowrap">Category</th>
                            <th class="text-nowrap">Company</th>
                            <th class="text-nowrap">Package</th>
                            <th class="text-nowrap">Price</th>
                            <th class="text-nowrap">Expire</th>
                            <th class="text-nowrap">Personal Email</th>
                            <th class="text-nowrap">Personal Phone</th>
                            <th class="text-nowrap">Company Email</th>
                            <th class="text-nowrap">Company Phone</th>
                            <th class="text-nowrap">Status</th>
                            <th class="text-nowrap">url</th>                   
                            <th class="text-nowrap">Action</th>                   
                          </thead>
                          <tbody>
                       </tbody>
                    </table>
                  </div>               
               </div>
             </div>
          </div>
       </div>
</div>
 </section> 
 <?php include APPPATH . 'views/admin/footer.php'; ?>
 <style type="text/css">
    td.details-control { background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center 12px; cursor: pointer; }
    tr.details td.details-control { background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center 12px; }
    .datatable_loader { text-align: center; }
    .datatable_loader img { width: 80px; }
</style>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>


<script>


  var defaltimageurl='<?= base_url()?>globalassets/admin/dist/img/user2-160x160.jpg';
  var imageurl='<?= base_url()?>uploads/';
  var base_url='<?= base_url()?>';
  var datatable_loader='<div class="datatable_loader"><img src="'+base_url+'/globalassets/admin/preloader1.gif"></div>';
  var small_loader='<div class="small_loader"><img src="'+base_url+'/globalassets/admin/preloader1.gif"></div>';

    $(function(){
   
   var company_list_table = $('#company_list').DataTable({
      "processing": true,
      "pageLength": <?php echo $this->config->item('record_per_page');?>,
      "serverSide": true,
      "sortable": true,
      "lengthMenu": [[10,20, 30, 50,100], [10,20, 30, 50,100]],
      "language": {
      "emptyTable": "<?php echo $this->config->item('record_not_found_title');?>" 
      },
       "order": [
         [4, "desc"]
      ],
      "ajax":{
         url :"<?php echo base_url('admin-view-all-company-ajax')?>", 
         type: "post",   
         data: function (d) {      
              d.package_id = $('#change_package').val();
              d.category_id = $('#change_category').val();
            }      
      } ,
      "columns": [ 
			{data: 'id',"orderable":false},
      {"class":'details-control',"orderable":false,"sortable":false,"data": null, "defaultContent": '' },		 
      {data: 'image',class: 'text-nowrap',"orderable":false},       
			{data: 'name',class: 'text-nowrap',"orderable":false},
      {data: 'score',class: 'text-nowrap',"orderable":true},
			{data: 'category_name',class: 'text-nowrap',"orderable":false},
			{data: 'sub_name',class: 'text-nowrap',"orderable":false}, 
      {data: 'package_name',class: 'text-nowrap',"orderable":false},  
      {data: 'price',class: 'text-nowrap',"orderable":false},  
      {data: 'expiredate',class: 'text-nowrap',"orderable":false},  
			{data: 'email',class: 'text-nowrap',"orderable":false},
      {data: 'sub_phone',class: 'text-nowrap',"orderable":false},
      {data: 'cpemail',class: 'text-nowrap',"orderable":false},
      {data: 'cpmobile',class: 'text-nowrap',"orderable":false},
			{data: 'status',class: 'text-center',"orderable":false}, 
			{data: 'company_url',class: 'text-center',"orderable":false}, 
      {data: 'id',class: 'text-nowrap','orderable':false}, 
			
 		],
 		
 		"columnDefs": [				 
					{
              render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1 +'.';
          },          
           "targets": 0 
          }, 
    			
          {
					   "render": function ( data, type, row ) {
           if(row['image']){
              return '<span id="image_'+row['id']+'"><img src="' + imageurl + row['image']+'" class="img-circle" alt="User Image" height="30px;" width="30px";></span>';
            }else{
              return '<span id="image_'+row['id']+'"><img src="' +defaltimageurl +'" class="img-circle" alt="User Image" height="30px;" width="30px";></span>';
             }
					 },
 					  "targets":  2
				  }, 

          {
              "render": function ( data, type, row ) {  
              var html=claculateReview(row['score']);
               //var html= '<span class="stars" data-rating="'+claculateReview(row['score'])+'" data-num-stars="5" ></span>';
              return html;  
           },
            "targets":  4
          },           
   				
          {
					  "render": function ( data, type, row ) {
   						var html ='<span id="status'+row['id']+'">';
  						html +='<a href="javascript:void(0)" onClick="changeCompanyUsersStatus('+row['id']+','+row['status']+')">'+getStatus(data)+'</a>';
  						html +='</span>';
   						return html;
					   },
 					  "targets": 14 
				  },

        {
            "render": function ( data, type, row ) {            
            return '<a href="'+base_url+ 'admin-update-company/' + data+'" class="btn btn-success btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a href="javascript:void(0);" onClick="deleteRecord('+data+');" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>';
          },
            "targets": 16
        }    
			 
   				],

          "dom": "<<'dt-toolbar customShowEntry'<''l>>"+"t"+"<'dt-toolbar-footer row'<'col-md-5'i><'col-md-7'p>>r","autoWidth" : true,
                "footerCallback": function ( row, data, start, end, display ) {
                //setCheckboxSelected(data);
              }  
 
      });

   /*=========================    FILETR    ==================================*/


  $('#change_category,#change_package').change(function(){
      company_list_table.ajax.reload();
     });

  /*===========================================================*/
  
  $('#company_list tbody').on( 'click', 'tr td.details-control', function () {
 
    var tr  =  $(this).closest('tr');
    var row =  company_list_table.row(tr);
    if ( row.child.isShown() ) {
      tr.removeClass( 'details' );
      row.child.hide();
    } else {
      tr.addClass( 'details' );
            companyDetails(row);
            var id = row.data().id; 
            $(tr).attr('row_id', id);
            close_existing_module(id,company_list_table);
      }
    }); 
    /*===========================================================*/

   });
   
/*===========================================================*/
 function companyDetails(row){
  var id = row.data().id; 
  row.child(datatable_loader).show();
  $.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-company-users-details-ajax')?>',
      data: {'id':id},
      success: function(ajaxresponse){
      response=JSON.parse(ajaxresponse);
      var res=response['data'];
         row.child(res).show();
       }
    }); 
 } 

/*===========================================================*/
 function close_existing_module(main_id,order_list_table){
   $('#company_list >tbody >tr').each(function() {
    var tr = $(this).closest('tr');
    var row = order_list_table.row( tr );
    var id=$(tr).attr("row_id");
    if (typeof(id) == "undefined") {
        id=0;
    }
    if ($(this).hasClass('details') && main_id!=id) {
      row.child.hide();
      tr.removeClass('details');
    }
  }); 
} 

   
/*===========================================================*/   
  function changeCompanyUsersStatus(id,status){
	 if(id>0){
		if(status==1){
			var set_status=0;
		}else{
			var set_status=1;
		}
 		$.ajax({
			type: "POST",
			url: '<?php echo base_url('admin-change-company-users-status')?>',
			data: {'id':id,'status':set_status},
			success: function(ajaxresponse){
				response=JSON.parse(ajaxresponse);
				if(response['status']){ 
					if(set_status){
						var html='<a href="javascript:void(0)" onClick="changeCompanyUsersStatus('+id+','+set_status+')"><span class="btn btn-sm btn-success">Active</span></a>';
            
					}else{
						var html='<a href="javascript:void(0)" onClick="changeCompanyUsersStatus('+id+','+set_status+')"><span class="btn btn-sm btn-danger">In-active</span></a>';
					}
          $('.successmsg').html('<div class="alert alert-success">Your status change successfully <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a></div>');
					$('#status'+id).html(html);

					company_list_table.ajax.reload();
				}
			}
		});	
	}
}

function claculateReview(rating){

            var fullStar = new Array(Math.floor(rating + 1)).join('<i class="fa fa-star"></i>');

            var halfStar = ((rating%1) !== 0) ? '<i class="fa fa-star-half-o"></i>': '';

            var noStar = new Array(Math.floor(5 + 1 - rating)).join('<i class="fa fa-star-o"></i>');

            var html=fullStar + halfStar + noStar;
            return html;
            
}

/*===========================================================*/


function updateCompany(id){
$.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-update-company-ajax')?>',
      data: {'id':id},
    }); 

}

</script>