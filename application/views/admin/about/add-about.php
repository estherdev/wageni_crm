<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add About Us
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add About Us</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">About Us Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
            <form method="post" id="about" action="<?= base_url('add-about/');?>" enctype="multipart/form-data">

              <div class="box-body">
              <div class="form-group row">
              	<div>
              	   <input type = "hidden"name="id" value="<?php echo $about[0]['id']; ?>" />
              	</div>

                <div class="col-md-6">
                  <label for="title">Name</label>
                  <input type="text" class="form-control" name="title" value="<?= $about[0]['title'];?>" />
                  <?= form_error('title')?>            
                </div>

                <div class="col-md-6">
                  <label for="Image">Image</label>
                  <input type="file" class="form-control"  name="userfile" value="<?= $about[0]['image'];?>"  />
                  <input type="hidden"  name="imghidden" value="<?= $about[0]['image'];?>"  />
                   <?= form_error('userfile')?>   
                </div> 
                 <div class="col-md-12">
                  <label for="title">Content</label>
                  <textarea type="text" class="form-control" name="title_text" /><?= $about[0]['content'];?></textarea> 
                  <?= form_error('title_text')?>            
                </div>
              
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-success">Update</button>
               <!--  <button type="reset" class="btn btn-danger">Reset</button> -->
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.content-wrapper -->
    </div>
      
    <!-- /.content-wrapper -->
  </div>

  <!-- /.content-wrapper -->
</section><!-- /.content -->
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here -->
