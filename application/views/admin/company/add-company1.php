<!--header Section Start Here -->

<?php include APPPATH . 'views/admin/header.php';?>

<!--header Section End Here -->



<!-- Left side column. contains the logo and sidebar -->

<?php include APPPATH . 'views/admin/main-sidebar.php';?>

<!-- Right side column. Contains the navbar and content of the page -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->

  <section class="content-header">

    <h1>

      Dashboard

      <small>Version 2.0</small>

    </h1>

    <ol class="breadcrumb">

      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

      <li class="active">Dashboard</li>

    </ol>

  </section>



  <!-- Main content -->

  <section class="content">

    <div class="row">

      <!-- left column -->

      <div class="col-md-12 ">

        <!-- general form elements -->

        <div class="box box-primary">

          <div class="box-header">

            <h3 class="box-title">Quick Example</h3>

          </div><!-- /.box-header -->

          <!-- form start -->

          <form role="form">

            <div class="box-body">

              <div class="form-group row">

                <div class="col-md-6">

                  <label for="c-name">Company Name</label>

                  <input type="text" class="form-control" id="c-name" placeholder="Enter Company Name">

                </div>

                <div class="col-md-6">

                  <label for="e-address">Email address</label>

                  <input type="email" class="form-control" id="e-address" placeholder="Enter email">

                </div>

              </div>

              <div class="form-group row">

                <div class="col-md-6">

                  <label for="m-number">Phone number</label>

                  <input type="number" class="form-control" id="m-number" placeholder="Enter Phone number">

                </div>

                <div class="col-md-6">

                  <label for="c-address">Address</label>

                  <input type="text" class="form-control" id="c-address" placeholder="Enter Company Address">

                </div>

                

              </div>

              <div class="form-group row">

                <div class="col-md-6">

                  <label for="select-country">Email address</label>

                  <select type="email" class="form-control" id="select-country" >

                    <option>--Select Country--</option>

                  </select>

                </div>

                <div class="col-md-6">

                 <label for="select-city">Email address</label>

                 <select type="email" class="form-control" id="select-city" >

                  <option>--Select City--</option>

                </select>

              </div>

            </div>

            <div class="form-group row">

              <div class="col-md-6">

                <label for="zip-code">Email address</label>

                <input type="number" class="form-control" id="zip-code" placeholder="Enter Zip Code">

              </div>

              <div class="col-md-6">

                

              </div>

            </div>

            

          </div><!-- /.box-body -->



          <div class="box-footer">

            <button type="submit" class="btn btn-primary">Submit</button>

          </div>

        </form>

      </div><!-- /.box -->

    </div>

  </div>

</section><!-- /.content -->

</div><!-- /.content-wrapper -->



<!--footer Section Start Here -->			

<?php include APPPATH . 'views/admin/footer.php';?>

<!--footer Section End Here -->