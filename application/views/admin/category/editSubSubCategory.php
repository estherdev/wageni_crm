
<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Update Category Process</h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Update Category Process</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Category Process Details</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
              <?php if($this->session->flashdata("success")):?>
              <div class="alert alert-success"><?= $this->session->flashdata("success");?>
              <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
              </div>
              <?php endif;?>
              <?php if($this->session->flashdata("failed")):?>
              <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
                <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
              </div>
              <?php endif;?>
          <form role="form" id="sub_sub_cat" action="<?= base_url('admin/masters/Category/updateCategoryProcess/'.$records[0]['sub_sub_cat_id']);?>" method="post" >
            <div class="box-body">
              <div class="form-group row">
                <div class="col-md-4">
                	<!-- [sub_sub_cat_id] => 6
            [main_cat_id] => 1
            [sub_category_id] => 10
            [sub_sub_cat_name] => morning
            [cat_id] => 1
            [category_name] => Hotel
            [createdDtm] => 2018-10-12 05:18:40
            [sub_cat_id] => 10
            [sub_cat_name] => Hotel on Airport -->
                  <label for="category_name">Select Category  </label>
                    <select class="form-control required" id="main_cat_id" name="main_cat_id">
                    <option value="">Select Category </option>
                     <?php
                       if(!empty($category ))
                        {
                            foreach ($category  as $ct)
                            {
                                ?>
                                <option value="<?php echo $ct->cat_id ?>" <?php echo set_select('main_cat_id', $ct->cat_id); ?> <?php if($ct->cat_id == $records[0]['main_cat_id']){echo "selected='selected'";}?>><?php echo $ct->category_name ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                      <?= form_error('main_cat_id');?>
                </div>

                <div class="col-md-4">
                  <label>Sub Category</label>
                  <select class="form-control scat"  name="subcat" id="subcat" >
                   <option value="<?= $records[0]['sub_category_id'];?>"<?php if('sub_cat_id' == $records[0]['sub_category_id']){echo "selected='selected'";}?>><?php echo $records[0]['sub_cat_name']?></option>
                  </select>
                  <?= form_error('subcat');?>
                </div>

                <div class="col-md-4">
                  <label for="category_name">Process</label>
                  <input type="text" class="form-control"  value="<?= $records[0]['sub_sub_cat_name'];?>" name="process_name">
                  <?= form_error('process_name');?>
                </div>
              </div>
              <div class="box-footer text-center">
                <button type="submit" class="btn btn-success">Update</button>
                <a href="<?= base_url('view-category-process');?>" class="btn btn-danger" role="button">Back</a>
              </div>
            </form>
          </div><!-- /.box -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <?php include APPPATH . 'views/admin/footer.php'; ?>
  <!-- page script -->

  <script>
   $(document).ready(function(){
    $('#main_cat_id').change(function(){
     var cat_id = $('#main_cat_id').val();
    
     if(cat_id != '')
     {
      $.ajax({
       url:"<?php echo base_url(); ?>admin/Dashboard/fetch_subcategory",
       method:"POST",
       data:{cat_id:cat_id},
       success:function(data)
       {
        $('#subcat').html(data);
        $('#subsubcat').html('<option value="">Select Process</option>');
       }
      });
     }
     else
     {
      $('#subcat').html('<option value="none">Select Sub category</option>');
      $('#subsubcat').html('<option value="">Select Process</option>');
     }
    });
   
    $('#subcat').change(function(){
     var sub_cat_id = $('#subcat').val();
     if(sub_cat_id != '')
     {
      $.ajax({
       url:"<?php echo base_url(); ?>admin/Dashboard/fetch_subsubcat",
       method:"POST",
       data:{sub_cat_id:sub_cat_id},
       success:function(data)
       {
        $('#subsubcat').html(data);
       }
      });
     }
     else
     {
      $('#subsubcat').html('<option value="">Select Process</option>');
     }
    });
    
   });
</script>

