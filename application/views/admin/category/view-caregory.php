<?php include APPPATH . 'views/admin/header.php';?>
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>

<div class="content-wrapper">
 <section class="content-header">
  <h1> Category Management </h1>
</section>
<section class="content">
 <form>
  <div class="row">
    <div class="col-md-2">
      <select class="form-control" id="change_status">
        <option value="">Select Status</option>
        <option value="1">Active</option>
        <option value="0">In-active</option>
      </select>
    </div>
    <div class="col-md-4"><input type="text" class="form-control" id="search" placeholder="Search Name"></div>

    <div class="col-xs-6 text-right">
      <div class="form-group">
        <a class="btn btn-success" href="<?php echo base_url('admin-manage-category'); ?>"><i class="fa fa-plus"></i> Add New</a>
      </div>
    </div>
  </div>

  
</form>

<div class="row">	
  <div class="col-xs-12">
    <div class="box">               
      <div class="box-body">
       <table class="table table-bordered" id="category_list">
         <thead>
           <tr>
             <th>Sr. No.</th>
             <th>Name</th>
             <th>Status</th>
             <th>Action</th>
           </tr>
         </thead>
         <tbody>
          
         </tbody>
       </table>
       
     </div>
     
   </div>
 </div>
</div>
</section>
</div>
<?php include APPPATH . 'views/admin/footer.php'; ?>
<script>
 var category_list_table='';
 $(function () { 
  category_list_table = $('#category_list').DataTable({
    "processing": true,
    "pageLength": <?php echo $this->config->item('record_per_page');?>,
    "serverSide": true,
    "sortable": true,
    "lengthMenu": [[10,20, 30, 50,100], [10,20, 30, 50,100]],
    "language": {
      "emptyTable": "<?php echo $this->config->item('record_not_found_title');?>"
    },
    "order": [
    [0, "desc"]
    ],
    "ajax":{
      url :"<?php echo base_url('admin-manage-category-ajax')?>", 
      type: "post", 
      data: function (d) {      
        d.status = $('#change_status').val();
        d.search = $('#search').val();
        
      } 
    } ,
    "columns": [ 
    {data: 'id',"orderable":false},
    {data: 'name',"orderable":false,class:'center'},
    {data: 'status',"orderable":false,class:'center'}, 
    {data: 'id',"orderable":false,class:'center'},
    ],
    
    "columnDefs": [
    {
      "render": function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1+'.';
      },
      
      "targets": 0 
    },
    {
      "render": function ( data, type, row ) {
        return data;
      },
      
      "targets": 1 
    },
    
    {
      "render": function ( data, type, row ) {
        var html ='<span id="loader'+row['id']+'"></span><span id="status'+row['id']+'">';
        html +='<a href="javascript:void(0)" onClick="changeCategoryStatus('+row['id']+','+row['status']+')">'+getStatus(data)+'</a>';
        html +='</span>';
        return html;
      },
      
      "targets": 2 
    },
    {
      "render": function ( data, type, row ) {
        var html='<a href="<?php echo base_url('admin-update-category/')?>'+row['id']+'" title="Update Category" class="btn btn-success btn-sm">';
        html +='<span class="glyphicon glyphicon-edit"></span></a> ';
           /* html +='<a href="javascript:void(0);" onClick="deleteRecord('+data+');" title="Delete Category" class="btn btn-danger btn-sm">';
           html +='<span class="glyphicon glyphicon-trash"></span></a>';*/
           
           return html;
         },
         
         "targets": 3 
       },
       
       ],
       "dom": "<<'dt-toolbar'<'col-xs-12 col-sm-6'><'col-sm-6 col-xs-12'l>>"+
       "t"+"<'dt-toolbar-footer row'<'col-md-5'i><'col-md-7'p>>r","autoWidth" : true,
       "footerCallback": function ( row, data, start, end, display ) {
        //setCheckboxSelected(data);
      }
    });

  $('#change_status').change(function(){
    category_list_table.ajax.reload();
  });

  $('#search').on('keyup', function() {
    category_list_table.search(this.value).draw();
  });
  $('#delete_record').click(function(){
    actionPerform('<?php echo base_url('admin-delete-category')?>',category_list_table);
  });
})

 function changeCategoryStatus(id,status){

  if(id>0){
    changeDataStatus('changeCategoryStatus',id,status,'<?php echo base_url('admin-change-category-status')?>',category_list_table);
  }
} 

function actionPerform(action_url,table_id){
  $('#deleteModalPopup').modal('hide');
  var action_id=$('#action_id').val();
  $.ajax({
    type: "POST",
    url: action_url,
    data: {'action_id':action_id},
    success: function(ajaxresponse){
      response=JSON.parse(ajaxresponse);
      if(!response['status']){
        $('#success_message').html(form_error_message);
      }else{
        $('#success_message').html('<div class="alert alert-success">'+response['message']+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        table_id.ajax.reload();
        closePopup('success_message');
      }  
    }
  });
}

function changeDataStatus(function_name,id,status,url,current_url=''){
  if(id>0){
    $('#status'+id).html('');
    $('#loader'+id).html(small_loader);
    if(status==1){
      var set_status=0;
    }else{
      var set_status=1;
    }
    $.ajax({
      type: "POST",
      url:url,
      data: {'id':id,'status':set_status},
      success: function(ajaxresponse){
        response=JSON.parse(ajaxresponse);
        if(response['status']){ 
          if(set_status){
            var html='<a href="javascript:void(0)" onClick="'+function_name+'('+id+','+set_status+')"><span class="btn btn-sm btn-success">Active</span></a>';
          }else{
            var html='<a href="javascript:void(0)" onClick="'+function_name+'('+id+','+set_status+')"><span class="btn btn-sm btn-danger">In-active</span></a>';
          }
          $('#status'+id).html(html);
          $('#loader'+id).html('');
          if(current_url!=''){
            current_url.ajax.reload();
          }
        }
      }
    }); 
  }
}


function deleteRecord(id){
  if(id>0){
    $('#deleteModalPopup').modal('show');
    $('#action_id').val(id);
    $('#delete_confirmation_message').html('Are you sure want to delete this Record?');
  }
}
function closePopup(id){
  $("#"+id).fadeTo(5000, 500).slideUp(500, function(){
    $("#"+id).slideUp(5000);
  });
}

</script>



