
<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Update Category Process</h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Update Category Process</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Category Process Details</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
              <?php if($this->session->flashdata("success")):?>
              <div class="alert alert-success"><?= $this->session->flashdata("success");?>
              <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
              </div>
              <?php endif;?>
              <?php if($this->session->flashdata("failed")):?>
              <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
                <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
              </div>
              <?php endif;?>
          <form role="form" action="<?= base_url('view-category-process');?>" method="post" >
            <div class="box-body">
              <div class="form-group row">
                <div class="col-md-4">
                  <label for="category_name">Select Category </label>
                    <select class="form-control required" id="main_cat_id" name="main_cat_id">
                    <option value="none">Select Category </option>
                     <?php
                        if(!empty($category ))
                        {
                            foreach ($category  as $ct)
                            {
                                ?>
                                <option value="<?php echo $ct->cat_id ?>" <?php if($ct->cat_id == set_value('category')) {echo "selected=selected";} ?>><?php echo $ct->category_name ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                      <?= form_error('main_cat_id');?>
                </div>

                <div class="col-md-4">
                  <label>Sub Category</label>
                  <select class="form-control scat" name="subcat" id="subcat" >
                  </select>
                </div>

                <div class="col-md-4">
                  <label for="category_name">Process</label>
                  <input type="text" class="form-control"  name="process_name" placeholder="Enter Process Name">
                  <?= form_error('process_name');?>
                </div>
              </div>
              <div class="box-footer text-center">
                <button type="submit" class="btn btn-success">Submit</button>
                 <a href="<?= base_url('view-category-process');?>" class="btn btn-danger" role="button">Back</a>
              </div>
            </form>
          </div><!-- /.box -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><b>Category Process List</h3></b>
            </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped text-center">
                    <thead>
                      <tr>
                        <th>S.No.</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Category Process</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i= 1; foreach ($process as $key => $subcat) {?>
                      <tr>
                        <td><?= $i++; ?></td>
                        <td><?= ucfirst($subcat->category_name); ?></td>
                        <td><?= ucfirst($subcat->sub_cat_name); ?></td>
                        <td><?= ucfirst($subcat->sub_sub_cat_name); ?></td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-info" href="<?php echo base_url('edit-category-process/'.$subcat->sub_sub_cat_id) ;?>" title="Edit"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-sm btn-danger deleteCategory"  data-toggle="modal" data-target="#deleteModal<?= $subcat->sub_sub_cat_id?>" title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                            <div id="deleteModal<?= $subcat->sub_sub_cat_id?>" class="modal fade">
                            <div class="modal-dialog">
                            <div class="modal-content">
                            <!-- dialog body -->
                              <div class="modal-body">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              Are You Sure to <strong>Process</strong> Delete?
                              </div>
                            <!-- dialog buttons -->
                            <div class="modal-footer"><button type="button" class="btn btn-primary" onclick="window.location='<?php echo base_url('delete-category-process/'.$subcat->sub_sub_cat_id); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                            </div>
                            </div>
                            </div>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <?php include APPPATH . 'views/admin/footer.php'; ?>
  <!-- page script -->

        <script>
   $(document).ready(function(){
    $('#main_cat_id').change(function(){
     var cat_id = $('#main_cat_id').val();
    
     if(cat_id != '')
     {
      $.ajax({
       url:"<?php echo base_url(); ?>admin/Dashboard/fetch_subcategory",
       method:"POST",
       data:{cat_id:cat_id},
       success:function(data)
       {
        $('#subcat').html(data);
        $('#subsubcat').html('<option value="">Select Process</option>');
       }
      });
     }
     else
     {
      $('#subcat').html('<option value="none">Select Sub category</option>');
      $('#subsubcat').html('<option value="">Select Process</option>');
     }
    });
   
    $('#subcat').change(function(){
     var sub_cat_id = $('#subcat').val();
     if(sub_cat_id != '')
     {
      $.ajax({
       url:"<?php echo base_url(); ?>admin/Dashboard/fetch_subsubcat",
       method:"POST",
       data:{sub_cat_id:sub_cat_id},
       success:function(data)
       {
        $('#subsubcat').html(data);
       }
      });
     }
     else
     {
      $('#subsubcat').html('<option value="">Select Process</option>');
     }
    });
    
   });
</script>

