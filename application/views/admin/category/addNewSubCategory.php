
<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Add Sub Category</h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Add Sub Category</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Sub Category Details</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
              <?php if($this->session->flashdata("success")):?>
              <div class="alert alert-success"><?= $this->session->flashdata("success");?>
              <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
              </div>
              <?php endif;?>
              <?php if($this->session->flashdata("failed")):?>
              <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
                <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
              </div>
              <?php endif;?>
          <form role="form" id="sub_cat" action="<?= base_url('view-sub-category');?>" method="post" >
            <div class="box-body">
              <div class="form-group row">
                <div class="col-md-6">
                  <label for="category_name">Select Category </label>
                    <select class="form-control required" id="main_cat_id" name="main_cat_id">
                    <option value="">Select Category </option>
                     <?php
                        if(!empty($category ))
                        {
                            foreach ($category  as $ct)
                            {
                                ?>
                                <option value="<?php echo $ct->cat_id ?>" <?php if($ct->cat_id == set_value('category')) {echo "selected=selected";} ?>><?php echo $ct->category_name ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                      <?= form_error('main_cat_id');?>
                </div>
                <div class="col-md-6">
                  <label for="category_name">Sub Category Name</label>
                  <input type="text" class="form-control"  name="sub_cat_name" placeholder="Enter Sub Category Name">
                  <?= form_error('sub_cat_name');?>
                </div>
              </div>
              <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
              </div>
            </form>
          </div><!-- /.box -->
        </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->
      </div><!-- /.content-wrapper -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><b>Sub Category List</h3></b>
            </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped text-center">
                    <thead>
                      <tr>
                        <th>S.No.</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i= 1; foreach ($subcategory as $key => $subcat) {?>
                      <tr>
                        <td><?= $i++; ?></td>
                        <td><?= ucfirst($subcat->category_name); ?></td>
                        <td><?= ucfirst($subcat->sub_cat_name); ?></td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-info" href="<?php echo base_url('edit-sub-category/'.$subcat->sub_cat_id) ;?>" title="Edit"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-sm btn-danger deleteCategory"  data-toggle="modal" data-target="#deleteModal<?= $subcat->sub_cat_id?>" title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                            <div id="deleteModal<?= $subcat->sub_cat_id?>" class="modal fade">
                            <div class="modal-dialog">
                            <div class="modal-content">
                            <!-- dialog body -->
                              <div class="modal-body">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              Are You Sure to <strong> Sub Category</strong> Delete?
                              </div>
                            <!-- dialog buttons -->
                            <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('delete-sub-category/'.$subcat->sub_cat_id); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                            </div>
                            </div>
                            </div>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <?php include APPPATH . 'views/admin/footer.php'; ?>
  <!-- page script -->

