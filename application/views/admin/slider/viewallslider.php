
<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Home Page Slider</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Home Page Slider</li>
    </ol>
  </section>
     

  <!-- Main content -->
  <section class="content">
     <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <div class="row">

      <div class="col-md-12 col-sm-6 col-xs-12">

        <!-- general form elements -->

        <div class="box box-primary">

          <div class="box-header">

            <h3 class="box-title">Add Slider</h3>

          </div><!-- /.box-header -->

          <!-- form start -->

          <form role="form" id="viewslider" action="<?= base_url('view-slider');?>" method="post" enctype="multipart/form-data" >

            <div class="box-body">
              <div class="form-group row">
                <div class="col-md-6">
                  <label for="slider_title">Title</label>
                  <input type="text" class="form-control"  name="slider_title" placeholder="Enter Title">
                  <?= form_error('slider_title');?>
                </div>

                <div class="col-md-6">
                  <label for="slider_sub_title">Sub Title</label>
                  <input type="text" class="form-control" name="slider_sub_title" placeholder="Enter Sub Title">
                  <?= form_error('slider_sub_title');?>
                </div>
              </div>
              <div class="form-group row">

                <div class="col-md-4">
                  <label for="slider_link_title">Url Title</label>
                  <input type="text" class="form-control" name="slider_link_title" placeholder=""> 
                  <?= form_error('slider_link_title');?>
                </div>

                <div class="col-md-4">
                  <label for="slider_link">Url</label>
                  <input type="text" class="form-control" name="slider_link" placeholder=""> 
                  <?= form_error('slider_link');?>
                </div>              

                <div class="col-md-4">
                  <label for="slider_image">Banner</label>
                  <input type="file" class="form-control" name="slider_image" placeholder=""> 
                  <?= form_error('slider_image');?>
                </div>                  

              </div><!-- /.box-body -->

              <div class="box-footer text-center">

                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>

              </div>

            </form>

          </div><!-- /.box -->

        </div><!-- /.content-wrapper -->

      </div><!-- /.content-wrapper -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Home Sliders</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Title</th>
                    <th>Sub Title</th>
                    <th>Link Text</th>
                    <th>Url</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i= 1; foreach ($homeslider as $key => $slider) {?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= ucfirst($slider->title); ?></td>
                    <td><?= ucfirst($slider->sub_title); ?></td>
                    <td><?= $slider->url_link_text; ?></td>
                    <td><?= $slider->url_link; ?></td>
                    <td><img src="<?= base_url().$slider->slider_image?>" height="50px" width="70px"></td>
                     <td class="text-center" style="white-space: nowrap">

                            <a class="btn btn-sm btn-info" href="<?php echo base_url().'editSlider/'.$slider->slider_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>

                            <a class="btn btn-sm btn-danger deleteSlider"  data-toggle="modal" data-target="#deleteModal<?= $slider->slider_id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>

                        </td>

                            <div id="deleteModal<?= $slider->slider_id ; ?>" class="modal fade">

                               <div class="modal-dialog">

                              <div class="modal-content">

                              <!-- dialog body -->

                              <div class="modal-body">

                               <button type="button" class="close" data-dismiss="modal">&times;</button>

                               Are You Sure to <strong>Slider</strong> Delete?

                              </div>

                              <!-- dialog buttons -->

                              <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('deleteSlider/'.$slider->slider_id ); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>

                              </div>

                               </div>

                             </div>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <?php include APPPATH . 'views/admin/footer.php'; ?>
  <!-- page script -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>

</body>
</html>
