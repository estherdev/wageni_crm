
<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Update Home Page Slider</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Update Home Page Slider</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">

      <div class="col-md-12 col-sm-6 col-xs-12">

        <!-- general form elements -->

        <div class="box box-primary">

          <div class="box-header">

            <h3 class="box-title">Update Sliders</h3>

          </div><!-- /.box-header -->

          <!-- form start -->

          <form role="form" id="editslider" action="<?= base_url('updateSlider/'.$sliderById[0]->slider_id);?>" method="post" enctype="multipart/form-data" >

            <div class="box-body">
              <div class="form-group row">
                <div class="col-md-6">
                  <label for="slider_title">Title</label>
                  <input type="text" class="form-control"  name="slider_title" value="<?= $sliderById[0]->title ; ?>" placeholder="Enter Title">
                  <?= form_error('slider_title');?>
                </div>

                <div class="col-md-6">
                  <label for="slider_sub_title">Sub Title</label>
                  <input type="text" class="form-control" name="slider_sub_title" value="<?= $sliderById[0]->sub_title ; ?>" placeholder="Enter Sub Title">
                  <?= form_error('slider_sub_title');?>
                </div>
              </div>
              <div class="form-group row">

                <div class="col-md-4">
                  <label for="slider_link_title">Url Title</label>
                  <input type="text" class="form-control" name="slider_link_title" value="<?= $sliderById[0]->url_link_text ; ?>" placeholder=""> 
                  <?= form_error('slider_link_title');?>
                </div>

                <div class="col-md-4">
                  <label for="slider_link">Url</label>
                  <input type="text" class="form-control" name="slider_link" value="<?= $sliderById[0]->url_link ; ?>" placeholder=""> 
                  <?= form_error('slider_link');?>
                </div>              

                <div class="col-md-4">
                  <label for="slider_image">Banner</label>
                  <input type="hidden" name="imghidden" value="<?= $sliderById[0]->slider_image; ?>">
                  <input type="file" class="form-control" name="slider_image" value="<?= $sliderById[0]->slider_image ; ?>" placeholder=""> 
                  <?= form_error('slider_image');?>
                </div>                  

              </div><!-- /.box-body -->

              <div class="box-footer text-center">

                <button type="submit" class="btn btn-success" name="update">Update</button>
                <a href="<?= base_url('view-slider');?>" class="btn btn-danger" role="button">Back</a>
              </div>

            </form>

          </div><!-- /.box -->

        </div><!-- /.content-wrapper -->

      </div><!-- /.content-wrapper -->
      
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <?php include APPPATH . 'views/admin/footer.php'; ?>
