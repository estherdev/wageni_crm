<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Wageni| CRM Forgot Password</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?= base_url();?>globalassets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?= base_url();?>globalassets/admin/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?= base_url();?>globalassets/admin/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
        <div class="login-logo">
            <a href=""><img src="<?= base_url();?>globalassets/admin/logo1.png"></a>
       <!--  <a href=""><img src="<?//= base_url();?>globalassets/website/img/logo.jpg"></a> -->
        <!-- <a href="#"><b>Wageni</b>CRM</a> -->
      </div>
      </div> 
      <div class="login-box-body">
        <h3 class="login-box-msg"><strong>Forgot Password</strong></h3>
        <p class="statusMsg"></p>  
         <div id="error"></div> 
        <form id="admin-login-form" method="post" action="<?= base_url('admin/AdminLogin/forgotPassword');?>">
          <div class="form-group has-feedback">
             <p id="lemailerr"></p>
            <input type="email" class="form-control" name="adminEmail" id="adminEmail" placeholder="Enter Email ID"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="row submitBtn">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat regBtn" name="forgotPassword" id="btn-submit" >Send</button>
              <a class="" href="<?= base_url('loginMe')?>">Sign In</a>
            </div> 
          </div>
        </form>
      </div> 
    </div> 
  </body>
</html>

