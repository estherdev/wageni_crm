<!--header Section Start Here -->
<?php include APPPATH . 'views/webadmin/header.php';?>
<!--header Section End Here -->

<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/webadmin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Guest User 
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add Guest User </li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Guest User Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
            <form method="post" id="guest" action="<?= base_url('guestuser');?>">

              <div class="box-body">
              <div class="form-group row">

                <div class="col-md-3">
                  <label>Hotel Name</label>
                   <select class="form-control subcat" name="subcat" id="subcat" >
                    
                  </select>
                </div>               
                <!-- <div class="col-md-4">
                  <label for="hotelname"><?//= $this->lang->line('Hotel_Name')?></label>
                  <input type="text"  class = "form-control" autofocus ="" name="hotel_name" />
                  <?//= form_error('hotel_name')?>            
                </div> -->

                <div class="col-md-3">
                  <label for="username"><?= $this->lang->line('User_Name')?></label>
                  <input type="text" class="form-control"  name="user_name"  />
                   <?= form_error('user_name')?>   
                </div>

                <div class="col-md-3">
                  <label for="usermail"><?= $this->lang->line('User_Mail_Id')?></label>
                  <input type="text" class="form-control"  name="user_id"  />
                   <?= form_error('user_id')?>   
                </div>

                <div class="col-md-3">
                  <label for="usermail">Phone</label>
                  <input type="text" class="form-control"  name="user_phone"  />
                   <?= form_error('user_phone')?>   
                </div>

                </div>

                <div class="form-group row">
                 <div class="col-md-3">
                  <label for="adult"><?= $this->lang->line('No_Of_Adult')?></label>
                  <input type="text"  class = "form-control" name="no_of_adult" />
                  <?= form_error('no_of_adult')?>            
                </div>

                <div class="col-md-3">
                  <label for="content"><?= $this->lang->line('No_Of_Children')?></label>
                  <input type="text"  class = "form-control" name="no_of_children" />
                  <?= form_error('no_of_children')?>            
                </div>

                <div class="col-md-3">
                  <label for="content"><?= $this->lang->line('No_Of_Room')?></label>
                  <input type="text"  class = "form-control" name="no_of_room" />
                  <?= form_error('no_of_room')?>            
                </div>

                <div class="col-md-3">
                  <label>Nationality</label>
                   <select class="form-control user_nationality" name="user_nationality" id="user_nationality" >
                      <option value="">Select Nationality</option>
                      <?php 
                      		$countries=getCountries();
                      		foreach ($countries as $key => $countrie) {?>

                     <option value="<?= $countrie['id']?>"><?= $countrie['name']?></option>
                      <?php } ?>
                  </select>
                </div>  
                </div>
                <div class="form-group row">
                  <div class='col-md-3'>
                    <div class="form-group">
                      <label>Check In:</label>
                      <input type='text' class="form-control" name="check_in"  id='datetimepicker6'>

                      <?= form_error('check_in')?>  
                    </div>
                  </div>
               <!--    <div class='col-md-4'>
                    <div class="form-group">
                      <label>Address</label>
                      <input type='text' class="form-control" name="address" id='addres'>

                      <?= form_error('address')?>  
                    </div>
                  </div> -->
                  <div class='col-md-3'>
                    <div class="form-group">
                      <label>Check Out:</label>
                      <input type='text' class="form-control" name="check_out" id='datetimepicker7'>
                      
                      <?= form_error('check_out')?>  
                    </div>
                  </div>

                  <div class='col-md-6'>
                    <div class="form-group">
                      <label>Address:</label>
                      <textarea type='text' class="form-control" name="user_address" id='user_address'>  </textarea> 
                      
                      <?= form_error('user_address')?>  
                    </div>
                  </div>


              <!-- <div class="col-md-4">
                  <label for="content"><?= $this->lang->line('Check_In')?></label>
                  <input type="text"  class = "form-control" id='datetimepicker1' name="check_in" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  <?= form_error('check_in')?>            
                </div> -->

                 <!-- <div class="col-md-4">
                  <label for="content"><?= $this->lang->line('Check_Out')?></label>
                  <input type="text"  class = "form-control" id='datetimepicker2' name="check_out" />
                  <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  <?= form_error('check_out')?>            
                </div>   -->               
              
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.content-wrapper -->
    </div>
    <!-- /.content-wrapper -->
  
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><b>Guest User Details</h3></b>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">

              <table id="example1" class="table table-bordered table-striped table-sm text-center text-nowrap">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th><?= 'Category'; ?></th>
                    <th><?= $this->lang->line('Hotel_Name')?></th>
                    <th><?= $this->lang->line('User_Name')?></th>
                    <th><?= $this->lang->line('User_Mail_Id')?></th>
                    <th><?= $this->lang->line('No_Of_Adult')?></th>
                    <th><?= $this->lang->line('No_Of_Children')?></th>
                    <th><?= $this->lang->line('No_Of_Room')?></th>
                    <th><?= $this->lang->line('Check_In')?></th>
                    <th><?= $this->lang->line('Check_Out')?></th>
                    <!-- <th>Status</th> -->
                    <th>Pre Stay</th>
                    <th>On Stay</th>
                    <th>Post Stay</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i= 1; foreach ($alluser as $key => $user) {?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= ucfirst($user->category_name); ?></td>
                    <td><?= ucfirst($user->sub_cat_name); ?></td>
                    <td><?= ucfirst($user->user_name); ?></td>                   
                    <td><?= $user->user_email_id; ?></td>                   
                    <td><?= $user->no_of_adult; ?></td>                   
                    <td><?= $user->no_of_children; ?></td>                   
                    <td><?= $user->no_of_room; ?></td>                   
                    <td><?= $user->check_in; ?></td>                   
                    <td><?= $user->check_out; ?></td>                   
                    <!-- <td><?= $user->status; ?></td>                    -->
                    <td><i class="fa fa-check-circle-o fa-2x text-success" ></i></td>   
                    <td><i class="fa fa-times-circle-o fa-2x text-danger" ></i></td>   
                    <td><i class="fa fa-times-circle-o fa-2x text-danger" ></i></td>                     
                    <td class="text-center" style = "white-space: nowrap">
                    <!--  <a class="btn btn-sm btn-info" href="<?php //echo base_url("edit-guestuser/".$user->user_id);?>" title="Edit"><i class="fa fa-pencil"></i></a> -->
                 <!--    <a class="btn btn-sm btn-info" href="<?php// echo base_url('editGuest/'.base64_encode(urlencode($user->user_id)));?>" title="Edit"><i class="fa fa-pencil"></i></a> -->
                    <a class="btn btn-sm btn-danger deleteSlider"  data-toggle="modal" data-target="#deleteModal<?= $user->user_id?>" title="Delete"><i class="fa fa-trash"></i></a>
                    </td>
                        <div id="deleteModal<?= $user->user_id?>" class="modal fade">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        <!-- dialog body -->
                          <div class="modal-body">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          Are You Sure to <strong>Guest</strong> Delete?
                          </div>
                        <!-- dialog buttons -->
                        <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('delete-guestuser/'.$user->user_id); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                        </div>
                        </div>
                        </div>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->

  <!-- /.content-wrapper -->
</section><!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/webadmin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>


<script>
  $(document).ready(function(){

    var cat_id = '<?= $this->session->userdata('category')?>';

    if(cat_id != '')
    {
      $.ajax({
        url:"<?php echo base_url(); ?>website/SubsControler/fetch_subcategory",
        method:"POST",
        data:{cat_id:cat_id},
        success:function(data)
        {
          $('#subcat').html(data);
        }
      });
    }
    else
    {
      $('#subcat').html('<option value="">Select Hotel</option>');
    }

  });
</script>
 <script type="text/javascript">
       /*$(function () {
            $('#datetimepicker6').datetimepicker();
            $('#datetimepicker7').datetimepicker();
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });
        });*/
    </script>



  <script type="text/javascript">
 var vals = [];
$(document).ready(function(){
 $("#datetimepicker6").datepicker({
            dateFormat: "dd-mm-yy",
            maxDate: 0,
            onSelect: function () {
                var dt2 = $('#datetimepicker7');
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                var dt2Date = dt2.datepicker('getDate');
                //difference in days. 86400 seconds in day, 1000 ms in second
                var dateDiff = (dt2Date - minDate)/(86400 * 1000);
                
                startDate.setDate(startDate.getDate() + 30);
                if (dt2Date == null || dateDiff < 0) {
                    dt2.datepicker('setDate', minDate);
                }
                else if (dateDiff > 30){
                    dt2.datepicker('setDate', startDate);
                }
                //sets dt2 maxDate to the last day of 30 days window
                dt2.datepicker('option', 'minDate', startDate);
                dt2.datepicker('option', 'minDate', minDate);
            }
        });
        $('#datetimepicker7').datepicker({
            dateFormat: "dd-mm-yy",
            maxDate: 0
        });
       });
    </script>