<aside class="main-sidebar">
    <section class="sidebar">
       <div class="user-panel">
          <?php $Aprofile = admin_login();  ?>
         <div class="pull-left image">
             <?php if($Aprofile['userfile']== false) {?>
            <img src="<?= base_url();?>globalassets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" height="30px;" width="30px;"/>
            <?php }else { ?>
            <img src="<?= base_url();?>uploads/<?= $Aprofile['userfile'];?>" class="img-circle" alt="User Image" height="30px;" width="30px;" />
            <?php } ?>
         </div>
         <div class="pull-left info">
            <p><?= ucfirst($this->session->userdata('admin_name'));?></p>
            <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
         </div>
      </div>
      </form>
       <ul class="mainmenu sidebar-menu">
         <li class="header">MAIN NAVIGATION</li>
         <li class="treeview">
            <a href="<?= base_url('admin/Dashboard')?>">
            <i class="fa fa-dashboard"></i> <span><?= $this->lang->line('DASHBOARD');?></span> <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li>
         
         <li class="treeview">
            <a href="javascript:void(0)">
            <i class="fa fa-user"></i> <span>Customer</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?= base_url('admin-add-company');?>"><i class="fa fa-circle-o"></i> Add Customer</a></li>
                <li><a href="<?= base_url('admin-view-customer');?>"><i class="fa fa-circle-o"></i> View Customer</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="<?= base_url('admin-view-all-company')?>">
               <i class="fa fa-bar-chart-o"></i> <span>All Company Status</span><i class="fa fa-angle-left pull-right"></i> </a>
            </a>
         </li>
         <li class="treeview">
            <a href="<?= base_url('admin-view-company-status')?>">
               <i class="fa fa-cubes"></i> <span>Account Status</span><i class="fa fa-angle-left pull-right"></i> </a>
            </a>
         </li>

         <li class="treeview">
            <a href="javascript:void(0)">
            <i class="fa fa-file-text"></i> <span>Survey</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
               <li><a href="<?= base_url('survey');?>"><i class="fa fa-circle-o"></i> Create </a></li>
               <li><a href="<?= base_url('allQueByCat');?>"><i class="fa fa-circle-o"></i> View</a></li>
               <li><a href="<?= base_url('admin-view-all-questions-list');?>"><i class="fa fa-circle-o"></i> Add Survey Questions List</a></li>

            </ul>
         </li>
         <li class="treeview">
            <a href="<?= base_url('admin-view-feedback')?>">
           <!--  <a href="<?= base_url('user-complaints')?>"> -->
               <i class="fa fa-comments"></i> <span>Feedback</span><i class="fa fa-angle-left pull-right"></i> </a>
            </a>
         </li>
         <li class="treeview">
            <a href="javascript:void(0)">
            <i class="fa fa-user-secret"></i> <span>Admin</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?= base_url('add-admin-role');?>"><i class="fa fa-circle-o"></i> Admin Role</a></li>
                <li><a href="<?= base_url('add-user');?>"><i class="fa fa-circle-o"></i> Add Employee</a></li>
               <!-- <li><a href="<?//= base_url('add-subscriber');?>"><i class="fa fa-circle-o"></i>Add Subscriber</a></li> -->
            </ul>
         </li>
         <li class="treeview">
            <a href="javascript:void(0)">
            <i class="fa fa-asterisk"></i> <span>Masters</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
               <li><a href="<?= base_url('admin-view-category');?>"><i class="fa fa-circle-o"></i> Category</a></li>
              <!--  <li><a href="<?= base_url('view-category');?>"><i class="fa fa-circle-o"></i> Category</a></li> -->
               <li><a href="<?= base_url('view-sub-category');?>"><i class="fa fa-circle-o"></i> Sub Category</a></li> 
               <li><a href="<?= base_url('view-category-process');?>"><i class="fa fa-circle-o"></i>Category Process</a></li> 
            </ul>
         </li>
         <li class="treeview">
                <a href="javascript:void(0)">
                    <i class="fa fa-file"></i> <span>Manage Website Pages</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                 <li><a href="<?= base_url('view-clientlogo');?>"><i class="fa fa-circle-o"></i>Client Logo</a></li>
                 <li><a href="<?= base_url('view-history');?>"><i class="fa fa-circle-o"></i>History</a></li>
                 <li><a href="<?= base_url('view-slider');?>"><i class="fa fa-circle-o"></i>Slider</a></li>
                 <li><a href="<?= base_url('view-contact');?>"><i class="fa fa-circle-o"></i>Contact</a></li>
                 <li><a href="<?= base_url('view-blog');?>"><i class="fa fa-circle-o"></i>Blogs</a></li>
                 <li><a href="<?= base_url('view-testimonials');?>"><i class="fa fa-circle-o"></i>Testimonials</a></li>
                 <li><a href="<?= base_url('view-packages');?>"><i class="fa fa-circle-o"></i> Packages</a></li>
                 <li><a href="<?= base_url('view-practice');?>"><i class="fa fa-circle-o"></i> Practice Area</a></li>
                 <li><a href="<?= base_url('view-faq');?>"><i class="fa fa-circle-o"></i> FAQ</a></li>
                 <li><a href="<?= base_url('add-about');?>"><i class="fa fa-circle-o"></i> About</a></li>

             </ul>
            </li>
           <li class="treeview">
            <a href="javascript:void(0)">
            <i class="fa fa-building"></i> <span>Company</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
<!--                <li><a href="<?= base_url('new-company');?>"><i class="fa fa-circle-o"></i> New Company</a></li>
 -->               <li><a href="<?= base_url('view-company-admin');?>"><i class="fa fa-circle-o"></i> Company Admin</a></li>
               <li><a href="<?= base_url('view-company-sub-admin');?>"><i class="fa fa-circle-o"></i> Company Sub Admin </a></li>
            </ul>
         </li> 
         <!-- 
         <li class="treeview">
            <a href="<?//= base_url('add-company');?>">
            <i class="fa fa-dashboard"></i> <span>Manage Company</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
         </li> -->
      </ul>
   </section>
   <!-- /.sidebar -->
</aside>