
<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php'; ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Home Page FAQ</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Home Page FAQ</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("danger")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("danger");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <div class="row">

      <div class="col-md-12 col-sm-6 col-xs-12">

        <!-- general form elements -->

        <div class="box box-primary">

          <div class="box-header">

            <h3 class="box-title">FAQ</h3>

          </div><!-- /.box-header -->

          <!-- form start -->
            <?php echo $this->session->flashdata('successmsg');?>
          <form role="form" id="faq" action="<?= base_url('view-faq');?>" method="post" >

            <div class="box-body">
              <div class="form-group row">
                <div class="col-md-12">
                  <label for="question">Que.</label>
                  <input type="text" class="form-control"  name="question" placeholder="Enter Question">
                  <?= form_error('question');?>
                </div>

                <div class="col-md-12">
                  <label for="answer">Ans.</label>
                  <textarea type="text" class="form-control" name="answer" placeholder="Enter Answer"></textarea>
                  <?= form_error('answer');?>
                </div>

              </div>


              <div class="box-footer text-center">

                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>

              </div>

            </form>

          </div><!-- /.box -->

        </div><!-- /.content-wrapper -->

      </div><!-- /.content-wrapper -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><b>FREQUENTLY ASKED QUESTIONS</h3></b>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i= 1; foreach ($alldata as $key => $faq) {?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= ucfirst($faq->faq_que); ?></td>
                    <td><?= ucfirst($faq->faq_ans); ?></td>                   
                    <td class="text-center" style="white-space: nowrap">
                            <a class="btn btn-sm btn-info" href="<?php echo base_url('editFaq/'.$faq->faq_id) ;?>" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-sm btn-danger deleteSlider"  data-toggle="modal" data-target="#deleteModal<?= $faq->faq_id?>" title="Delete"><i class="fa fa-trash"></i></a>
                    </td>
                            <div id="deleteModal<?= $faq->faq_id?>" class="modal fade">
                            <div class="modal-dialog">
                            <div class="modal-content">
                            <!-- dialog body -->
                              <div class="modal-body">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              Are You Sure to <strong>Faq</strong> Delete?
                              </div>
                            <!-- dialog buttons -->
                            <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('deleteFaq/'.$faq->faq_id); ?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                            </div>
                            </div>
                            </div>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  <?php include APPPATH . 'views/admin/footer.php'; ?>
  <!-- page script -->

