<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit History
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Edit History</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">History Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->                       
            <form method="post" id="edithistory" action="<?= base_url('admin/masters/History/editHistory/');?><?= $records[0]['h_id']?>">

              <div class="box-body">
              <div class="form-group row">

                <div class="col-md-6">
                  <label for="title">Date</label>
                  <input type="date" value="<?php echo $records[0]['history_date'];?>" class = "form-control" name="date" />
                  <?= form_error('date')?>            
                </div>

                <!--  <div class="col-md-6">
                  <label for="created date">Title</label>
                  <input type="text" value="<?php //echo $records[0]['title'];?>" class = "form-control" name="title" />
                  <?= form_error('title')?>            
                </div> -->

                 <div class="col-md-12">
                  <label for="content">Content</label>
                  <textarea type="text"  class = "form-control" name="content" /><?php echo $records[0]['content'];?></textarea>
                  <?= form_error('content')?>            
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-success">Update</button>
                <a href="<?= base_url('view-history');?>" class="btn btn-danger" role="button">Back</a>

            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->
      </div>
  </div>

  <!-- /.content-wrapper -->
</section><!-- /.content -->
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>