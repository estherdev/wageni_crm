<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add History
      </h1>
      <ol class="breadcrumb">
         <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
         <li class="active">Add History</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
         <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
         <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form method="post" id="historyupdate" action="<?= base_url('admin/masters/History/');?>" enctype="multipart/form-data">
                  <div class="box-body">
                     <div class="form-group row">
                        <div>
                           <input type = "hidden"name="hc_id" value="<?php echo $records[0]['hc_id']; ?>" />
                        </div>
                        <!-- <div class="col-md-6">
                           <label for="title">Title</label>
                           <input type="text" class="form-control" name="title" value="<?= $records[0]['title'];?>" />
                           <?//= form_error('title')?>            
                        </div> -->
                        <div class="col-md-6">
                           <label for="Image">Image</label>
                           <input type="file" class="form-control"  name="userfile" value="<?= $records[0]['image'];?>"  />
                           <input type="hidden"  name="imghidden" value="<?= $records[0]['image'];?>"  />
                           <?= form_error('userfile')?>   
                        </div>
                        <div class="col-md-12">
                           <label for="title">Content</label>
                           <textarea type="text" class="form-control" name="content" /><?= $records[0]['content'];?></textarea> 
                           <?= form_error('content')?>            
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                     <button type="submit" class="btn btn-success">Update</button>
                    <!--  <a href="<?= base_url('view-history');?>" class="btn btn-danger" role="button">Back</a> -->
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!-- /.content-wrapper -->
      </div>
      <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header">
                  <h3 class="box-title">History Details</h3>
               </div>
               <form method="post" id="history" action="<?= base_url('admin/masters/History/insertHistory');?>" enctype="multipart/form-data">
                  <div class="box-body">
                     <div class="form-group row">
                        <div class="col-md-6">
                           <label for="Date">Date</label>
                           <input type="date" id="date" class="form-control"  name="date"  />
                           <?= form_error('date')?>   
                        </div>
                       <!--  <div class="col-md-6">
                           <label for="title">Title</label>
                           <input type="text"  id="title" class = "form-control"name="title" />
                           <?= form_error('title')?>            
                        </div> -->
                        <div class="col-md-12">
                           <label for="content">Content</label>
                           <textarea type="text"  id="content" class = "form-control"name="content" /></textarea>
                           <?= form_error('content')?>            
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                      <input type="submit" id="submit" value="Submit" name="sub"  class="btn btn-primary" />
                     <!-- <button type="submit" id="register" value="Register" disabled="disabled" class="btn btn-primary">Submit</button> -->
                     <button type="reset" class="btn btn-danger">Reset</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
       </div>
         <!-- /.content-wrapper -->
         <div class="row">
            <div class="col-md-12 col-sm-6 col-xs-12">
               <!-- general form elements -->
               <div class="box box-primary">
                  <div class="box-header">
                     <h3 class="box-title">History Details</h3>
                  </div>
                  <div class="box-body table-responsive">
                     <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                           <tr>
                              <th>S.No.</th>
                              <th>Date</th>
                             <!--  <th>Title</th> -->
                              <th>Content</th>
                               <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php $i= 1; foreach ($alldata as $key => $pack) {?>
                           <tr>
                              <td><?= $i++; ?></td>
                              <td><?= ($pack->history_date);?></td>
                              <!-- <td><?= ucfirst($pack->title); ?></td> -->
                              <td><?= ucfirst($pack->content); ?></td>
                              <td class="text-center" style="white-space: nowrap">
                                 <a class="btn btn-sm btn-info" href="<?= base_url('admin/masters/History/getHistoryId/'.$pack->h_id)?>" title="Edit"><i class="fa fa-pencil"></i></a> 
                                 <a class="btn btn-sm btn-danger deletepack"  data-toggle="modal" data-target="#deleteModal<?= $pack->h_id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                              </td>
                              <div id="deleteModal<?= $pack->h_id ; ?>" class="modal fade">
                                 <div class="modal-dialog">
                                    <div class="modal-content">
                                       <!-- dialog body -->
                                       <div class="modal-body">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          Are You Sure to <strong>Delete</strong> History?
                                       </div>
                                       <!-- dialog buttons -->
                                       <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('admin/masters/History/delete/'.$pack->h_id);?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                                    </div>
                                 </div>
                              </div>
                           </tr>
                           <?php } ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <!-- /.box -->
            </div>
         </div>
      </div>
   </section>
   <!-- /.content-wrapper -->
</div>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
<!-- /.content-wrapper -->
</div>
</section>
<!-- /.content-wrapper -->
<!-- /.content -->
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here -->
<script type="text/javascript">
   $(function () {
     $("#example1").dataTable();
     $('#example2').dataTable({
       "bPaginate": true,
       "bLengthChange": false,
       "bFilter": false,
       "bSort": true,
       "bInfo": true,
       "bAutoWidth": false
     });
   });
</script>
<script> 

$(function() {
    $('form > input').keyup(function() {alert("hello");

        var empty = false;
        $('form > input').each(function() {
            if ($(this).val() == '') {
                empty = true;
            }
        });

        if (empty) {
            $('#register').attr('disabled', 'disabled');
        } else {
            $('#register').removeAttr('disabled');
        }
    });
});()
</script>>