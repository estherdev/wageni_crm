<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Blog
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add Blog</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Blog Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
            <form method="post" id="addblog" action="<?= base_url('view-blog');?>" enctype="multipart/form-data">

              <div class="box-body">
              <div class="form-group row">

                <div class="col-md-6">
                  <label for="title">Title</label>
                  <input type="text"  class = "form-control"name="title" />
                  <?= form_error('title')?>            
                </div>

                <div class="col-md-6">
                  <label for="Image">Image</label>
                  <input type="file" class="form-control"  name="userfile"  />
                   <?= form_error('userfile')?>   
                </div>

                 <div class="col-md-6">
                  <label for="content">Content</label>
                  <input type="text"  class = "form-control"name="content" />
                  <?= form_error('content')?>            
                </div>

                <div class="col-md-6">
                  <label for="created date">Created Date</label>
                  <input type="text"  class = "form-control"name="created_date" />
                  <?= form_error('created_date')?>            
                </div>

                <div class="col-md-6">
                  <label for="created by">Created By</label>
                  <input type="text"  class = "form-control"name="created_by" />
                  <?= form_error('created_by')?>            
                </div>


              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.content-wrapper -->
    </div>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Blog</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Title</th>
                    <th>Userfile</th>
                    <th>Content</th>
                    <th>Created By</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>    
                  <?php $i= 1; foreach ($alldata as $key => $pack) {?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= ucfirst($pack->title); ?></td>
                    <td><img src="<?php echo base_url();?>uploads/<?= $pack->userfile; ?>" height="50px" ></td>
                    <td><?= ucfirst($pack->content); ?></td>
                    <td><?= ucfirst($pack->created_by); ?></td>
               
                     <td class="text-center" style="white-space: nowrap">
                        <a class="btn btn-sm btn-info" href="<?= base_url('edit-blog/'.$pack->id)?>" title="Edit"><i class="fa fa-pencil"></i></a> 
                        <a class="btn btn-sm btn-danger deletepack"  data-toggle="modal" data-target="#deleteModal<?= $pack->id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                      </td>
                      <div id="deleteModal<?= $pack->id ; ?>" class="modal fade">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        <!-- dialog body -->
                        <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                         Are You Sure to <strong>Blog</strong> Delete?
                        </div>
                        <!-- dialog buttons -->
                        <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('delete-blog/'.$pack->id);?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                        </div> 
                        </div>
                      </div>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    <!-- /.content-wrapper -->
  </div>

  <!-- /.content-wrapper -->
</section><!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>