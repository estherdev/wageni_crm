<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit Blog
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Edit Blog</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Blog Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->                       
            <form method="post" id="editblog" action="<?= base_url('update-blog/');?><?= $records[0]['id']?>" enctype="multipart/form-data">

              <div class="box-body">
              <div class="form-group row">

                <div class="col-md-6">
                  <label for="title">Title</label>
                  <input type="text" value="<?php echo $records[0]['title'];?>" class = "form-control" name="title" />
                  <?= form_error('title')?>            
                </div>

                <div class="col-md-6">
                  <label for="Image">Image</label>
                  <input type="hidden" name="hiddenimg" value="<?php echo $records[0]['userfile'];?>">
                  <input type="file" value="<?php echo $records[0]['userfile'];?>" class="form-control"  name="userfile"  />
                   <?= form_error('userfile')?>   
                </div></div>

                <div class="form-group row">
                 <div class="col-md-6">
                  <label for="content">Content</label>
                  <input type="text" value="<?php echo $records[0]['content'];?>"  class = "form-control" name="content" />
                  <?= form_error('content')?>            
                </div>

                <div class="col-md-6">
                  <label for="created date">Created Date</label>
                  <input type="text" value="<?php echo $records[0]['created_date'];?>" class = "form-control" name="created_date" />
                  <?= form_error('created_date')?>            
                </div></div>

               <div class="form-group row">
                <div class="col-md-6">
                  <label for="created by">Created By</label>
                  <input type="text" value="<?php echo $records[0]['created_by'];?>" class = "form-control" name="created_by" />
                  <?= form_error('created_by')?>            
                </div>


              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-success">Update</button>
                <a href="<?= base_url('view-blog');?>" class="btn btn-danger" role="button">Back</a>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
  </div>

  <!-- /.content-wrapper -->
</section><!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/admin/footer.php';?>
