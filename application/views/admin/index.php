<?php if(isset($_SESSION['admin_login_id']))   {   ?>
<?php include "header.php";?>
<?php include "main-sidebar.php";?>

<script src="<?= base_url();?>globalassets/admin/dist/js/pages/dashboard.js"></script>
<div class="content-wrapper">
   <section class="content-header">
      <h1> Dashboard <small></small> </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Dashboard</li>
      </ol>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-aqua"><i class="fa fa-hotel"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Total Company</span>
                  <span class="info-box-number"><?php echo totalCompany();?><small></small></span>
                  <a href="<?= base_url();?>/admin-view-all-company" class="widgets-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-green"><i class="fa fa-hotel"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Active Company</span>
                  <span class="info-box-number"><?php echo totalActiveCompany();?></span>
                  <a href="<?= base_url();?>/admin-view-all-company" class="widgets-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
               </div>
            </div>
         </div>
         <div class="clearfix visible-sm-block"></div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-red"><i class="fa fa-hotel"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">In-Active Company</span>
                  <span class="info-box-number"><?php echo totalInctiveCompany();?></span>
                  <a href="<?= base_url();?>/admin-view-all-company" class="widgets-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Company Users</span>
                  <span class="info-box-number"><?php echo totalCompanyUsers();?></span>
                  <a href="<?= base_url();?>/add-user" class="widgets-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-green"><i class="fa fa-list-alt"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Total Category</span>
                  <span class="info-box-number"><?php echo totalCategory();?></span>
                  <a href="<?= base_url();?>/view-category" class="widgets-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
               </div>
            </div>
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-green"><i class="fa fa-gift"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Total Package</span>
                  <span class="info-box-number"><?php echo totalPackage();?></span>
                  <a href="<?= base_url();?>/view-packages" class="widgets-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
               </div>
            </div>
         </div>
      </div>

      <!-- Canvas Bar Chart -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <!-- <i class="fa fa-bar-chart-o"></i> -->
          <h3 class="box-title">All Complaints</h3>

          <div class="box-tools pull-right">
           
          </div>
        </div>
        <div class="box-body">
          <div id="chartContainer" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
          <script src="http://activeacademy.net/assets/backend/js/canvasjs.min.js"></script>
        </div>
      </div>
 

      <div class="row">


          <div class="col-md-12">           
           <div class="box box-primary">
            <div class="box-header with-border">
            <!--   <i class="fa fa-bar-chart-o"></i> -->
                <h3 class="box-title">Trip Advisor Matrix</h3>
              <div class="box-tools pull-right">               
              </div>
            </div>
            <div class="box-body">
             <div class="tripAdvisor">
                <img src="<?= base_url();?>/globalassets/admin/images.png" alt="tripAdvisor">
                <p><strong>Current Rank #798</strong> <span>Hotel Noida</span></p>
              </div>
            </div>
          </div>
         </div>

      </div>
         <!--  <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-th"></i>

          

              <div class="box-tools pull-right">
               
              </div>
            </div>
            <div class="box-body">
              <div class="tripAdvisor">
                <img src="<?= base_url();?>/globalassets/admin/images.png">
                <p><strong>Current Rank #798</strong> <span>Hotel Noida</span></p>
              </div>
            </div>
           </div> -->
    </section>
 </div>
 <?php include "footer.php"; }else{redirect('loginMe');} ?>


<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
  animationEnabled: true,

  title: {
   text: "All Complaints"
 },
 axisY: {
   title: "Number of Complaints"
 },
 toolTip: {
   content: "{label} <br/>{name} : {y} Complaints"
 },
  data: [{
    type: "stackedColumn",
    showInLegend: true,
    color: "#dd4b39",
    name: "Very Poor",
    dataPoints: [<?php foreach($complaints as $row):
            ?>
            { label:"<?php if(!empty($row['cf_time'])){echo date("j M Y", strtotime($row['cf_time']));} ?>",y:<?php echo valueBydate($row['cf_time'],1);?>},
            <?php  endforeach;?>
            ]
    },
    {        
      type: "stackedColumn",
      showInLegend: true,
      name: "Poor",
      color: "#EDCA93",
      dataPoints: [<?php foreach($complaints as $row):
            ?>
            { label:"<?php if(!empty($row['cf_time'])){echo date("j M Y", strtotime($row['cf_time']));} ?>",y:<?php echo valueBydate($row['cf_time'],2);?>},
            <?php  endforeach;?>
            ]
    },
    {        
      type: "stackedColumn",
      showInLegend: true,
      name: "Average",
      color: "#00c0ef",
      dataPoints: [<?php foreach($complaints as $row):
            ?>
            { label:"<?php if(!empty($row['cf_time'])){echo date("j M Y", strtotime($row['cf_time']));} ?>",y:<?php echo valueBydate($row['cf_time'],3);?>},
            <?php  endforeach;?>
            ]
    },
    {        
      type: "stackedColumn",
      showInLegend: true,
      name: "Good",
      color: "#695A42",
      dataPoints: [<?php foreach($complaints as $row):
            ?>
            { label:"<?php if(!empty($row['cf_time'])){echo date("j M Y", strtotime($row['cf_time']));} ?>",y:<?php echo valueBydate($row['cf_time'],4);?>},
            <?php  endforeach;?>
            ]
    },
    {        
      type: "stackedColumn",
      showInLegend: true,
      name: "Very Good",
      color: "#00a65a",
      dataPoints: [<?php foreach($complaints as $row):
            ?>
            { label:"<?php if(!empty($row['cf_time'])){echo date("j M Y", strtotime($row['cf_time']));} ?>",y:<?php echo valueBydate($row['cf_time'],5);?>},
            <?php  endforeach;?>
            ]
  }]
});
chart.render();

function toolTipContent(e) {
  var str = "";
  var total = 0;
  var str2, str3;
  for (var i = 0; i < e.entries.length; i++){
    var  str1 = "<span style= 'color:"+e.entries[i].dataSeries.color + "'> "+e.entries[i].dataSeries.name+"</span>: <strong>"+e.entries[i].dataPoint.y+"</strong><br/>";
    total = e.entries[i].dataPoint.y + total;
    str = str.concat(str1);
  }
  str2 = "<span style = 'color:DodgerBlue;'><strong>"+(e.entries[0].dataPoint.x).getFullYear()+"</strong></span><br/>";
  total = Math.round(total * 100) / 100;
  str3 = "<span style = 'color:Tomato'>Total:</span><strong>"+total+"</strong><br/>";
  return (str2.concat(str)).concat(str3);
}

}
</script>