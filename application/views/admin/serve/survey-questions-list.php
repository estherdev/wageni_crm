<?php include APPPATH . 'views/admin/header.php';?>

<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<div class="content-wrapper">
  <section class="content-header">
    <h1> All Questions Filter & Search</h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">All Questions Filter & Search </li>
    </ol>
  </section>
  <section class="content">
       <div class="row">
         <div class="col-md-12 col-sm-6 col-xs-12">
      
             <div class="box box-primary">
               <div class="box-header">   
                  <?php //prd($this->session->all_userdata()); ?>
                 <div class="col-md-3">            
                  <select class="form-control" name="change_category" id="change_category">
                    <option value=""> Select Category</option>    
                        <?php $scores= getCategory();  
                        //pr($scores);                
                          foreach ($scores as $score) {?> 
                        <option value="<?= $score['cat_id'];?>"><?= ucfirst($score['category_name'])?></option>
                     <?php  } ?>
                   </select>
                </div> 
                <div class="col-md-3">            
                  <select class="form-control" name="change_stay" id="change_stay">
                    <option value=""> Select Stay</option>                                           
                         <option value="<?= 'Pre Stay';?>"><?= ucfirst('Pre Stay')?></option>
                         <option value="<?= 'On Stay';?>"><?= ucfirst('On Stay')?></option>
                         <option value="<?= 'Post Stay';?>"><?= ucfirst('Post Stay')?></option>                    
                   </select>
                </div>
                <div class="col-md-3">            
                  <select class="form-control" name="change_type" id="change_type">
                    <option value=""> Select Stay</option>                                           
                         <option value="<?= 'qmc';?>"><?= ucfirst('Multiple Choice')?></option>
                         <option value="<?= 'qdd';?>"><?= ucfirst('Dropdown')?></option>
                         <option value="<?= 'qchb';?>"><?= ucfirst('Checkboxes')?></option>                    
                         <option value="<?= 'qsr';?>"><?= ucfirst('Star Rating')?></option>                    
                   </select>
                </div>
                <div class="col-md-2">
                <div class="searchBlock">
                  <input id="search" name="search" class="form-control" type="text" placeholder="Search">
                 </div>
                </div>
               </div>                
            </div>
          </div>
       </div>
  
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title"><b> All Questions</h3></b>
          </div>
          <div class="box-body table-responsive">

            <table id="questions_list" class="table table-bordered table-striped table-hover table-sm text-center text-nowrap">
              <thead>
                <tr>
                  <th>S No.</th>                  
                  <th>#</th>                  
                  <th>Category</th>                  
                  <th>Sub Category</th>                  
                  <th>Process</th>                  
                  <th>Question</th>                  
                  <th>Question Tpye</th>                  
                  <th>Action</th>                  
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div> 
        </div> 
      </div> 
    </div> 

 </section> 
</div>
 <?php include APPPATH . 'views/admin/footer.php';?>
 <style type="text/css">
td.details-control { background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center 12px; cursor: pointer; }
tr.details td.details-control { background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center 12px; }
.datatable_loader { text-align: center; }
.datatable_loader img { width: 80px; }
.small_loader { position: relative; height: 20px; width: 100%; overflow: hidden; }
.small_loader img { width: 50px; position: absolute; top: -10px; left: 0; }
</style>

 
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>


<script>
 //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })

  var defaltimageurl='<?= base_url()?>globalassets/admin/dist/img/user2-160x160.jpg';
  var imageurl='<?= base_url()?>uploads/';
  var base_url='<?= base_url()?>';

   var datatable_loader='<div class="datatable_loader"><img src="'+base_url+'/globalassets/admin/preloader1.gif"></div>';
   var small_loader='<div class="small_loader"><img src="'+base_url+'/globalassets/admin/preloader1.gif"></div>';

   $(function(){
   
   var questions_list_table = $('#questions_list').DataTable({
      "processing": true,
      "pageLength": <?php echo $this->config->item('record_per_page');?>,
      "serverSide": true,
      "sortable": true,
      "lengthMenu": [[10,20, 30, 50,100], [10,20, 30, 50,100]],
      "language": {
      "emptyTable": "<?php echo $this->config->item('record_not_found_title');?>" 
      },
       "order": [
         [1, "desc"]
      ],
      "ajax":{
         url :"<?php echo base_url('admin-view-all-questions-list-ajax')?>", 
         type: "post",   
         data: function (d) {      
                d.category_id = $('#change_category').val();
                d.stay_id = $('#change_stay').val();
                d.type_id = $('#change_type').val();
                d.search = $('#search').val();
             }      
      } ,
      "columns": [ 
      {data: 'id',"orderable":false},
      {"class":'details-control',"orderable":false,"sortable":false,"data": null, "defaultContent": '' },   
      {data: 'category_name',class: 'text-nowrap',"orderable":false},
      {data: 'sub_cat_name',class: 'text-nowrap',"orderable":false},
      {data: 'sub_sub_cat_name',class: 'text-nowrap',"orderable":false},
      {data: 'que',class: 'text-nowrap',"orderable":false},
      {data: 'q_type',class: 'text-nowrap',"orderable":false},
      {data: 'id',"orderable":false},
        
        
    ],
    
    "columnDefs": [        
              {
                  render: function (data, type, row, meta) {
                  return meta.row + meta.settings._iDisplayStart + 1 +'.';
              },          
               "targets": 0 
              }, 


              {
      "render": function ( data, type, row ) {            
        var html='';
        html +='<a href="javascript:void(0);" onClick="deleteRecord('+data+');" class="btn btn-danger btn-sm">';
        html +='<i class="fa fa-trash" aria-hidden="true"></i></a></div>';
        return html;

      },
        "targets": 7
    }
                        
          ],
 
      });

   /*=========================    FILETR    ==================================*/



  $('#search').on('keyup', function() {
    questions_list_table.search(this.value).draw();
  });

  $('#change_category,#change_stay,#change_type').change(function(){
      questions_list_table.ajax.reload();
  });


  $('#delete_record').click(function(){
      actionPerform('<?php echo base_url('admin-delete-question')?>',questions_list_table);
    });



  /*===========================================================*/
  
  $('#questions_list tbody').on('click', 'tr td.details-control', function(){
 
    var tr  =  $(this).closest('tr');
    var row =  questions_list_table.row(tr);
        if(row.child.isShown()){
          tr.removeClass('details');
          row.child.hide();
        }else{
          tr.addClass('details');
          var id = row.data().id; 
          $(tr).attr('row_id', id);
          questionsDetails(row);
          close_existing_module(id,questions_list_table);
      }
    });
    
});

   /*===========================================================*/
 function close_existing_module(main_id, questions_list_table){
   $('#questions_list >tbody >tr').each(function() {
    var tr = $(this).closest('tr');
    var row = questions_list_table.row( tr );
    var id=$(tr).attr("row_id");
    if (typeof(id) == "undefined") {
        id=0;
    }
    if ($(this).hasClass('details') && main_id!=id) {
      row.child.hide();
      tr.removeClass('details');
    }
  }); 
} 


/*===========================================================*/
 function questionsDetails(row){
  var id = row.data().id; 
  row.child(datatable_loader).show();
  var html='<form id="add_option"><table class="table table-dark table-striped table-hover" id="innertable">';
  $.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-all-questions-details-ajax')?>',
      data: {'id':id},
      success: function(ajaxresponse){
      response=JSON.parse(ajaxresponse);
          html += response['data'];
          html += '</table></form>';
         row.child(html).show();
       }
    }); 
 } 


function questionsDetailsrow(id){   
  $.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-all-questions-details-ajax')?>',
      data: {'id':id},
      success: function(ajaxresponse){
      response=JSON.parse(ajaxresponse);
          html = response['data'];
  
         $('#innertable').html(html);
       }
    }); 
 } 



   
/*===========================================================*/
 

function sendNotification(id,status){
  if(id>0){
    $('#notification'+id).hide();
    $('#loader'+id).html(small_loader);
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('sub-admin-send-notification-to-guest')?>',
      data: {'id':id,'status':status},
      success: function(ajaxresponse){
        response=JSON.parse(ajaxresponse);
        if(response['status']){
          $('#success_message').html('<div class="alert alert-success">'+response['message']+'!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        }else{
          $('#success_message').html('<div class="alert alert-danger">'+response['message']+'!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
         }
        $('#notification'+id).show();
        $('#loader'+id).html('');
        setTimeout(function () {
             location.reload()
        }, 100);
        // $('#change_order_status').trigger('change');
      }
    }); 
  }
} 
/*===========================================================*/

  function getComplaintsStatus(status){
    if(status==1){
      return "<span class='btn btn-sm btn-danger'>Pending</span>";
   }else if(status==2){
     return "<span class='btn btn-sm btn-warning'>Ongoing</span>";
   }else if(status==3){
    return "<span class='btn btn-sm btn-success'>Issue has been addressed</span>";
   }else{
  return "<span class='btn btn-sm btn-danger'>Pending</span>";
   }
 }
   
/*===========================================================*/   
  function changeCompanyUsersStatus(id,status){
   if(id>0){
    if(status==1){
      var set_status=0;
    }else{
      var set_status=1;
    }
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-change-company-users-status')?>',
      data: {'id':id,'status':set_status},
      success: function(ajaxresponse){
        response=JSON.parse(ajaxresponse);
        if(response['status']){ 
          if(set_status){
            var html='<a href="javascript:void(0)" onClick="changeCompanyUsersStatus('+id+','+set_status+')"><span class="btn btn-sm btn-success">Active</span></a>';
          }else{
            var html='<a href="javascript:void(0)" onClick="changeCompanyUsersStatus('+id+','+set_status+')"><span class="btn btn-sm btn-danger">In-active</span></a>';
          }
          $('#status'+id).html(html);
          company_list_table.ajax.reload();
        }
      }
    }); 
  }
}
 
 
</script>
   <script type="text/javascript">
  function makeTrim(x) {
    if (x) {
      return x.replace(/^\s+|\s+$/gm, '');
    } else {
      return x;
    }
  }


    function validEmail(email) {
    var re = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return re.test(email);
  }
   
 
   
  </script>


<script>


  $('#addguest').click(function(){
   
    $('#message_box').html('');
    $('#success_message').html('');
    $('.form-control').removeClass('error');
    $('.error').html('');
    var errorMessage=[];
    var branch=makeTrim($('#subcat').val());
    var name=makeTrim($('#user_name').val());
    var email=makeTrim($('#user_id').val());
    var address=makeTrim($('#address').val());
    var contact_no=makeTrim($('#user_phone').val());
    var room=makeTrim($('#no_of_room').val());
    var children=makeTrim($('#no_of_children').val());
    var adult=makeTrim($('#no_of_adult').val());
    var user_room=makeTrim($('#user_room').val());
    var nationality=makeTrim($('#user_nationality').val());
    var datein=makeTrim($('#datetimepicker6').val());
    var dateout=makeTrim($('#datetimepicker7').val());
    var timein=makeTrim($('#check_in_time').val());
    var timeout=makeTrim($('#check_out_time').val());


    if(name==''){
      errorMessage['user_name']="User Name is Required!";
    }else{
      if(name.length<3){
        errorMessage['user_name']="User Name should be greater than 2 characters in length!";
      }
    }
    if(email==''){
          errorMessage['user_id']="Email is Required!";
    }else{
        if(!validEmail(email)){
            errorMessage['user_id']="Invilid Email Address!";
        }
    }
    if(contact_no==''){
          errorMessage['user_phone']="Contact No. is Required!";
    }else if(isNaN(contact_no) || (contact_no.length>12) || (contact_no.length<6)){
        errorMessage['user_phone']="Invilid Contact No!";
    } 
    if(children==''){
          errorMessage['no_of_children']="children is Required!";
    }else if(isNaN(children) || (children.length>5) || (children.length<0)){
        errorMessage['no_of_children']="Invilid children No!";
    }
    if(adult==''){
          errorMessage['no_of_adult']="Adult is Required!";
    }else if(isNaN(adult) || (adult.length>5) || (adult.length<0)){
        errorMessage['no_of_adult']="Invilid Adult No!";
    }
    if(room==''){
          errorMessage['no_of_room']="Room No. is Required!";
    }else if(isNaN(room) || (room.length>5) || (room.length<0)){
        errorMessage['no_of_room']="Invilid Room No!";
    }
    if(user_room==''){
          errorMessage['user_room']="Room No. is Required!";
    }else if(isNaN(user_room) || (user_room.length>5) || (user_room.length<0)){
        errorMessage['user_room']="Invilid Room No!";
    }
    if(branch==''){
      errorMessage['subcat']="Branch is Required!";
    } 
    if(address==''){
      errorMessage['address']="Address is Required!";
    }
   if(nationality ==''){
      errorMessage['user_nationality']="nationality is Required!";
    }
   if(datein ==''){
      errorMessage['datetimepicker6']="Check in Date is Required!";
    }
   if(dateout ==''){
      errorMessage['datetimepicker7']="Check out Date is Required!";
    }
   if(timein ==''){
      errorMessage['check_in_time']="Check in time is Required!";
    }
   if(timeout ==''){
      errorMessage['check_out_time']="Check out time is Required!";
    }
 

    var errorLength=Object.keys(errorMessage).length;
    if(errorLength>0){
      for (var key in errorMessage) {
        if (errorMessage.hasOwnProperty(key)) {           
          $('#'+key).addClass('error');
          $('#'+key+'_error').html(errorMessage[key]);
        }
      }
      
     $('#success_message').html('<div class="alert alert-danger">There is error in submitting form!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
      

    }else{
      var form = $("#add_guestuser")[0];
      var formData = new FormData(form);
      $.ajax({
        type: "POST",
        url: '<?php echo base_url('save-guest-user')?>',
        data: formData,
        async : false,
        cache : false,
        contentType : false,
        processData : false,
        success: function(ajaxresponse){
          response=JSON.parse(ajaxresponse);
          if(!response['status']){
            $.each(response, function(key, value) {
              $('#'+key).addClass('error');
              $('#'+key+'_error').html(value);
            });
            $('#success_message').html('<div class="alert alert-danger">There is error in submitting form!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
             
          }else{
            $("#add_guestuser")[0].reset();                
            
            $('#success_message').html('<div class="alert alert-success">'+response['message']+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            setTimeout(function(){ location.reload(); }, 1000);           

          }  
        }
      });   
    }

  });

</script>

