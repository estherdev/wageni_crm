<!--header Section Start Here -->
<?php require APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php require APPPATH . 'views/admin/main-sidebar.php';?>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Question
         <small>All Question</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?= base_url('allQueByCat');?>"><i class="fa fa-arrow-left"></i> Back</a></li>
         <li class="active">All Question</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12 ">
            <!-- general form elements -->
            <div class="box ">
               <div class="box-header">
                  <h3 class="box-title">All Question</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <div class="box-body">
                  <?php
                     if(!empty($allquestions) || $allquestions != NULL || $allquestions != ""){
                     
                     for($que=0; $que< count($allquestions);  $que++)
                     {
                         $allquestions[$que][0]; 
                         $allquestions[$que][1]; 
                         $allquestions[$que][2];            
                         $allquestions[$que][3];        
                     
                         switch ($allquestions[$que][2]) {
                             case 'qmc':                       
                             ?>
                  <div class="page-answer highlight">
                     <div class="qustion-title">
                        <label><span>Que.<?= $que+1;?> </span><?= $allquestions[$que][1] ?></label>
                        <div class="pull-right">
                           <div class="tab-option">
                              <ul>
                                 <li class="edit"> <a href="<?= base_url('admin/Dashboard/getQuestionnaireById/'.$allquestions[$que][0])?>" title="Edit"><i class="fa fa-pencil"></i></a></li>
                                 <!--  <li><a href=""> Edit</a></li> -->
                                 <li class="delete" data-id="<?= $allquestions[$que][0] ?>"><a href="<?= base_url('admin/Dashboard/deleteQuestionnaire/')?>"><?= $this->lang->line('DEL');?></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <ul class="option">
                        <?php for($qmc = 0; $qmc<count($allquestions[$que][3]); $qmc++) {   ?>
                        <li> <input type="radio" data-radio-score="<?= $allquestions[$que][3][$qmc]->que_score ?>" id="radio-<?=$allquestions[$que][0]?>-<?= $qmc; ?>" name="qmc<?= $allquestions[$que][0]; ?>" value="<?= $allquestions[$que][3][$qmc]->que_score ?>">
                           <label for="radio-<?= $allquestions[$que][0]?>-<?= $qmc; ?>"><?= $allquestions[$que][3][$qmc]->que_type ?></label>
                        </li>
                        <?php } ?>                        
                     </ul>
                  </div>
                  <?php 
                     break;
                     
                     case 'qdd':                       
                     ?>
                  <div class="page-answer highlight">
                     <div class="qustion-title">
                        <label><span>Que.<?= $que+1;?> </span><?= $allquestions[$que][1]?></label>
                        <div class="pull-right">
                           <div class="tab-option">
                              <ul>
                                 <li class="edit"> <a href="<?= base_url('admin/Dashboard/getQuestionnaireById/'.$allquestions[$que][0])?>" title="Edit"><i class="fa fa-pencil"></i></a></li>
                                 <!--  <li><a href=""> Edit</a></li> -->
                                 <li class="delete" data-id="<?= $allquestions[$que][0] ?>"><a href="<?= base_url('admin/Dashboard/deleteQuestionnaire/')?>"><?= $this->lang->line('DEL');?></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <ul class="option">
                        <li>
                           <div class="row">
                              <div class="col-md-6" style="padding:0px;">
                                 <select class="form-control scat" name=""  data-select-score="<?= $allquestions[$que][3][$qmc]->que_score ?>" >
                                    <?php for($qdd = 0; $qdd<count($allquestions[$que][3]); $qdd++) {?>
                                    <option  value="<?= $allquestions[$que][3][$qdd]->que_score ?>" ><?= $allquestions[$que][3][$qdd]->que_type ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
                  <?php 
                     break;                       
                     
                     case 'qchb':                       
                     ?>
                  <div class="page-answer highlight">
                     <div class="qustion-title">
                        <label><span>Que.<?= $que+1;?> </span><?= $allquestions[$que][1] ?></label>
                        <div class="pull-right">
                           <div class="tab-option">
                               <ul>
                                 <li class="edit"> <a href="<?= base_url('admin/Dashboard/getQuestionnaireById/'.$allquestions[$que][0])?>" title="Edit"><i class="fa fa-pencil"></i></a></li>
                                 <!--  <li><a href=""> Edit</a></li> -->
                                 <li class="delete" data-id="<?= $allquestions[$que][0] ?>"><a href="<?= base_url('admin/Dashboard/deleteQuestionnaire/')?>"><?= $this->lang->line('DEL');?></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <ul class="option">
                        <?php for($qchb = 0; $qchb<count($allquestions[$que][3]); $qchb++) {?>
                        <li>
                           <input type="checkbox" id="check-<?=$allquestions[$que][0]?>-<?= $qchb ?>" name="qchb<?=$allquestions[$que][0]?>">
                           <label for="check-<?=$allquestions[$que][0]?>-<?= $qchb; ?>"><?= $allquestions[$que][3][$qchb]->que_type ?></label>  
                        </li>
                        <?php } ?>                         
                     </ul>
                  </div>
                  <?php 
                     break;   
                     
                        case 'qmt':                       
                         ?>  
                  <div class="page-answer highlight ">
                     <div class="qustion-title">
                        <label><span>Que.<?= $que+1;?> </span><?= $allquestions[$que][1] ?></label> 
                        <div class="pull-right">
                           <div class="tab-option">
                              <ul>
                                 <li class="edit"> <a href="<?= base_url('admin/Dashboard/getQuestionnaireById/'.$allquestions[$que][0])?>" title="Edit"><i class="fa fa-pencil"></i></a></li>
                                 <!--  <li><a href=""> Edit</a></li> -->
                                 <li class="delete" data-id="<?= $allquestions[$que][0] ?>"><a href="<?= base_url('admin/Dashboard/deleteQuestionnaire/')?>"><?= $this->lang->line('DEL');?></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <ul class="option">
                        <?php for($qmt = 0; $qmt<count($allquestions[$que][3]); $qmt++) {?>
                        <li>
                           <div class="row">
                              <label style="display:inline-block; float:left; font-weight:600;"></label>
                              <div class="col-md-6"><input type="text" value="<?= $allquestions[$que][3][$qmt]->que_type ?>"<?= $allquestions[$que][3][$qmt]->que_type ?> class="form-control"></div>
                           </div>
                        </li>
                        <?php } ?>      
                     </ul>
                  </div>
                  <?php 
                     break;  
                     
                     
                     
                     case 'qst':                       
                     ?>  
                  <div class="page-answer highlight">
                     <div class="qustion-title">
                        <label><span>Que.<?= $que+1;?> </span><?= $allquestions[$que][1] ?></label> 
                        <div class="pull-right">
                           <div class="tab-option">
                               <ul>
                                 <li class="edit"> <a href="<?= base_url('admin/Dashboard/getQuestionnaireById/'.$allquestions[$que][0])?>" title="Edit"><i class="fa fa-pencil"></i></a></li>
                                 <!--  <li><a href=""> Edit</a></li> -->
                                 <li class="delete" data-id="<?= $allquestions[$que][0] ?>"><a href="<?= base_url('admin/Dashboard/deleteQuestionnaire/')?>"><?= $this->lang->line('DEL');?></a></li>
                              </ul>
                           </div>
                        </div>
                        <div class="row form-group">
                           <!-- <div class="col-md-6">
                              <input type="text" class="form-control">
                           </div> -->
                        </div>
                     </div>
                  </div>
                  <?php 
                     break;  
                     
                     
                     case 'qsr':                       
                     ?>  
                  <div class="page-answer highlight">
                     <div class="qustion-title">
                        <label><span>Que.<?= $que+1;?> </span><?= $allquestions[$que][1] ?></label> 
                        <div class="pull-right">
                           <div class="tab-option">
                              <ul>
                                 <li class="edit"> <a href="<?= base_url('admin/Dashboard/getQuestionnaireById/'.$allquestions[$que][0])?>" title="Edit"><i class="fa fa-pencil"></i></a></li>
                                 <!-- <li><a class="btn btn-sm btn-info" href="" title="Edit"><i class="fa fa-pencil"></i></a></li>  -->
                                 <!--  <li><a href=""> Edit</a></li> -->
                                 <li class="delete" data-id="<?= $allquestions[$que][0] ?>"><a href=""><?= $this->lang->line('DEL');?></a></li>
                              </ul>
                           </div>
                        </div>
                        <div class="row form-group">
                           <div class="reting-row rating-stars" id="rating-star">
                              <input type="number" readonly class="rating-value hidden" name="" id="">
                              <ul>
                                 <li class="rating-star"><i class="fa fa-star-o"></i></li>
                                 <li class="rating-star"><i class="fa fa-star-o"></i></li>
                                 <li class="rating-star"><i class="fa fa-star-o"></i></li>
                                 <li class="rating-star"><i class="fa fa-star-o"></i></li>
                                 <li class="rating-star"><i class="fa fa-star-o"></i></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php 
                     break;   
                     
                     default:
                         
                     break;
                     }
                     }
                     }
                     else{
                     echo "<div class='no-result'><img src='/globalassets/admin/dist/img/no-record.jpg' alt='No Record Found'></div>";
                     }
                     ?>  
               </div>
            </div>
            <!-- /.box -->   
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php require APPPATH . 'views/admin/footer.php';?>  
<script>
   $(document).on('click', '.delete', function(){  
          var qid = $(this).attr("data-id");  
          if(confirm("Are you sure you want to delete ?"))  
          {  
               $.ajax({ 
                   type:"POST", 
                    url:"<?php echo base_url(); ?>admin/Dashboard/deleteQuestionnaire",  
                    data:{'qid':qid},  
                    success:function(data)  
                    {  
                       if(data==1){
                           alert('delete'); 
                           window.reload(); 
                       }else{
   
                       }
                         //alert(data);  
                         
                    }  
               });  
          }  
          else  
          {  
               return false;       
          }  
     });  
</script>