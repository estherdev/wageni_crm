<html>
   <head>
      <title>Codeigniter Dynamic Dependent Select Box using Ajax</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <style>
         .box
         {
         width:100%;
         max-width: 650px;
         margin:0 auto;
         }
      </style>
   </head>
   <body>
      <div class="container box">
         <br />
         <br />
         <h3 align="center">Codeigniter Dynamic Dependent Select Box using Ajax</h3>
         <br />
         <div class="form-group">
            <select name="category" id="category" class="form-control input-lg">
               <option value="">Select Category</option>
               <?php
                  foreach($category as $cat)
                  {
                   echo '<option value="'.$cat->cat_id.'">'.$cat->category_name.'</option>';
                  }
                  ?>
            </select>
         </div>
         <br />
         <div class="form-group">
            <select name="subcat" id="subcat" class="form-control input-lg">
               <option value="">Select Sub Category</option>
            </select>
         </div>
         <br />
         <div class="form-group">
            <select name="subsubcat" id="subsubcat" class="form-control input-lg">
               <option value="">Select Sub Sub Category</option>
            </select>
         </div>
      </div>
   </body>
</html>
<script>
   $(document).ready(function(){
    $('#category').change(function(){
     var cat_id = $('#category').val();
    
     if(cat_id != '')
     {
      $.ajax({
       url:"<?php echo base_url(); ?>admin/masters/UserCat/fetch_subcategory",
       method:"POST",
       data:{cat_id:cat_id},
       success:function(data)
       {
        $('#subcat').html(data);
        $('#subsubcat').html('<option value="">Select Sub Sub Cat</option>');
       }
      });
     }
     else
     {
      $('#subcat').html('<option value="">Select Sub category</option>');
      $('#subsubcat').html('<option value="">Select sub sub category</option>');
     }
    });
   
    $('#subcat').change(function(){
     var sub_cat_id = $('#subcat').val();
     if(sub_cat_id != '')
     {
      $.ajax({
       url:"<?php echo base_url(); ?>admin/masters/UserCat/fetch_subsubcat",
       method:"POST",
       data:{sub_cat_id:sub_cat_id},
       success:function(data)
       {
        $('#subsubcat').html(data);
       }
      });
     }
     else
     {
      $('#subsubcat').html('<option value="">Select Sub Sub Category</option>');
     }
    });
    
   });
</script>