<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Guest User Complaints
    </h1>
    <ol class="breadcrumb">
      <li class="active">Guest User Complaints</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
  
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><b>Guest User Complaints Details</h3></b>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">

              <table id="example1" class="table table-bordered table-striped table-sm text-center text-nowrap">
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Name</th>
                    <th>Hotel Name</th>
                    <!--<th>Cat</th>-->
                    <th>Que.</th>
                    <!--<th>Complaints</th>-->
                    <th>Feedback</th>
                    <!--<th>Pre Stay</th>
                    <th>On Stay</th>
                    <th>Post Stay</th>-->
                    <th>Genrate Time</th>
                    <th>Current Status</th>
                    <th>Assign </th>
                    <th>Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i= 1; foreach ($Complaints as $key => $value) {
                    if ($value->bus_que_score=='1' || $value->bus_que_score=='2'){ ?>
                  <tr>
                    <td><?= $i++; ?></td>                           
                    <td><?= ucfirst($value->user_name)?></td>                             
                    <td><?= $value->category_name?></td>                             
                    <!--<td><?= $value->cf_cat_id?></td>-->
                    <td><?= $value->bus_q_que?></td>
                    <!--<td><?= $value->bus_que_type?></td>-->
                    <td>
                      <?php if ($value->bus_que_score=='1')
                        {
                          echo '<i class="fa fa-times-circle-o fa-1x text-danger"> Very Poor</i>';
                        }
                          elseif ($value->bus_que_score=='2')
                          {
                            echo '<i class="fa fa-times-circle-o fa-1x text-warning"> Poor</i>';
                          }
                            elseif ($value->bus_que_score=='3')
                            {
                              echo '<i class="fa fa-check-circle-o fa-1x text-primary"> Average</i>';
                            }
                              elseif ($value->bus_que_score=='4')
                              {
                                echo '<i class="fa fa-check-circle-o fa-1x text-success"> Good</i>';
                              }
                              elseif ($value->bus_que_score=='5')
                              {
                                echo '<i class="fa fa-check-circle-o fa-1x text-success"> Very Good</i>';                            
                              } 
                        else
                        {
                           echo '<i class="fa fa-check-circle-o fa-1x text-success"> No Complaints</i>';
                        }
                    ?></td>                             
                      <!-- <td><i class="fa fa-check-circle-o fa-2x text-success" ></i></td> 
                    <td><i class="fa fa-times-circle-o fa-2x text-danger" ></i></td>   
                    <td><i class="fa fa-times-circle-o fa-2x text-danger" ></i></td>  
                                        -->
                    <?php                                        
                    $post_date = strtotime($value->cf_time);
                    $now = time();
                    $units = 5;
                    timespan($post_date, $now, $units);
                    ?>

                    <td><i class="fa fa-clock-o fa-1x" > <?= timespan($post_date, $now, $units);?> Ago</i></td>    
                    <td>
                      <?php if ($value->cf_status=='1'){
                          echo '<i class="fa-1x text-danger"> Pending</i>';
                        }
                          elseif ($value->cf_status=='2'){
                            echo '<i class="fa-1x text-warning"> Ongoing</i>';
                          }
                          elseif ($value->cf_status=='3'){
                            echo '<i class="fa-1x text-success"> Issue has been addressed</i>';
                          }
                        else{
                           echo '<i class="fa-1x text-danger">Pending</i>';
                        }?>
                          
                        </td>  
                    <td>
                    <?php
                     //echo $post_date = date($value->cf_time);
                    $past_time =  date('Y-m-d H:i',strtotime($value->cf_time));
                    //$past_time = strtotime('2018-11-01 12:02');
                    $current_time = time();
                    $difference = $current_time - strtotime($past_time);
                    $difference_minute =  $difference/60;
                    $d= intval($difference_minute);
                    //echo $d;
                    if ( intval($d) < '1440') {
                      echo "Superviser";
                    } 
                     elseif ( intval($d) < '2880') {
                      echo "Manger";
                     }
                     else {
                      echo "G Manger";
                    }
                    
                    //echo 'Difference in minute between two different Time : '.intval($difference_minute);

                    ?></td>                       
                    
                      <!-- <select class="apply" onchange="javascript:window.location.href='<?//= base_url('complaints-rafer'); ?>/'+this.value;"> -->
                     
                    <td class="text-center" style = "white-space: nowrap">
                    <a class="btn btn-sm btn-success"  data-toggle="modal" data-target="#UpdateModal<?= $value->cf_id?>" title="Update"><i class="fa fa-pencil"></i></a>
                    </td>

                     <div id="UpdateModal<?= $value->cf_id?>" class="modal fade">
                        <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Are You Sure to <strong>Guest Complaints</strong> Change ?</h4>
                          </div>
                          <div class="modal-body">                          
                            <form method="post" action="<?php echo base_url('complaintsRafer/'.$value->cf_id); ?>">
                              <div class="input-group">
                                <input type="hidden" class="form-control" name="applyid" value="<?= $value->cf_id?>">
                                <select class="apply form-control" name="applychange" id="apply">
                                  <option value="1">Pending</option>
                                  <option value="2">Ongoing</option>
                                  <option value="3">Issue has been addressed</option>
                                </select>
                                <span class="input-group-addon">
                                  <button type="submit" class="btn btn-success" id="submit" >Apply</button> 
                                </span>
                              </div>
                            </form>
                          </div>
                        <div class="modal-footer"><!-- <button type="button" class="btn btn-primary"  onclick="window.location='<?php //echo base_url('complaintsRafer/'.$value->cf_id); ?>';">Confirm</button> --><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                        </div>
                        </div>
                        </div>
                  </tr>    
                  <?php } ?>

                  <?php } ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->

  <!-- /.content-wrapper -->
</section><!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>


  <script>

/*$("#submit").click(function (e) {

       e.preventDefault();
       alert($("#apply").val());
          $.ajax({
            type: 'post',
            url: '<?//= base_url('complaintsRafer');?>',
            data: $('form').serialize(),
            success: function () {
              alert('Status Change Successfully');
            }
          });

    });*/
    /*  $(function () {

        $('form').on('submit', function (e) {

          e.preventDefault();
          alert($('form').serialize());
          $.ajax({
            type: 'post',
            url: '<?//= base_url('complaintsRafer');?>',
            data: $('form').serialize(),
            success: function () {
              alert('Status Change Successfully');
            }
          });

        });

      });*/
    </script>




