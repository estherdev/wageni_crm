<!--header Section Start Here -->
<?php require APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php require APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Survey
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-arrow-left"></i> Back</a></li>
      <li class="active">Add Survey</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12 ">
        <!-- general form elements -->
        <div class="box ">
          <div class="box-header">
            <h3 class="box-title">Survey</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <div id="viewAllQue" class="viewAllQue" >
            </div>
          	<div id="messageResponse" class="messageResponse bg-success" >
       		</div>
       		<form id="nextQues" class="nextQues" method="post">
           <div class="qustio-box">
            <div class="row form-group">
              <div class="rate-select">
                <div class="col-md-4">
                  <label>Category</label>
                  <select class="form-control cat" name="cat" id="cat"  >
                    <option value="none">Select Category</option>
                     <?php
                  foreach($category as $cat)
                  {
                   echo '<option value="'.$cat->cat_id.'">'.$cat->category_name.'</option>';
                  }
                  ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label>Sub Category</label>
                  <select class="form-control scat" name="subcat" id="subcat" >
                  <option value="">Select Sub Category</option>
                  </select>
                </div>

                <div class="col-md-4">
                  <label>Process</label>
                  <select class="form-control scat" name="subsubcat" id="subsubcat" >
                  <option value="">Select Process</option>
                  </select>
                </div>
              </div>
            </div> 
            <hr/>
            <div class="input-dropbox"> 

            <div class="qan">Que</div>
            <div class="input-box">
              	<input type="text" class="form-control surveyQue" name="surveyQue" id="surveyQue" placeholder="Enter your question">
            </div>
            <input type="hidden" name="q_type" id="q_type" class="q_type">
            <div class="dropbox">
            <a><span class="selectedli" id="selectedli">Multiple Choice</span><i class="fa fa-angle-down"></i></a>
            <div class="dropdown-box">
                <ul id="selectdropdownquestion">
	                <li class="selectli">
	                  <a data-action="MultipleChoiceQuestion" class="select-li" id="qmc" data-help="qmc" href="javascript:void(0)"><i class="fa fa-list select-menu-list"></i>Multiple Choice</a>
	                </li>
	                <li class="selectli">
	                  <a data-action="DropdownQuestion" class="select-li" id="qdd" data-help="qdd" href="javascript:void(0)"><i class="fa fa-sort select-menu-list"></i>Dropdown</a>
	                </li>
	                <li class="selectli">
	                  <a data-action="CheckboxQuestion" class="select-li" id="qchb" data-help="qchb" href="javascript:void(0)"><i class="fa fa-check-square-o select-menu-list"></i>Checkboxes</a>
	                </li>
	                <li class="selectli">
	                  <a data-action="StarRatingQuestion" class="select-li" id="qsr" data-help="qsr" href="javascript:void(0)"><i class="fa fa-star select-menu-list"></i>Star Rating</a>
	                </li>
	                <li class="selectli">
	                  <a data-action="SingleTextboxQuestion" class="select-li" id="qst" data-help="qst" href="javascript:void(0)"><i class="fa fa-square-o select-menu-list"></i>Single Textbox</a>
	                </li>
	                <li class="selectli">
	                  <a data-action="MultipleTextboxQuestion" class="select-li" id="qmt" data-help="qmt" href="javascript:void(0)"><i class="fa fa-bars select-menu-list"></i>Multiple Textboxes</a>
	                </li>
                </ul>
            </div>
            </div>
           <div class="help"><i class="fa fa-exclamation-circle"></i></div>
         </div>
       </div> 
       <div id="selectedInputLi" class="selectedInputLi" >
       </div>
       <img src="<?= base_url();?>uploads/loader.gif" id="gif" style="display: none">
       </form>
   </div>
 </div><!-- /.box -->
 <!-- general form elements -->

</div>
</div>   
</section><!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php require APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.min.js"></script>
<script>
  $(document).ready(function() {

   $('#selectdropdownquestion li a').click(function() {
        //Get the data-aaction of list items
        /* to check if other div is alreay open*/
        var len= $('.answer-q').length;

        if(len >=1)
        {
          $('.answer-q').remove();
        }

        var selectInputLi = $(this).attr('data-action');
        var selectQueType = $(this).attr('data-help');
        $("#q_type").val(selectQueType);

        switch (selectInputLi) {

        case "MultipleChoiceQuestion":
		        var html='';
		        html +='<div class="answer-q">';
		        html +='<div class="anwer-body">';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-radio"><input type="radio" id="test4"><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" name="qmc[]" class="form-control" placeholder="Enter an answer choice"></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-radio"><input type="radio" id="test4"><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" name="qmc[]" class="form-control" placeholder="Enter an answer choice"></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-radio"><input type="radio" id="test4"><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" name="qmc[]" class="form-control" placeholder="Enter an answer choice"></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';       
		        html +='<div class="form-radio"><input type="radio" id="test4"><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" name="qmc[]" class="form-control" placeholder="Enter an answer choice"></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $("#surveyQue").focus();

            break;

        case "DropdownQuestion":
		         var html='';
		        html +='<div class="answer-q">';
		        html +='<div class="anwer-body">';
		        html +='<div class="form-group form-row">';
		        html +='<div class="input-box"><input type="text" class="form-control" name="qdd[]" ></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="input-box"><input type="text" class="form-control" name="qdd[]" ></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="input-box"><input type="text" class="form-control" name="qdd[]" ></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="input-box"><input type="text" class="form-control" name="qdd[]" ></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $("#surveyQue").focus();

            break;
              
          case "CheckboxQuestion":
		        var html='';
		        html +='<div class="answer-q">';
		        html +='<div class="anwer-body">';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-checkbox"><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qchb[]" ></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="#"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-checkbox"><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qchb[]" ></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="#"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-checkbox"><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qchb[]" ></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='<div class="form-group form-row">';
		        html +='<div class="form-checkbox"><label for="test4"></label></div>';
		        html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qchb[]" ></div>';
		        html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $("#surveyQue").focus();

            break;
              
          case "StarRatingQuestion":
		        var html='';
		        html +='<div class="answer-q">';        
		        html +='<div class="rating-box">';        
		        html +='<input type="hidden" class="form-control starno" name="qsr" id="starno" value="5" />';
		        html +='<div class="rating-foot"><div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i> Next Qustion </a></div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $("#surveyQue").focus();

            break;
              
          case "SingleTextboxQuestion":

		        var html='';
				html +='<div class="answer-q">';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a>';
		        html +='</div>';
		        html +='<div class=""> <input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $("#surveyQue").focus();

            break;
              
        case "MultipleTextboxQuestion":
		        var html='';
				html +='<div class="answer-q">';  
				html +='<div class="anwer-body add-label">';
				html +='<div class="form-group form-row">';
				html +='<div class="form-radio"><label for="test1">Label 1</label></div>';
				html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice"  name="qmt[]"></div>';
				html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
				html +='</div>';
				html +='<div class="form-group form-row">';
				html +='<div class="form-radio"><label for="test2">Label 2</label></div>';
				html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qmt[]"></div>';
				html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
				html +='</div>';
				html +='<div class="form-group form-row">';
				html +='<div class="form-radio"><label for="test3">Label 3</label></div>';
				html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qmt[]"></div>';
				html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
				html +='</div>';
				html +='<div class="form-group form-row">';
				html +='<div class="form-radio"><label for="test4">Label 4</label></div>';
				html +='<div class="input-box"><input type="text" class="form-control" placeholder="Enter an answer choice" name="qmt[]"></div>';
				html +='<div class="add-remove"><a href="javascript:void(0)"><i class="fa fa-plus-circle"></i></a> <a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a></div>';
				html +='</div>';
				html +='</div>';
		        html +='<div class="answer-foot">';
		        html +='<div><a href="javascript:void(0)" class="btn btn-success subQues"><i class="fa fa-arrow-circle-o-right"></i>Next Qustion</a></div>';
		        html +='<div class=""><input type="reset" class="btn btn-primary"><input type="submit" class="btn btn-success subQues" value="Save"></div>';
		        html +='</div>';
		        html +='</div>';
		        $("#selectedInputLi").append(html);
		        $("#surveyQue").focus();

            break;

          default:
              	text = "No value found";            
        //var surveyQue = $('#selectedli').val(); 
  		//alert(surveyQue); 
      }
		/* var str = document.getElementById("selectedli").innerHTML; 
		var res = str.replace("Multiple Choice",selectInputLi);
		document.getElementById("selectedli").innerHTML = res;*/
		//$("#selectedli").html($("#selectedli").html().replace(selectInputLi));
		//$("#selectedli").append('<span class="selectedli">'+selectInputLi+'</span>');
   });
  });


 $(function() {
        $('form').on('submit', function(e) {
        e.preventDefault();
       
      	//var json = $(this).serializeJSON();
		//console.log(json);
		$.ajax({
            dataType: 'html',
            type: 'post',
            url: '<?= base_url();?>questionSubmit',
            data: $('#nextQues').serialize(),
            //beforeSubmit: validator,

            success: function(responseData) {
                $('#messageResponse').html('<p>Insert Successfully</p>');
                $('#messageResponse').fadeIn(1000);
                $("form").trigger("reset");
            },
            error: function(responseData){
                console.log('Ajax request not recieved!');
            }
        });
        //$('#gif').show(); 
        //$("#viewAllQue").append(html);
        });
    });
</script>


      <script>
   $(document).ready(function(){
    $('#cat').change(function(){
     var cat_id = $('#cat').val();
    
     if(cat_id != '')
     {
      $.ajax({
       url:"<?php echo base_url(); ?>admin/Dashboard/fetch_subcategory",
       method:"POST",
       data:{cat_id:cat_id},
       success:function(data)
       {
        $('#subcat').html(data);
        $('#subsubcat').html('<option value="">Select Process</option>');
       }
      });
     }
     else
     {
      $('#subcat').html('<option value="">Select Sub category</option>');
      $('#subsubcat').html('<option value="">Select Process</option>');
     }
    });
   
    $('#subcat').change(function(){
     var sub_cat_id = $('#subcat').val();
     if(sub_cat_id != '')
     {
      $.ajax({
       url:"<?php echo base_url(); ?>admin/Dashboard/fetch_subsubcat",
       method:"POST",
       data:{sub_cat_id:sub_cat_id},
       success:function(data)
       {
        $('#subsubcat').html(data);
       }
      });
     }
     else
     {
      $('#subsubcat').html('<option value="">Select Process</option>');
     }
    });
    
   });
</script>