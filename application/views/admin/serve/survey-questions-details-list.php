
    <thead class="success">
        <input type="hidden" name="serialno" value="1" id="serialno">
        <input type="hidden" name="questionid" value="" id="questionid">
        <tr>
            <th class="">S.No.</th>
            <th class="text-center text-nowrap">Ans</th>
            <th class="text-center text-nowrap">Score</th>
        </tr>
    </thead>
    <tbody >
        <?php if(count($questionsDetails)>0){
            $count=count($questionsDetails);
            $sr=1; foreach($questionsDetails as $qd){ ?>
            <tr>  
                <td class="text-center text-nowrap"><?php echo $sr;?></td> 
                <td class="text-center text-nowrap"><?php echo $qd['que_type'];?></td> 
                <td class="text-center text-nowrap"><?php echo $qd['score_name'];?></td> 
                <td class="text-center text-nowrap"> 
                    <?php if($sr >= $count){?>
                        <a href="javascript:void(0);" class="btn btn-success" onClick="addQueOption('<?php echo $qd['que_ans_id']; ?>','<?php echo $qd['que_id']; ?>')" ><i class="fa fa-plus-circle" aria-hidden="true"></i></a>  <a href="javascript:void(0);" class="btn btn-success" id="save">Save</a></td> 
                    <?php }?>                     
            </tr>
        <?php $sr++; } ?>
         <?php } else { ?>
            <tr><td class="text-center" colspan="3"><?php echo ucfirst($this->config->item('record_not_found_title'));?></td></tr>
        <?php } ?>
    </tbody>


<script type="text/javascript">
    

      function addQueOption(optionid,queid){ 

        $('#questionid').val(queid);
        var serailno = $('#serialno').val();

        var Select=' -- Select -- ';
        var Poor='Poor';
        var Fair='Fair';
        var Good='Good';
        var Excellent='Excellent';

        var placeholder='Enter an answer choice...';
        var fafaiconplus='<i class="fa fa-plus-circle"></i>';
        var fafaiconminus='<i class="fa fa-minus-circle"></i>';
 
        var dropdown ='';

            //dropdown +='<select>';
            dropdown +='<option>'+Select+'</option>';
            dropdown +='<option value="1">'+Poor+'</option>';
            dropdown +='<option value="2">'+Fair+'</option>';
            dropdown +='<option value="3">'+Good+'</option>';
            dropdown +='<option value="4">'+Excellent+'</option>';
           // dropdown +='</select>';


        console.log('QuestionId => '+queid+' Optionid => '+optionid);      
            
           var addmore ='<tr id="trserialno'+serailno+'">';
           addmore +='<td>';
           addmore +='<input class="form-control" type="text" name="optionid'+serailno+'" id="optionid'+serailno+'" placeholder="'+placeholder+'">';
           addmore +='<input type="hidden" name="sr[]" value="'+serailno+'" class="sr">';
           addmore +='<input type="hidden" name="queid'+serailno+'" id="queid'+serailno+'" value="'+queid+'" class="queid" ></td>';
           addmore +='<td><select class="form-control" name="score_name'+serailno+'" id="score_name'+serailno+'" >'+dropdown+'</select></td>';
           addmore +='<td><a href="javascript:void(0);" onClick="deleteOptionidRow('+serailno+')" class="btn btn-danger remove">'+fafaiconminus+'</a></td>';
           addmore +='</tr>';

            $('#innertable').append(addmore);

            serailno++ ;
            $('#serialno').val(serailno);
            
        }




    function deleteOptionidRow(id){        
        var option_id=$('#serialno').val(); 
        if(option_id>1){
             $('#trserialno'+id).remove();
        }else{            
             $('#trserialno'+id).remove();
        }
    }



    $(function(){

    $('#save').click(function(){
       /*  $('.sr').each(function(){
                var sr = $(this).val();
                var optionid=$('#optionid'+sr).val();
                var score_name=$('#score_name'+sr).val();     
                alert('optionid => '+optionid+' score_name => '+score_name);    
            })*/
            saveQueeOptions();
        })    
    })
  function saveQueeOptions(){
    $.ajax({
      type: "POST",
      url: '<?php echo base_url('admin-change-question-options')?>',
      data: $('#add_option').serialize(),
      success: function(ajaxresponse){
        response=JSON.parse(ajaxresponse);
        if(response){
          //alert('admin');
          //$('#trserialno').html('');
           //$('#add_option').html(''),
        }
      }
    }); 
     var questionid = $('#questionid').val();
    questionsDetailsrow(questionid);
}

</script> 