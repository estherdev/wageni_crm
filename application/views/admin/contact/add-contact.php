<!-- 
<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
 -->
<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Contact
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Add Contact</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Contact Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
            <form method="post" action="<?= base_url('admin/masters/Contact');?>" enctype="multipart/form-data">

              <div class="box-body">
              <div class="form-group row">

                 <div class="col-md-6">
                  <label for="title">Address</label>
                  <input type="text"  class = "form-control"name="address" />
                  <?= form_error('address')?>            
                </div>

                 <div class="col-md-6">
                  <label for="title">Phone</label>
                  <input type="text"  class = "form-control"name="phone" />
                  <?= form_error('phone')?>            
                </div>
                <!-- 
                <div class="container">
  <div class="row">
    <h2>Multiple fields contact form</h2>
      <div class="col-md-4">
            <div class="contacts">
                <label>Contacts:</label>
                    <div class="form-group multiple-form-group input-group">
                        <div class="input-group-btn input-group-select">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="concept">Phone</span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#phone">Phone</a></li>
                                <li><a href="#fax">Fax</a></li>
                                <li><a href="#skype">Skype</a></li>
                                <li><a href="#email">E-mail</a></li>
                                <li><a href="#www">Web</a></li>
                            </ul>
                            <input type="hidden" class="input-group-select-val" name="contacts['type'][]" value="phone">
                        </div>
                        <input type="text" name="contacts['value'][]" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-success btn-add">+</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
  </div>
</div> -->

                 <div class="col-md-6">
                  <label for="title">Phone</label>
                  <input type="text"  class = "form-control"name="phone2" />          
                </div>

                 <div class="col-md-6">
                  <label for="title">Phone</label>
                  <input type="text"  class = "form-control"name="phone3" />            
                </div>

                 <div class="col-md-6">
                  <label for="title">Email</label>
                  <input type="text"  class = "form-control"name="email" />
                  <?= form_error('email')?>            
                </div>
                         
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.content-wrapper -->
    </div>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Contact</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped text-center">
                <thead>
                  <tr> 
                    <th>S.No.</th>
                    <th>Address</th>
                    <th>Phone1</th>
                    <th>Phone2</th>
                    <th>Phone3</th>
                    <th>Email</th>
                    <!-- <th>Action</th> -->
                  </tr>
                </thead>
                <tbody>    
                  <?php $i= 1; foreach ($alldata as $key => $pack) {?>
                  <tr> 
                    <td><?= $i++; ?></td>
                    <td><?= ucfirst($pack->address); ?></td>
                    <td><?=($pack->phone1);?></td>
                    <td><?=($pack->phone2);?></td>
                    <td><?=($pack->phone3);?></td>
                    <td><?=($pack->contact_email); ?></td>
                     <td class="text-center">
                        <a class="btn btn-sm btn-info" href="<?= base_url('edit-contact/'.$pack->id)?>" title="Edit"><i class="fa fa-pencil"></i></a> 
                        <a class="btn btn-sm btn-danger deletepack"  data-toggle="modal" data-target="#deleteModal<?= $pack->id ; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                      </td>
                      <div id="deleteModal<?= $pack->id ; ?>" class="modal fade">
                        <div class="modal-dialog">
                        <div class="modal-content">
                        <!-- dialog body -->
                        <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                         Are You Sure to <strong>Contact</strong> Delete?
                        </div>
                        <!-- dialog buttons -->
                        <div class="modal-footer"><button type="button" class="btn btn-primary"  onclick="window.location='<?php echo base_url('delete-contact/'.$pack->id);?>';">Confirm</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                        </div> 
                        </div>
                      </div>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    <!-- /.content-wrapper -->
  </div>

  <!-- /.content-wrapper -->
</section><!-- /.content -->
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>