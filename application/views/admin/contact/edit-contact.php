<!--header Section Start Here -->
<?php include APPPATH . 'views/admin/header.php';?>
<!--header Section End Here -->
<!-- Left side column. contains the logo and sidebar -->
<?php include APPPATH . 'views/admin/main-sidebar.php';?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit Contact
    </h1>
    <ol class="breadcrumb">
      <li><a href="javascript:void()"><i class="fa fa-dashboard"></i> Back</a></li>
      <li class="active">Edit Contact</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
         <?php if($this->session->flashdata("success")):?>
      <div class="alert alert-success"><?= $this->session->flashdata("success");?>
      <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
      <?php if($this->session->flashdata("failed")):?>
      <div class="alert alert-danger"><?= $this->session->flashdata("failed");?>
        <a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
      </div>
      <?php endif;?>
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Contact Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->                       
            <form method="post" id="contact" action="#" enctype="multipart/form-data">
              <div class="box-body">
              	  <input type="hidden" name="id" value="<?= $records[0]->id; ?>">
              <div class="form-group row">
                 <div class="col-md-6">
                  <label for="title">Address</label>
                  <input type="text" value="<?php echo $records[0]->address;?>" class = "form-control" name="address" />
                  <?= form_error('address')?>            
                </div>
                <div class="col-md-6">
                  <label for="title">Phone</label>
                  <input type="number" value="<?php echo $records[0]->phone1;?>" class ="form-control" name="phone" />
                  <?= form_error('phone')?>
                </div>
              </div>

                 <div class="form-group row">
                 <div class="col-md-6">
                  <label for="title">Phone2</label>
                  <input type="number" value="<?php echo $records[0]->phone2;?>" class ="form-control" name="phone2" />
                  <?= form_error('phone2')?>
                </div>

                 <div class="col-md-6">
                  <label for="title">Phone3</label>
                  <input type="number" value="<?php echo $records[0]->phone3;?>" class ="form-control" name="phone3" />
                  <?= form_error('phone3')?>
                </div>
              </div>

               <div class="form-group row">
                <div class="col-md-6">
                  <label for="title">Email</label>
                  <input type="text" value="<?php echo $records[0]->contact_email;?>" class="form-control" name="email" />
                  <?= form_error('email')?>
                </div>
              </div>

              <div class="form-group row">
               <div class="col-md-6">
                  <label for="title">F_Phone</label>
                  <input type="text" value="<?php echo $records[0]->fphone;?>" class ="form-control" name="fphone" />
                  <?= form_error('fphone')?>
                </div>
                <div class="col-md-6">
                  <label for="title">F_Address</label>
                  <input type="text" value="<?php echo $records[0]->faddress;?>" class = "form-control" name="faddress" />
                  <?= form_error('faddress')?>            
                </div>
              </div>

               <div class="form-group row">
                <div class="col-md-6">
                  <label for="title">F_Email</label>
                  <input type="text" value="<?php echo $records[0]->femail;?>" class="form-control" name="femail" />
                  <?= form_error('femail')?>
                </div>
                <div class="col-md-6">
                  <label for="title">F_Description</label>
                  <input type="text" value="<?php echo $records[0]->fdescription;?>" class="form-control" name="fdescription" />
                  <?= form_error('fdescription')?>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-success">Update</button>
                <!--<button type="reset" class="btn btn-danger">Reset</button>-->
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
  </div>

  <!-- /.content-wrapper -->
</section><!-- /.content -->
</div>
<!--footer Section Start Here -->     
<?php include APPPATH . 'views/admin/footer.php';?>
<!--footer Section End Here -->
  <script type="text/javascript">
    $(function () {
      $("#example1").dataTable();
      $('#example2').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });
    });
  </script>