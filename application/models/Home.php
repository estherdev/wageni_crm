<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Home (UserController)
 * Home Class to control Home Pages related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 28 August 2018
 */
class Home extends BaseController
{
 
    //  This is default constructor of the class
 
    public function __construct()
    {
        parent::__construct();  
        $this->load->model('LoginModel');
    }
    
    // This function used to load the first screen of the user
 
    public function index()
    {
        $this->global['pageTitle'] = 'Home';    
        $this->global['homeslider'] = $this->MastersModel->fetchSlider();  
        $this->global['alldata'] = $this->MastersModel->FetchClientLogo();
        $this->global['alltestimonials'] = $this->MastersModel->getTest();        
        $this->global['packge_demo'] = $this->LoginModel->fetch_packge_demo();
        $this->global['practice'] = $this->MastersModel->fetchPractice();
        $this->global['teamdata'] = $this->MastersModel->fetchTeam();        
        $this->load->view("website/home", $this->global);
    }
   public function login()
    {
        $loginData=array('uEmail'=>html_escape(trim($this->input->post('lemail', TRUE))),
                       'uPass'=>html_escape(trim($this->input->post('lpass', TRUE)))
                            );
       
        $loginresult= $this->LoginModel->checkfrontLogin($loginData);   
        //print_r($loginresult); die;
        if($loginresult['status'] =='true' ){
            $this->session->set_userdata('sub_login_id',$loginresult['data']->sub_login_id);
            $this->session->set_userdata('added_by',$loginresult['data']->added_by);
            $this->session->set_userdata('useremail',$loginresult['data']->sub_email);
            $this->session->set_userdata('username',$loginresult['data']->sub_name);
            $this->session->set_userdata('fname',$loginresult['data']->fname);
            $this->session->set_userdata('lname',$loginresult['data']->lname);
            $this->session->set_userdata('userrole',$loginresult['data']->sub_login_role);
            $this->session->set_userdata('rolename',$loginresult['data']->subscriber_role_name);
            $this->session->set_userdata('member_since',$loginresult['data']->subscriber_created);
            $this->session->set_userdata('userfile',$loginresult['data']->userfile);
            $this->session->set_userdata('category',$loginresult['data']->category);
            $this->session->set_userdata('category_name',$loginresult['data']->category_name);
        }
        echo json_encode($loginresult);         
    }
    public function logout()
    {
     	$this->session->sess_destroy('useremail');
        redirect(base_url());
    }

    // Company Dashboard  
    public function web_admin_dashboard()
    {
        //$this->isLogin();
     	$this->global['pageTitle'] = 'Web Admin Dashboard';   
     	$this->global['totalBranch'] = $this->MastersModel->totalBranch();     
        $this->load->view("webadmin/index", $this->global);
    } 

    public function sendSurvey()
    {
     $this->load->view("website/emailsurvey");
    }

    public function company_register()
    {
        //echo "<pre/>"; print_r($this->input->post()); die;
        $this->form_validation->set_rules('fname', ' First Name', 'required|min_length[1]|max_length[15]|trim');  
        $this->form_validation->set_rules('lname', 'Last Name', 'required|min_length[1]|max_length[15]|trim');  
        $this->form_validation->set_rules('company_name', 'Company Name', 'required|trim');  
        //$this->form_validation->set_rules('sub_phone', '', 'required|min_length[9]|max_length[13]|trim'); 
        $this->form_validation->set_rules('company_email', 'Email', 'required|valid_email|is_unique[subscriberlogin.sub_email]|trim');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('company_url', 'Company Url', 'required');
        $this->form_validation->set_rules('hvroom', 'total Rooms', 'required');
        /*echo $p=random_string('alnum', 16);
        echo "</br>";
        echo $pa=random_string('sha1', 16);*/
        $pass="123456";
        if($this->form_validation->run()){
        $company_registerData=array('fname'=>html_escape(trim($this->input->post('fname', TRUE))),
                                    'lname'=>html_escape(trim($this->input->post('lname', TRUE))),
                                    'sub_phone'=>html_escape(trim($this->input->post('company_phone', TRUE))),
                                    'sub_name'=>html_escape(trim($this->input->post('company_name', TRUE))),
                                    'sub_email'=>html_escape(trim($this->input->post('company_email', TRUE))),
                                    'company_url'=>html_escape(trim($this->input->post('company_url', TRUE))),
                                    'country'=>html_escape(trim($this->input->post('country', TRUE))),
                                    'category'=>html_escape(trim($this->input->post('category', TRUE))),
                                    'sub_login_role'=>1,
                                    'added_by'=>0,
                                    'sub_pass'=>$pass,
                                    'hvroom'=>html_escape(trim($this->input->post('hvroom', TRUE)))
                            );

         $this->session->set_userdata('reg_name',$company_registerData['fname']);
         $this->session->set_userdata('reg_email',$company_registerData['sub_email']);
       
         $loginresult= $this->LoginModel->companyRegister($company_registerData);  
            
        if($loginresult ==TRUE )
            {  
                echo "registered";
                $name=$this->session->userdata('reg_name');
                $email=$this->session->userdata('reg_email');
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'mastcrew22@gmail.com',
                    'smtp_pass' => 'crew@1234',
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
                $mailMsg='<!DOCTYPE html>
                <html>
                <head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Wageni Newsletter</title>
                <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
                </head>
                <body style="background-color: #f1f1f1; font-family: raleway, sans-serif; font-size: 15px; margin: 0; padding: 0;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f1f1f1; font-family: "raleway", sans-serif; font-size: 15px;">
                <tr>
                <td align="center">
                <table border="0" cellpadding="10" cellspacing="0" width="600" align="center">
                <tr>
                <td align="center">
                <table border="0" cellpadding="15" cellspacing="0" width="600" style="background: transparent;">
                <tr>
                <td align="center"><a href=""><img src="http://wageni.us.tempcloudsite.com/globalassets/admin/logo1.png"></a></td>
                </tr>
                </table>
                <table border="0" cellpadding="10" cellspacing="10" width="600" style="background: #fff;">
                <tr>
                <td align="center" style="font-size: 36px; font-weight: bold; color: #555; letter-spacing: 1px;">Welcome to Wageni CRM.</td>
                </tr>
                <tr>
                <td>Thank You for registering on Wageni CRM. Your account is waiting for admin approval.</td>
                </tr>
                </table>
                <table border="0" cellpadding="15" cellspacing="0" width="600" style="background: transparent;">
                <tr>
                <td align="center">Copyright &copy; 2018 <a href="http://wageni.us.tempcloudsite.com">wagenicrm.com</a>. All rights reserved.</td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                </body>
                </html>';
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");          
                $this->email->from('praveen@panindia.in', 'Wageni CRM');
                $this->email->to($email);
                $this->email->subject('New User Registered');
                $this->email->message($mailMsg);
                $result = $this->email->send();
            }else{
                echo "err";
            }                   
        }
        else
        {
            echo "1";
        }
    }

    /* ********************************************** Forgot password ************************************************* */

    public function ForgotPassword()
      {  
        $reponse = array();
        $email = $this->input->post('lost_email');
        $this->load->model('LoginModel');     
        $findemail = $this->LoginModel->ForgotPassword($email);  
                 ////echo("<pre/>"); print_r($findemail);
        $reponse = array('success' => isset($findemail))
        if($findemail){
          $this->LoginModel->sendpassword($findemail);  
                   //echo $this->email->print_debugger();die;
        }
        header('Content-type: application/json');
        echo json_encode($reponse);

    }

    /* ********************************************** Forgot password ************************************************* */
   
}

?>