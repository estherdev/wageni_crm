<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/*
 Model : UMAsters Model 
*/

class MastersModel extends CI_Model

{
    public function __construct() 
	{
        parent::__construct();
    }
	var $homepage_slider = 'homepage_slider';
	var $faq = 'faq';
	var $packages = 'packages';
	var $subscriberlogin = 'subscriberlogin';
	var $sub_mapping = 'subscriberpackagemapping';
	var $subscriberroles = 'subscriberroles';
	var $about_us = 'about_us';


	/*	Insert homepage_slider  */
	public function insertSlider($data)	{
		return $this->db->insert($this->homepage_slider,$data);
	}

	/*	Fetch homepage_slider 	*/

	public function fetchSlider()	{

		$this->db->select('*'); 
		$this->db->from($this->homepage_slider);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}

	/*	Fetch homepage_slider By Id  */

	public function gethomepage_slider($id){

		$this->db->from($this->homepage_slider);		
		$this->db->where('slider_id',$id);
		$query = $this->db->get();
		if($query){
			return $query->result();
		}
		else{  
			return false;
		}
	}

	/*	Updates homepage_slider	*/

	public function updateSlider($homepage_sliderData)	{

		$this->db->where('slider_id',$homepage_sliderData['slider_id']);
	    $this->db->update($this->homepage_slider, $homepage_sliderData);
	    return true;
	}

	/* 	Delete homepage_slider		*/

	public function deleteSlider($id)	{
		$this->db->where('slider_id', $id);
		return $this->db->delete($this->homepage_slider); 
	}


	/******************************************************8*   Blog Start  **************************************************/ 

	public function blogInsert($data)
	{
		return $this->db->insert('blog', $data);
	}

	public function fetchBlog()
	{
		$this->db->select('*');
        $query = $this->db->get('blog');
        return $query->result();
	}

	public function getblogdatabyid($id)
	{

 	 	  $this->db->select("*");
  	      $this->db->from('blog');
		  $this->db->where('id',$id);
		  $query = $this->db->get();
		 // echo($id);die;
	     return $query->result_array();
	}

	public function updateblogdata($data,$id)
	{
		$this->db->where('id',$id);
		
		return $this->db->update('blog',$data);
		 
		 //print_r($data);die;
	}

	public function deleteBlog($id)
	{
		$this->db->where('id',$id);
		return $this->db->delete('blog');
	}

	
	/*********************************************** Blog End  *****************************************************/


 	/*********************************************** Testimonials Start ********************************************/

 	/* Insert the Testimonials*/
 	public function testInsert($data)
 	{
 		return $this->db->insert('testimonials',$data);

 	}

 	/*Fetch Testimonials*/
 	public function getTest()
 	{
 		$this->db->select('*');
 		$query = $this->db->get('testimonials');
 		return $query->result();
 	}

 	/*Fetch Testimonials By Id*/
 	public function getTestDataById($id)
 	{
 		$this->db->select('*');
 		$this->db->from('testimonials');
 		$this->db->where('id',$id);
 		$query = $this->db->get();
 		return $query->result_array();
 	}

 	/*Testimonials Updated*/
 	public function testUpdate($data,$id)
 	{
 		$this->db->where('id',$id); 
 		return $this->db->update('testimonials',$data);
 	}

 	/*Testimonials Deleted*/
 	public function deleteTesti($id)
 	{
 		$this->db->where('id', $id);
 		return $this->db->delete('testimonials');
 	}

 	/*********************************************** Testimonials End ********************************************/


 	/*********************************************** About Start ********************************************/
 			/*About Insert*/
 	public function aboutUpdate($data,$a)

 	{	//echo "/<pre>";print_r($data);die;
 		$this->db->where('id',$a);
 		return $this->db->update($this->about_us,$data);
 	}

 		/* Fetch About */
 	public function getAbout()
 	{
 		$this->db->select('*');
 		$query = $this->db->get($this->about_us);
 		return $query->result_array();
 	}
 		


 	
    /*********************************************** About End ********************************************/	

 	
 	/*********************************************** Contact Start ***************************************/
    	
    	      /***** Insert the Contact ****/   	
    public function insertContact($data)
		{
			return $this->db->insert('contact', $data);
		}

	          	/****  Fetch the Contact ****/
		public function getContact()
		{
			$this->db->select('*');
            $query = $this->db->get('contact');
            return $query->result_array();
		}

		   /****  Fetch the Contact By Id ****/
		public function getContactById($id)
		{
			  $this->db->select("*");
      	      $this->db->from('contact');
    		  $this->db->where('id',$id);
    		  $query = $this->db->get();
    	      return $query->result_array();
		}

			/**** Update the Contact  ****/
		public function updateContact($data)
		{
			$this->db->where('id',$id);
			return $this->db->update('contact',$data);
		}

			/**** Delete the Contact ****/
		public function deleteContact($id)
		{
			$this->db->where('id',$id);
			return $this->db->delete('contact');
		}

    /*********************************************** Contact End   ***************************************/


    /*********************************************** Faq Start ********************************************/
   

    /* Faq Insert */
    public function faqInsert($data)
	{
		return $this->db->insert($this->faq, $data);
	}

	/*	Fetch Homepage FAQ 	*/
	public function getdata()	{

		$this->db->select('*'); 
		$this->db->from($this->faq);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}

}
    public  function getdatabyid($id)
	{
		  $this->db->select("*");
		  $this->db->from($this->faq);
		  $this->db->where('faq_id',$id);
		  $query = $this->db->get();
		  return $query->result_array();
	}


	public function faqUpdate($data,$id)
	{
	
		$this->db->where('faq_id',$id); 
  	  return $this->db->update($this->faq,$data);
//echo $this->db->last_query();die;  
	}


	public function deleteFaq($id)
    {
        $this->db->where('faq_id', $id);
       return $this->db->delete($this->faq);

    }
	
    /****************************************************   Faq End *********************************************   */

    /************************************************** ClientLogo Start *************************************** */

    public function logoInsert($data)
	{
		return $this->db->insert('client_logo', $data);
	}

			/*	Fetch Homepage Client Logo 	*/
	public function FetchClientLogo($data)
	{
		$this->db->select('*');
		$query = $this->db->get('client_logo');
		return $query->result_array();
	}

	public function getClientLogoByid($id)
	{
		$this->db->select('*');
		$this->db->from('client_logo');
		$this->db->where('cl_id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function updateClientLogo($data,$id)
	{
		$this->db->where('cl_id',$id);
		return $this->db->update('client_logo',$data);
	}

	public function deleteClientLogo($id)
	{
		$this->db->where('cl_id',$id);
		return $this->db->delete('client_logo');
	}


    /************************************************** ClientLogo End *************************************** */


    /****************************************************   Start Package 	***************************************/
   
    /*	Insert packages  */
	public function insertPackage($packagesdata)
	{
		return $this->db->insert($this->packages,$packagesdata);
	}
	/*	Fetch packages 	*/
	public function fetchPackage()
	{
		$this->db->select('*'); 
		$this->db->from($this->packages);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
	/*	Fetch packages By Id  */
	public function fetchPackageId($id){
		$this->db->from($this->packages);		
		$this->db->where('package_id',$id);
		$query = $this->db->get();
		if($query){
			return $query->result();
		}
		else{  
			return false;
		}
	}
	/*	Updates packages	*/
	public function updatePackages($data)
	{
		$this->db->where('package_id',$data['package_id']);
		$this->db->update($this->packages, $data);
		return true;
	}
	/* 	Delete Packages		*/
	public function deletePackages($id)
	{
		$this->db->where('package_id', $id);
		return $this->db->delete($this->packages); 
	}
    /* End Package 	*/

    /* Fetch All Subscriber for Add company */
    public function fetchCompanyUser()
    {
    	$this->db->select('*'); 
		$this->db->from($this->subscriberlogin);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
    }

    /****************************************************   Start Company 	***************************************/
   
    /*	Insert Company  */
	public function insertCompany($data)
	{
		return $this->db->insert($this->sub_mapping,$data);
	}
    /*	Fetch Company  */
	public function fetchCompany()
	{
		$this->db->select('spm.*,sl.sub_name,p.package_name,p.package_price'); 
		$this->db->from('subscriberpackagemapping as spm');
		$this->db->join('subscriberlogin as sl','spm.subscriber_id=sl.sub_login_id','left' );
		$this->db->join('packages as p' ,'spm.subscriber_package_id=p.package_id','left' );
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
    /*	Fetch New Company  */
	public function fetchNewCompany()
	{
		$this->db->select('sl.*,role.subscriber_role_name as role'); ;
		$this->db->from('subscriberlogin as sl');
		$this->db->join('subscriberroles as role' ,'sl.sub_login_role=role.subscriber_roll_id','left' );
		$this->db->where('added_by','0');
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
		/*	Change Status 	*/
	public function statusCompany($Id,$NewStatus)
	{		
		$this->db->where('sub_login_id',$Id);
		$this->db->set('sub_isActive',$NewStatus);
		return $this->db->update('subscriberlogin');
	}
	

}