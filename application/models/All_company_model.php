<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class All_company_model extends CI_Model
{
	public function __construct() 
	{
		parent::__construct();
	}
	var $admin_login = 'admin_login';
	var $adminemprole = 'adminemprole';
	var $subscriberlogin = 'subscriberlogin';
	var $subscriberroles = 'subscriberroles';
	var $subscriberpackagemapping = 'subscriberpackagemapping';
	var $packages = 'packages';
	var $subscriber_branch = 'subscriber_branch';


	public function fetchSubscribersRole(){
		$this->db->select('*'); 
		$this->db->from($this->subscriberroles);		
 		$query = $this->db->get();		
		return ($query->num_rows() > 0)?$query->result():array();	
	}
	public function changeDataStatus($table,$update_data){
		if(count($update_data)>0 && !empty($table)){
			$this->db->where('sub_login_id',$update_data['sub_login_id']);
			$response=$this->db->update($table,$update_data);
			return $response;
		}
	}

	public function fetchUserRecord($id)	{
		$this->db->select('*'); 
		$this->db->from($this->subscriberlogin);		
		$this->db->where('sub_login_id',$id);
 		$query = $this->db->get();		
		return ($query->num_rows() > 0)?$query->row_array():array();	
	}	

	public function companyList(){ 
		$this->db->select("*");
		$this->db->from("subscriberlogin sl");		
 		$this->db->where('sl.added_by',0);	
		$this->db->order_by('sl.sub_login_id' ,'DESC');	
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():array();		
	}

	public function getCompanyListAdmin($id){
			$this->db->select("
			sl.sub_login_id id,
			sl.added_by company_id,
  			sb.branch_name branch_id,
			sl.total_review ,
			sl.fname first_name,
			sl.lname last_name,
			sl.sub_name ,			
			sl.sub_phone phone,
 			sl.sub_login_role ,
 			sl.sub_email email,
 			sl.sub_isActive status,
 			sr.subscriber_role_name role
 		"); 

		$this->db->from('subscriberlogin sl');
		$this->db->join('subscriberroles as sr','sl.sub_login_role=sr.subscriber_roll_id','inner');	
		$this->db->join('subscriber_branch as sb','sl.branch_id=sb.sub_branch_id','inner');	
 		$this->db->order_by('sl.sub_login_id' ,'DESC');	
 		$this->db->where('sl.added_by',$id);	
		$query = $this->db->get();		
		//echo $this->db->last_query();		
		return ($query->num_rows() > 0)?$query->result_array():array();		
	}

	public function fetchCompanyBYId($id){
		$where=array('c.sub_login_id',$id);
		$this->db->select("
					spm.*,
					c.*,
					p.package_name,
					p.package_price,
					p.package_createddate,
					sr.subscriber_role_name as role,
					cat.category_name 
				");
		$this->db->from('subscriberpackagemapping as spm');
		$this->db->join('subscriberlogin as c','spm.subscriber_id=c.sub_login_id','inner');
		$this->db->join('subscriberroles as sr','c.sub_login_role=sr.subscriber_roll_id','inner');		
		$this->db->join('packages as p','spm.subscriber_package_id=p.package_id','inner');
		$this->db->join('category as cat','c.category=cat.cat_id','inner');		 
		$this->db->where('c.sub_login_id',$id);		 
		$query = $this->db->get();		
		return ($query->num_rows() > 0)?$query->row_array():array();		
	}

	
	public function companyListAjax($post,$count=false)	{		 
		$where=array();		
		$change_package=$post['package_id'];
  		$change_category=$post['category_id'];  		
	 
		if($change_package>0){
			$where['spm.subscriber_package_id']=$change_package;
		}
		if($change_category>0){
			$where['c.category']=$change_category;
		}		
		$where['c.added_by']=0; 

	$this->db->select("
		spm.*,
		c.*,
		p.package_name,
		p.package_price,
		p.package_createddate,
		sr.subscriber_role_name as role,
		cat.category_name, (SELECT AVG(business_questionnaire_answer.bus_que_score)  FROM `customer_feedback` 
		JOIN business_questionnaire_answer ON customer_feedback.cf_score = business_questionnaire_answer.bus_que_ans_id 
		JOIN subscriberlogin ON subscriberlogin.sub_login_id = customer_feedback.busId where subscriberlogin.sub_login_id =c.sub_login_id) as score
	");
	$this->db->from('subscriberpackagemapping as spm');
	$this->db->join('subscriberlogin as c','spm.subscriber_id=c.sub_login_id','inner');
	$this->db->join('subscriberroles as sr','c.sub_login_role=sr.subscriber_roll_id','inner');		
	$this->db->join('packages as p','spm.subscriber_package_id=p.package_id','inner');
	$this->db->join('category as cat','c.category=cat.cat_id','inner');
	
	if(count($where)>0){
		$this->db->where($where);
	}
		if ($post['order'][0]['column']!='' && isset($post['order'][0]['dir']) && !empty($post['order'][0]['dir'])) {
	    $order_by = '';
	    switch ($post['order'][0]['column']) {
	       
			case 4:
	        $order_by = 'c.sub_login_id';
	        break;
		
	        default:
	        $order_by = 'c.sub_login_id';
	        break;
	    }
             
        $dir_by = '';
        switch ($post['order'][0]['dir']) {
            
            case 'asc':
                $dir_by = 'asc';
                break;
            case 'desc':
                $dir_by = 'desc';
                break;
            default:
               $dir_by = 'asc';
                break;
        }
        $this->db->order_by($order_by,$dir_by); 
    }

        if(!$count){
			$start=$post['start'];
			$length=$post['length'];
			if(!$start){
				$start=0;
			}
			if(!$length){
				$length=10;
			}
			$this->db->limit($length,$start);
		}		
 		
		$result = $this->db->get();
 		$resultArray = $result->result_array();	
     	if($count){
			return $result->num_rows();
		}else{
			return $resultArray;
		}
	}

	public function subadminGetAllComplaintsAjax($id){
	if ($this->session->userdata('sub_login_id')) {
		$this->db->where('cf.busId', $this->session->userdata('sub_login_id'));
	} 
	if($id>0){
		$this->db->where('cf.nps_id', $id);
	}

	$this->db->select("
				cf.*,
				gu.*,
				bqa.*,
				nps.id as nps_main_id,
				nps.cat_id as npscatid,
				nps.sub_cat_id as npssubcatid,
				nps.process_id as npsprocessid,
				nps.busid as npsbusid,
				nps.npscore as npsscore,
				nps.added_time as npsadded_time,
				frs.id as frsid,
				frs.name as frsname,
				frs.status as frsstatus,
				sl.fname,
				sl.lname,
				sl.sub_login_id,
				c.category_name,
				sc.sub_cat_name,
				ssc.sub_sub_cat_name,
				q.bus_q_que
			");
		$this->db->from('customer_feedback as cf');
		$this->db->join('guest_user as gu','cf.cf_user_id=gu.user_id','inner');
		$this->db->join('netpscore as nps','cf.nps_id=nps.id','inner');
		$this->db->join('subscriberlogin as sl','cf.busId=sl.sub_login_id','inner');
		$this->db->join('questionnaire as q','cf.cf_que_id=q.bus_qid','inner');
		$this->db->join('business_questionnaire_answer as bqa','cf.cf_score=bqa.bus_que_ans_id','inner');
		$this->db->join('category as c','cf.cf_cat_id=c.cat_id','inner');
		$this->db->join('sub_category as sc','cf.cf_cat_sub_id=sc.sub_cat_id','inner');
		$this->db->join('sub_sub_category as ssc','cf.cf_cat_process_id=ssc.sub_sub_cat_id','inner');
		$this->db->join('feedback_rating_status frs', 'cf.cf_status=frs.id','inner');
 		$this->db->order_by('cf.cf_id', 'DESC');
 		
		
		$query = $this->db->get();
 		$result= $query->result();
		if ($result==true) {
 			return $query->result();
		} else {
			return false;
		}
		
	}



     public function customDate($post){
           $change_date=$post['change_date'];
           $today=date('d-m-Y');
           switch ($change_date){

            case 0:
            //today
            $from_date=$today;
            $to_date=$today;
            break;

            case 1:
            //yesterday
            $from_date=date('d-m-Y',strtotime("-1 days"));
            $to_date=date('d-m-Y',strtotime("-1 days"));
            break;

            case 2:
             //Last seven Days
            $from_date=date('d-m-Y',strtotime("-7 days"));
            $to_date=$today;
            break;

            case 3:
            //Last Fifteen days
            $from_date=date('d-m-Y',strtotime("-15 days"));
            $to_date=$today;
            break;

            case 4:
            //this month
            $from_date=date('01-m-Y');
            $to_date=$today;
            break;

            case 5:
            //last month
            $from_date=date('01-m-Y',strtotime('last month'));
            $to_date=date('t-m-Y',strtotime('last month'));
            break;

            case 6:
             //last six months
            $from_date=date('d-m-Y', strtotime(date('Y-m-d') .' -6 months'));
            $to_date=$today;
            break;

            case 7:
            //this year
            $from_date=date('01-01-Y');
            $to_date=$today;
            break;

            case 8:
            //Last Year
            $from_date=date("d-m-Y",strtotime("last year January 1st"));
            $to_date=date("d-m-Y",strtotime("last year December 31st"));
            break;

            case 9:
            $from_date=$post['from_date'] ? $post['from_date']:'01-01-1970';
            $to_date=$post['to_date'] ? $post['to_date']:$today;
            break;

            default :
            $from_date=$today;
            $to_date=$today;
            
           }
           $response['from_date']=$from_date;
           $response['to_date']=$to_date;
           return $response;
     }


	public function adminGetAllComplaintsAjax($post,$count=false)	{		 
				
 		$where=array();	
 		$like=array();	
 		$bus_id='';
		$feedback_id=$post['feedback_id']; 
		$status_id=$post['status_id']; 
		$search=trim($post['search']); 

		if (isset($post['nps_main_id_'])) {			
		$nps_id=$post['nps_main_id_'];
	 	 	if($nps_id>0){
		 		$where['cf.nps_id']=$nps_id;
		 	}
	 	}

 		if ($this->session->userdata('sub_login_id')) {
			$bus_id= $this->session->userdata('sub_login_id');
		}else{
			$bus_id=$post['bus_id'];			
		}
		
		if (!$this->session->userdata('sub_login_id')) {
			$change_category=$post['category_id'];
			if($change_category>0){
				$where['cf.cf_cat_id']=$change_category;
			}
		}   		

   		if($feedback_id>0){
			//$where['bqa.bus_que_score']=$feedback_id;
			$where['cf.cf_score']=$feedback_id;
		}

		if($status_id>0){
			$where['cf.cf_status']=$status_id;
		}
/*
		if($status_id>0){
			$where['cf.cf_status']=$status_id;
		}
*/
		if($bus_id>0){
			$where['cf.busId']=$bus_id ? $bus_id : '';
		}

		if(strlen($search)>0){
			$like['bqa.bus_que_type']=$search;
			$like['frs.name']=$search;
			$like['q.bus_q_que']=$search;
			$like['sl.fname']=$search;
			$like['c.category_name']=$search;
		}
		

		$change_date=$post['change_date'];
          
           if($change_date!=''){
               $custom_date=$this->customDate($post);
               $this->db->where("DATE_FORMAT(cf.cf_time,'%d-%m-%Y') >=", $custom_date['from_date']);
               $this->db->where("DATE_FORMAT(cf.cf_time,'%d-%m-%Y') <=", $custom_date['to_date']);
           }

	$this->db->select("
				cf.*,
				gu.*,
				bqa.*,
				nps.id as nps_main_id,
				nps.cat_id as npscatid,
				nps.sub_cat_id as npssubcatid,
				nps.process_id as npsprocessid,
				nps.busid as npsbusid,
				nps.npscore as npsscore,
				nps.added_time as npsadded_time,
				frs.id as frsid,
				frs.name as frsname,
				frs.status as frsstatus,
				sl.fname,
				sl.lname,
				sl.sub_login_id,
				c.category_name,
				sc.sub_cat_name,
				ssc.sub_sub_cat_name,
				q.bus_q_que
				");
		$this->db->from('customer_feedback as cf');
		$this->db->join('guest_user as gu','cf.cf_user_id=gu.user_id','inner');
		$this->db->join('netpscore as nps','cf.nps_id=nps.id','inner');
		$this->db->join('subscriberlogin as sl','cf.busId=sl.sub_login_id','inner');
		$this->db->join('questionnaire as q','cf.cf_que_id=q.bus_qid','inner');
		$this->db->join('business_questionnaire_answer as bqa','cf.cf_score=bqa.bus_que_ans_id','inner');
		$this->db->join('category as c','cf.cf_cat_id=c.cat_id','inner');
		$this->db->join('sub_category as sc','cf.cf_cat_sub_id=sc.sub_cat_id','inner');
		$this->db->join('sub_sub_category as ssc','cf.cf_cat_process_id=ssc.sub_sub_cat_id','inner');
		$this->db->join('feedback_rating_status frs', 'cf.cf_status=frs.id','inner');
 	
		if(count($where)>0){
			$this->db->where($where);
		}

		if(count($like)>0){
	 		$this->db->group_start();
 			$this->db->like(array('c.category_name'=>$search));
			$this->db->or_like($like);
			$this->db->group_end();	
		}

		if ($post['order'][0]['column']!='' && isset($post['order'][0]['dir']) && !empty($post['order'][0]['dir'])) {
            $order_by = '';
            switch ($post['order'][0]['column']) {
               
				case 0:
                $order_by = 'cf.cf_id';
                break;
			
                default:
                $order_by = 'cf.cf_id';
                break;
            }
             
            $dir_by = '';
            switch ($post['order'][0]['dir']) {
                
                case 'asc':
                    $dir_by = 'asc';
                    break;
                case 'desc':
                    $dir_by = 'desc';
                    break;
                default:
                   $dir_by = 'asc';
                    break;
            }
            $this->db->order_by($order_by,$dir_by); 
        }

        if(!$count){
			$start=$post['start'];
			$length=$post['length'];
			if(!$start){
				$start=0;
			}
			if(!$length){
				$length=10;
			}
			$this->db->limit($length,$start);
		}		
 		
		$result = $this->db->get();
		//echo $this->db->last_query();
   		$resultArray = $result->result_array();	
      	if($count){
			return $result->num_rows();
		}else{
				return $resultArray;
		}
	}

	public function complaintsDetails($where){		
		$this->db->select('cf.*,gu.*,frs.*,q.bus_q_que as bque');
		$this->db->from('customer_feedback cf');
		$this->db->join('guest_user gu', 'cf.cf_user_id=gu.user_id','inner');
		$this->db->join('feedback_rating_status frs', 'cf.cf_status=frs.id','inner');
		$this->db->join('questionnaire as q','cf.cf_que_id=q.bus_qid','inner');
		if(count($where)>0){
			$this->db->where($where);
		}
 		$result=$this->db->get();
		$response=$result->row_array();
 		return $response;
	}

	public function AddUpdateData($table,$save_data){
		if(!empty($table) && count($save_data)>0){
			if($save_data['id']>0){
				$this->db->set('cf_status',$save_data['cf_status']);
				$this->db->where('cf_id',$save_data['id']);
				$this->db->update($table);
				//$this->db->update($table,$save_data['cf_status']);
  				return $save_data['id'];
			}/*else{
				$this->db->insert($table,$save_data);
  				return $this->db->insert_id();
			}*/
		}
	}







	
}