<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
Model : User Model 
*/
class PackageModel extends CI_Model
{
	public function __construct() 
	{
		parent::__construct();
	}
	var $packages = 'packages';
	/*	Insert packages  */
	public function insertPackage($packagesdata)
	{
		return $this->db->insert($this->packages,$packagesdata);
	}
	/*	Fetch packages 	*/
	public function fetchPackage()
	{
		$this->db->select('*'); 
		$this->db->from($this->packages);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
	/*	Fetch packages By Id  */
	public function getPackages($id){
		$this->db->from($this->packages);		
		$this->db->where('package_id',$id);
		$query = $this->db->get();
		if($query){
			return $query->result();
		}
		else{  
			return false;
		}
	}
	/*	Updates packages	*/
	public function updatePackage($packagesData)
	{
		$this->db->where('package_id',$packagesData['id']);
		$this->db->update($this->packages, $packagesData);
		return true;
	}
	/* 	Delete Packages		*/
	public function deletePackage($packageId)
	{
		$this->db->where('package_id', $packageId);
		return $this->db->delete($this->packages); 
	}
//  This function is used to get the Package information
	function getPackage()
	{
		$this->db->select('*');
		$this->db->from($this->packages);
		$query = $this->db->get();
		return $query->result();
	}
	function getPackageInfo($packageId)
	{
		$this->db->select('*');
		$this->db->from($this->packages);
		$this->db->where('package_id', $packageId);
		$query = $this->db->get();        
		return $query->result();
	}
//Edit Package Info By Id 
	function editPackage($packageInfo, $packageId)
	{
		$this->db->where('package_id', $packageId);
		$this->db->update($this->packages, $packageInfo);        
		return TRUE;
	}   
}