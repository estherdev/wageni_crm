<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
Model : User Model 
*/
class AdminLoginModel extends CI_Model
{
  public function __construct() 
  {
    parent::__construct();
  }
  var $admin_login = 'admin_login';
  var $subscriberlogin = 'subscriberlogin';
  /*   front Login    */
  public function checkAdminLogin($loginData)
  { 
    $this->db->select('*');
    $this->db->from($this->admin_login);
    $this->db->where('admin_email', $loginData['admin_email']);
    $this->db->where('admin_pass', $loginData['admin_pass']);        
    $query=$this->db->get();
    if($query->result() == true)
    {
      if($query->num_rows() > 0)
      {
        if($query->result()[0]->admin_isActive=='1')
        {
          return array('status' =>'true', 'response'=>'succeess','message'=>'','data'=>$query->row() );
        }
        else
        {  
          return array('status' =>'false', 'response'=>'inactive','message'=>'Your account is waiting for the admin approval','data'=>'' );
        }
      }
      else
      {
        return array('status' =>'false', 'response'=>'invalid','message'=>'Invalid credentials','data'=>'' );
      }
    }
    else
    {   
      return array('status' =>'false', 'response'=>'invalid','message'=>'Invalid credentials','data'=>'' );
    }      
  }
  public function checkSubscriberLogin($loginData)
  { 
    $this->db->select('*');
    $this->db->from($this->subscriberlogin);
    $this->db->where('sub_email', $loginData['sub_email']);
    $this->db->where('sub_pass', $loginData['sub_pass']);        
    $query=$this->db->get();
    if($query->result() == true)
    {
      if($query->num_rows() > 0)
      {
        if($query->result()[0]->sub_isActive=='1')
        {
          return array('status' =>'true', 'response'=>'succeess','message'=>'','data'=>$query->row() );
        }
        else
        {  
          return array('status' =>'false', 'response'=>'inactive','message'=>'Your account is waiting for the admin approval','data'=>'' );
        }
      }
      else
      {
        return array('status' =>'false', 'response'=>'invalid','message'=>'Invalid credentials','data'=>'' );
      }
    }
    else
    {   
      return array('status' =>'false', 'response'=>'invalid','message'=>'Invalid credentials','data'=>'' );
    }      
  }

  public function ForgotPassword($email)
     {
            $this->db->select('admin_email');
            $this->db->from('admin_login'); 
            $this->db->where('admin_email', $email); 
            $query=$this->db->get();
            return $query->row();
     }

     public function sendpassword($findemail)
    {
      //$email = 'praveen@panindia.in';
      $query1=("SELECT *  from admin_login where admin_email = '".$findemail."' ");
     
      $result= $this->db->query($query1)->row();

      //echo("<pre/>");print_r($result->admin_pass);die;

       $user_email=$result->admin_email;

         if((!strcmp($findemail, $user_email))){
                     $pass=$result->admin_pass; 
                     $name=$result->admin_name;
                     $name=ucfirst($name);
                     //Mail Code

                      $config = Array(
                       'protocol'  => 'smtp',
                       'smtp_host' => 'ssl://smtp.googlemail.com',
                       'smtp_port' => 465,
                       'smtp_user' => 'mastcrew22@gmail.com', 
                       'smtp_pass' => 'crew@1234', 
                       'mailtype'  => 'html',
                       'charset'   => 'iso-8859-1'
                       );
                $mailMsg='<!DOCTYPE html>
                <html>
                <head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Wageni Newsletter</title>
                <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
                </head>
                <body style="background-color: #f1f1f1; font-family: raleway, sans-serif; font-size: 15px; margin: 0; padding: 0;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f1f1f1; font-family: "raleway", sans-serif; font-size: 15px;">
                <tr>
                <td align="center">
                <table border="0" cellpadding="10" cellspacing="0" width="600" align="center">
                <tr>
                <td align="center">
                <table border="0" cellpadding="15" cellspacing="0" width="600" style="background: transparent;">
                <tr>
                <td align="center"><a href=""><img src="http://wageni.us.tempcloudsite.com/globalassets/admin/logo1.png"></a></td>
                </tr>
                </table>
                <table border="0" cellpadding="10" cellspacing="10" width="600" style="background: #fff;">
                <tr>
                <td align="center" style="font-size: 36px; font-weight: bold; color: #555; letter-spacing: 1px;">Welcome to Wageni CRM.</td>
                </tr>
                <tr>
                <td>Dear '.$name.'</td>
                </tr>
                <tr>
                <td>Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>'.$pass.'</b></td>
                </tr>
                <tr><td>Thanks & Regards</td>
                </tr>
                <tr><td><b>Wageni CRM Team</b></td>
                </tr>
                </table>
                <table border="0" cellpadding="15" cellspacing="0" width="600" style="background: transparent;">
                <tr>
                <td align="center">Copyright &copy; 2018 <a href="http://wageni.us.tempcloudsite.com">wagenicrm.com</a>. All rights reserved.</td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                </body>
                </html>';       
                         $this->load->library('email', $config);
                         $this->email->set_newline("\r\n");
                         $this->email->from('srawat@panindia.in'); 
                         $this->email->to($user_email);
                         $this->email->subject('reset_super_admin_password');
                         $this->email->message($mailMsg);
                         if($this->email->send())
                          //echo $this->email->print_debugger();die;
                   {
                  echo 'Email sent.';
                    }
                  }
                     else
                         {
                            $this->session->set_flashdata('success','Email not found try again!');
                            redirect(base_url('admin/AdminLogin/Forgot').'','refresh');
                          }
                

    }

}