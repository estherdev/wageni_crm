<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/*
 Model : Masters Model 
*/

class MastersModel extends CI_Model

{
    public function __construct() 
	{
        parent::__construct();
    }
    //Define Tables Name
	var $homepage_slider = 'homepage_slider';
	var $faq = 'faq';
	var $packages = 'packages';
	var $subscriberlogin = 'subscriberlogin';
	var $sub_mapping = 'subscriberpackagemapping';
	var $subscriberroles = 'subscriberroles';
	var $about_us = 'about_us';
	var $testimonials = 'testimonials';
	var $clients_logo = 'clients_logo';
	var $subscriber_branch = 'subscriber_branch';
	var $category = 'category';
	var $sub_category = 'sub_category';
	var $sub_sub_category = 'sub_sub_category';
	var $history = 'history';
	var $history_content = 'history_content';
	var $questionnaire_masters = 'questionnaire_masters';
	var $questionnaire = 'questionnaire';
	var $business_questionnaire_answer = 'business_questionnaire_answer';

	/*******************************************************   Home Slider Start  ********************************************/ 

	// Check Exists
	public function checkExist($table,$where){
		if(!empty($table) && count($where)>0){		 
			$this->db->where($where);
			$result=$this->db->get($table);
			$total_rows=$result->num_rows();
			if(!$total_rows){
				return true;
			}
			return false;
		}
	}

	
	/*	Insert homepage_slider  */
	public function insertSlider($data)	{
		return $this->db->insert($this->homepage_slider,$data);
	}

	/*	Fetch homepage_slider 	*/
	public function fetchSlider()	{
		$this->db->select('*'); 
		$this->db->from($this->homepage_slider);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}

	/*	Fetch homepage_slider By Id  */
	public function gethomepage_slider($id){
		$this->db->from($this->homepage_slider);		
		$this->db->where('slider_id',$id);
		$query = $this->db->get();
		if($query){
			return $query->result();
		}else{  
			return false;
		}
	}

	/*	Updates homepage_slider	*/
	public function updateSlider($homepage_sliderData)	{

		$this->db->where('slider_id',$homepage_sliderData['slider_id']);
	    $this->db->update($this->homepage_slider, $homepage_sliderData);
	    return true;
	}

	/* 	Delete homepage_slider		*/
	public function deleteSlider($id)	{
		$this->db->where('slider_id', $id);
		return $this->db->delete($this->homepage_slider); 
	}

	/*******************************************************   Blog Start  ********************************************/ 
		/* Insert the Blog*/
	public function blogInsert($data)	{
		return $this->db->insert('blog', $data);
	}

	/* get the number of records */
	 public function record_count()     {
       return $this->db->count_all("blog");
	 }
	
		/* fetch the blog for pagination */
	 public function fetch_blog($limit, $start)	 {
       $this->db->limit($limit, $start); 
       $query = $this->db->get("blog"); 
       if ($query->num_rows() > 0)  { 
          foreach ($query->result() as $row) 
		   { 
              $data[] = $row; 
           }
           return $data;
        }
       return false;
     }  

     /* Fetch sideblog with limit */
	public function fetchBlogWithLimit()	{	
		$this->db->select('*');
		$this->db->from('blog');
		$this->db->order_by('id','DESC');
		$this->db->limit(10);
        $query = $this->db->get();
        return $query->result();
	}

	/* fetch blog */
	public function fetchBlog()	{
		$this->db->select('*');
		$this->db->order_by('id','DESC');
        $query = $this->db->get('blog');
        return $query->result();
	}

	/* fetch Blog by Id */
	public function getblogdatabyid($id)	{
		$this->db->select("*");
		$this->db->from('blog');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	/* Update Blog */
	public function updateblogdata($data,$id)	{
		$this->db->where('id',$id);		
		return $this->db->update('blog',$data);		 
	}

	/* Delete Blog */
	public function deleteBlog($id)	{
		$this->db->where('id',$id);
		return $this->db->delete('blog');
	}

 	/*********************************************** Testimonials Start ********************************************/

 	/* Insert the Testimonials*/
 	public function testInsert($data) {
 		return $this->db->insert($this->testimonials,$data);
 	}

 	/*Fetch number of records */
	public function record_test(){ 
       return $this->db->count_all($this->testimonials);
	}
		
	/* Fetch testimonials for  pagination */
	public function fetch_test($limit, $start){
       $this->db->limit($limit, $start); 	   
       $query = $this->db->get($this->testimonials); 	   
       if ($query->num_rows() > 0){ 
           foreach ($query->result() as $row){ 
               $data[] = $row; 
           }
           return $data;
       }
       return false;
    }  

 	/*Fetch Testimonials*/
 	public function getTest() 	{
 		$this->db->select('*');
 		$this->db->order_by('id','DESC');
 		$query = $this->db->get($this->testimonials);
 		if ($query==true) {
 		return $query->result();	
 		}else{
 			return false;
 		}
 	}

 	/*Fetch Testimonials By Id*/
 	public function getTestDataById($id) 	{
 		$this->db->select('*');
 		$this->db->from($this->testimonials);
 		$this->db->where('id',$id);
 		$query = $this->db->get();
 		return $query->result_array();
 	}

 	/*Testimonials Updated*/
 	public function testUpdate($id,$data) 	{
 		$this->db->where('id',$id); 
 		return $this->db->update($this->testimonials,$data);
 	}

 	/*Testimonials Deleted*/
 	public function deleteTesti($id) 	{
 		$this->db->where('id', $id);
 		return $this->db->delete($this->testimonials);
 	}

 	/*********************************************** About Start ********************************************/
 	/*About Insert*/
 	public function aboutUpdate($data,$a) 	{	
 	//echo "/<pre>";print_r($data);die;
 		$this->db->where('id',$a);
 		return $this->db->update($this->about_us,$data);
 	}

 	/* Fetch About */
 	public function getAbout() 	{
 		$this->db->select('*');
 		$query = $this->db->get($this->about_us);
 		if ($query== true) {
 			return $query->result_array();
 		}else{
 			return false;
 		}		
 	}

    /*********************************************** Contact Start ***************************************/
    	
    /***** Insert the Contact ****/   	
    public function insertContact($data)	{
		return $this->db->insert('contactdetail', $data);
	}

   /****  Fetch the Contact ****/
	public function getContact()	{
		$this->db->select('*');
        $query = $this->db->get('contactdetail');
        return $query->result();
	}

	/****  Fetch the Contact By Id ****/
	public function getContactById($id)	{
		$this->db->select("*");
		$this->db->from('contactdetail');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

		/**** Update the Contact  ****/
	public function updateContact($data,$id)	{
		$this->db->where('id',$id);
		return $this->db->update('contactdetail',$data);
	}

		/**** Delete the Contact ****/
	public function deleteContact($id)	{
		$this->db->where('id',$id);
		return $this->db->delete('contactdetail');
	}

    /*********************************************** Faq Start ********************************************/
   

    /* Faq Insert */
    public function faqInsert($data)	{
		return $this->db->insert($this->faq, $data);
	}

	/*	Fetch Homepage FAQ 	*/
	public function getdata()	{

		$this->db->select('*'); 
		$this->db->from($this->faq);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}

	/* Fetch FAQ by Id*/
    public  function getdatabyid($id)	{
		$this->db->select("*");
		$this->db->from($this->faq);
		$this->db->where('faq_id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

		/* Upadte FAQ */
	public function faqUpdate($data,$id)	{
		$this->db->where('faq_id',$id); 
  		return $this->db->update($this->faq,$data);
	}

	  /* Delete FAQ */
	public function deleteFaq($id)    {
    	$this->db->where('faq_id', $id);
  		return $this->db->delete($this->faq);
    }

    /************************************************** ClientLogo Start *************************************** */

    	/* Insert Client Logo*/
    public function logoInsert($data)	{
		return $this->db->insert($this->clients_logo, $data);
	}

	/*	Fetch Homepage Client Logo 	*/
	public function FetchClientLogo()	{
		$this->db->select('*');
		$query = $this->db->get($this->clients_logo);
		return $query->result_array();
	}

		/* Fetch Client Logo by Id*/
	public function getClientLogoByid($id)	{
		$this->db->select('*');
		$this->db->from($this->clients_logo);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

		/* Update Client Logo */
	public function updateClientLogo($data,$id)	{
		$this->db->where('id',$id);
		return $this->db->update($this->clients_logo,$data);
	}

	   /* Delete client Logo */
	public function deleteClientLogo($id)	{
		$this->db->where('id',$id);
		return $this->db->delete($this->clients_logo);
	}

    /************************************************** Start Branch  *****************************************/

      /*	Insert Branch  */
	public function insertBranch($data)	{
		return $this->db->insert($this->subscriber_branch,$data);
	}
	/*	Fetch Branch 	*/
	public function fetchSubPackage()	{
		$this->db->select('spm.*,p.*'); 
		$this->db->from('subscriberpackagemapping as spm');
		$this->db->join('packages as p' ,'spm.subscriber_package_id=p.package_id','left' );
		$this->db->where('subscriber_id', $this->session->userdata('sub_login_id'));
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}
	/*	Fetch Branch Limit	*/
	public function check_sub_branch_limit($branchlimit)	{
		$this->db->select('*'); 
		$this->db->from($this->subscriber_branch);
		$this->db->where('subs_id', $this->session->userdata('sub_login_id'));
		$query = $this->db->get();
		if($query->num_rows() >= $branchlimit)  {
  			return $query->row();
  		}else{  
  			return false;
  		}
	}
	
	/*	Fetch All Branch */
	public function fetchBranchById(){
		$this->db->from($this->subscriber_branch);		
		$this->db->where('subs_id',$this->session->userdata('sub_login_id'));
		$query = $this->db->get();
		$result=  $query->result();
		if($result){
			return $result;
		}else{  
			return array();
		}
	}

	/* Fetch Branch Data By Id */
	public function getBranchById($id)		{
			$this->db->select("*");
			$this->db->from($this->subscriber_branch);
			$this->db->where('sub_branch_id',$id);
			$query = $this->db->get();
			return $query->result();
			//echo("<pre/>");print_r($result);die;
		}

			/* Update The Branch Data */
	public function updateBranch($data,$id)		{
			$this->db->where('sub_branch_id',$id);		
			return $this->db->update($this->subscriber_branch,$data);		 
		}

	//Delete Branch
	public function deleteBranch($id)	{
		$this->db->where('sub_branch_id', $id);
		return $this->db->delete($this->subscriber_branch); 
	}

	public function totalBranch()	{	
		$this->db->from($this->subscriber_branch);
		$this->db->where('subs_id', $this->session->userdata('sub_login_id'));
		return $this->db->count_all_results();
	}

    /****************************************************   Start Package 	***************************************/
   
    /*	Insert packages  */
	public function insertPackage($packagesdata)	{
		return $this->db->insert($this->packages,$packagesdata);
	}
	/*	Fetch packages 	*/
	public function fetchPackage()	{
		$this->db->select('*'); 
		$this->db->from($this->packages);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}	else{
			return false;
		}
	}
	/*	Fetch packages By Id  */
	public function fetchPackageId($id){
		$this->db->from($this->packages);		
		$this->db->where('package_id',$id);
		$query = $this->db->get();
		if($query){
			return $query->result();
		}	else{  
			return false;
		}
	}
	/*	Updates packages	*/
	public function updatePackages($data)	{
		$this->db->where('package_id',$data['package_id']);
		$this->db->update($this->packages, $data);
		return true;
	}
	/* 	Delete Packages		*/
	public function deletePackages($id)	{
		$this->db->where('package_id', $id);
		return $this->db->delete($this->packages); 
	}
    /* End Package 	*/

    /* Fetch All Subscriber for Add company */
    public function fetchCompanyUser()    {
    	$this->db->select('*'); 
		$this->db->from($this->subscriberlogin);
		$this->db->where('added_by','0');
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}	else{
			return false;
		}
    }


    /****************************************************   Start Company 	***************************************/
   
    /*	Insert Company  */
	public function insertCompany($data)	{
		return $this->db->insert($this->sub_mapping,$data);
	}
    /*	Fetch Company  */
	public function fetchCompany()	{
		$this->db->select('spm.*,sl.sub_name,p.package_name,p.package_price'); 
		$this->db->from('subscriberpackagemapping as spm');
		$this->db->join('subscriberlogin as sl','spm.subscriber_id=sl.sub_login_id','left' );
		$this->db->join('packages as p' ,'spm.subscriber_package_id=p.package_id','left' );
		//$this->db->where('sl.added_by','0');
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}	else{
			return false;
		}
	}
    /*	Fetch New Company  */
	public function fetchNewCompany()	{
		$this->db->select('sl.*,role.subscriber_role_name as role'); ;
		$this->db->from('subscriberlogin as sl');
		$this->db->join('subscriberroles as role' ,'sl.sub_login_role=role.subscriber_roll_id','left' );
		$this->db->where('added_by',0);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}	else{
			return false;
		}
	}

	 /*	Insert packages  */
	public function masterinsert($table,$arraydata)	{
		 $this->db->insert($table,$arraydata);
	}


	/*	Change Status 	*/
	public function statusCompany($Id,$NewStatus)	{		
		$this->db->where('sub_login_id',$Id);
		$this->db->set('sub_isActive',$NewStatus);
		return $this->db->update('subscriberlogin');
	}


	/*	Change Status 	*/
	public function statusSurveyCompany($Id,$Category)	{		
		$this->db->where('sub_login_id',$Id);
		$this->db->set('survey_que_status',1);
		$query=$this->db->update('subscriberlogin');

		if($query){
		$this->db->select('qm.*');
        $this->db->from('questionnaire_masters as qm');
        $this->db->where('qm.q_cat_id',$Category);
        
        $query = $this->db->get();
         $allquestions = $query->result();
        if ($allquestions== true) {        
           
            $response = "";
            foreach ($allquestions as $key => $values) {
                    $qid = $values->qid;
                    $qname = $values->q_que;
                    $q_type = $values->q_type;
                    $sub_cat = $values->q_sub_cat_id;
                    $cat_process = $values->cat_process;
                    $this->db->select('ans.*');
                    $this->db->order_by('ans.que_id','DESC');
                    $this->db->from('questionnaire_answer as ans');
                    $this->db->where_in('ans.que_id',$qid);
                    $query = $this->db->get();
                    $result=$query->result();
                    if ($result== true) {
                        $response[$key][] = $qid;
                        $response[$key][] = $qname;
                        $response[$key][] = $q_type;
                        $response[$key][] = $query->result();
                        $response[$key][] = $sub_cat;
                        $response[$key][] = $cat_process;
                        
                    }else{
                        return false;
                    }
                }
                //prd($response);
                return $response;            
	        }else{
	            return false;
	        }
		}
		else{
			return false;
		}
	}
	public function CheckQuestionStatusCompany($Id)    {  	
       $this->db->select('survey_que_status');
       $this->db->where('sub_login_id',$Id);
       //$this->db->where('survey_que_status','0');
		$query = $this->db->get($this->subscriberlogin);
		if ($query== true) {
			return $query->row()->survey_que_status=='0';
		}else{
			return false;
		}
    }


	
	/*********************************************** History Start ********************************************/	
	public function historyUpdate($data,$a)	{		
		/* History Update*/
		$this->db->where('hc_id',$a);
		return $this->db->update($this->history_content,$data);
	}

	/*Fetch the History*/
	public function getHistoryData()	{
		$this->db->select('*');
		$query = $this->db->get($this->history_content);
		if ($query== true) {
			return $query->result_array();
		}else{
			return false;
		}
	}

	public function getHistoryDataId()	{
		$this->db->select('*');
		$this->db->from($this->history_content);
		$query = $this->db->get();
		if ($query== true) {
			return $query->result_array();
		}else{
			return false;
		}
	}

	/* Insert the History */
	public function historyInsert($data)	{
		 $this->db->insert($this->history,$data);
		if($this->db->affected_rows() > 0) {
	          return true;
	      }else{ 	         
	          die($this->db->last_query());
	      }
	}

	/*Fetch Data*/
	public function getHistory()	{
		$this->db->select('*');
		$query = $this->db->get($this->history);
		if ($query== true) {
			return $query->result();
		}else{
			return false;
		}
	}

	/* Fetch History Data By id*/
	public function getHistoryId($id)	{
		$this->db->select('*');
		$this->db->from($this->history);
		$this->db->where('h_id',$id);
		$query = $this->db->get();
		if ($query== true) {
			return $query->result_array();
		}else{
			return false;
		}
	}

	/* Update the History */
	public function updateHistory($data,$id)	{
		$this->db->where('h_id',$id);	
		return $this->db->update($this->history,$data);
	}


	/* Delete the History */
	public function deleteHistory($id)	{
		$this->db->where('h_id',$id);
		return $this->db->delete($this->history);
	
	}

	/*********************************************** Category Start ********************************************/
		/* Insert  Category */
	public function insertCategory($data)	{
		$this->db->insert($this->category,$data);
		if($this->db->affected_rows() > 0) {
	          return true;
	      }
	      else{ 	         
	          die($this->db->last_query());
	      }
	}	
	//Fetch All 
	public function fetchCategory()	{
		$this->db->select('*');
		$this->db->order_by('cat_id','DESC');
		$query = $this->db->get($this->category);
		if ($query== true) {
			return $query->result();
		}else{
			return false;
		}
	}	
	//Fetch By Id
	public function fetchCategoryById($id)	{
		$this->db->select('*');
		$this->db->order_by('cat_id','ASC');
		$this->db->where('cat_id',$id);
		$query = $this->db->get($this->category);
		if ($query== true) {
			return $query->result();
		}else{
			return false;
		}
	}		
	/* Update the History */
	public function updateCategory($data)	{
		$this->db->where('cat_id',$data['cat_id']);	
		$this->db->set('category_name',$data['category_name']);	
		return $this->db->update($this->category);
	}
	public function deleteCategory($id)	{
		$this->db->where('cat_id',$id);
		return $this->db->delete($this->category);
		
	}
	/*********************************************** Sub Category Start ********************************************/
		/* Insert  Category */
	public function insertSubCategory($data)	{
		$this->db->insert($this->sub_category,$data);
		if($this->db->affected_rows() > 0) {
	          return true;
	      }
	      else{ 	         
	          die($this->db->last_query());
	      }
	}	
	//Fetch All 
	public function fetchSubCategory()	{
		$this->db->select('scat.*,cat.category_name');
		$this->db->order_by('sub_cat_id','DESC');
		$this->db->from('sub_category as scat');
		$this->db->join('category as cat','scat.main_cat_id=cat.cat_id','left');
		$query = $this->db->get();
		if ($query== true) {
			return $query->result();
		}else{
			return false;
		}
	}	
	//Fetch By Id
	public function fetchSubCategoryById($id)	{
		$this->db->select('*');
		$this->db->order_by('sub_cat_id','DESC');
		$this->db->where('sub_cat_id',$id);
		$query = $this->db->get($this->sub_category);
		if ($query== true) {
			return $query->result();
		}else{
			return false;
		}
	}		
	/* Update the History */
	public function updateSubCategory($data)	{
		$this->db->set('main_cat_id',$data['main_cat_id']);	
		$this->db->set('sub_cat_name',$data['sub_cat_name']);	
		$this->db->where('sub_cat_id',$data['sub_cat_id']);	
		return $this->db->update($this->sub_category);
	}
	public function deleteSubCategory($id)
	{
		$this->db->where('sub_cat_id',$id);
		return $this->db->delete($this->sub_category);
		
	}

	/***********************************************  Category Process ********************************************/
	
		/* Insert The Category Process */
	public function insertCategoryProcess($data)	{
		return $this->db->insert($this->sub_sub_category,$data);
	}

		/* Fetch All Category Process */
	public function fetchCategoryProcess()	{
		$this->db->select('scatp.*,cat.category_name,scat.sub_cat_name');
		$this->db->order_by('sub_sub_cat_id','DESC');
		$this->db->from('sub_sub_category as scatp');
		$this->db->join('category as cat','scatp.main_cat_id=cat.cat_id','left');
		$this->db->join('sub_category as scat','scatp.sub_category_id=scat.sub_cat_id','left');
		$query = $this->db->get();
		if ($query== true) {
			return $query->result();
		}else{
			return false;
		}
	}

	/* Fetch Category Process By Id */
	public function fetchCatProcessById($id)	{
		$this->db->select('scatp.*,cat.*,scat.*');
		$this->db->order_by('sub_sub_cat_id','DESC');
		$this->db->from('sub_sub_category as scatp');
		$this->db->where('sub_sub_cat_id',$id);
		$this->db->join('category as cat','scatp.main_cat_id=cat.cat_id','left');
		$this->db->join('sub_category as scat','scatp.sub_category_id=scat.sub_cat_id','left');
		$query = $this->db->get();
		return $query->result_array();
	}	

		/* Update Category Process */
	public function updateCategoryProcess($data,$id)	{
		$this->db->where('sub_sub_cat_id',$id);		
		return $this->db->update('sub_sub_category',$data);	
	}

		/* Delete Category Process */
	public function deleteCategoryProcess($id)	{
		$this->db->where('sub_sub_cat_id',$id);
		return $this->db->delete($this->sub_sub_category);
		
	}

	/*********************************************** Questionnaire Start ********************************************/
	//For Super Admin
	public function inserQuestionnaire($data)	{
		$this->db->insert($this->questionnaire_masters,$data);
		$last_insert_que_id = $this->db->insert_id();
		if($last_insert_que_id== true){
			
			$a = array('qtype' =>$this->input->post($data['q_type']),'que_score'=>$this->input->post('q_score[]') );		
			//$a=	$queOptions['qtype'];
			//print_r($que_score=	$queOptions['que_score']);
			//print_r($a);

		$questionnaire_answer=array();

			for($j=0; $j<count($a['qtype']); $j++) {

       	 		  $qtype = $a['qtype'][$j]; 
       	 		  $que_score = $a['que_score'][$j]; 

       	 		 /* $questionnaire_answer = array(
						'que_id' => $last_insert_que_id,
						'que_type'=> $qtype,
						'que_score'=> $que_score
					);*/

       	 		// $result_que_answer =$this->db->insert('questionnaire_answer',$questionnaire_answer);


       	 		$que_answer = "INSERT INTO questionnaire_answer(que_id,que_type,que_score) VALUES ('".$last_insert_que_id."','".$qtype."','".$que_score."')";
				$result_que_answer= $this->db->query($que_answer);	
       		
       		}	

       		//print_r($questionnaire_answer); die();
       		return $result_que_answer;	
		}else{
			return false;
		}
	}


		


	//For Admin
	public function inserQuestionnaireForAdmin($data)	{		
		$this->db->insert($this->questionnaire,$data);

		$last_insert_que_id = $this->db->insert_id();
		if($last_insert_que_id>0){
				$qmc=$this->input->post('qmc');
				$questionnaire_answer=array();
				if(count($qmc)>0){
					foreach ($qmc as $key => $value) {

						$questionnaire_answer= array(
							'bus_que_id' =>$last_insert_que_id,
							'bus_que_score'=>$this->input->post('q_score')[$key],
							'businessId'=>$this->session->userdata('sub_login_id'),
							'bus_que_type'=>$value
						);
						$result_que_answer= $this->db->insert($this->business_questionnaire_answer,$questionnaire_answer);
					}
				}
       		return $result_que_answer;	
		}else{
			return false;
		}
	}

	// Get all Questionnaire for Sub Admin
	public function getSubQuestionnaireById($id)	{
		$this->db->select("*");
		$this->db->from('questionnaire');
		$this->db->where('bus_qid',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	// Get Answer by id For Sub Admin
	public function getSubAnswerById($id)	{
		$this->db->select("*");
		$this->db->from('business_questionnaire_answer');
		$this->db->where('bus_que_id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	// Update The Questionnaire For Sub Admin
	public function updateSubQuestionnaire($data,$id)	{	
		$this->db->where('bus_qid',$id); 
		$res = $this->db->update('questionnaire',$data);

		if ($res == true) {
			$this->db->select("*");
			$this->db->from('business_questionnaire_answer');
			$this->db->where('bus_que_id',$id);
			$query1 = $this->db->get();
			$Resultquery1 =  $query1->result();
			if ($Resultquery1== false)	{
				return $res;
			}else{
				$this->db->where("bus_que_id", $id);  
				$query = $this->db->delete("business_questionnaire_answer"); 
				if ($query== true){
					$a = array('fieldname'=>$this->input->post('field_name[]'),
								'que_score' =>$this->input->post('q_score[]'));		
					for($j=0; $j<count($a['fieldname']); $j++) {
						$fieldname = $a['fieldname'][$j];
						$que_score = $a['que_score'][$j];

						$que_answer = "INSERT INTO business_questionnaire_answer(bus_que_id,bus_que_type,bus_que_score) VALUES ('".$id."','".$fieldname."','".$que_score."')";
						$result_que_answer= $this->db->query($que_answer);	

					}	
					return $result_que_answer;	
				} else {
					return false;
				}
			}	          
		}else{
			return false;
		}	
	}

	// Delete The Questionnaire For Sub Admin
	public function deleteSubQuestionnaire($bus_qid){
			$this->db->where("bus_qid", $bus_qid);  
			$this->db->delete("questionnaire"); 
			$this->db->where("bus_que_id", $bus_qid);  
			$this->db->delete("business_questionnaire_answer"); 
			return true;
		}

	public function fetchQuestionnaire($categoryid)	{
		$this->db->select('qm.*');
		$this->db->from('questionnaire_masters as qm');
		$this->db->where('qm.q_cat_id',$categoryid);
		//$this->db->where('qm.q_sub_cat_id',$subcategoryid);
		//echo $this->db->last_query(); die;
		$query = $this->db->get();
		if ($query== true) {
			return $query->result();
		}else{
			return false;
		}
	}

	public function fetchAnswer($qid)	{
		$this->db->select('ans.*')
		->from('questionnaire_answer as ans')
		->where_in('ans.que_id',$qid);
		//echo $this->db->last_query(); die;
		$query = $this->db->get();
		if ($query== true) {
			return $query->result();
		}else{
			return false;
		}
	}

	function getQue($categoryid,$subcategoryid,$cat_process){
		$this->db->select('qm.*');
		$this->db->from('questionnaire_masters as qm');
		$this->db->where('qm.q_cat_id',$categoryid);
		$this->db->where('qm.q_sub_cat_id',$subcategoryid);
		$this->db->where('qm.cat_process',$cat_process);
		
		$query = $this->db->get();
		if ($query== true) {		
			$allquestions = $query->result();
			$response = "";
			foreach ($allquestions as $key => $values)
        		{
        			$qid = $values->qid;
        			$qname = $values->q_que;
					$q_type = $values->q_type;
					$cat_process = $values->cat_process;
					$this->db->select('ans.*');
					$this->db->order_by('ans.que_id','DESC');
					$this->db->from('questionnaire_answer as ans');
					$this->db->where_in('ans.que_id',$qid);
					$query = $this->db->get();
					if ($query== true) {
						$response[$key][] = $qid;
						$response[$key][] = $qname;
						$response[$key][] = $q_type;
						$response[$key][] = $query->result();
						$response[$key][] = $cat_process;
						
					}else{
						return false;
					}
        		}
        		return $response;			 
        		//echo $this->db->last_query();
		}else{
			return false;
		}
	}

	//Get Question And Answer By Id For Update For Super Admin
	public function getQuestionnaireById($id)	{
		/*$this->db->select('questionnaire_masters.q_que,questionnaire_answer.que_type');
		$this->db->from('questionnaire_masters');
		$this->db->join('questionnaire_answer', 'questionnaire_answer.que_id = questionnaire_masters.qid');
		$this->db->where('questionnaire_answer.que_id',$id);
		$query = $this->db->get();
		return $query->result_array();*/
		$this->db->select("*");
		$this->db->from('questionnaire_masters');
		$this->db->where('qid',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	// Get Answer by id For Super Admin
	public function getAnswerById($id)	{
		$this->db->select("*");
		$this->db->from('questionnaire_answer');
		$this->db->where('que_id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	// Update The Questionnaire For Admin
	public function updateQuestionnaire($data,$id)	{	
		
	$this->db->where('qid',$id); 
	$res = $this->db->update('questionnaire_masters',$data);
		if ($res == true) {
		$this->db->select("*");
		$this->db->from('questionnaire_answer');
		$this->db->where('que_id',$id);
		$query1 = $this->db->get();
		$Resultquery1 =  $query1->result();
			if ($Resultquery1== false){
				return $res;
			}else{
	          $this->db->where("que_id", $id);  
	          $query = $this->db->delete("questionnaire_answer");
	          if ($query== true){
	          	$a = array('fieldname'=>$this->input->post('field_name[]'),	'que_score'=>$this->input->post('q_score[]'));		
		       	   	 for($j=0; $j<count($a['fieldname']); $j++) {
		       	 		$fieldname = $a['fieldname'][$j]; 
		       	 	    $que_score = $a['que_score'][$j]; 
		       	 		$que_answer = "INSERT INTO questionnaire_answer(que_id,que_type,que_score) VALUES ('".$id."','".$fieldname."','".$que_score."')";
						$result_que_answer= $this->db->query($que_answer);	       		
		       		}	
       				return $result_que_answer;	
 	          } else {
	         	return false;
	          }
	        }
 		} else {
			return false;
		}	
	}

	// Delete The Questionnaire For Super Admin
	public function deleteQuestionnaire($qid){
		$this->db->where("qid", $qid);  
		$this->db->delete("questionnaire_masters"); 
		$this->db->where("que_id", $qid);  
		$this->db->delete("questionnaire_answer"); 
		return true;
	}

	function getQueByAdmin($categoryid,$subcategoryid,$cat_process){
		$this->db->select('qm.*');
		$this->db->from('questionnaire as qm');
		$this->db->where('qm.bus_q_cat_id',$categoryid);
		$this->db->where('qm.bus_q_sub_cat_id',$subcategoryid);
		$this->db->where('qm.bus_cat_process',$cat_process);
		$this->db->where('qm.businessId',$this->session->userdata('sub_login_id'));
		
		$query = $this->db->get();
		if ($query== true) {		
			$allquestions = $query->result();
			$response = "";
			foreach ($allquestions as $key => $values){
        			$qid = $values->bus_qid;
        			$qname = $values->bus_q_que;
					$q_type = $values->bus_q_type;
					$cat_process = $values->bus_cat_process;
					$this->db->select('ans.*');
					$this->db->order_by('ans.bus_que_id','DESC');
					$this->db->from('business_questionnaire_answer as ans');
					$this->db->where_in('ans.bus_que_id',$qid);
					$query = $this->db->get();
					if ($query== true) {
						$response[$key][] = $qid;
						$response[$key][] = $qname;
						$response[$key][] = $q_type;
						$response[$key][] = $query->result();
						$response[$key][] = $cat_process;
						
					}else{
						return false;
					}
        		}
        		return $response;
		}else{
			return false;
		}
	}

	public function mailstatuswisequesnew($categoryid,$subcategoryid,$cat_process,$id,$user_id){
		$this->db->select('qm.*');
		$this->db->from('questionnaire as qm');
		$this->db->where('qm.bus_q_cat_id',$categoryid);
		$this->db->where('qm.bus_q_sub_cat_id',$subcategoryid);
		$this->db->where('qm.bus_cat_process',$cat_process);
		$this->db->where('qm.businessId',$id);
		
		$query = $this->db->get();
		$allquestions = $query->result_array();

		if ($allquestions) {		
			
			$response = "";
			foreach ($allquestions as $key => $values)
        		{
					//echo "<pre/>"; print_r($values); die;

        			$qid 	= $values['bus_qid'];
        			$qname 	= $values['bus_q_que'];
					$q_type = $values['bus_q_type'];
					//$cat_process = $values['bus_cat_process'];
					$sub_cat_id = $values['bus_q_sub_cat_id'];
					$this->db->select('ans.*');
					$this->db->order_by('ans.bus_que_id','DESC');
					$this->db->from('business_questionnaire_answer as ans');
					$this->db->where_in('ans.bus_que_id',$qid);
					$query = $this->db->get();


					$answerresult=$query->result_array();
 						if ($answerresult > 0 ) {
							$response[$key]['owner_id'] 	= $id;
							$response[$key]['user_id'] 		= $user_id;
							$response[$key]['cat_process'] 	= $cat_process;
							$response[$key]['sub_cat_id'] 	= $subcategoryid;
							$response[$key]['qid'] 			= $values['bus_qid'];
							$response[$key]['cat_id'] 		= $values['bus_q_cat_id'];
							$response[$key]['que'] 			= $values['bus_q_que']; 
							$response[$key]['q_type'] 		= $values['bus_q_type'];
							$response[$key]['allresult'] 	= $answerresult;							
						}else{
						return array();
					}
        		}
        		return $response;			 
        		//echo $this->db->last_query();
		}else{
			return array();
		}
	}


	/*********************************************** sub sub category drop down start *******************************************/
    public function fetch_category()	{
		$this->db->order_by("category_name", "ASC");
		$query = $this->db->get("category");
		return $query->result();
	}

    public function fetch_subcategory($main_cat_id)	{
		$this->db->where('main_cat_id', $main_cat_id);
		$this->db->order_by('sub_cat_name', 'ASC');
		$query = $this->db->get('sub_category');
		$output = '<option value="">Select Branch</option>';
		foreach($query->result() as $row){
			$output .= '<option value="'.$row->sub_cat_id.'">'.$row->sub_cat_name.'</option>';
		}
		return $output;
	}

    public function fetch_subsubcat($sub_cat_id)     {
        $this->db->where('sub_category_id', $sub_cat_id);
        $this->db->order_by('sub_sub_cat_name', 'ASC');
        $query = $this->db->get('sub_sub_category');
        $output = '<option value="none">Select Process</option>';
        foreach($query->result() as $row){
          $output .= '<option value="'.$row->sub_sub_cat_id.'">'.$row->sub_sub_cat_name.'</option>';
        }
        return $output;
    }

   /*********************************************** Practice Area Start *********************************************************/

		/* Insert the Practice Area */
	public function PracticeInsert($data)	{
		return $this->db->insert('practice_area', $data);
	}

		/* Fetch The Practice Area */
	public function fetchPractice()	{
		$this->db->select('*');
        $query = $this->db->get('practice_area');
        return $query->result();
	}

		/*Fetch Practice Area By Id */
	public function getPracticedatabyid($id)	{
		$this->db->select("*");
		$this->db->from('practice_area');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

		/* Update the Practice Area */
	public function updatePracticeArea($data,$id)	{
		$this->db->where('id',$id);		
		return $this->db->update('practice_area',$data);		 
	}

		/* Delete the Practice Area */
	public function deletePrcatice($id)	{
		$this->db->where('id',$id);
		return $this->db->delete('practice_area');
	}

   /****************************************** Practice Area Start ************************************************************/

   /************************************************ Team Start ***************************************************************/
		
		 /* Team Inserted */
	public function teamInsert($data)	{
		return $this->db->insert('team', $data);
	}

		/* Fetch The Team */
	public function fetchTeam()	{
		$this->db->select('*');
        $query = $this->db->get('team');
        return $query->result();
	}

		/* Fetch Team data by id */
	public function getTeamdatabyid($id)	{
		$this->db->select("*");
		$this->db->from('team');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

		/* Update the Team Data */
	public function updateTeam($data,$id)	{
		$this->db->where('id',$id);		
		return $this->db->update('team',$data);		 
	}
		/* Delete the Team Data */
	public function deleteTeam($id)	{
		$this->db->where('id',$id);
		return $this->db->delete('team');
	}
	/* Update the Team Data */
	public function updatePreStaySurvey($email)	{
		$this->db->set('pre_stay',1);	
		$this->db->where('user_email_id',$email);
		return $this->db->update('guest_user');		 
	}



	public function getSurveyQueList($where)	{

		/*if(count($where)>0){
			$this->db->where($where);
			}*/
		//$email='praveen@panindia.in';
		$this->db->select("gu.*");
		$this->db->from('guest_user as gu');
  		if(count($where)>0){
		$this->db->where($where);
		}
		//$this->db->where('gu.pre_stay',0);
		$query = $this->db->get();
 		return ($query->num_rows() > 0)?$query->row_array():array();	

 	/*	if ($mailrespose >0) {	


 			//return $mailrespose;



			$owner_id=$mailrespose['owner_id'];		 
			$user_id=$mailrespose['user_id'];		 
			$bus_cat_process=$mailrespose['process'];		 
			$this->db->select('q.*');
			$this->db->from('questionnaire as q');
			//$this->db->where('q.bus_q_cat_id',$categoryid);
			//$this->db->where('q.bus_q_sub_cat_id',$subcategoryid);
			//$this->db->where('q.bus_cat_process',$bus_cat_process);
			$this->db->where('q.businessId',$owner_id);

			$query = $this->db->get();
			$allquestions = $query->result_array();

				if ($allquestions>0) {
					$response = "";
					foreach ($allquestions as $key => $values) {
 						$this->db->select('ans.*');
						$this->db->from('business_questionnaire_answer as ans');
						$this->db->where_in('ans.bus_que_id',$values['bus_qid']);
						//$this->db->order_by('ans.bus_que_id','DESC');
						$query = $this->db->get();
						$answerresult=$query->result_array();
							//echo "<pre/>"; print_r($answerresult); die();
						if ($answerresult > 0 ) {
							$response[$key]['owner_id'] = $owner_id;
							$response[$key]['user_id'] = $user_id;
							$response[$key]['qid'] = $values['bus_qid'];
							$response[$key]['cat_id'] = $values['bus_q_cat_id'];
							$response[$key]['que'] = $values['bus_q_que']; 
							$response[$key]['q_type'] = $values['bus_q_type'];
							$response[$key]['allresult'] = $answerresult;							
						}
	        		}
 	        		return $response;
		        }else{
					return array();
				}
		}else{
			return array();
		}*/		
	}


	public function mailstatuswiseques($where)
	{
		if(count($where)>0){
			$this->db->where($where);
		}			 		 
			$this->db->select('q.*');
			$this->db->from('questionnaire as q');	
			$query = $this->db->get();
			$allquestions = $query->result_array();
				if ($allquestions>0) {
					$response = "";
					foreach ($allquestions as $key => $values) {
 						$this->db->select('ans.*');
						$this->db->from('business_questionnaire_answer as ans');
						$this->db->where_in('ans.bus_que_id',$values['bus_qid']);
						//$this->db->order_by('ans.bus_que_id','DESC');
						$query = $this->db->get();
						$answerresult=$query->result_array();
							//echo "<pre/>"; print_r($answerresult); die();
						if ($answerresult > 0 ) {
							//$response[$key]['owner_id'] = $owner_id;
							//$response[$key]['user_id'] = $user_id;
							$response[$key]['qid'] = $values['bus_qid'];
							$response[$key]['cat_id'] = $values['bus_q_cat_id'];
							$response[$key]['que'] = $values['bus_q_que']; 
							$response[$key]['q_type'] = $values['bus_q_type'];
							$response[$key]['allresult'] = $answerresult;							
						}
	        		}
 	        		return $response;
		        }else{
					return array();
				}		
	}



	public function mailstatuswisequesproccess($where){
		$this->db->select('q.*'); 
 		$this->db->from('sub_sub_category as q');
 		if(count($where)>0){
			$this->db->where($where);
		}
		$query = $this->db->get();	
 		$result = $query->row_array();	
		if($result){
			return $result;
		}else{
			return false;
		}
	}



	public function fetchSurveyQuestions($email){
		$this->db->select("*");
		$this->db->from('guest_user');
		//$this->db->where('hotel_name',$id);
		//$this->db->where('token',$email);
		$this->db->where('user_email_id',$email);
		$this->db->where('pre_stay',0);
		$query = $this->db->get();
		$mailrespose=$query->row();	
 		if (isset($mailrespose)){		
			$uid=$mailrespose->user_id;
			$owner_id=$mailrespose->owner_id;
			$mailrespose->check_in;
			$categoryid = $mailrespose->hotel_branch_name;
			$subcategoryid = $mailrespose->hotel_name;
			$bus_cat_process = $mailrespose->process;

			$this->db->select('*');
			$this->db->from('questionnaire');
			$this->db->where('bus_q_cat_id',$categoryid);
			$this->db->where('bus_q_sub_cat_id',$subcategoryid);
			$this->db->where('bus_cat_process',$bus_cat_process);
			$this->db->where('businessId',$owner_id);
			$query = $this->db->get();
			$allquestions = $query->result();			
				if ($allquestions){
					$response = "";
					foreach ($allquestions as $key => $values){
	        			$qid = $values->bus_qid;
	        			$qname = $values->bus_q_que;
						$q_type = $values->bus_q_type;
	        			$bus_q_cat_id = $values->bus_q_cat_id;
	        			$bus_q_sub_cat_id = $values->bus_q_sub_cat_id;
						$cat_process = $values->bus_cat_process;
						$this->db->select('ans.*');
						$this->db->from('business_questionnaire_answer as ans');
						$this->db->where_in('ans.bus_que_id',$qid);
						$this->db->order_by('ans.bus_que_id','DESC');
						$query = $this->db->get();
						$result=$query->result();
						if (isset($result)){
							$response[$key][] = $owner_id;
							$response[$key][] = $qid;
							$response[$key][] = $qname;
							$response[$key][] = $q_type;
							$response[$key][] = $query->result();
							$response[$key][] = $bus_q_cat_id;
							$response[$key][] = $bus_q_sub_cat_id;
							$response[$key][] = $cat_process;
							$response[$key][] = $uid;							
						}
	        		}
	        		return $response;
		        }else{
					return array();
				}
		}else{
			return array();
		}		
	}
	
	public function fetchSurveyDummyold($email)	{
		$this->db->select("*");
		$this->db->from('guest_user');
		//$this->db->where('hotel_name',$id);
		$this->db->where('user_email_id',$email);
		$this->db->where('pre_stay','0');
		$query = $this->db->get();
		$mailrespose=$query->row();	
		if($query == true){
			$mailrespose=$query->row();		
				$uid=$mailrespose->user_id;
				$owner_id=$mailrespose->owner_id;
				$mailrespose->check_in;
				$categoryid = $mailrespose->hotel_branch_name;
				$subcategoryid = $mailrespose->hotel_name;
				/*
				$uid=!empty($mailrespose->user_id);
				$owner_id=!empty($mailrespose->owner_id);
				!empty($mailrespose->check_in);
				$categoryid = !empty($mailrespose->hotel_branch_name);
				$subcategoryid = !empty($mailrespose->hotel_name);*/

				$this->db->select('*');
				$this->db->from('questionnaire');
				$this->db->where('bus_q_cat_id',$categoryid);
				$this->db->where('bus_q_sub_cat_id',$subcategoryid);
				$this->db->where('businessId',$owner_id);
				//$this->db->where('bus_cat_process','1');
					$query = $this->db->get();
					//echo $this->db->last_query(); die();
				
					if ($query== true) {		
						$allquestions = $query->result();
						$response = "";
						foreach ($allquestions as $key => $values){
			        			$qid = $values->bus_qid;
			        			$qname = $values->bus_q_que;
								$q_type = $values->bus_q_type;
			        			$bus_q_cat_id = $values->bus_q_cat_id;
			        			$bus_q_sub_cat_id = $values->bus_q_sub_cat_id;
								$cat_process = $values->bus_cat_process;
								$this->db->select('ans.*');
								$this->db->order_by('ans.bus_que_id','DESC');
								$this->db->from('business_questionnaire_answer as ans');
								$this->db->where_in('ans.bus_que_id',$qid);
								$query = $this->db->get();
								if ($query== true) {
									$response[$key][] = $owner_id;
									$response[$key][] = $qid;
									$response[$key][] = $qname;
									$response[$key][] = $q_type;
									$response[$key][] = $query->result();
									$response[$key][] = $bus_q_cat_id;
									$response[$key][] = $bus_q_sub_cat_id;
									$response[$key][] = $cat_process;
									$response[$key][] = $uid;
									
								}else{
									return false;
								}
			        		}
			        		return $response;			 
					}else{
						return false;
					}
				}else{
					return false;
			}
	}



	public function surveyInsert($npscoreid){
		$response=$this->input->post('que-ans');
		$cat=$this->input->post('cat');
		$subcat=$this->input->post('subcat');
		$id=$this->input->post('uid');
		$busid=$this->input->post('busid');
		$process=$this->input->post('process');
		$npscore=$this->input->post('npscore');
		//prd($this->input->post()); 
		if (!empty($response)){
			$resultresponse = array();
			foreach ($response as $key => $value){
				foreach($value as $anotherkey => $anothervalue){
					$cf_cat_id 			=	$cat ;
					$uid 				=	$id;
					$businessId 		=	$busid ;
					$cf_cat_sub_id		=	$subcat;
					$cf_cat_process_id  =	$process;
					$anothervalue 		=	$anothervalue;
					$npscore 			=	$npscore;

					$data =array(
						'cf_cat_id'			=>	$this->input->post('cat'),
						'nps_id'			=>	$npscoreid,
						'cf_cat_sub_id'		=>	$this->input->post('subcat'),
						'cf_cat_process_id'	=>	$this->input->post('process'),
						'cf_que_id'			=>	$key,
						'cf_score'			=>	$anothervalue,
						'cf_user_id'		=>	$this->input->post('uid'),
						'user_ip_address'	=>	$_SERVER["REMOTE_ADDR"],
						'busId'				=>	$this->input->post('busid'),
					);					
					$resultresponse 	=	$this->db->insert('customer_feedback', $data);

					//$sql =  'INSERT INTO customer_feedback(cf_cat_id,cf_cat_sub_id,cf_cat_process_id,cf_que_id,cf_score,cf_user_id,busId)VALUES('.$cf_cat_id.','.$cf_cat_sub_id.','.$cf_cat_process_id.','.$key.','.$anothervalue.','.$uid.','.$businessId.')';
					//echo $sql; 
					//$rinsnationality[]= $this->db->query($sql);
					//die;
				}
			}			
			return $resultresponse;			
		} else {
			return array();
		}
	
	}

	public function insertNetpscore($data)	{
		$this->db->insert('netpscore', $data);
		if($this->db->affected_rows()) { 
	        return $this->db->insert_id();
	    }else{
	    	return array();
	    }
	}
	public function updateGuestStatus($id,$where){			
 		$this->db->where('user_id',$id); 
 		return $this->db->update('guest_user',$where);		 
	}

	public function fetchSurveyFeedback(){
		$this->db->select('cf.*,gu.*,bqa.*,c.category_name,sc.sub_cat_name,ssc.sub_sub_cat_name,q.bus_q_que');
		$this->db->from('customer_feedback as cf');
		$this->db->order_by('cf_id', 'DESC');
		$this->db->join('guest_user as gu','cf.cf_user_id=gu.user_id','left');
		$this->db->join('questionnaire as q','cf.cf_que_id=q.bus_qid','left');
		$this->db->join('business_questionnaire_answer as bqa','cf.cf_score=bqa.bus_que_ans_id','left');
		$this->db->join('category as c','cf.cf_cat_id=c.cat_id','left');
		$this->db->join('sub_category as sc','cf.cf_cat_sub_id=sc.sub_cat_id','left');
		$this->db->join('sub_sub_category as ssc','cf.cf_cat_process_id=ssc.sub_sub_cat_id','left');
		
		$query = $this->db->get();
		$result= $query->result();
		if ($result==true) {
 			return $query->result();
		} else {
			return false;
		}
		
	}

	 	/*Testimonials Updated*/
 	public function updateComplain($data) {
 		//prd($data);
 		$this->db->set('cf_status', $data['cf_status']); 
 		$this->db->where('cf_id',$data['cf_id']); 
 		return $this->db->update('customer_feedback');
 	}

 	/* Super Admin Profile */
 	public function fetchAdminProfileData()	{
 		$this->db->select('*');
 		$query = $this->db->get('admin_login');
 		return $query->result_array(); 
 	}

 	public function UpdateAdminProfile($data,$id){
		$this->db->where('admin_login_id',$id);
		return $this->db->update('admin_login',$data);
 	}
 
 	public function adminGetAllComplaintsAjax()	{
		if ($this->session->userdata('sub_login_id')) {
			$this->db->where('cf.busId', $this->session->userdata('sub_login_id'));
		} 

		$this->db->select("
				cf.*,
				gu.*,
				bqa.*,				
				frs.id as frsid,
				frs.name as frsname,
				frs.status as frsstatus,
				c.category_name,
				sc.sub_cat_name,
				ssc.sub_sub_cat_name,
				q.bus_q_que
			");

		$this->db->from('customer_feedback as cf');
		$this->db->join('guest_user as gu','cf.cf_user_id=gu.user_id','inner');
		$this->db->join('questionnaire as q','cf.cf_que_id=q.bus_qid','inner');
		$this->db->join('business_questionnaire_answer as bqa','cf.cf_score=bqa.bus_que_ans_id','inner');
		$this->db->join('category as c','cf.cf_cat_id=c.cat_id','inner');
		$this->db->join('sub_category as sc','cf.cf_cat_sub_id=sc.sub_cat_id','inner');
		$this->db->join('sub_sub_category as ssc','cf.cf_cat_process_id=ssc.sub_sub_cat_id','inner');
		$this->db->join('feedback_rating_status frs', 'cf.cf_status=frs.id','inner');
		$this->db->order_by('cf_id', 'ASC');
		
		$query = $this->db->get();
 		$result= $query->result();
		if ($result==true) {
 			return $query->result();
		} else {
			return false;
		}
		
	}


	public function adminGetAllComplaintsAjaxforgraph()	{ 
		$this->db->select("
				cf.*,
				gu.*,
				bqa.*,				
				frs.id as frsid,
				frs.name as frsname,
				frs.status as frsstatus,
				c.category_name,
				sc.sub_cat_name,
				ssc.sub_sub_cat_name,
				q.bus_q_que
				");
		$this->db->from('customer_feedback as cf');
		$this->db->order_by('cf_id', 'DESC');
		$this->db->join('guest_user as gu','cf.cf_user_id=gu.user_id','inner');
		$this->db->join('questionnaire as q','cf.cf_que_id=q.bus_qid','inner');
		$this->db->join('business_questionnaire_answer as bqa','cf.cf_score=bqa.bus_que_ans_id','inner');
		$this->db->join('category as c','cf.cf_cat_id=c.cat_id','inner');
		$this->db->join('sub_category as sc','cf.cf_cat_sub_id=sc.sub_cat_id','inner');
		$this->db->join('sub_sub_category as ssc','cf.cf_cat_process_id=ssc.sub_sub_cat_id','inner');
		$this->db->join('feedback_rating_status frs', 'cf.cf_status=frs.id','inner');

		if ($this->session->userdata('sub_login_id')) {
			$this->db->where('cf.busId', $this->session->userdata('sub_login_id'));
		} 
		$query = $this->db->get();
 		$result= $query->result_array();
		if ($result==true) {
 			return $query->result_array();
		} else {
			return false;
		}
		
	}



	public function categoryList($where,$status=false){
		if($status==false){
			$where['c.is_deleted']=0;
		}
		if(count($where)>0){
			$this->db->where($where);
		}
		$result=$this->db->get('allcategory c');
		return $result->result_array();
	}

	public function CategoryData($where){
		if(count($where)>0){
			$this->db->where($where);
			$this->db->select('*');
			$this->db->from('allcategory');
			$result=$this->db->get();
			$response=$result->row_array();
			return $response;
		}
	}

		public function getCategoryLevel($where){
		$this->db->where($where);
		$result=$this->db->get('allcategory c');
		$row=$result->row_array();
		return $row;
		
	}

	public function checkExists($table,$where){
		if(!empty($table) && count($where)>0){
			$this->db->where($where);
			$result=$this->db->get($table);
			$total_rows=$result->num_rows();
			if(!$total_rows){
				return true;
			}
			return false;
		}
	}

	public function AddUpdateData($table,$save_data){
		if(!empty($table) && count($save_data)>0){
			if($save_data['id']>0){
				$this->db->where('id',$save_data['id']);
				$this->db->update($table,$save_data);
				  return $save_data['id'];
			}else{
				$this->db->insert($table,$save_data);
				  return $this->db->insert_id();
			}
		}
	}

	public function quesOptionUpdate($table,$save_data){
		if(!empty($table) && count($save_data)>0){
			//if($save_data['id']>0){
				//$this->db->where('id',$save_data['id']);
				//$this->db->update($table,$save_data);
				 // return $save_data['id'];
			//}else{
				$this->db->insert($table,$save_data);
				  return $this->db->insert_id();
			//}
		}
	}	

	public function selectrow($table,$id){
		if(!empty($table) && count($id)>0){
			$this->db->select("*");
			$this->db->from($table);
			$this->db->where('bus_que_ans_id',$id);
			$result = $this->db->get();
     		return $resultArray = $result->row_array(); 			
		}
	}

	public function selectall($table,$where){
		if(!empty($table) && count($where)>0){
			$this->db->where($where);
			$this->db->select("*");
			$this->db->from($table);
			$result = $this->db->get();
     		return $resultArray = $result->result_array(); 			
		}
	}





	public function manageCategoriesAjax($post,$count=false){
        $search=trim($post['search']);
        $status=$post['status'];

        $where['c.is_deleted']=0;
        if($status!=''){
            $where['c.status']=$status;
        }  
       
        if(strlen($search)>0){
            $like['c.name']=$search;
        }
         $like=array();
        $where=array();
        $this->db->select('c.*');

        $this->db->from('allcategory c');
        if(count($where)>0){
            $this->db->where($where);
        }
        if(count($like)>0){
            $this->db->group_start();
            $this->db->or_like($like);
            $this->db->group_end();
        }
        if ($post['order'][0]['column']!='' && isset($post['order'][0]['dir']) && !empty($post['order'][0]['dir'])) {
            $order_by = '';
            switch ($post['order'][0]['column']) {
                case 1:
                    $order_by = 'c.name';
                    break;

                case 2:
                    $order_by = 'c.status';
                    break;
                default:
                    $order_by = 'c.id';
                    break;
            }

            $dir_by = '';
            switch ($post['order'][0]['dir']) {

                case 'asc':
                    $dir_by = 'asc';
                    break;
                case 'desc':
                    $dir_by = 'desc';
                    break;
                default:
                    $dir_by = 'asc';
                    break;
            }
        $this->db->order_by($order_by,$dir_by); 
    }
    //
    if(!$count){
        $start=$post['start'];
        $length=$post['length'];
        if(!$start){
            $start=0;
        }
        if(!$length){
            $length=10;
        }
        $this->db->limit($length,$start);
    }


    $result = $this->db->get();
    $resultArray = $result->result_array(); 
    	if($count){
        	return $result->num_rows();
    	}else{
         	return $resultArray;
    	}
    }

	public function changeDataStatus($table,$update_data){
		if(count($update_data)>0 && !empty($table)){
			$this->db->where('id',$update_data['id']);
			$response=$this->db->update($table,$update_data);
			return $response;
		}
	}

	public function superAdminInserQuestionnaire($data){
			$this->db->insert('questionnaire_masters',$data);
			$last_insert_que_id = $this->db->insert_id();
			if($last_insert_que_id > 0){
				return $last_insert_que_id;	
			}else{
				return array();	
			}
		}	

	public	function superAdminInserQuestionnaireNew($data){
			$this->db->insert('questionnaire_masters_new',$data);
			$last_insert_que_id = $this->db->insert_id();
			if($last_insert_que_id > 0){
				return $last_insert_que_id;	
			}else{
				return array();	
			}
		}
	public function deleteRecord($id,$table){
		if(!empty($id)>0 && !empty($table)){
			$this->db->where('qid',$id);
			$result=$this->db->delete($table);
			if($result){
				$this->db->where('que_id',$id);
				$response=$this->db->delete('questionnaire_answer');
				if($response){
					return $response;
				}else{
					false;
				}
			}
		}
		return false;
	}
	public function subdeleteRecord($id,$table){
		if(!empty($id)>0 && !empty($table)){
			$this->db->where('bus_qid',$id);
			$result=$this->db->delete($table);
			if($result){
				$this->db->where('bus_que_id',$id);
				$response=$this->db->delete('business_questionnaire_answer');
				if($response){
					return $response;
				}else{
					false;
				}
			}
		}
		return false;
	}

	public function superAdminInserQuestionnaireAnswer($qansoptions){     
			for($i=0; $i<count($qansoptions['qtype']); $i++) {
       	 		$last_insert_que_id = $qansoptions['id']; 
       	 		$qtype = $qansoptions['qtype'][$i]; 
       	 		$que_score = $qansoptions['que_score'][$i]; 
       	 		$questionnaire_answer = array(
						'que_id' => $last_insert_que_id,
						'que_type'=> $qtype,
						'que_score'=> $que_score
					);

       	 		$result_que_answer =$this->db->insert('questionnaire_answer',$questionnaire_answer);
       	 	}

       	 	if($result_que_answer > 0){
				return $result_que_answer;	
			}else{
				return array();	
			}
 	}
 	//TEST
	public function superAdminInserQuestionnaireAnswerNew($qansoptions){
 			for($i=0; $i<count($qansoptions['qtype']); $i++) {

       	 		$last_insert_que_id = $qansoptions['id']; 
       	 		$qtype = $qansoptions['qtype'][$i]; 
       	 		$que_score = $qansoptions['que_score'][$i]; 

       	 		$questionnaire_answer = array(
						'que_id' => $last_insert_que_id,
						'que_type'=> $qtype,
						'que_score'=> $que_score
					);

       	 		$result_que_answer =$this->db->insert('questionnaire_answer_new',$questionnaire_answer);
       	 	}

       	 	if($result_que_answer > 0){
				return $result_que_answer;	
			}else{
				return array();	
			}
 	}

 	//TEST
	public function fetchSurveyQuestionsGuestUser()	{
		$email='praveen@panindia.in';
		$this->db->select("gu.*");
		$this->db->from('guest_user as gu');
  		$this->db->where('gu.user_email_id',$email);
		$this->db->where('gu.pre_stay',0);
		$query = $this->db->get();
		$mailrespose=$query->row_array(); 

 		if ($mailrespose >0) {		 
			$owner_id=$mailrespose['owner_id'];		 
			$user_id=$mailrespose['user_id'];		 
			$this->db->select('qmn.*');
			$this->db->from('questionnaire_masters_new as qmn');
			$query = $this->db->get();
			$allquestions = $query->result_array();
				if ($allquestions>0) {
					$response = "";
					foreach ($allquestions as $key => $values) {
 						$this->db->select('ans.*');
						$this->db->from('questionnaire_answer_new as ans');
						$this->db->where_in('ans.que_id',$values['qid']);
						$this->db->order_by('ans.que_id','DESC');
						$query = $this->db->get();
						$answerresult=$query->result_array();
						if ($answerresult > 0 ) {
							$response[$key]['owner_id'] = $owner_id;
							$response[$key]['user_id'] = $user_id;
							$response[$key]['qid'] = $values['qid'];
							$response[$key]['cat_id'] = $values['q_cat_id'];
							$response[$key]['que'] = $values['q_que']; 
							$response[$key]['q_type'] = $values['q_type'];
							$response[$key]['allresult'] = $answerresult;							
						}
	        		}
 	        		return $response;
		        }else{
					return array();
				}
		}else{
			return array();
		}		
	}


	public function surveyQueByCat($id,$category)	{ 		 	  	 
			$this->db->select('qmn.*');
			$this->db->from('questionnaire_masters as qmn');
			$this->db->where('qmn.q_cat_id',$category);
			$query = $this->db->get();
			$allquestions = $query->result_array();
				if ($allquestions>0) {
					$response = array();
					foreach ($allquestions as $key => $values) {
 						$this->db->select('ans.*');
						$this->db->from('questionnaire_answer as ans');
						$this->db->where_in('ans.que_id',$values['qid']);
						$this->db->order_by('ans.que_id','DESC');
						$query = $this->db->get();
						$answerresult=$query->result_array();
						if ($answerresult > 0 ) {
							$response[$key]['owner_id'] = $id;
							$response[$key]['qid'] = $values['qid'];
							$response[$key]['cat_id'] = $values['q_cat_id'];
							$response[$key]['SUB_cat_id'] = $values['q_sub_cat_id'];
							$response[$key]['process_cat_id'] = $values['cat_process'];
							$response[$key]['que'] = $values['q_que']; 
							$response[$key]['q_type'] = $values['q_type'];
							$response[$key]['allresult'] = $answerresult;							
						}
	        		}
 	        		return $response;
		        }else{
					return array();
				}
		 
	}

	//ADMIN
	public function allQuestionsList($id,$category)	{
			
			$response = array();
			$this->db->select('qmn.*');
			$this->db->from('questionnaire_masters as qmn');
			if($category>0){				
				$this->db->where('qmn.q_cat_id',$category);
			}			
			$query = $this->db->get();
			$allquestions = $query->result_array();
				if ($allquestions>0) {
					foreach ($allquestions as $key => $values) {
 						$this->db->select('ans.*');
						$this->db->from('questionnaire_answer as ans');
						$this->db->where_in('ans.que_id',$values['qid']);
						$this->db->order_by('ans.que_id','DESC');
						$query = $this->db->get();
						$answerresult=$query->result_array();
						if ($answerresult > 0 ) {
							if ($id>0) {
								$response[$key]['owner_id'] = $id;
							}							
							$response[$key]['qid'] = $values['qid'];
							$response[$key]['cat_id'] = $values['q_cat_id'];
							$response[$key]['SUB_cat_id'] = $values['q_sub_cat_id'];
							$response[$key]['process_cat_id'] = $values['cat_process'];
							$response[$key]['que'] = $values['q_que']; 
							$response[$key]['q_type'] = $values['q_type'];
							$response[$key]['allresult'] = $answerresult;							
						}
	        		}
 	        		return $response;
		        }else{
					return array();
				}
		 
	}

	public function allQuestionsListForSAdmin(){
		
		$response = array();
		$this->db->select('qmn.*');
		$this->db->from('questionnaire_masters as qmn');			 			
		$query = $this->db->get();
		$allquestions = $query->result_array();
		if ($allquestions>0) {
			foreach ($allquestions as $key => $values) {
				$this->db->select('ans.*');
				$this->db->from('questionnaire_answer as ans');
				$this->db->where_in('ans.que_id',$values['qid']);
				$this->db->order_by('ans.que_id','DESC');
				$query = $this->db->get();
				$answerresult=$query->result_array();
				//prd($values);
				if ($answerresult > 0 ) {
					 						
					$response[$key]['qid'] = $values['qid'];
					$response[$key]['cat_id'] = $values['q_cat_id'];
					$response[$key]['SUB_cat_id'] = $values['q_sub_cat_id'];
					$response[$key]['process_cat_id'] = $values['cat_process'];
					$response[$key]['que'] = $values['q_que']; 
					$response[$key]['q_type'] = $values['q_type'];
					$response[$key]['allresult'] = $answerresult;							
				}
    		}
        		return $response;
        }else{
			return array();
		}		 
	}


	//SUN-ADMIN
	public function getquestionnaireOptionList($id)	{
		$this->db->select('ans.*,sn.*');
		$this->db->from('business_questionnaire_answer as ans');
		$this->db->join('score_name as sn', 'ans.bus_que_score = sn.score_id');
		$this->db->where_in('ans.bus_que_id',$id);		
		$query = $this->db->get();			
		return ($query->num_rows() > 0)?$query->result_array():array();		
	}

	//ADMIN
	public function getquestionnaireOptionListAdmin($id){
		$this->db->select('ans.*,sn.*');
		$this->db->from('questionnaire_answer as ans');
		$this->db->join('score_name as sn', 'ans.que_score = sn.score_id');
		$this->db->where_in('ans.que_id',$id);		
		$query = $this->db->get();			
		return ($query->num_rows() > 0)?$query->result_array():array();		
	}

	public function complaintsListNetPromterScore(){
		$this->db->select('nps.*,c.category_name ,ssc.sub_cat_name,sp.sub_sub_cat_name,gu.user_name');
		$this->db->from('netpscore as nps');
		$this->db->join('guest_user as gu', 'nps.user_id = gu.user_id','inner');
		$this->db->join('category as c', 'nps.cat_id = c.cat_id','inner');
		$this->db->join('sub_category as ssc', 'nps.sub_cat_id = ssc.sub_cat_id','inner');
		$this->db->join('sub_sub_category as sp', 'nps.process_id = sp.sub_sub_cat_id','inner');		
		$this->db->order_by('nps.id','DESC');		
		$query = $this->db->get();			
		return ($query->num_rows() > 0)?$query->result_array():array();		
	}

	public function complaintsListNetPromterScoreAjax($post,$count=false){
		$this->db->select('nps.*,c.category_name ,ssc.sub_cat_name,sp.sub_sub_cat_name,gu.user_name');
		$this->db->from('netpscore as nps');
		$this->db->join('guest_user as gu', 'nps.user_id = gu.user_id','inner');
		$this->db->join('category as c', 'nps.cat_id = c.cat_id','inner');
		$this->db->join('sub_category as ssc', 'nps.sub_cat_id = ssc.sub_cat_id','inner');
		$this->db->join('sub_sub_category as sp', 'nps.process_id = sp.sub_sub_cat_id','inner');

		 if ($post['order'][0]['column']!='' && isset($post['order'][0]['dir']) && !empty($post['order'][0]['dir'])) {
            $order_by = '';
            switch ($post['order'][0]['column']) {
                case 1:
                    $order_by = 'nps.user_id';
                    break;

                case 2:
                    $order_by = 'nps.sub_cat_id';
                    break;
                default:
                    $order_by = 'nps.id';
                    break;
            }

            $dir_by = '';
            switch ($post['order'][0]['dir']) {

                case 'asc':
                    $dir_by = 'asc';
                    break;
                case 'desc':
                    $dir_by = 'desc';
                    break;
                default:
                    $dir_by = 'asc';
                    break;
            }
        $this->db->order_by($order_by,$dir_by); 
    }
    
    if(!$count){
        $start=$post['start'];
        $length=$post['length'];
        
        if(!$start){
            $start=0;
        }
        if(!$length){
            $length=10;
        }
    	$this->db->limit($length,$start);
    }   
    $result = $this->db->get();
    $resultArray = $result->result_array(); 
		
		if($count){
    		return $result->num_rows();
		}else{
     		return $resultArray;
		}

}

	



	public function allAdminQuestionsListAjax($post,$count=false){
		$response=array();
		$like=array();
        $where=array();

		$search 		= trim($post['stay_id']);
        $category_id 	= $post['category_id']; 
        if($category_id>0){
            $where['qmn.q_cat_id']=$category_id;
        }

        if(strlen($search)>0){
            $like['sp.sub_sub_cat_name']=$search;
        }  
       
      
     
        if(count($like)>0){
            $this->db->group_start();
            $this->db->or_like($like);
            $this->db->group_end();
        }
        if(count($where)>0){
            $this->db->where($where);
        }

		$this->db->select('qmn.*,c.category_name ,ssc.sub_cat_name,sp.sub_sub_cat_name');
		$this->db->from('questionnaire_masters as qmn');
 		$this->db->join('category as c', 'qmn.q_cat_id = c.cat_id','inner');
		$this->db->join('sub_category as ssc', 'qmn.q_sub_cat_id = ssc.sub_cat_id','inner');
		$this->db->join('sub_sub_category as sp', 'qmn.cat_process = sp.sub_sub_cat_id','inner');

        if ($post['order'][0]['column']!='' && isset($post['order'][0]['dir']) && !empty($post['order'][0]['dir'])) {
            $order_by = '';
            switch ($post['order'][0]['column']) {
                case 1:
                    $order_by = 'qmn.q_cat_id';
                    break;

                case 2:
                    $order_by = 'qmn.q_cat_id';
                    break;
                default:
                    $order_by = 'qmn.q_cat_id';
                    break;
            }

            $dir_by = '';
            switch ($post['order'][0]['dir']) {

                case 'asc':
                    $dir_by = 'asc';
                    break;
                case 'desc':
                    $dir_by = 'desc';
                    break;
                default:
                    $dir_by = 'asc';
                    break;
            }
        $this->db->order_by($order_by,$dir_by); 
    }
    //
    if(!$count){
        $start=$post['start'];
        $length=$post['length'];
        if(!$start){
            $start=0;
        }
        if(!$length){
            $length=10;
        }
        $this->db->limit($length,$start);
    }


   
    $result = $this->db->get();
    //echo $this->db->last_query();
    $resultArray = $result->result_array(); 
    	if($count){
        	return $result->num_rows();
    	}else{
         	return $resultArray;
    	}
    	 



		}


	public function allQuestionsListAjax($post,$count=false){
		$response=array();
		$like=array();
        $where=array();

 
		$search 		= trim($post['search']);
		$stay_id 		= trim($post['stay_id']);
		$change_type 	= trim($post['type_id']);
        $category_id 	= $post['category_id']; 
        
        if($category_id>0){
            $where['qmn.bus_q_sub_cat_id']=$category_id;
        }

        if($this->session->userdata('category')){
            $where['qmn.bus_q_cat_id']=$this->session->userdata('category');
        }

       if(strlen($search)>0){
            $like['qmn.bus_q_que']=$search;
            $like['ssc.sub_cat_name']=$search;
            $like['qmn.bus_q_type']=$search;
            $like['sp.sub_sub_cat_name']=$search;
        }

        if(strlen($stay_id)>0){
            $like['sp.sub_sub_cat_name']=$stay_id;
        }
        
        if(strlen($change_type)>0){
            $where['qmn.bus_q_type']=$change_type;
        }
        
     
        if(count($like)>0){
            $this->db->group_start();
            $this->db->or_like($like);
            $this->db->group_end();
        } 
        if(count($where)>0){
            $this->db->where($where);
        }

		$this->db->select('qmn.*,c.category_name ,ssc.sub_cat_name,sp.sub_sub_cat_name');
		$this->db->from('questionnaire as qmn');
		$this->db->join('category as c', 'qmn.bus_q_cat_id = c.cat_id','inner');
		$this->db->join('sub_category as ssc', 'qmn.bus_q_sub_cat_id = ssc.sub_cat_id','inner');
		$this->db->join('sub_sub_category as sp', 'qmn.bus_cat_process = sp.sub_sub_cat_id','inner');
 
        if ($post['order'][0]['column']!='' && isset($post['order'][0]['dir']) && !empty($post['order'][0]['dir'])) {
            $order_by = '';
            switch ($post['order'][0]['column']) {
                case 1:
                    $order_by = 'qmn.bus_q_cat_id';
                    break;

                case 2:
                    $order_by = 'qmn.bus_q_cat_id';
                    break;
                default:
                    $order_by = 'qmn.bus_q_cat_id';
                    break;
            }

            $dir_by = '';
            switch ($post['order'][0]['dir']) {

                case 'asc':
                    $dir_by = 'asc';
                    break;
                case 'desc':
                    $dir_by = 'desc';
                    break;
                default:
                    $dir_by = 'asc';
                    break;
            }
        $this->db->order_by($order_by,$dir_by); 
    }
    //
    if(!$count){
        $start=$post['start'];
        $length=$post['length'];
        if(!$start){
            $start=0;
        }
        if(!$length){
            $length=10;
        }
        $this->db->limit($length,$start);
    }


   
    $result = $this->db->get();
   // echo $this->db->last_query();
    $resultArray = $result->result_array(); 
    	if($count){
        	return $result->num_rows();
    	}else{
         	return $resultArray;
    	} 
	}


	public function complaintsListByUser($id)
	{
		return $id;
	}



 
/*******************************************************  End     ********************************************************/
	

}













