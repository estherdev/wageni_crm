<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/*
 Model : Masters Model 
*/

class CartModel extends CI_Model

{
    public function __construct() 
	{
        parent::__construct();
    }
    //Define Tables Name
	var $homepage_slider = 'homepage_slider';
	var $faq = 'faq';
	var $packages = 'packages';
	var $subscriberlogin = 'subscriberlogin';
	var $sub_mapping = 'subscriberpackagemapping';
	var $subscriberroles = 'subscriberroles';
	var $about_us = 'about_us';
	var $testimonials = 'testimonials';
	var $clients_logo = 'clients_logo';
	var $subscriber_branch = 'subscriber_branch';
	var $category = 'category';
	var $sub_category = 'sub_category';
	var $sub_sub_category = 'sub_sub_category';
	var $history = 'history';
	var $history_content = 'history_content';
	var $questionnaire_masters = 'questionnaire_masters';
	var $questionnaire = 'questionnaire';
	var $business_questionnaire_answer = 'business_questionnaire_answer';

 
	public function fetchPackagedetail($packageid)	{
		$this->db->select('*')->from('packages')->where('package_id', $packageid);
		return $this->db->get()->result_array();
	}

	 
	public function insertuser($data){
		 $this->db->insert('subscriberlogin',$data);
		 return $this->db->insert_id();
	}

	 
	public function insertpackageorder($data){
		return $this->db->insert('subscriberpackagemapping',$data);
	}




/*******************************************************  End     ********************************************************/
	

}