<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
Model : Auth Model 
*/
class AuthModel extends CI_Model
{
	public function __construct() {
		parent::__construct();
	}
	var $admin_login = 'admin_login';
	var $adminemprole = 'adminemprole';
	var $subscriberlogin = 'subscriberlogin';
	var $subscriberroles = 'subscriberroles';
	var $subscriberpackagemapping = 'subscriberpackagemapping';
	var $packages = 'packages';
	var $subscriber_branch = 'subscriber_branch';


	public function getcustomerExportData(){		
		$this->db->select("
			cat.category_name,						
			c.hvroom,						
			c.fname,						
			c.lname,
 			c.sub_name,
			c.sub_phone,
			c.company_url,
			c.sub_email,
			cu.name,
			c.contacttel,
			c.contactno,
			c.accno,
			c.contactaddress,
			c.cpname,
			c.cpemail,
			c.cptitle,
			c.cpmobile,
			c.cpaddress,
			c.cpoffice,
			sl.subscriber_role_name
		");
		$this->db->from('subscriberlogin c');
		$this->db->join('category cat','c.category=cat.cat_id','inner');
		$this->db->join('countries cu','c.country=cu.id','inner');
		$this->db->join('subscriberroles sl','c.sub_login_role=sl.subscriber_roll_id','inner');
 	    $this->db->where('c.added_by',0);
		$result=$this->db->get();
		return $result->result_array();
   }

	/*	Insert Users  */
	public function insertUsers($usersdata)	{
		return $this->db->insert($this->admin_login,$usersdata);
	}
	/*	Fetch Users 	*/
	public function fetchUsers()	{
		$this->db->select('ae.admin_emp_role_name ,al.*');
		$this->db->from('admin_login as al');
		$this->db->join('adminemprole as ae','al.admin_role_id=ae.admin_role_id','left');
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
	/*	Fetch Users Role	*/
	public function fetchAdminRole()	{
		$this->db->select('*'); 
		$this->db->from($this->adminemprole);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
	/*	Fetch Users By Id  */
	public function fetchUsersById($id){
		$this->db->select('ae.admin_emp_role_name ,al.*');
		$this->db->from('admin_login as al');
		$this->db->join('adminemprole as ae','al.admin_role_id=ae.admin_role_id','left');		
		$this->db->where('admin_login_id',$id);
		$query = $this->db->get();
		if($query){
			return $query->result();
		}
		else{  
			return false;
		}
	}
	
	/*	Updates Users	*/
	public function updateUsers($data)	{
		$this->db->where('admin_login_id',$data['admin_login_id']);
		$this->db->update($this->admin_login, $data);
		return true;
	}
	/* 	Delete Users		*/
	public function deleteUsers($id)	{
		$this->db->where('admin_login_id', $id);
		return $this->db->delete($this->admin_login); 
	}
	/*	 Subscriber  */
	public function insertSubscriber($usersdata)	{		
		$this->db->insert($this->subscriberlogin,$usersdata);
		$last_insert_id = $this->db->insert_id();
		if ($last_insert_id==true) {
			$packaigeid=$this->input->post('userpackage') ;
			$subscriber_end_date= date('d-m-Y', strtotime('+364 days') );

			$packaige = array('subscriber_id'=>$last_insert_id,
            'subscriber_package_id'=>$packaigeid,    
            'subscriber_start_date'=>date('d-m-Y'),
            'subscriber_end_date'=>$subscriber_end_date,
            /*'package_desc'=>$packagedesc		*/
        );	
			return $this->db->insert($this->subscriberpackagemapping,$packaige);
		}else{
			return false;
		}
	}

	public function insertSubAdminUsers($usersdata)	{
		//echo "<pre/>"; print_r($usersdata);
		//die;
		$this->db->insert($this->subscriberlogin,$usersdata);
		$last_insert_id = $this->db->insert_id();
		if ($last_insert_id==true) {
			/*$packaigeid=$this->input->post('userpackage') ;
			$subscriber_end_date= date('d-m-Y', strtotime('+364 days') );

			$packaige = array('subscriber_id'=>$last_insert_id,
            'subscriber_package_id'=>$packaigeid,    
            'subscriber_start_date'=>date('d-m-Y'),
            'subscriber_end_date'=>$subscriber_end_date,
           
        );*/	
			return true;//$this->db->insert($this->subscriberpackagemapping,$packaige);
		}else{
			return false;
		}

	}
	/*	Fetch Subscriber 	*/
	public function fetchSubscriber()	{		
		/*$this->db->select('sl.*,spm.*,sr.subscriber_role_name as role');
		$this->db->order_by('sub_login_id','DESC'); 
		$this->db->from('subscriberlogin as sl');
		$this->db->join('subscriberroles as sr','sl.sub_login_role=sr.subscriber_roll_id','left');	
		$this->db->join('subscriberpackagemapping as spm','sl.sub_login_id=spm.subscriber_id','left');	
		$this->db->where('added_by',0);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
		*/	
		$this->db->select('spm.*,sl.*,p.package_name,p.package_price,p.package_createddate,sr.subscriber_role_name as role, c.category_name');
		$this->db->from('subscriberpackagemapping as spm');
		$this->db->join('subscriberlogin as sl','spm.subscriber_id=sl.sub_login_id','left');
		$this->db->join('subscriberroles as sr','sl.sub_login_role=sr.subscriber_roll_id','left');		
		$this->db->join('packages as p','spm.subscriber_package_id=p.package_id','left');
		$this->db->join('category as c','sl.category=c.cat_id','left');
		$this->db->where('sl.added_by',0);		
		$query = $this->db->get();
		if($query){
			return $query->result();
		}
		else{  
			return false;
		}
	}

	
	
	/*
	public function fetchSubscriber()
	{		
		$this->db->select('spm.*,sl.*,sr.subscriber_role_name as role,p.package_name');
		$this->db->order_by('sub_login_id','DESC'); 
		$this->db->from('subscriberpackagemapping as spm');
		$this->db->join('subscriberlogin as sl','spm.subscriber_id=sl.sub_login_id','left');
		$this->db->join('subscriberroles as sr','sl.sub_login_role=sr.subscriber_roll_id','left');		
		$this->db->join('packages as p','spm.subscriber_package_id=p.package_id','left');	
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}*/

	/*	Fetch Subscribers By Id  */
	public function fetchSubscribersId($id){
		$this->db->select('spm.*,sl.*,p.package_name,sr.subscriber_role_name as role,cat.cat_id,cat.category_name');
		$this->db->from('subscriberpackagemapping as spm');
		$this->db->join('subscriberlogin as sl','spm.subscriber_id=sl.sub_login_id','left');
		$this->db->join('subscriberroles as sr','sl.sub_login_role=sr.subscriber_roll_id','left');		
		$this->db->join('packages as p','spm.subscriber_package_id=p.package_id','left');			
		$this->db->join('category as cat','sl.category=cat.cat_id','left');			
		$this->db->where('sub_login_id',$id);
		$query = $this->db->get();
		if($query){
			return $query->result();
		}
		else{  
			return false;
		}
	}/*
	public function fetchSubscribersId($id){
		$this->db->select('sl.*,sr.subscriber_role_name as role');
		$this->db->from('subscriberlogin as sl');
		$this->db->join('subscriberroles as sr','sl.sub_login_role=sr.subscriber_roll_id','left');			
		//$this->db->join('packages as p','subscriberpackagemapping.subscriber_package_id=p.package_id','left');			
		$this->db->where('sub_login_id',$id);
		$query = $this->db->get();
		if($query){
			return $query->result();
		}
		else{  
			return false;
		}
	}*/
	/*	Fetch Subscribers By Id  */
	public function fetchSubscribersSessionId(){
		$this->db->select('sl.*,sb.*,sr.subscriber_role_name as role');		
		$this->db->from('subscriberlogin as sl');		
		$this->db->join('subscriberroles as sr','sl.sub_login_role=sr.subscriber_roll_id','left');		
		$this->db->join('subscriber_branch as sb','sl.branch_id=sb.sub_branch_id','left');		
		$this->db->where('added_by',$this->session->userdata('sub_login_id'));
		$query = $this->db->get();
		if($query){
			return $query->result();
		}
		else{  
			return false;
		}
	}

	/* 	Delete All Company Data By Id		*/
	public function deleteAllCompanyDataById($id)	{
		$this->db->where('sub_login_id', $id);
		$companyDelete= $this->db->delete($this->subscriberlogin); 
		if ($companyDelete== true) {
			$this->db->where('added_by', $id);
			$companyAddUsersDelete= $this->db->delete($this->subscriberlogin); 

			if ($companyAddUsersDelete== true) {
				$this->db->where('subs_id', $id);
				$companyAddBranchesDelete= $this->db->delete($this->subscriber_branch); 

				if ($companyAddBranchesDelete== true) {
				$this->db->where('subscriber_id', $id);
					return $companyMappingDelete= $this->db->delete($this->subscriberpackagemapping); 
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}		
	} 
	/* 	Suspend All Company Data By Id		*/
	public function suspendAllCompanyDataById($id)	{
		$this->db->where('sub_login_id', $id);
		$this->db->set('sub_isActive', 0);
		$companyDelete= $this->db->update($this->subscriberlogin); 
		if ($companyDelete== true) {
			$this->db->where('added_by', $id);
			$this->db->set('sub_isActive',0);			
			$companyAddUsersDelete= $this->db->update($this->subscriberlogin); 

			if ($companyAddUsersDelete== true) {
				$this->db->where('subs_id', $id);
				$this->db->set('branch_status',0);
				$companyAddBranchesDelete= $this->db->update($this->subscriber_branch); 

				if ($companyAddBranchesDelete== true) {
				$this->db->where('subscriber_id', $id);
				$this->db->set('pac_status',0);
					return $companyMappingDelete= $this->db->update($this->subscriberpackagemapping); 
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}		
	}  
	/*	Updates Subscribers	*/
	public function updateSubscribers($data)	{
		$packaigeid=$this->input->post('userpackage') ;
		$subscriber_end_date= date('d-m-Y', strtotime('+364 days') );
	
		$this->db->where('sub_login_id',$data['sub_login_id']);
		$last_insert_id=$this->db->update($this->subscriberlogin, $data);
		

		if ($last_insert_id==true) {			
			$this->db->set('subscriber_package_id',$packaigeid);			
			$this->db->set('subscriber_start_date',date('d-m-Y'));			
			$this->db->set('subscriber_end_date',$subscriber_end_date);			
			$this->db->where('subscriber_id',$data['sub_login_id']);	
			//print_r($data['sub_login_id']);		
			//print_r($packaigeid);
			//echo $this->db->last_query(); die;
			 return $this->db->update('subscriberpackagemapping');
			//echo $this->db->last_query(); die;
		}else{
			return false;
		}		
	}

		/* Delete Subscriber */
	public function deleteSubscribers($id)	{
		$this->db->where('sub_login_id', $id);
		return $this->db->delete($this->subscriberlogin); 
	} 

	/* Delete Sub- Admin Company */
	public function deletesubscriberSubAdmin($id)	{
		$this->db->where('sub_login_id', $id);
		return $this->db->delete($this->subscriberlogin); 
	} 

	/* Insert Admin Role */
	public function insertAdminRole($data)	{
		return $this->db->insert($this->adminemprole,$data);
	}

	 /*Get Admin role by Id*/
	public function getAdminRoleById($id)	{
		$this->db->select("*");
		$this->db->from($this->adminemprole);
		$this->db->where('admin_role_id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}
		/*update Admin Role*/
	public function updateAdminRole($data)	{
		$this->db->where('admin_role_id',$data['admin_role_id']);
		return $this->db->update($this->adminemprole, $data);
		/*if($query){
			return $query->result();
		}
		else{
			return false;
		}*/
	}
	
	/* 	Delete Admin 	Role	*/
	public function deleteAdminRole($id)	{
		$this->db->where('admin_role_id', $id);
		return $this->db->delete($this->adminemprole); 
	}  
	/* Insert User Role */
	public function insertUserRole($data)	{
		return $this->db->insert($this->subscriberroles,$data);
	} 
	/* Fetch User Role Id */
	public function fetcUserRoleById($id)	{
		$this->db->from($this->subscriberroles);
		$this->db->where('subscriber_roll_id',$id);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
	/* 	Delete Subscribers	Role	*/
	public function updateUserRole($data)	{
		$this->db->where('subscriber_roll_id', $data['subscriber_roll_id']);
		return $this->db->update($this->subscriberroles,$data); 
	} 
	/* 	Delete Subscribers	Role	*/
	public function deleteSubscribersRole($id)	{
		$this->db->where('subscriber_roll_id', $id);
		return $this->db->delete($this->subscriberroles); 
	} 
	/*	Fetch Subscribers Role for dropdown As role wise */
	public function fetchSubscribersRole()	{
		$this->db->select('*'); 
		$this->db->from($this->subscriberroles);		
		//$this->db->where('subscriber_roll_id >',$this->session->userdata('userrole'));
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
		/*	Check Add User Limit 	*/
	public function check_add_user_limit()	{
		$this->db->select('*'); 
		$this->db->from('subscriberlogin');
		$this->db->where('added_by', $this->session->userdata('sub_login_id'));
		$query = $this->db->get();
		if($query->num_rows() > 1){  			
  			return $query->row();
  		}else{  
  			return false;
  		}
	}

	/*	Fetch  Subscriber Admin 	*/
	public function fetchSubscriberAdmin()	{
		$this->db->select('*'); 
		$this->db->from($this->subscriberlogin);
		$this->db->where('sub_login_role','1');
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
	/*	Fetch  Subscriber Admin 	*/
	public function fetchSubscriberSubAdmin()	{
		$this->db->select('*'); 
		$this->db->from($this->subscriberlogin);
		$this->db->where('sub_login_role !=' , 1);
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}

	public function fetchCategory()	{
		$this->db->select('*'); 
		$this->db->from('category');
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}
		/* Update The Profile */
	public function updateProfile($usersdata,$id)	{
		$this->db->where('sub_login_id',$id);		
		return $this->db->update('subscriberlogin',$usersdata);	
	}

		/******************************************* Guest User Start ***********************************************************/

	
	public function fetchhotelprocess($data)	{
		$this->db->select('sb.*'); 
 		$this->db->from('questionnaire as q');
 		$this->db->join('sub_sub_category as sb','q.bus_q_sub_cat_id=sb.sub_category_id','inner');		 
		$this->db->where('q.businessId',$data['owner_id']);	
		$this->db->where('q.bus_q_cat_id',$data['category']);	
		$this->db->where('q.bus_q_sub_cat_id',$data['subcat']);	
		$this->db->where('sb.sub_sub_cat_name','Pre stay');	
		$query = $this->db->get();	
		//echo  $this->db->last_query();	
		$result = $query->result_array();	
		if($result){
			return $result;
		}
		else{
			return false;
		}
		
	}
	public function getProcess($where)	{
		$this->db->select('sb.*'); 
 		$this->db->from('questionnaire as q');
 		$this->db->join('sub_sub_category as sb','q.bus_q_sub_cat_id=sb.sub_category_id','inner');	 
		if(count($where)>0){
			$this->db->where($where);
		}
		$query = $this->db->get();	
 		//$result = $query->result_array();	
 		$result = $query->row_array();	
		if($result){
			return $result;
		}
		else{
			return false;
		}
		
	}
	public function getProcessByMailResposce($where)	{
		$this->db->select('q.bus_q_cat_id,q.bus_q_sub_cat_id,sb.sub_sub_cat_name'); 
 		$this->db->from('questionnaire as q');
 		//$this->db->join('subscriberlogin as sl','q.businessId=sl.sub_login_id','inner');		 
 		$this->db->join('sub_sub_category as sb','q.bus_q_sub_cat_id=sb.sub_category_id','inner');	 
 
		if(count($where)>0){
			$this->db->where($where);
		}
		$query = $this->db->get();	
 		$result = $query->result_array();	
		if($result){
			return $result;
		}
		else{
			return false;
		}
		
	}


	public function guestUserDetails($where){		
 		$this->db->select('gu.* ,  sb1.category_name ,sb.sub_cat_name'); 
		$this->db->order_by('user_id', 'DESC');
		$this->db->from('guest_user as gu');
		$this->db->join('sub_category as sb','gu.hotel_name=sb.sub_cat_id','inner');		
		$this->db->join('category as sb1','gu.hotel_branch_name=sb1.cat_id','inner');
		
		if(count($where)>0){
			$this->db->where($where);
		}
 		$result=$this->db->get();
		$response=$result->row_array();
 		return $response;
	}

	public function AddUpdateData($table,$save_data){
		if(!empty($table) && count($save_data)>0){
			if($save_data['user_id']>0){

				//echo "string".$save_data['stay'];

				//$this->db->where($save_data);
				//$this->db->set('cf_status',$save_data['cf_status']);
				$this->db->where('user_id',$save_data['user_id']);
				$this->db->update($table,$save_data);
				//$this->db->update($table,$save_data['cf_status']);
				//echo $this->db->last_query();

  				return $save_data['user_id'];
			}
		}
	}

	public function updateGuestUserAfterMailSent($id,$data)	{
 		$this->db->where('user_id',$id);		
		return $this->db->update('guest_user',$data);	
	}


	/*Insert the Guest user*/
	public function insertGuestUser($data)	{
		return $this->db->insert('guest_user',$data);
		$last_insert_id = $this->db->insert_id();
		if($last_insert_id > 0){
			return $last_insert_id;	
		}else{
			return array();	
		}
	}
	/*Fetch  the Guest user*/
	public function getGuestUserData($id)	{
		$this->db->from('guest_user');	
		$this->db->where('user_id',$id);
		$query = $this->db->get();
		$result= $query->row_array();
		if($result>0){
			return $result;
		}
		else{  
			return false;
		}
	}

	/* Fetch All Guest Data */
	public function fetchGuestUserData()	{
		//echo $this->session->userdata('category'); die;
		$this->db->select('gu.* ,  sb1.category_name ,sb.sub_cat_name'); 
		$this->db->order_by('user_id', 'DESC');
		$this->db->from('guest_user as gu');
		$this->db->join('sub_category as sb','gu.hotel_name=sb.sub_cat_id','inner');		
		$this->db->join('category as sb1','gu.hotel_branch_name=sb1.cat_id','inner');
		$this->db->where('gu.owner_id',$this->session->userdata('sub_login_id'));		
		//$this->db->join('gu.user_id',$this->session->userdata(''));	
		$query = $this->db->get();	
 		return ($query->num_rows() > 0)?$query->result_array():array();		
		 
	}
	/* Fetch All Guest Data */
	public function getGuestUserFilterData()	{
 		$this->db->select('gu.* ,  sb1.category_name ,sb.sub_cat_name'); 
		$this->db->order_by('user_id', 'DESC');
		$this->db->distinct('gu.hotel_name');
		$this->db->from('guest_user as gu');
		$this->db->join('sub_category as sb','gu.hotel_name=sb.sub_cat_id','inner');		
		$this->db->join('category as sb1','gu.hotel_branch_name=sb1.cat_id','inner');
		$this->db->where('gu.owner_id',$this->session->userdata('sub_login_id'));		
 		$query = $this->db->get();	
 		return ($query->num_rows() > 0)?$query->result_array():array();		
		 
	}


	public function fetchGuestUserDataListAjax($post,$count=false)	{		 
		$where=array();		
		//$change_package=$post['package_id'];
  		$user_id=$post['user_id']; 
  		
	 
		/*if($change_package>0){
			$where['spm.subscriber_package_id']=$change_package;
		}*/
		if($user_id>0){
			$where['gu.user_id']=$user_id;
		}
		
		//$where['gu.status']=0; 

		$this->db->select('gu.* ,  sb1.category_name ,sb.sub_cat_name'); 
		$this->db->order_by('gu.user_id', 'DESC');
		$this->db->from('guest_user as gu');
		$this->db->join('sub_category as sb','gu.hotel_name=sb.sub_cat_id','inner');		
		$this->db->join('category as sb1','gu.hotel_branch_name=sb1.cat_id','inner');
		$this->db->where('gu.owner_id',$this->session->userdata('sub_login_id'));	
	
		if(count($where)>0)		{
			$this->db->where($where);
		}
			if ($post['order'][0]['column']!='' && isset($post['order'][0]['dir']) && !empty($post['order'][0]['dir'])) {
            $order_by = '';
            switch ($post['order'][0]['column']) {
               
				case 2:
                $order_by = 'gu.user_name';
                break;
			
                default:
                $order_by = 'gu.user_id';
                break;
            }
             
            $dir_by = '';
            switch ($post['order'][0]['dir']) {
                
                case 'asc':
                    $dir_by = 'asc';
                    break;
                case 'desc':
                    $dir_by = 'desc';
                    break;
                default:
                   $dir_by = 'asc';
                    break;
            }
            $this->db->order_by($order_by,$dir_by); 
        }

        if(!$count){
			$start=$post['start'];
			$length=$post['length'];
			if(!$start){
				$start=0;
			}
			if(!$length){
				$length=10;
			}
			$this->db->limit($length,$start);
		}		
 		
		$result = $this->db->get();
 		$resultArray = $result->result_array();	
     	if($count){
				return $result->num_rows();
			}else{
 				return $resultArray;
			}
	}


	/* Fetch All Guest Data By Id */
	public function getGuestDataById($id){
		$this->db->from('guest_user');	
		$this->db->where('user_id',$id);
		$query = $this->db->get();
		if($query){
			return $query->result_array();
		}
		else{  
			return false;
		}
	}

	/* Update Guest Data */
	public function updateGuestUser($data,$id)	{
		$this->db->where('user_id',$id);
		return $this->db->update('guest_user',$data);
	}

	/* Delete Guest Data */
	public function deleteGuestUser($id)	{
		$this->db->where('user_id',$id);
		return $this->db->delete('guest_user');
	}

 /************************************************ Guest User End ************************************************************/


 /************************************************ Score Start ************************************************************/
		/* Insert The Score Data */
	public function insertScore($data)	{
		return $this->db->insert('score_name',$data);
	}

	 /* Fetch The Score Data */	 
	public function fetchScoreData()	{
		$this->db->select('*'); 
		$this->db->from('score_name');
		$query = $this->db->get();		
		if($query){
			return $query->result();
		}
		else{
			return false;
		}
	}

	/* Fetch The Score Data By Id */
	public function getScoreDataById($id){
		$this->db->from('score_name');		
		$this->db->where('score_id',$id);
		$query = $this->db->get();
		if($query){
			return $query->result_array();
		}
		else{  
			return false;
		}
	}

		/* Update The Score Data */
	public function UpdateScore($data,$id)	{
		$this->db->where('score_id',$id);
		return $this->db->update('score_name',$data);
	}

	  /* Delete The Score Data */
	public function deleteScoreData($id)	{
		$this->db->where('score_id',$id);
		return $this->db->delete('score_name');
	}



 /************************************************ Score End ************************************************************/
 	/*SELECT AVG(business_questionnaire_answer.bus_que_score) FROM `customer_feedback` JOIN business_questionnaire_answer ON customer_feedback.cf_score = business_questionnaire_answer.bus_que_ans_id where busId = 106*/

 	public function fetchFeedback()	{
		$this->db->select('cf.*,gu.*,bqa.*,c.category_name,sc.sub_cat_name,ssc.sub_sub_cat_name,q.bus_q_que,sl.sub_name');
		$this->db->from('customer_feedback as cf');
		$this->db->order_by('cf_id', 'DESC');
		$this->db->join('guest_user as gu','cf.cf_user_id=gu.user_id','left');
		$this->db->join('questionnaire as q','cf.cf_que_id=q.bus_qid','left');
		$this->db->join('business_questionnaire_answer as bqa','cf.cf_score=bqa.bus_que_ans_id','left');
		$this->db->join('category as c','cf.cf_cat_id=c.cat_id','left');
		$this->db->join('sub_category as sc','cf.cf_cat_sub_id=sc.sub_cat_id','left');
		$this->db->join('sub_sub_category as ssc','cf.cf_cat_process_id=ssc.sub_sub_cat_id','left');
		$this->db->join('subscriberlogin as sl','cf.busId=sl.sub_login_id','left');
		
		$query = $this->db->get();
		$result=$query->result();
		if ($result==true) {
			//echo "<pre/>"; print_r($query->result()); die;
			return $query->result();
		} else {
			return false;
		}		
	}

/*	public function companyAVGScore()
	{
		$this->db->select("customer_feedback.busId ,subscriberlogin.sub_name , AVG(business_questionnaire_answer.bus_que_score) as avgrage_score");
		$this->db->from('customer_feedback as cf');
		$this->db->join('business_questionnaire_answer as bqa','cf.cf_score=bqa.bus_que_ans_id','left');
		$this->db->join('subscriberlogin as sl','sl.sub_login_id = cf.busId','left');
		$this->order_by('cf.busId');
		$query = $this->db->get();
		$result=$query->result();
		if ($result) {
 			return $query->result_array();
		} else {
			return false;
		}

		/*$select = "SELECT customer_feedback.busId ,subscriberlogin.sub_name , AVG(business_questionnaire_answer.bus_que_score) FROM `customer_feedback` 
		JOIN business_questionnaire_answer ON customer_feedback.cf_score = business_questionnaire_answer.bus_que_ans_id 
		JOIN subscriberlogin ON subscriberlogin.sub_login_id = customer_feedback.busId
		GROUP by customer_feedback.busId");
		
		
	}
	*/

	/*SELECT customer_feedback.busId ,subscriberlogin.sub_name , AVG(business_questionnaire_answer.bus_que_score) 
	FROM `customer_feedback` 
	JOIN business_questionnaire_answer ON customer_feedback.cf_score = business_questionnaire_answer.bus_que_ans_id 
	JOIN subscriberlogin ON subscriberlogin.sub_login_id = customer_feedback.busId
	where busId = 106*/


}