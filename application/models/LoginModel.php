<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/*
 Model : Login Model 
*/
 
 class LoginModel extends CI_Model
 {
  public function __construct() 
  {
    parent::__construct();
  }
  var $subscriberlogin = 'subscriberlogin';
  var $subscriberroles = 'subscriberroles';
  var $subscriberpackagemapping = 'subscriberpackagemapping';
  
  /*   front Login    */
  public function checkfrontLogin($loginData)
  { 
    $this->db->select('sl.*, slr.*,c.category_name');
    //$this->db->select('sl.*, slr.*,per.*');
    $this->db->from('subscriberlogin as sl');
    $this->db->join('subscriberroles as slr', 'sl.sub_login_role = slr.subscriber_roll_id', 'left');
    $this->db->join('category as c', 'sl.category = c.cat_id', 'left');
    $this->db->where('sub_email', $loginData['uEmail']);
    $this->db->where('sub_pass', $loginData['uPass']);  
    $query=$this->db->get();
    $result= $query->row();
   // if($result == true){
    if( $query->num_rows() > 0){
      if($result->sub_isActive==1){
        return array('status' =>true, 'response'=>'succeess','message'=>'','data'=>$query->row() );
      }else{  
        return array('status' =>false, 'response'=>'inactive','message'=>'Your account is waiting for the admin approval','data'=>array() );
      }
    }else{
      return array('status' =>false, 'response'=>'invalid','message'=>'Invalid credentials','data'=>array() );
    }
        /*}else{   
            return array('status' =>'false', 'response'=>'invalid','message'=>'Invalid credentials','data'=>'' );
          }  */    
        }
        public function companyRegister($company_registerData)
    {      //echo "<pre/>"; print_r($company_registerData['category']);die;
    $this->db->insert('subscriberlogin', $company_registerData);
    $insert_id = $this->db->insert_id();
    if ($insert_id > 0 ){ 
      $package_end_date= date('m-d-Y', strtotime('+364 days') );
      
      $companyPackageMap =array(
        'subscriber_id'=>$insert_id,
        'subscriber_package_id'=>$this->input->post('package'),    
        'subscriber_start_date'=>date('m-d-Y'),
        'subscriber_end_date'=>$package_end_date
      );     

      return $this->db->insert('subscriberpackagemapping', $companyPackageMap);
    } else{
      return false;
    }  
  }


  //checks ajax requests
  public function check_user_exist($usr)   {
    $this->db->where("uEmail",$usr);
    $query=$this->db->get("users");
    $this->db->where('status',1);   
    if($query->num_rows()>0){
      return true;
    }else{
      return false;
    }
  }

  //Admin Forgot Password
  public function file_admin_exists($forgotemail){
   $this->db->select('*'); 
   $this->db->from('admin');
   $this->db->where('emailId', $forgotemail);
   $query = $this->db->get();
   if ($query) {
     return $query->result();
   } else {
     return false;
   }
 }

 /* Registration Dropdown */

 function fetch_packge_demo(){
  $this->db->select('*'); 
  $this->db->order_by("package_id", "ASC");
  $query = $this->db->get("packages");
  return $query->result();
}

/*     public function ForgotPassword($email)
     {
            $this->db->select('sub_email,sub_pass');
            $this->db->from('subscriberlogin'); 
            $this->db->where('sub_email', $email); 
            $query=$this->db->get();
            return $query->row();
     }
*/
       //Frontend Forgot Password
     public function filename_exists($forgotemail)  {
       $this->db->select('*'); 
       $this->db->from('subscriberlogin');
       $this->db->where('sub_email', $forgotemail);
       $query = $this->db->get();
       if ($query) {
         return $query->result();
       } else {
         return false;
       }
     }

     public function sendpassword($findemail)
     {
      //$email = 'srawat@panindia.in';
      //echo  $findemail;die;
      $query1=$this->db->query("SELECT *  from subscriberlogin where sub_email = '".$findemail."' ");

      $row=$query1->row(); 

      //echo("<pre/>");print_r($row);die;

      $user_email=$row->sub_email;

       //print_r($user_email);
      if((!strcmp($email, $user_email))){
       $pass=$row->sub_pass; 
       $name=$row->fname;
       $name=ucfirst($name);
                    // print_r($pass);die;
                     //Mail Code

       $config = Array(
         'protocol'  => 'smtp',
         'smtp_host' => 'ssl://smtp.googlemail.com',
         'smtp_port' => 465,
         'smtp_user' => 'mastcrew22@gmail.com', 
         'smtp_pass' => 'crew@1234', 
         'mailtype'  => 'html',
         'charset'   => 'iso-8859-1'
       );
       $mailMsg='<!DOCTYPE html>
       <html>
       <head>
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>Wageni Newsletter</title>
       <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
       </head>
       <body style="background-color: #f1f1f1; font-family: raleway, sans-serif; font-size: 15px; margin: 0; padding: 0;">
       <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f1f1f1; font-family: "raleway", sans-serif; font-size: 15px;">
       <tr>
       <td align="center">
       <table border="0" cellpadding="10" cellspacing="0" width="600" align="center">
       <tr>
       <td align="center">
       <table border="0" cellpadding="15" cellspacing="0" width="600" style="background: transparent;">
       <tr>
       <td align="center"><a href=""><img src="http://wageni.us.tempcloudsite.com/globalassets/admin/logo1.png"></a></td>
       </tr>
       </table>
       <table border="0" cellpadding="10" cellspacing="10" width="600" style="background: #fff;">
       <tr>
       <td align="center" style="font-size: 36px; font-weight: bold; color: #555; letter-spacing: 1px;">Welcome to Wageni CRM.</td>
       </tr>
       <tr>
       <td>Dear '.$name.'</td>
       </tr>
       <tr>
       <td>Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>'.$pass.'</b></td>
       </tr>
       <tr><td>Thanks & Regards</td>
       </tr>
       <tr><td><b>Wageni CRM Team</b></td>
       </tr>
       </table>
       <table border="0" cellpadding="15" cellspacing="0" width="600" style="background: transparent;">
       <tr>
       <td align="center">Copyright &copy; 2018 <a href="http://wageni.us.tempcloudsite.com">wagenicrm.com</a>. All rights reserved.</td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
       </body>
       </html>';   

       $this->load->library('email', $config);
       $this->email->set_newline("\r\n");
       $this->email->from('srawat@panindia.in'); 
       $this->email->to($user_email);
       $this->email->subject('Password');
       $this->email->message($mailMsg);
       if($this->email->send()){
                          //echo $this->email->print_debugger();die;                
        return 'success';
      }
      
    }
    else
    {
      return "error";
    }
    

  }



}