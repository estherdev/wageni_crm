<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : SubsControler (UserController)
 * SubsControler Class to control all Website SubsControler related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 14 August 2018
 */
class Score extends BaseController
{ 
    public function __construct()
    {
        parent::__construct(); 
        //$this->loadSubSidebar();
        $this->load->model('AuthModel'); 
    }
      public function index()
    {	
		//$this->global['title'] = "Add Feedback";
		$this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
		$this->form_validation->set_rules('score_name', 'score_name', 'required|strip_tags|xss_clean');
		

		 if ($this->form_validation->run() == true){
		 	 $data=array(
            'score_name' => trim($this->input->post('score_name')));
		 	 //echo("<pre/>");print_r($data);die;
		 	 $result= $this->AuthModel->insertScore($data);
		 	 //echo("<pre/>");print_r($data);die;
		 	 if($result==TRUE){
		 	 	$this->session->set_flashdata('success',' Inserted Successfully.');
		 	 	redirect('website/Score','refresh');
		 	 }

		 }
		 else
		 {
		 	$error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);
            $this->global['allscore']= $this->AuthModel->fetchScoreData();
            //echo("<pre/>");print_r($this->global['allscore']);die;         
            $this->load->view('admin/guestuser/addScore',$this->global);
            } 
		 }

		 public function editScoreData($id)
		 {
		 	$this->global['records'] = $this->AuthModel->getScoreDataById($id);
			//echo("<pre/>"); print_r($this->global['records']);die;
			$this->load->view('admin/guestuser/editScore',$this->global);
		 }

	  public function updateScore($id)
		    {	
				//$this->global['title'] = "Add Feedback";
				$this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
				$this->form_validation->set_rules('score_name', 'score_name', 'required|strip_tags|xss_clean');
				

				 if ($this->form_validation->run() == true){
				 	 $data=array(
				 	'score_id'=> $id,
		            'score_name' => trim($this->input->post('score_name')));
				 	 //echo("<pre/>");print_r($data);die;
				 	 $result= $this->AuthModel->UpdateScore($data,$id);
				 	 //echo("<pre/>");print_r($data);die;
				 	 if($result==TRUE){
				 	 	$this->session->set_flashdata('success',' Updated Successfully.');
				 	 	redirect('website/Score','refresh');
				 	 }

				 }
				 else
				 {
				 	$error = validation_errors();
		            $this->session->set_flashdata('validationerrormsg',$error);
		            $this->global['allscore']= $this->AuthModel->fetchScoreData();
		            //echo("<pre/>");print_r($this->global['allscore']);die;         
		            $this->load->view('admin/guestuser/addScore',$this->global);
		            } 
				 }

	 public function deleteScore($id)
		 {
			$result= $this->AuthModel->deleteScoreData($id);
		   if ($result == true) {
			$this->session->set_flashdata('success', 'Successfully Deleted!');
			redirect('website/Score', 'refresh');
			}else{
			$this->session->set_flashdata('success', 'Failed!');
			redirect('website/Score', 'refresh');
		}
	}
}