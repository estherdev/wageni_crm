<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

/*Sun Admin*/

class Company extends BaseController{
    public function __construct() {
        parent::__construct();
        $this->load->model(array('All_company_model' => 'ACM','AuthModel' => 'AUTHM', 'PackageModel' => 'PM', 'MastersModel' => 'MM'));
    }

      public function complaints($id){
          if($id){
    				$data['nps_main_id_']=$id;
    			}else{
    				$data['nps_main_id_']=$id;
    			}
          	//$data['complaints'] =	$this->MM->adminGetAllComplaintsAjax();

          	$data['complaints'] =	$this->ACM->subadminGetAllComplaintsAjax($id);
          	//prd($data);
            $data['customers']  =	$this->ACM->companyList();
            $data['category'] 	=  	$this->MM->fetchCategory(); 
            $this->load->view("webadmin/complaints/complaints", $data); 
      }

      public function complaintsListByUser($id){          
          if($id){
    				$data['nps_main_id']=$id;
    			}else{
    				$data['nps_main_id']=$id;
    			}

			   $data['complaints']=$this->MM->adminGetAllComplaintsAjax();
         // $this->load->view("webadmin/complaints/complaints-list", $data); 
         $this->load->view("webadmin/complaints/complaints", $data); 
      }

        public function viewAllComplaintsList(){
            if ($this->input->is_ajax_request()) { 
                  
                  $data = $this->ACM->adminGetAllComplaintsAjax($_POST,false);
                  $totalData = $this->ACM->adminGetAllComplaintsAjax($_POST,true);
                  $response=array();
                     
                      if(count($data)>0){
                          foreach($data as $row){
                            $difference_day = difference_day($row['cf_time']);
                            $userrole= $this->session->userdata('userrole');
                              if(intval($difference_day) < '1440' && $userrole==3){//Superviser
                                  $response[]=$this->genrateNpsRowbyId($row);
                              }
                              elseif(intval($difference_day) < '2880' && $userrole==4){//Manger
                                  $response[]=$this->genrateNpsRowbyId($row);
                              }
                              elseif($userrole==2 || $userrole==1){//G Manger
                                   $response[]=$this->genrateNpsRowbyId($row);
                              }
                          }
                      }
                      $json_data = array(
                      "recordsTotal"    => $totalData,  
                      "recordsFiltered" => $totalData,
                      "data"            => $response 
              );
              echo json_encode($json_data);
            }
          }
      
      public function viewAllCompanyList(){  
            if ($this->input->is_ajax_request()) {
                  $data = $this->ACM->companyListAjax($_POST,false);
                  $totalData = $this->ACM->companyListAjax($_POST,true);
                  $response=array();
                      if(count($data)>0){
                          foreach($data as $row){     
                          $score = number_format($row['score'], 2);                       
                              $response[]=array(
                                  'id'=>$row['sub_login_id'],
                                  'name'=>ucfirst($row['fname'].' '.$row['lname']),
                                  'category_name'=>ucfirst($row['category_name']),
                                  'sub_name'=>ucfirst($row['sub_name']),
                                  'package_name'=>ucfirst($row['package_name']),
                                  //'price'=> $row['package_price'].' '.$this->config->item('krw'),
                                  'price'=> $row['package_price'],
                                  'score'=> $row['score'] ? number_format($row['score'], 2) : 0.0,
                                  'expiredate'=>ucfirst($row['subscriber_end_date']),
                                  'email'=>$row['sub_email'],
                                  'status'=>$row['sub_isActive'],
                                  'image'=>$row['userfile'],
                                  'all'=>$row,
                                  'company_url'=>$row['company_url']
                              );
                          }
                      }
                      $json_data = array(
                      "recordsTotal"    => $totalData,  
                      "recordsFiltered" => $totalData,
                      "data"            => $response 
              );
                echo json_encode($json_data);
           }       
        }

    public function changeCompanyStatus(){
      if ($this->input->is_ajax_request()) {
        $id=$this->input->post('id');
        $status=$this->input->post('status');
        if($id>0){
          $update_data=array('id'=>$id,'status'=>$status);
          //$response['status']=$this->ACM->changeDataStatus('subscriberlogin',$update_data);
          if($response['status']){
            $response['message']="Company Status Updated Successfully!";        
           }
          $data=array('status'=>1);
          echo json_encode($data);
        }
      }
    }

    public function adminCompanyUsersDetailsAjax(){
     if ($this->input->is_ajax_request()) {       
        $id=$this->input->post('id');
        if($id>0){
            $data['companyListAdmin'] = $this->ACM->getCompanyListAdmin($id);
            if(count($data)>0){
               $response['data'] =$this->load->view("webadmin/allusers/all_company_users", $data,true);      
               echo json_encode($response);   
            }
        }
      }
    }

    public function adminUpdateCompanyAjax($id){
      $this->isSuperAdmin();
      $data['companydata']= $this->ACM->fetchCompanyBYId($id);
      $data['role'] = $this->ACM->fetchSubscribersRole();                     
      $data['package'] = $this->PM->fetchPackage(); 
      $data['category'] = $this->MM->fetchCategory(); 
      $this->load->view("webadmin/allusers/update_company", $data); 
    }
    /* public function updateCompany($id){
        $data['companydata'] = $this->ACM->fetchCompanyBYId($id);
         $this->load->view("admin/allusers/update_company", $data);  
     }*/
       public function allQuestionsList(){
        $id=$this->session->userdata('sub_login_id');
        $category=$this->session->userdata('category');
            $data['questions']=$this->MM->allQuestionsList($id,$category);
             $this->load->view("webadmin/serve/survey-questions-list", $data); 
        } 

       public function allQuestionsDetailsAjax(){            
          if ($this->input->is_ajax_request()) {
               $data = $this->MM->allQuestionsListAjax($_POST,false);// prd($data);
                $totalData = $this->MM->allQuestionsListAjax($_POST,true);
                  $response=array();
                  if(count($data)>0){
                      foreach($data as $row){ 
                      // echo "sub-admin"; pr($row);    
                           $response[]=array(
                              'ownerid'=>$row['businessId'],
                              'id'=> $row['bus_qid'],
                              'cat_id'=>$row['bus_q_cat_id'],
                              'category_name'=>ucfirst($row['category_name']),
                              'SUB_cat_id'=>$row['bus_q_sub_cat_id'],
                              'sub_cat_name'=>ucfirst($row['sub_cat_name']),
                              'process_cat_id'=>$row['bus_cat_process'],
                              'sub_sub_cat_name'=>ucfirst($row['sub_sub_cat_name']),
                              'que'=>$row['bus_q_que'],
                              'q_type'=>$row['bus_q_type'],
                              'all'=>$row
                           );
                      }
                  }
                  $json_data = array(
                  "recordsTotal"    => $totalData,  
                  "recordsFiltered" => $totalData,
                  "data"            => $response 
          );
              echo json_encode($json_data);
         }     
      }


            //adminDeleteQuestion
  function subadminDeleteQuestion(){
    if ($this->input->is_ajax_request()) {
      $id=$this->input->post('action_id');
      if(!empty($id)){
         $response['status']=$this->MM->subdeleteRecord($id,'questionnaire');
        if($response['status']){
          if($response['status']){
            $response['message']="Question Deleted Successfully!";            
           }        
        }
         echo json_encode($response);
      }
    }
  }


     public function viewallQuestionsDetailsAjax(){
       if ($this->input->is_ajax_request()) {         
          $id=$this->input->post('id');
          if($id>0){
              $data['questionsDetails'] = $this->MM->getquestionnaireOptionList($id);
              if(count($data)>0){         
                  $response['data'] =$this->load->view("webadmin/serve/survey-questions-details-list", $data,true);      
                  echo json_encode($response);   
              }
          }
        }
      }

     public function saveQuestionOptionsAjax(){
       if ($this->input->is_ajax_request()) {
           $sr = $this->input->post('sr');
            $response=array();
            if(count($sr)>0){            
              foreach($sr as $key){
                $option_data=array(
                  'bus_que_type'=>$this->input->post('optionid'.$key),
                  'businessId'=>$this->session->userdata('sub_login_id'),
                  'bus_que_id'=>$this->input->post('queid'.$key),
                  'bus_que_score'=>$this->input->post('score_name'.$key)
                );
                $response['id']=$this->MM->quesOptionUpdate('business_questionnaire_answer',$option_data);
              }
              if($response['id']>0){ 
                echo json_encode($response);
              }




              ///*$inserto=$this->MM->quesOptionUpdate('business_questionnaire_answer',$option_data);
         /*     if($insert>0){
                
                    $return['option']=$this->MM->selectrow('business_questionnaire_answer',$insert['id']);
                   
                      $where = array(
                          'businessId' =>$return['option']['businessId'],
                          'bus_que_id'=>$return['option']['bus_que_id']
                      );

                    $alloption['alloption']=$this->MM->selectall('business_questionnaire_answer',$where);
                   prd($alloption);
              }*/


            }
          }
      }

    public function complaintsListNetPromterScore(){
        $data['complaints']=$this->MM->complaintsListNetPromterScore();
        $this->load->view("webadmin/complaints/complaints-list", $data); 
    }  
    
    public function complaintsListNetPromterScoreAjax(){            
        if ($this->input->is_ajax_request()) {
              $data = $this->MM->complaintsListNetPromterScoreAjax($_POST,false); prd($data);
              $totalData = $this->MM->complaintsListNetPromterScoreAjax($_POST,true);
              $response=array();
                if(count($data)>0){
                    foreach($data as $row){  
                      $day = difference_day($row['added_time']);                           
                      $userrole= $this->session->userdata('userrole');
                        if(intval($day) < '1440' && $userrole==3){//Superviser
                            $response[]=$this->genrateNpsRow($row);
                        }
                        elseif(intval($day) < '2880' && $userrole==4){//Manger
                            $response[]=$this->genrateNpsRow($row);
                        }
                        elseif($userrole==2 || $userrole==1){//G Manger
                             $response[]=$this->genrateNpsRow($row);
                        }                             
                    }
                }
                $json_data = array(
                "recordsTotal"    => $totalData,  
                "recordsFiltered" => $totalData,
                "data"            => $response 
        );
        echo json_encode($json_data);
         }     
    }  


  public function genrateNpsRow($row){
      return array(
          'busid'             =>  $row['busid'],
          'id'                =>  $row['id'],
          'user_id'           =>  $row['user_id'],
          'user_name'         =>  ucfirst($row['user_name']),
          'cat_id'            =>  $row['cat_id'],
          'sub_cat_id'        =>  $row['sub_cat_id'],
          'process_cat_id'    =>  $row['process_id'],
          'added_time'        =>  DMYTime($row['added_time']),
          'category_name'     =>  ucfirst($row['category_name']),
          'sub_cat_name'      =>  ucfirst($row['sub_cat_name']),
          'sub_sub_cat_name'  =>  ucfirst($row['sub_sub_cat_name']),
          //'npscore'           =>  1,//$this->config->item('netpscore')[$row['npscore']],
          'npscore'           =>  $row['npscore'],
           'all'               =>  $row
       );

  }

  public function genrateNpsRowbyId($row){ 

    return array(
        'id'=>$row['cf_id'],
        'name'=>ucfirst($row['user_name']),
        'category_name'=>ucfirst($row['category_name']),
        'question'=>$row['bus_q_que'],
        'answer'=>$row['bus_que_type'],
        'cf_status'=>$row['cf_status'],
        'frsid'=>$row['frsid'],
        'frsname'=>$row['frsname'],
        'busid'=>ucfirst($row['fname']),
        'status'=>complaintsstatus($row['cf_status']),
        'complaints'=>complaintsdate($row['bus_que_score']),
        'assign'=>complaintsassign($row['cf_time']),
        'nps_main_id'=>$row['nps_main_id'],
        'npscatid'=>$row['npscatid'],
        'npssubcatid'=>$row['npssubcatid'],
        'npsprocessid'=>$row['npsprocessid'],
        'npsbusid'=>$row['npsbusid'],
        'npsscore'=>$row['npsscore'],
        'npsadded_time'=>$row['npsadded_time'],
        'complaintstime'=>'<i class="fa fa-clock-o fa-1x">  ' . elapsedtime($row['cf_time']). " Ago </i>",
        'all'=>$row
     );
  }
            

}