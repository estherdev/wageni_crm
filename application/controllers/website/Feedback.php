<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : SubsControler (UserController)
 * SubsControler Class to control all Website SubsControler related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 14 August 2018
 */
class Feedback extends BaseController
{ 
    public function __construct(){
        parent::__construct(); 
         //$this->loadSubSidebar();
           $this->load->model(array('All_company_model' => 'ACM','AuthModel' => 'AUTHM',  'PackageModel' => 'PM', 'MastersModel' => 'MM'));

    }

    public function gusestUserList(){
        $data['alluser']= $this->AUTHM->fetchGuestUserData();
        $data['alluserdata']= $this->AUTHM->getGuestUserFilterData();
        //echo("<pre/>");print_r($data);die;         
        $this->load->view('webadmin/guestuser/addGuestUser',$data);
    }



        public function sendNotification(){
            if ($this->input->is_ajax_request()) {
                $response=array();
                $id=$this->input->post('id');
                $status=$this->input->post('status');
                $updatestatus='';
                $stay='';

                $pre_stay=01; $post_stay=03; $on_stay=02;

    

                if ($status==$pre_stay) {
				    $updatestatus=1;
                    $stay='pre_stay';
                    $mail_status=$pre_stay;
                    $staysubject='Pre Stay';
				} elseif ($status==$post_stay) {
				    $updatestatus=1;
                    $stay='post_stay';
                    $mail_status=$post_stay;
                    $staysubject='Post Stay';
				} else {
				    $updatestatus=1;
                    $stay='on_stay';
                    $mail_status=$on_stay;
                    $staysubject='On Stay';
				}


              $data=array();
              $response['status']=false;
              $response['message']="There are an error in sending notification";
             
                if ($id>0){
                    //$this->db->where('gu.owner_id',$this->session->userdata('sub_login_id'));   
                $where=array('gu.user_id'=>$id);
                $guestinfo=$this->AUTHM->guestUserDetails($where);

                if(count($guestinfo)>0){
                   
                    $update_data=array('user_id'=>$id,$stay => $updatestatus,'mail_status'=>$mail_status);
                    $result= $this->AUTHM->AddUpdateData('guest_user',$update_data);
                    
                    if ($result) {
                        $where=array('gu.user_id'=>$id);
                        $data['info'] = $this->AUTHM->guestUserDetails($where);                    
                        $to = $data['info']['user_email_id'];
                        $data['name'] = $data['info']['user_name'];
                        
                        $q_owner_id = $data['info']['owner_id'];
                        $q_hotel_id = $data['info']['hotel_name'];
                        $q_process  = $data['info']['process'];
                        

                        $where = array(
						  			'q.businessId'=>$q_owner_id,
						  			'q.bus_q_cat_id'=>$this->session->userdata('category'),
								  	'q.bus_q_sub_cat_id'=>$q_hotel_id,
								  	'sb.sub_sub_cat_name'=>$staysubject
								  ); 			 
         
		  				$data['alluser']= $this->AUTHM->getProcess($where);
		  				//echo "<pre/>"; print_r($data); die;

                        $data['quetypename']=base64_encode($stay);
                        $data['email1']=base64_encode($data['info']['user_email_id']);
                        $mail_data =  $this->load->view('email_templates/survey-mail-link', $data,true);
                                 
                        $subject = 'Wageni CRM '.$staysubject;
         
                        $sent = sendEmail($to,'',$subject,$mail_data);
                        if($sent){

                             $response['status']=true;
                             $response['message']="Mail Sent successfully sent to the Guest";
                            
                        }
                    }
                  }               
              }
              echo json_encode($response);
            }
        } 


        public function viewAllGusestUserList(){  
          if ($this->input->is_ajax_request()) {
                 $data = $this->AUTHM->fetchGuestUserDataListAjax($_POST,false);
                  $totalData = $this->AUTHM->fetchGuestUserDataListAjax($_POST,true);
           // echo("<pre/>");print_r($data);die;    

                    $response=array();

                    $pre_stay=01; $post_stay=03; $on_stay=02;
                   
                    if(count($data)>0){
                        foreach($data as $row){   
                        
                            $response[]=array(
                                'id'=>$row['user_id'],
                                'name'=>ucfirst($row['user_name']),
                                'category_name'=>ucfirst($row['category_name']),
                                'hotel_name'=>$row['hotel_name'],
                                'process'=>$row['process'],
                                'email'=>$row['user_email_id'],
                                'no_of_adult'=>$row['no_of_adult'],
                                'no_of_children'=>$row['no_of_children'],
                                'no_of_room'=>$row['no_of_room'],
                                'room_no'=>$row['room_no'],
                                'check_in'=>$row['check_in'],
                                'check_out'=>$row['check_out'],
                                'check_in_time'=>$row['check_in_time'],
                                'check_out_time'=>$row['check_out_time'],
                                'status'=>$row['status'],
                                'pre_stay'=>$row['pre_stay'],
                                'post_stay'=>$row['post_stay'],
                                'on_stay'=>$row['on_stay'],
                                'pre_stay_key'=>$pre_stay,
                                'post_stay_key'=>$post_stay,
                                'on_stay_key'=>$on_stay,
                                'category_name'=>ucfirst($row['category_name']),
                                'sub_cat_name'=>ucfirst($row['sub_cat_name']),
                                'all'=>$row
                             );
                        }
                    }

                    $json_data = array(
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalData,
                    "data"            => $response 
            );
            echo json_encode($json_data);
             }                              
        }

	public function redirect_Guest(){
		redirect('guestuser','refresh');
	}

    public function fetchFeedback() {
         $data['feedback']= $this->AUTHM->fetchFeedback();
         //$data['feedback'] = $this->AUTHM->fetchSubscriber();  
         //echo "<pre/>";print_r($data);die;
         $this->load->view('admin/serve/view-all-review',$data);
    }

	/* Fetch Guest User Data  By Id*/
	public function getGuestData($id){ 	
        $this->isLogin();
        $id= base64_decode(urldecode($id));   
		$this->global['records'] = $this->AUTHM->getGuestDataById($id);
		$this->load->view('admin/guestuser/editGuestUser',$this->global);
	}

		/* Update Guest User Data */
	public function editGuestUser($id)	{	
        $this->isLogin();
		$this->global['title'] = "Add Feedback";
		$this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
		//$this->form_validation->set_rules('hotel_name', 'hotel_name', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('user_name', 'user_name', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('no_of_adult', 'no_of_adult', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('no_of_children', 'no_of_children', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('no_of_room', 'no_of_room', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('check_in', 'check_in', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('check_out', 'check_out', 'required|strip_tags|xss_clean');

		 if ($this->form_validation->run() == true){
		 	 $data=array(
			 	'user_id'		 =>  $id,
	            'hotel_name' 	 =>  $this->input->post('hotel_name'),
	            'user_name'  	 =>  $this->input->post('user_name'),
	            'no_of_adult'	 =>  $this->input->post('no_of_adult'),
	            'no_of_children' =>  $this->input->post('no_of_children'),
	            'no_of_room' 	 =>  $this->input->post('no_of_room'),
	            'check_in'		 =>  $this->input->post('check_in'),
	            'check_out' 	 =>  $this->input->post('check_out')
	        );	
		 	 $result= $this->AUTHM->updateGuestUser($data,$id);
		 	 if($result==TRUE){
		 	 	$this->session->set_flashdata('success',' Updated Successfully.');
		 	 	$this->redirect_Guest();
		 	 }
		 }
		 else
		 {
		 	$error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);
           // $this->load->view('admin/guestuser/editGuestUser',$this->global);
            } 
		 }

		 /* Delete Guest Data  */
	public function deleteGuestUser($id){  
        $this->isLogin();
		$result= $this->AUTHM->deleteGuestUser($id);
		if ($result == true){
			$this->session->set_flashdata('success', 'Successfully Deleted!');
			$this->redirect_Guest();
			}else{
			$this->session->set_flashdata('success', 'Failed!');
			$this->redirect_Guest();
		}
	}

    public function Dummy()    {   
        $this->isLogin();
        $this->load->view('admin/guestuser/dummy');       
    }

	public function index()    {   
        $this->isLogin();
        $type_error='';
        $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
		//$this->form_validation->set_rules('hotel_name', 'hotel_name', 'required|strip_tags|xss_clean|callback_select_Hotel');
		$this->form_validation->set_rules('user_name', 'user_name', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('user_id', 'user_id', 'required|valid_email|xss_clean|is_unique[guest_user.user_email_id]');
		$this->form_validation->set_rules('no_of_adult', 'no_of_adult', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('no_of_children', 'no_of_children', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('no_of_room', 'no_of_room', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('check_in', 'check_in', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('check_out', 'check_out', 'required|strip_tags|xss_clean');
        /*echo $p=random_string('alnum', 16);
        echo "</br>";*/
        $token=random_string('sha1', 16);
        //$pass="123456";
        if($this->form_validation->run()){

           $selectdata=array(
            'category'       =>   $this->session->userdata('category'),
            'owner_id'       =>   $this->session->userdata('sub_login_id'),
            'subcat'         =>   $this->input->post('subcat')
            );
            
            $fetchhotelprocess= $this->AUTHM->fetchhotelprocess($selectdata);                 
            $process= $fetchhotelprocess[0]['sub_sub_cat_id']?$fetchhotelprocess[0]['sub_sub_cat_id']:0;
             $check_in   =   date('Y-m-d',strtotime($this->input->post('check_in')));
            $check_out  =   date('Y-m-d',strtotime($this->input->post('check_out')));

            $data=array(
                'hotel_branch_name'   =>  $this->session->userdata('category'),
                'owner_id'            =>  $this->session->userdata('sub_login_id'),
                'hotel_name' 	      =>  trim($this->input->post('subcat')),
                //'process'             =>  trim($this->input->post('subsubcat')),
                'process'             =>  trim($process),
                'user_name'  	      =>  trim($this->input->post('user_name')),
                'user_email_id'       =>  trim($this->input->post('user_id')),
                'no_of_adult'	      =>  trim($this->input->post('no_of_adult')),
                'no_of_children'      =>  trim($this->input->post('no_of_children')),
                'no_of_room' 	      =>  trim($this->input->post('no_of_room')),
                'room_no'             =>  trim($this->input->post('room_no')),
                'user_phone'          =>  trim($this->input->post('user_phone')),
                'user_address'        =>  trim($this->input->post('user_address')),
                'user_nationality'    =>  trim($this->input->post('user_nationality')),
                'usertoken'		      =>  random_string('sha1', 16),
                'userip'              =>  $_SERVER['REMOTE_ADDR'],
                'check_in'            =>  $check_in,
                'check_out' 	      =>  $check_out,
                'check_in_time'       =>  trim($this->input->post('check_in_time')),
                'check_out_time'      =>  trim($this->input->post('check_out_time'))
            );
              // echo '<pre/>'; print_r($data); die;
                 $this->session->set_userdata('user_name',$data['user_name']);
                 $this->session->set_userdata('usertoken',$data['usertoken']);
                 $this->session->set_userdata('user_email_id',$data['user_email_id']);
                 $this->session->set_userdata('encode_user_email_id',$data['user_email_id']);
                       
                $name=ucfirst($this->session->userdata('user_name'));
                $email=$this->session->userdata('user_email_id');
                $usertoken=base64_encode($this->session->userdata('usertoken'));
                $email1=base64_encode($this->session->userdata('encode_user_email_id'));
                //echo $email."<br/>";
               // echo base64_decode($email);
       	//die;
         $register= $this->AUTHM->insertGuestUser($data);                 
         $this->session->set_flashdata('success','Inserted Successfully');
            
        if($register)
            {  
                $data['name']=$name;
                $data['email1']=$email1;
                $mail_data=  $this->load->view('email_templates/survey-mail-link', $data,true);

                $to=$email;             
                $subject='Wageni CRM';
 
                $sent= sendEmail($to,'',$subject,$mail_data);
                if($sent){

                     $this->redirect_Guest();
                }


               /* $username="moses";
                $Key="m6Z0mSfzivUKCujaQsvfdqnecYhjiTVhRyCqdj85sCPfAHqk61";
                $senderId="Smartlink";
                $tophonenumber="+254722453777";
                $finalmessage=urlencode('Thanks for using Wageni CRM . Please tell us about your experience in this 2-Minutes survey. Your feedback help us to create a better experience for you and for all of our customers. <a href="http://wageni.us.tempcloudsite.com/survey-form/'.trim($email1).' " target="_blank">Click Here</a>');            

                $Curl = curl_init();
                $CurlOptions = array(
                CURLOPT_URL => "https://sms.movesms.co.ke/api/compose?username=".$username."&api_key=".$Key."&sender=".$senderId."&to=".$tophonenumber."&message=".$finalmessage."&msgtype=5&dlr=0",    
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_POST => false,
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CONNECTTIMEOUT => 15,
                CURLOPT_TIMEOUT => 100,
                );
                curl_setopt_array($Curl, $CurlOptions);
                $data = curl_exec($Curl);   
                $array_data = explode("|",$data);*/
                 

                // print_r($data);
                //$this->redirect_Guest();
                }                  
            }  
        else
         {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);
            $this->global['alluser']= $this->AUTHM->fetchGuestUserData();
            $this->load->view('webadmin/guestuser/addGuestUser',$this->global);
        } 
    }



    public function saveGuest(){
        if ($this->input->is_ajax_request()) {
            $response['status']=true;
             $type_error='';

                $this->form_validation->set_rules('user_name', 'user_name', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('no_of_adult', 'no_of_adult', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('no_of_children', 'no_of_children', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('no_of_room', 'no_of_room', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('check_in', 'check_in', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('check_out', 'check_out', 'required|strip_tags|xss_clean');   

                $email=$this->input->post('user_id');
                 if(!empty($email)){
                    $where=array('user_email_id'=>$email );
                   // $where=array('user_email_id'=>$email,'is_deleted'=>0);
                     
                    $response['status']=$this->MM->checkExist('guest_user',$where);
                    if (!$response['status']) {
                        $type_error="Email Address already exists";
                    }
                    // echo $this->db->last_query();        die;
                }            
            
            if ($this->form_validation->run() == FALSE || $response['status']==FALSE) {
                $response=$this->form_validation->error_array();
                $response['status']=false;
                if(!empty($type_error)){
                    $response['error']=$type_error;
                }
                $response['message']='There is error in submitting form!';
            }
            else
            {                  
                $selectdata=array(
                    'category'   =>   $this->session->userdata('category'),
                    'owner_id'   =>   $this->session->userdata('sub_login_id'),
                    'subcat'     =>   $this->input->post('subcat')
                    );

                    $fetchhotelprocess= $this->AUTHM->fetchhotelprocess($selectdata);                 
                    
                    $process= $fetchhotelprocess[0]['sub_sub_cat_id']?$fetchhotelprocess[0]['sub_sub_cat_id']:0;
                   
                    $check_in   =   date('Y-m-d',strtotime($this->input->post('check_in')));
                    $check_out  =   date('Y-m-d',strtotime($this->input->post('check_out')));

                    $data=array(
                        'hotel_branch_name'   =>  $this->session->userdata('category'),
                        'owner_id'            =>  $this->session->userdata('sub_login_id'),
                        'hotel_name'          =>  $this->input->post('subcat'),
                        'process'             =>  $process,
                        'user_name'           =>  $this->input->post('user_name'),
                        'user_email_id'       =>  $this->input->post('user_id'),
                        'no_of_adult'         =>  $this->input->post('no_of_adult'),
                        'no_of_children'      =>  $this->input->post('no_of_children'),
                        'no_of_room'          =>  $this->input->post('no_of_room'),
                        'room_no'             =>  $this->input->post('room_no'),
                        'user_phone'          =>  $this->input->post('user_phone'),
                        'user_address'        =>  $this->input->post('user_address'),
                        'user_nationality'    =>  $this->input->post('user_nationality'),
                        'usertoken'           =>  random_string('sha1', 16),
                        'userip'              =>  $_SERVER['REMOTE_ADDR'],
                        'check_in'            =>  $check_in,
                        'check_out'           =>  $check_out,
                        'check_in_time'       =>  trim($this->input->post('check_in_time')),
                        'check_out_time'      =>  trim($this->input->post('check_out_time'))
                    );

					$this->session->set_userdata('user_name',$data['user_name']);
					$this->session->set_userdata('usertoken',$data['usertoken']);
					$this->session->set_userdata('user_email_id',$data['user_email_id']);
					$this->session->set_userdata('encode_user_email_id',$data['user_email_id']);

					$name=ucfirst($this->session->userdata('user_name'));
					$email=$this->session->userdata('user_email_id');
					$usertoken=base64_encode($this->session->userdata('usertoken'));
					$email1=base64_encode($this->session->userdata('encode_user_email_id'));
               
                	//echo "<pre/>"; print_r($data); die;
       
                $register= $this->AUTHM->insertGuestUser($data);                 
                $this->session->set_flashdata('success','Inserted Successfully');     
                               
                if($register>0) { 
                      
                    $userresult = $this->AUTHM->getGuestUserData($register);                      
                    $id=$userresult['user_id'];                    
                    $data=array('pre_stay' => 1,'mail_status'=>'01');            
                   // echo "<pre/>";print_r($userresult);
                    $updateGuestUserAfterMailSent = $this->AUTHM->updateGuestUserAfterMailSent($id,$data);  
                    $data['name']=$name;
                    $data['email1']=$email1;
                  	$stay='pre_stay';
                    $data['quetypename']=base64_encode($stay);
                    $mail_data=  $this->load->view('email_templates/survey-mail-link', $data,true);
                    $to=$email;             
                    $subject='Wageni CRM';     
                    $sent= sendEmail($to,'',$subject,$mail_data);                    
                }

                $response['message']='Added Successfully!';            
            }
            echo json_encode($response);
        }
        return false;
    }

    public function select_Hotel($selectValue) {
        if($selectValue == 'none') {
            $this->form_validation->set_message('select_Hotel', 'The Hotel field is required Please Select one.');
            return false;
        } else {
            return true;
        }
    }


    public function fortest(){
        $data['name']='Praveen';
        $data['email1']='praveen@panindia.in';
        echo $this->load->view('email_templates/survey-mail-link', $data,true);
    }

    /******************************************************************************************************************/

    public function complaintsRafer($id){ 
     //$this->isLogin();  
       $data = array('cf_id' =>$id ,'cf_status'=>$this->input->post('applychange'));
       $res= $this->MastersModel->updateComplain($data);
       if($res== true) {
       	    $this->session->set_flashdata('success','Complaints Change Successfully');
            redirect('user-complaints');
       }else {
       	    $this->session->set_flashdata('success','Faield');
            redirect('user-complaints');
       }       
    }

    public function complaintsRafer1($id){  
        //$this->isLogin(); 
        $data = array('cf_id' =>$id ,'cf_status' =>$this->input->post('applychange') );
        $res= $this->MastersModel->updateComplain($data);
        if($res== true) {
            $this->session->set_flashdata('success','Complaints Change Successfully');
            redirect('user-complaints-a');
        }else{
            $this->session->set_flashdata('success','Faield');
            redirect('user-complaints-a');
       }
    }

    // Fetch Complaints admin
    public function fetch_feedbck_result(){   
        $this->isSuperAdmin();      
        $data['Complaints']= $this->MastersModel->fetchSurveyFeedback();
        $this->load->view('admin/serve/userfeedbackresult', $data);      
    }
       // Fetch Complaints admin
    public function fetch_feedbck_result_a() {  
         //$this->isLogin();      
        $data['complaints']=$this->MM->adminGetAllComplaintsAjax();     
        $data['customers']=$this->ACM->companyList();
        $data['category'] =  $this->MM->fetchCategory(); 
        $this->load->view("webadmin/feedback/admin-view-all-complaints", $data);     
    }


     // Fetch Complaints Supervisior
    public function fetch_feedback_supervisior(){
        //$this->isLogin();
        $data['Supervisior']='Supervisior';
        $data['complaints']=$this->MM->complaintsListNetPromterScore();//prd($data);
        $this->load->view("webadmin/feedback/supervisior/complaints-list", $data); 
        //$this->load->view('webadmin/feedback/viewuserfeedbacksupervisiorresult', $data);      
    }
     // Fetch Complaints Manager
    public function fetch_feedback_manger(){
        //$this->isLogin();
        $data['Manager']='Manager';
         $data['complaints']=$this->MM->complaintsListNetPromterScore();
        $this->load->view("webadmin/feedback/manger/complaints-list", $data); 
     }
     // Fetch Complaints Genral Manager
    public function fetch_feedback_gernalmanger(){
        //$this->isLogin();
        $data['GenralManager']='Genral Manager';
        $data['complaints']=$this->MM->complaintsListNetPromterScore();
        $this->load->view("webadmin/feedback/gernalmanger/complaints-list", $data); 
     }
    
    /*******************************************************************************************************************/
}