<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : SubsControler (UserController)
 * SubsControler Class to control all Website SubsControler related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 14 August 2018
 */
class SubsControler extends BaseController
{ 
    public function __construct()
    {
        parent::__construct();  
        $this->load->model('AuthModel'); 
    }
    
   // Add Brach
 

    public function index()
    {
    	$this->isLogin();
        $this->global['pageTitle'] = 'Branch'; 
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'required|strip_tags|xss_clean');

        if ($this->form_validation->run() == true){
        $data = array('subs_id'=>$this->session->userdata('sub_login_id'),
            'branch_name'=>html_escape(trim($this->input->post('branch_name', TRUE)))
        );
           $dataresult= $this->MastersModel->insertBranch($data = $this->security->xss_clean($data)); 
           if($dataresult ==TRUE )
           {                        
               $this->session->set_flashdata('success','Branch Added Successfully.');
               redirect('view-branch', 'refresh');
           }  
        }
        else
        {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);
            $this->global['viewbranch'] = $this->MastersModel->fetchBranchById(); 
            $data = $this->MastersModel->fetchSubPackage(); 
            //print_r( $this->global['viewbranch']); die;
            $branchlimit=$data[0]->package_unit; 
            $this->global['branchlimi']=$data[0]->package_unit; 
            $this->global['sub_branch_limit'] = $this->MastersModel->check_sub_branch_limit($branchlimit); 
            $this->load->view("webadmin/branches/viewallbranch", $this->global);    
        }
         
    }

            /* Get Branch Data By Id*/
     public function getBranchById($id)
        {   
            $this->isLogin();
            $data['records'] = $this->MastersModel->getBranchById($id);
            //echo("<pre/>");print_r($data);die;
            $this->load->view('webadmin/branches/editbranch',$data);

        }

        /* Update Branch Data  */
     public function editBranch($id)
    {
        $this->isLogin();
        $this->form_validation->set_error_delimiters('<span class="error alert" style="color:red";>', '</span>');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'required|strip_tags|xss_clean');

        if ($this->form_validation->run() == true){
     
            $data=array(
                'sub_branch_id' => $id,
                'branch_name'   => $this->input->post('branch_name'));

            //echo("<pre/>");print_r($data);die;
            $res = $this->MastersModel->updateBranch($data,$id); 
            //echo("<pre/>");print_r($data);die;
            if($res ==TRUE )
            {   

                $this->session->set_flashdata('success',' Updated Successfully.');
                redirect('view-branch', 'refresh');
            }  
        }
        else 
        {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);                         
            $this->load->view('webadmin/branches/editbranch',$data);
        } 

    }

    //Delete Branch
    public function deleteBranch($id)
    {
    	$this->isLogin();
        $result=$this->MastersModel->deleteBranch($id);
        if($result ==TRUE ){                        
           $this->session->set_flashdata('success','Branch Delete Successfully.');
           redirect('view-branch', 'refresh');
       }else{
            $this->session->set_flashdata('danger','Failed.');
            redirect('view-branch', 'refresh');           
       }        
    }

    /* Add Subscriber Role */
    public function addUserRole()
    {
    	$this->isLogin();
    	//print_r($this->input->post());
        $this->global['pageTitle'] = 'wageniCRM : ADD Subscriber Role';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('rolename', 'Role', 'required|strip_tags|xss_clean|is_unique[adminemprole.admin_emp_role_name]');
       
        if ($this->form_validation->run() == true){
          
            $data = array('subscriber_role_name'=>$this->input->post('rolename'));
            $result= $this->AuthModel->insertUserRole($data = $this->security->xss_clean($data)); 
            if($result ==TRUE )
            {                       
                $this->session->set_flashdata('success','User Role Added Successfully.');
                redirect('add-user-role');
            }  
        }else{
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error);          
                $this->global['subscriberrole'] = $this->common->fetchSubscriberRoles(); 
                $this->load->view("webadmin/allusers/add-subscribers-role", $this->global);      
            } 
    }
    
    //Get User Id Data
    public function editUserRole($id)
    {   
        $this->isLogin();
        $this->global['subscriberrole']= $this->AuthModel->fetcUserRoleById($id);
        $this->load->view("webadmin/allusers/edit-subscribers-role", $this->global);           
    }
    //Get User Id Data
    public function updateUserRole($id)
    {   
        $this->isLogin();
        $this->global['pageTitle'] = 'wageniCRM : Update User Role';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('rolename', 'Role', 'required|strip_tags|Administrator|xss_clean');
       
        if ($this->form_validation->run() == true){
            $data = array('subscriber_roll_id'=>$id,'subscriber_role_name'=>$this->input->post('rolename'));
            $result= $this->AuthModel->updateUserRole($data = $this->security->xss_clean($data)); 
            if($result ==TRUE ){                       
                $this->session->set_flashdata('success','User Role Update Successfully.');
                redirect('add-user-role');
            }  
        }else{ 
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error); 
                 $this->session->set_flashdata('success','The Role field must contain a unique value..');
                redirect('add-user-role');        
        }      
    }

    //Delete Subscribers
    public function deleteSubscribersRole($id)
    {   
        $this->isLogin();
        $usersresult= $this->AuthModel->deleteSubscribersRole($id); 
        if ($usersresult == true) {
             $this->session->set_flashdata('success','User Role Deleted Successfully.');
            redirect('add-user-role','refresh');
        }else{
            redirect('add-user-role','refresh');
        }
    }
    
   /*   Add Employee    */
    public function addSubscriber()
     {
     	$this->isLogin();
        $this->global['pageTitle'] = 'wageniCRM : ADD User';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('fname', 'First Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('lname', 'Last Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('userbranch', 'Branch Name', 'required|strip_tags|xss_clean|callback_select_Branch');
        //$this->form_validation->set_rules('uname', 'User Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('upass', 'Password', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('uphone', 'Phone', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('userrole', 'Role', 'required|strip_tags|xss_clean|callback_select_Role');
        $this->form_validation->set_rules('uemail', 'Email', 'required|strip_tags|is_unique[subscriberlogin.sub_email]|xss_clean');
       
        if ($this->form_validation->run() == true){
            $usersdata = array('added_by'=>$this->session->userdata('sub_login_id'),
                            'fname'=>html_escape(trim($this->input->post('fname', TRUE))),
                            'lname'=>html_escape(trim($this->input->post('lname', TRUE))),
                            'branch_id'=>html_escape(trim($this->input->post('userbranch', TRUE))),
                            //'sub_username'=>html_escape(trim($this->input->post('uname', TRUE))),      
                            'sub_email'=>html_escape(trim($this->input->post('uemail', TRUE))),      
                            'sub_pass'=>html_escape(trim($this->input->post('upass', TRUE))),      
                            'sub_phone'=>html_escape(trim($this->input->post('uphone', TRUE))),        
                            'sub_login_role'=>$this->input->post('userrole')      
                        );
            $usersresult= $this->AuthModel->insertSubscriber($usersdata = $this->security->xss_clean($usersdata)); 
            if($usersresult ==TRUE )
            {                       
                $this->session->set_flashdata('success','User Added Successfully.');
                redirect('add-subscriber-employee');
            }  
        }
        else{
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error);  
                $this->global['viewbranch'] = $this->MastersModel->fetchBranchById();                   
                $this->global['check_add_user_limit'] = $this->AuthModel->check_add_user_limit();  
                //echo "<pre/>"; print_r($this->global['check_add_user_limit']); die;
                $this->global['subscriber'] = $this->AuthModel->fetchSubscribersSessionId();  
                $this->global['role'] = $this->AuthModel->fetchSubscribersRole();  
                $this->load->view("webadmin/allusers/add-subscribers", $this->global);      
        	} 
     }

     // Validate for Branch
      function select_Branch($selectValue)
         {
            if($selectValue == 'none')
            {
                $this->form_validation->set_message('select_Branch', 'The Branch field is required Please Select one.');
                return false;
            }
            else 
            {
                return true;
            }
         }

         // Validate for Role
     function select_Role($selectValue)
         {
            if($selectValue == 'none')
            {
                $this->form_validation->set_message('select_Role', 'The Role field is required Please Select one.');
                return false;
            }
            else 
            {
                return true;
            }
         }


    //Fetch  Subscribers By id
    public function getSubscriberById($id)
    {
    	$this->isLogin();
        $this->global['viewbranch'] = $this->MastersModel->fetchBranchById();  
        $this->global['editsubscriber']= $this->AuthModel->fetchSubscribersId($id);
        $this->global['role'] = $this->AuthModel->fetchSubscribersRole(); 
       // $this->global['role'] = $this->AuthModel->fetchSubscribersRole(); 
        $this->load->view("webadmin/allusers/edit-subscribers", $this->global);    
    }
    //Update Subscribers
    public function updateSubscriber($id)
    {
    	$this->isLogin();
        $this->global['pageTitle'] = 'wageniCRM : Update User';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('fname', 'First Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('lname', 'Last Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('userbranch', 'Branch Name', 'required|strip_tags|xss_clean|callback_select_Branch');
        //$this->form_validation->set_rules('username', 'User Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('upass', 'Password', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('uphone', 'Phone', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('userrole', 'Role', 'required|strip_tags|xss_clean|callback_select_Role');
        $this->form_validation->set_rules('uemail', 'Email', 'required|strip_tags|xss_clean');            
       
        if ($this->form_validation->run() == true){
          
            $data = array('sub_login_id'=>$id,
                'fname'=>html_escape(trim($this->input->post('fname', TRUE))),
                'lname'=>html_escape(trim($this->input->post('lname', TRUE))),
                'branch_id'=>html_escape(trim($this->input->post('userbranch', TRUE))),
                //'sub_username'=>html_escape(trim($this->input->post('username', TRUE))),      
                'sub_email'=>html_escape(trim($this->input->post('uemail', TRUE))),      
                'sub_pass'=>html_escape(trim($this->input->post('upass', TRUE))),      
                'sub_phone'=>html_escape(trim($this->input->post('uphone', TRUE))),        
                'sub_login_role'=>$this->input->post('userrole')      
            );
        $dataresult= $this->AuthModel->updateSubscribers($data = $this->security->xss_clean($data)); 
        if($dataresult ==TRUE )
        {                       
            $this->session->set_flashdata('success','User Updated Successfully.');
            redirect('add-subscriber-employee');
        }else
        { 
            $this->session->set_flashdata('failed','Failed.');
            redirect('add-subscriber-employee');
        }}
         else{
            $error = validation_errors();
            $this->session->set_flashdata('failed',$error);  
            redirect('edit-subscriber-employee/'.$id);
        }  

    }
    //Delete Subscribers
    public function deleteSubscribers($id)
    {
    	$this->isLogin();
        $usersresult= $this->AuthModel->deleteSubscribers($id); 
        if ($usersresult == true) {
             $this->session->set_flashdata('success','User Deleted Successfully.');
            redirect('add-subscriber-employee','refresh');
        }else{
            redirect('add-subscriber-employee','refresh');
        }
    }
    //

   



    public function fetch_subcategory()
    {
        if($this->input->post('cat_id'))
        {
            echo $this->MastersModel->fetch_subcategory($this->input->post('cat_id'));
        }
    }

    public function fetch_subsubcat()
    {   
        $this->isLogin();
        if($this->input->post('sub_cat_id'))
        {
            echo $this->MastersModel->fetch_subsubcat($this->input->post('sub_cat_id'));
        }
    }

    public function allQueByCat()
    {   
        $this->isLogin();
        /*$categories = $this->db->get("category")->result();
        $this->load->view('admin/serve/serve-questions', array('categories' => $categories )); */
        $data['category'] = $this->MastersModel->fetch_category();
        $this->load->view('webadmin/serve/serve-questions', $data);
    }

    public function fetchQuestionnaireById()
    {   
        $this->isLogin();
        $categoryid             =   $this->session->userdata('category');
        $subcategoryid          =   $this->input->post('subcat');
        $cat_process            =   $this->input->post('subsubcat');      
        $data['allquestions']   =   $this->MastersModel->getQueByAdmin($categoryid,$subcategoryid,$cat_process);      
        //echo "<pre/>"; print_r($data); die;
        $this->load->view('webadmin/serve/serve-questions-data', $data); 
    }

/*********************** Get Questionnaire  and answer By Id for Update for Sub Admin     *****************************/
    public function getSubQuestionnaireById()
    {   
        $this->isLogin();
        //echo("hello");die;
        $id             =   $this->uri->segment('4');   
        $data['allquestions']   =   $this->MastersModel->getSubQuestionnaireById($id);
        $data['allanswer']   =   $this->MastersModel->getSubAnswerById($id); 
        //echo "<pre/>";print_r($data['allquestions']);die;     
        $this->load->view('webadmin/serve/new-edit-serve-form', $data);      
    }

    // Update The Questionnaire for sub admin
    public function updateSubQuestionnaire($id)
    {   
        $this->isLogin();
        // echo('hello');die;
        $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('surveyQue', 'surveyQue', 'required|strip_tags|xss_clean');
        
        if($this->form_validation->run() == true){          
            $data= array('bus_qid'=>$id,
                         'bus_q_que'=>$this->input->post('surveyQue')
         );  
               //echo"<pre/>"; print_r($data);die;
        $result = $this->MastersModel->updateSubQuestionnaire($data,$id);   
        //echo"<pre/>"; print_r($data);die;
            if ($result==true) 
                  { 
                     $this->session->set_flashdata('success', 'Updated Successfuly!');
                    redirect('admin-allQueByCat','refresh');
                  }
                  else
                  {
                     $this->session->set_flashdata("danger","You are not able to update");
                     redirect('admin-allQueByCat','refresh');
                  }
        }                 

    }

     // Delete The Questionnaire for sub_admin
    public function deleteSubQuestionnaire()
     {  
         $this->isLogin();
         //print_r($bus_qid);die;
         $result =  $this->MastersModel->deleteSubQuestionnaire($_POST["bus_qid"]);  
         if ($result == true) {
         echo "1";
        // redirect ('admin-allQueByCat','refresh');
         }else{
                echo "2";
              }  
  
     }

/*************************************************** End Sub Admin **********************************************************/

     public function addQue(){
       $this->isLogin();
       $data['category'] = $this->MastersModel->fetch_category();
       $this->load->view("webadmin/serve/serve-form", $data);   
    }

    public function adminquestionSubmit(){

     $this->isLogin();
     if(!empty($this->session->userdata('added_by') == 0 ) || $this->session->userdata('sub_login_id') != NULL || $this->session->userdata('sub_login_id') != ""){

        $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('subcat', 'Sub Category', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('subsubcat', 'Process', 'required|strip_tags|xss_clean');
       // $this->form_validation->set_rules('surveyQue', 'Question', 'required|strip_tags|xss_clean');
        if($this->form_validation->run() == true){ 

            //print_r($this->input->post()); die;

            $data= array('businessId'=>$this->session->userdata('sub_login_id'),
                        'bus_q_cat_id'=>$this->session->userdata('category'),
                        'bus_q_sub_cat_id'=>$this->input->post('subcat'),
                        'bus_cat_process'=>$this->input->post('subsubcat'),
                        'bus_q_que'=>$this->input->post('surveyQue'),
                        'bus_q_type'=>$this->input->post('q_type')
            ); 
         
               $result = $this->MastersModel->inserQuestionnaireForAdmin($data);   

                if ($result){
                    $response['message'] = 'Inserted Successfuly!';
                    $response['status']  = 1;
                    $response['values']  = array();
                }else{
                    $response['message'] = 'Sorry ! Please try again.';
                    $response['status']  = 0;
                    $response['values']  =array();
                }

            } else {
                    $response['message'] = 'error';
                    $response['status']  = 0;
                    $response['values']  = $this->form_validation->error_array();
            }    

            echo json_encode($response); 
        
        }else{
                $response['message'] = 'Sorry! Please try again.';
                    $response['status'] = 0;
                    $response['values'] =array();
            echo json_encode($response); 
        }
    }


    /************************************************** Update profile ******************************************/

        /* Fetch Profile Data By id */
    public function fetchProfileData()
    {   
         $this->isLogin();
         $id = $this->session->userdata('sub_login_id');
         $data['profile']= $this->AuthModel->fetchSubscribersId($id);
         //echo('<pre/>');print_r($data);die;
         //$data['category'] = $this->authModel->();
         //$data['package'] = $this->PackageModel->fetchPackage($id);
         //$data['category'] = $this->AuthModel->fetchCategory($id);
         //echo('<pre/>');print_r($data['category']);die;
         $this->load->view('webadmin/edit_profile',$data);
    }

        /* Update the Profile */
    public function editProfile($id)
    {
        
        $this->isLogin();
        //$this->global['pageTitle'] = 'wageniCRM : ADD Subscriber';
        $setimage= $this->input->post('hiddenimg');
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('fname', 'First Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('lname', 'Last Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('upass', 'Password', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('uphone', 'Phone', 'required|strip_tags|xss_clean');

        if ($this->form_validation->run() == true){
            if(empty($_FILES['userfile']['name'])){   
                $updImage=$setimage;
            }else{
                $config['upload_path']     =  './uploads/';
                $config['allowed_types']   =  'jpeg|jpg|png'; 
                $config['max_size']        =  1024 * 10;
                $config['encrypt_name']    =  TRUE;
                $config['file_name']=$_FILES['userfile']['name'];
                $this->load->library('upload', $config);   
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $imagedetails= $this->upload->data();
                $updImage=$imagedetails['file_name'];
               // echo("<pre/>");print_r('$imagedetails');die;
            }
            $data = array(
                'sub_login_id'=>$id,
                'fname'     =>  html_escape(trim($this->input->post('fname', TRUE))),
                'lname'     =>  html_escape(trim($this->input->post('lname', TRUE))),
                'sub_pass'  =>  html_escape(trim($this->input->post('upass', TRUE))),      
                'sub_phone' =>  html_escape(trim($this->input->post('uphone', TRUE))),
                'added_by'  =>0,                          
                'company_url'   =>  html_escape(trim($this->input->post('weburl', TRUE))),        
                'contactaddress'=>  html_escape(trim($this->input->post('contactaddress', TRUE))),        
                'cpname'        =>  html_escape(trim($this->input->post('cpname', TRUE))),        
                'contacttel'    =>  html_escape(trim($this->input->post('contacttel', TRUE))),        
                'contactno'     =>  html_escape(trim($this->input->post('contactno', TRUE))),        
                'accno'         =>  html_escape(trim($this->input->post('accno', TRUE))),        
                'cpemail'       =>  html_escape(trim($this->input->post('cpemail', TRUE))),        
                'cptitle'       =>  html_escape(trim($this->input->post('cptitle', TRUE))),        
                'cpmobile'      =>  html_escape(trim($this->input->post('cpmobile', TRUE))),        
                'cpaddress'     =>  html_escape(trim($this->input->post('cpaddress', TRUE))),        
                'cpoffice'      =>  html_escape(trim($this->input->post('cpoffice', TRUE))),
                'userfile'  =>  $updImage       
            );
            
            $dataresult= $this->AuthModel->updateProfile($data,$id); 
            
            if($dataresult ==TRUE ) {                       
                $this->session->set_flashdata('success','User Profile  Updated Successfully.');
                redirect('edit-profile');
            }else{
                //$error = validation_errors();
                //$this->session->set_flashdata('failed',$error);  
                $this->session->set_flashdata('failed','Failed.');
                redirect('edit-profile');
            }  
        }
    }
    

   
}

?>