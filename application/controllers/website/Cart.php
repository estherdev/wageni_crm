<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

    require APPPATH . '/libraries/BaseController.php';
 
    class Cart extends BaseController
    {        
        public function __construct()
        {
            parent::__construct(); 
             $this->load->model(array('CartModel'));
        }
 

        public function buypackage($packageid){
          $data = array();
          $packagedetails = $this->CartModel->fetchPackagedetail($packageid); 
          $data['packages'] = $packagedetails['0'];

            if(isset($_POST['submitpackageorder'])){
              //echo "<pre>";print_r($_POST);die;
              $insertingpackageid = $this->input->post('packageid');//echo $insertingpackageid;die;
               $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
                $this->form_validation->set_rules('fname', 'First Name', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('lname', 'Last Name', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('company_email', 'Company Email', 'required|valid_email|is_unique[subscriberlogin.sub_email]|strip_tags|xss_clean');
                $this->form_validation->set_rules('company_phone', 'Company Phone', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('company_name', 'Company Name', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('company_url', 'Company Url', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('country', 'Country', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('hvroom', 'Room', 'required|strip_tags|xss_clean');
                $this->form_validation->set_rules('category', 'category', 'required|strip_tags|xss_clean');

                  if($this->form_validation->run()){     
                        $data= array('fname'=>$this->input->post('fname'),
                                    'lname'=>$this->input->post('lname'),
                                    'sub_email'=>$this->input->post('company_email'),
                                    'cpmobile'=>$this->input->post('company_phone'),
                                    'cpname'=>$this->input->post('company_name'),
                                    'company_url'=>$this->input->post('company_url'),
                                    'country'=>$this->input->post('country'),
                                    'hvroom'=>$this->input->post('hvroom'),
                                    'category'=>$this->input->post('category'),
                        );  
                       // echo "<pre/>";print_r($data);


                        $data['name']=$this->input->post('fname'). " ".$this->input->post('lname');
                        $data['content']='Thank You for registering on Wageni CRM. Your account is waiting for admin approval.';
                        $data['mailtitle']='Welcome to Wageni CRM.';
                        $mail_data=  $this->load->view('email_templates/register-mail', $data,true);

                        $to=$this->input->post('company_email');             
                        $subject='Wageni CRM : Registered';
         
                        $sent= sendEmail($to,'',$subject,$mail_data);                       

                        $last_user_insert_id = $this->CartModel->insertuser($data);   
 
                        if($last_user_insert_id>0){

                          $packageinsertiondata = array(
                                          'subscriber_id' => $last_user_insert_id,
                                          'subscriber_package_id' => $insertingpackageid,
                                          'subscriber_start_date' => date("m-d-Y"),
                                          'subscriber_end_date' => date('Y-m-d', strtotime('-1 day', strtotime('+1 years'))),
                                          'pac_status' =>0
                        );


                        $result = $this->CartModel->insertpackageorder($packageinsertiondata);   
 
                        if ($result>0){
                            $this->session->set_flashdata('success', 'You have successfully purchased this package!');
                            redirect('package');
                        }else{
                            $this->session->set_flashdata('error', 'Something went wrong!Please try again.');
                            redirect('package');
                        }

                        }
                      
                }else{
                  //echo "string2";die;
                     $error=validation_errors();
                }
            }

          $this->load->view("website/buypackagelist", $data); 
        }
     






    }