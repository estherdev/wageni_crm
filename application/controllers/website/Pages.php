<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

//require APPPATH . '/libraries/BaseController.php';

class Pages extends CI_Controller
{ 
    public function __construct() {
        parent::__construct();  
        $this->load->library("pagination");
        $this->load->helper('wageni');
        $this->load->model(array('All_company_model' ,'AuthModel' ,  'PackageModel', 'MastersModel'));
        //prd($this);

    }
     
    public function home()    {
        $data['pageTitle'] = 'Home';  
        $data['homeslider'] = $this->MastersModel->fetchSlider();       
        $this->load->view("website/home", $data);
    }
    public function about_us()    {
        $data['pageTitle'] = 'About Us';    
        $data['about'] = $this->MastersModel->getAbout();
        $data['practice'] = $this->MastersModel->fetchPractice();           
        $this->load->view("website/about-us", $data);
    }
    public function clients()    {
        $data['pageTitle'] = 'Clients';  
        $data['alldata'] = $this->MastersModel->FetchClientLogo();
		$config = array(); 
        $config["base_url"] = base_url('website/pages/clients/'); 
        $config["total_rows"] = $this->MastersModel->record_test();
        $config["per_page"] = 2; 
        $config["uri_segment"] = 4;
		$config['use_page_numbers'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data['alltest'] = $this->MastersModel->fetch_test($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();	   
        $this->load->view("website/clients", $data);
    }
    public function history() 
    {
        $data['pageTitle'] = 'History';
        $data['historycontent'] = $this->MastersModel->getHistoryData();
		$data['alldata'] = $this->MastersModel->getHistory(); 
        $this->load->view("website/history", $data);
    }
   public function testimonials()    {
        $data['pageTitle'] = 'Testimonials'; 
		$config = array(); 
        $config["base_url"] = base_url('website/pages/testimonials/'); 
        $config["total_rows"] = $this->MastersModel->record_test();
        $config["per_page"] = 2; 
        $config["uri_segment"] = 4;
		$config['use_page_numbers'] = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data['alltestimonials'] = $this->MastersModel->fetch_test($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();	   
        $this->load->view("website/testimonials", $data);
    }
    public function contact_us()    {
      $this->form_validation->set_error_delimiters('<span class="error" style="color: red";>','</span>');
      $this->form_validation->set_rules('uname', 'Name', 'required|strip_tags|xss_clean');
      $this->form_validation->set_rules('uemail', 'Email', 'required|strip_tags|xss_clean');
      $this->form_validation->set_rules('subject','Subject','strip_tags|xss_clean');

        if ($this->form_validation->run() == true){
            $data=array(
            'name'      => trim($this->input->post('uname')),
            'email'     => trim($this->input->post('uemail')),
            'sub'       => trim($this->input->post('subject')),
            'message'   => trim($this->input->post('message'))
        );
        
            $sender=$this->input->post('uemail');
            $this->session->set_userdata('sendermail',$sender);      
            
            $email=$this->session->userdata('sendermail');
            $config = Array(
                'protocol'  => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'mastcrew22@gmail.com',
                'smtp_pass' => 'crew@1234',
                'mailtype'  => 'html', 
                'charset'   => 'iso-8859-1'
            );
            
            $mailMsg= 'WageniCRM';
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");          
            $this->email->from('praveen@panindia.in', 'Wageni CRM');
            $this->email->to($email);
            $this->email->subject('WageniCRM');
            $this->email->message($mailMsg);
            $this->email->send();
            $this->email->send();       
        }
        
        $data['pageTitle'] = 'contact-us'; ;  
        $data['contact'] = $this->MastersModel->getContact(); 
        $this->load->view("website/contact-us", $data);
    }
    public function package()    {
        $data['pageTitle'] = 'Package'; 
        $data['packages'] = $this->common->fetchPackages();      
        $this->load->view("website/package", $data);
    }
    public function faq()    {
        $data['pageTitle'] = 'FAQ';        
        $data['pageHeading'] = 'FREQUENTLY ASKED QUESTIONS';        
        $data['allfaq'] = $this->common->fetchFaq();   
        $this->load->view("website/faq", $data);
    }
    public function blog()    {
        $data['pageTitle'] = 'Blog';           
		$config = array(); 
        $config["base_url"] = base_url('website/pages/blog/'); 
        $config["total_rows"] = $this->MastersModel->record_count();
        $config["per_page"] = 2; 
        $config["uri_segment"] = 4;
		$config['use_page_numbers'] = FALSE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0; 
        $data["results"] = $this->MastersModel->fetch_blog($config["per_page"], $page);
        $data['sideblog']= $this->MastersModel->fetchBlogWithLimit(); 
        $data["links"] = $this->pagination->create_links();	   
        $this->load->view("website/blog", $data);
    }
    public function user_dashboard()    {
        $data['pageTitle'] = 'User Dashboard';   
        $this->load->view("website/user-dashboard", $data);
    }

   public function blogDetails($id)	{
		$data['pageTitle']= 'BLOG DETAILS';
        $data['singleBlog']= $this->MastersModel->getblogdatabyid($id); 
        $data['allblog']= $this->MastersModel->fetchBlog();  
        $data['sideblog']= $this->MastersModel->fetchBlogWithLimit();           		
        $this->load->view("website/blog-details", $data);
	}

    public function getsurvey()    {
        $email= base64_decode($this->uri->segment(2));
        $qtype= base64_decode($this->uri->segment(3));
        $data['title'] = 'If u have any suggestion or any feedback for us.We not love to hear it!';        
        $selectwhere=array('gu.user_email_id'=> $email);
        $where=array('email'=> $email,'qtype'=> $qtype);


		$quedata['userinfo'] = $this->MastersModel->getSurveyQueList($selectwhere);
        $mail_status = $quedata['userinfo']['mail_status']; 
        $q_owner_id  = $quedata['userinfo']['owner_id']; 
        $q_hotel_id  = $quedata['userinfo']['hotel_name']; 
        $user_id 	 = $quedata['userinfo']['user_id']; 
        $pre_survey  = $quedata['userinfo']['user_pre_survey_status']; 
        $on_survey   = $quedata['userinfo']['user_on_survey_status']; 
        $post_survey = $quedata['userinfo']['user_post_survey_status']; 
        $done=1;

        $pre_stay=1; $on_stay=2; $post_stay=3; 

         	if( $qtype=='pre_stay' && ($pre_survey==0) ) {
                $updatestatus=1;
                $stay=$qtype;
                $done=$pre_survey;
                $mail_status=$pre_stay;
                $staysubject='Pre Stay';
            }	
            if ( $qtype=='post_stay'  && ($post_survey==0)) {
                $updatestatus=1;
                $stay=$qtype;
                $done=$post_survey;
                $mail_status=$post_stay;
                $staysubject='Post Stay';
            }
            if ( $qtype=='on_stay'  && ($on_survey==0)) {
                $updatestatus=1;
                $stay=$qtype;
                $done=$on_survey;
                $mail_status=$on_stay;
                $staysubject='On Stay';
            }

        if( ( $done == 1 )  || ( $done == 1 ) || ( $done ==1) ) {
        	$data['done']= array('email'=> $email,'qtype'=> $qtype);
        	$data['titlehead']= 'Thank you for giving your valuable time to complete the survey.';
        	$data['title']= 'Your survey already done';
        	$this->load->view("website/survey-questions-done", $data);        
        }else{			
			$processwhere = array(
			    'q.businessId'=>$q_owner_id,
			    'q.bus_q_sub_cat_id'=>$q_hotel_id,
			    'sb.sub_sub_cat_name'=>$staysubject ? $staysubject :'Pre Stay'
			  );  

			$alluser['alluser']= $this->AuthModel->getProcess($processwhere);
			$categoryid= $alluser['alluser']['main_cat_id'];
			$subcategoryid= $alluser['alluser']['sub_category_id'];
			$cat_process= $alluser['alluser']['sub_sub_cat_id'];
			$sub_sub_cat_name= $alluser['alluser']['sub_sub_cat_name'];
			$id= $q_owner_id;
			$data['allquestions']= $this->MastersModel->mailstatuswisequesnew($categoryid,$subcategoryid,$cat_process,$id,$user_id); 


			//prd($data);


			$this->load->view("website/survey-questions-new", $data);
		}
    }



    function testforquestion()     {
        $data['title'] = 'If u have any suggestion or any feedback for us.We love to hear it!';
        //$data['allquestions'] = $this->MastersModel->fetchSurveyQuestionsGuestUser();
        // echo "<pre/>"; print_r($data); die;
        $this->load->view("website/survey-questions-new", $data);
     }

    //Insert feedback
    public function guestSaveSurveyQuestion() {    

        if ($this->input->is_ajax_request()) {
                $response=array();
                $response['status']     =   false;
                $response['message']    =   "There are an error";
                $email=  base64_decode($this->input->post('semail'));
                $qtype=  base64_decode($this->input->post('staytype'));
                $stay='';

                if( $qtype=='pre_stay' ) {
                    $updatestatus=1;
                    $staysubject='user_pre_survey_status';
                    $stay='Pre Stay';
                }   
                if ( $qtype=='post_stay' ) {
                    $updatestatus=1;
                    $stay='Post Stay';
                    $staysubject='user_post_survey_status';
                }
                if ( $qtype=='on_stay' ) {
                    $updatestatus=1;
                    $stay='On Stay';
                    $staysubject='user_on_survey_status';
                }

                 $npscore = array(
                    'cat_id'        =>  $this->input->post('cat'),
                    'sub_cat_id'    =>  $this->input->post('subcat'),
                    'user_id'       =>  $this->input->post('uid'),
                    'busid'         =>  $this->input->post('busid'),
                    'process_id'    =>  $this->input->post('process'),
                    'npscore'       =>  $this->input->post('npscore')
                    );

                $npscoreid['id'] = $this->MastersModel->insertNetpscore($npscore);
                  
                if($npscoreid['id'] > 0) {  

                    $results = $this->MastersModel->surveyInsert($npscoreid['id']);  
                    if($results>0) {  
                        //$where=array();
                        $where=array($staysubject=> $updatestatus);
                        $results=$this->MastersModel->updateGuestStatus($npscore['user_id'], $where);                         
                        $data=array();
                        $data['titlehead']= 'Thank you for your feedback.We acknowledge that we may have failed you and highly regret this. Your issue is being addressed and will be resolved shortly.';
                        $data['title']= $stay;
                        $mail_data = $this->load->view("email_templates/user-survey-done" ,$data, true);

                        $to=$email;

                        $subject = 'Wageni CRM '.$stay. ' survey';
         
                        $sent = sendEmail($to,'',$subject,$mail_data);   

                        $response['status']=true;
                        $response['message']="Submit successfully";    
                    }

                }

                echo json_encode($response); 
            }           
    }




    public function success(){     
        $this->load->view("success");
    }
    public function payment(){  
        $this->load->view("website/payment");
    }
    public function checkout(){   
            $live= $this->input->post('live'); 
            $this->input->post('mpesa'); 
            $this->input->post('airtel'); 
            $this->input->post('equity'); 
            $this->input->post('mobilebanking'); 
            $this->input->post('debitcard'); 
            $this->input->post('creditcard'); 
            $this->input->post('mkoporahisi'); 
            $this->input->post('saida'); 
            $this->input->post('autopay'); 
            $oid= $this->input->post('oid'); 
            $inv=$this->input->post('inv'); 
            $amount=$this->input->post('ttl'); 
            $tel=$this->input->post('tel'); 
            $eml=$this->input->post('eml'); 
            $vid=$this->input->post('vid'); 
            $curr=$this->input->post('curr'); 
            $p1=$this->input->post('p1'); 
            $p2=$this->input->post('p2'); 
            $p3=$this->input->post('p3'); 
            $p4=$this->input->post('p4'); 
            $cbk=$this->input->post('cbk'); 
            $cst=$this->input->post('cst'); 
            $crl=$this->input->post('crl'); 
            $hsh=$this->input->post('hsh');
            $key = "W4ge254N19ysR";//use "demo" for testing where vid also is set to "demo"
            
            $datastring = $live.$oid.$inv.$amount.$tel.$eml.$vid.$curr.$p1.$p2.$p3.$p4.$cst.$cbk;
/*********************************************************************************************************/



/*https://payments.ipayafrica.com/v3/ke?live=0&mpesa=&airtel=&equity=&mobilebanking=&debitcard=4444444444444444&creditcard=&mkoporahisi=&saida=&autopay=0&oid=112&inv=112020102292999&ttl=900&tel=256712375678&eml=praveen%40pamindia.in&vid=demo&curr=KES&p1=airtel&p2=020102292999&p3=445454545454&p4=900&cbk=1&cst=1&crl=0&hsh=e2559cf26f46b3c4290857446f96366f7b5a12ea810fd7d997958409232a5818*/

        $generated_hash = hash_hmac('sha256',$datastring , $key);
        //prd($this->input->post());
         /*    Generate the form BELOW   */
            ?>
           <FORM action="https://payments.ipayafrica.com/v3/ke">
         

            
            <INPUT name="hsh" type="text" value="<?php echo $generated_hash ?>">
            <button type="submit">  Lipa  </button>
            
         </FORM>
        <?php 

    }
    
   
}

?>