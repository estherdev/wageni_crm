<?php defined('BASEPATH') or die('not allow access to script');

    /**
    * 
    **/
class Dashboard extends CI_Controller
    {
    	function __construct()
    	{
    		parent::__construct();
			
    	}

    	function index()
    	{
		   
			$this->load->view('admin/index');
    	}
		
		function dataTable()
    	{
			$data['view']='admin/shipment';
    		$this->load->view('admin/common/layout',$data);
    	}
		
		
    }
?>