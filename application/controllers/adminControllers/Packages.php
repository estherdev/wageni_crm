<?php defined('BASEPATH') or die('not allow access to script');

    /**
    * 
    **/
class Packages extends CI_Controller
    {
    	function __construct()
    	{
    		parent::__construct();
			
    	}

    	function index()
    	{
			$this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
			$this->form_validation->set_rules('package_name', 'Name', 'required|strip_tags|xss_clean');
			$this->form_validation->set_rules('package_price', 'Price', 'required|strip_tags|xss_clean');
			$this->form_validation->set_rules('package_desc', 'Description', 'required|strip_tags|xss_clean');
		
			if ($this->form_validation->run() == true){
			
				$packagesdata = array('package_name'=>html_escape(trim($this->input->post('package_name', TRUE))),
								'package_price'=>html_escape(trim($this->input->post('package_price', TRUE))),		
								'package_desc'=>html_escape(trim($this->input->post('package_desc', TRUE)))		
							);
				//print_r($packagesdata);die;
				$packagesresult= $this->PackageModel->insertPackage($packagesdata = $this->security->xss_clean($packagesdata)); 
				if($packagesresult ==TRUE )
				{  						
					$this->session->set_flashdata('successmsg',' Faq Packages Successfilly.');
					redirect('packages', 'refresh');
				}  
			 }
			else {
                    $error = validation_errors();
                    $this->session->set_flashdata('validationerrormsg',$error);
					$data['view']='admin/add_packages';
					$this->load->view('admin/common/layout',$data);
            }		
		    
    	}
		
		function fetchPackages()
    	{
			$data['allackage']=$this->PackageModel->fetchPackage();
			print_r($data);die;
			$data['view']='admin/shipment';
    		$this->load->view('admin/common/layout',$data);
    	}
		
		
    }
?>