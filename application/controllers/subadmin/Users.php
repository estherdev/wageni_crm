<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

    require APPPATH . '/libraries/BaseController.php';
    
    class Users extends BaseController
    {
        
        public function __construct()
        {
            parent::__construct(); 
             $this->load->model(array('All_company_model' => 'ACM','AuthModel' => 'AUTHM', 'PackageModel' => 'PM', 'MastersModel' => 'MM'));


        }

        public function index(){
         
         // $this->isLogin();
        $this->global['pageTitle'] = 'wageniCRM : ADD User';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('fname', 'First Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('lname', 'Last Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('userbranch', 'Branch Name', 'required|strip_tags|xss_clean|callback_select_Branch');
        //$this->form_validation->set_rules('uname', 'User Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('upass', 'Password', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('uphone', 'Phone', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('userrole', 'Role', 'required|strip_tags|xss_clean|callback_select_Role');
        $this->form_validation->set_rules('uemail', 'Email', 'required|strip_tags|is_unique[subscriberlogin.sub_email]|xss_clean');
       
        if ($this->form_validation->run() == true){
            $usersdata = array('added_by'=>$this->session->userdata('sub_login_id'),
                            'fname'=>html_escape(trim($this->input->post('fname', TRUE))),
                            'lname'=>html_escape(trim($this->input->post('lname', TRUE))),
                            'branch_id'=>html_escape(trim($this->input->post('userbranch', TRUE))),
                            //'sub_username'=>html_escape(trim($this->input->post('uname', TRUE))),      
                            'sub_email'=>html_escape(trim($this->input->post('uemail', TRUE))),      
                            'sub_pass'=>html_escape(trim($this->input->post('upass', TRUE))),      
                            'sub_phone'=>html_escape(trim($this->input->post('uphone', TRUE))),        
                            'sub_login_role'=>$this->input->post('userrole')      
                        );
            $usersresult= $this->AUTHM->insertSubAdminUsers($usersdata = $this->security->xss_clean($usersdata)); 
            if($usersresult ==TRUE )
            {                       
                $this->session->set_flashdata('success','User Added Successfully.');
                redirect('add-subscriber-employee');
            }  
        }
        else{
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error);  
                $data['viewbranch'] = $this->MM->fetchBranchById();                   
                $data['check_add_user_limit'] = $this->AUTHM->check_add_user_limit();  
                //echo "<pre/>"; print_r($data['viewbranch']); die;
                $data['subscriber'] = $this->AUTHM->fetchSubscribersSessionId();  
                $data['role'] = $this->AUTHM->fetchSubscribersRole();  
                //$this->load->view("webadmin/allusers/add-subscribers", $data);      
                $this->load->view("webadmin/allusers/add-sub-users", $data);      
          } 
            //$data['customers']=$this->ACM->companyList();
             //$data['category'] =  $this->MM->fetchCategory(); 
             //$this->load->view("admin/allusers/all_company", $data); 
        }

        public function complaints(){
            $data['complaints']=$this->MM->adminGetAllComplaintsAjax();     
            //echo "<pre/>"; print_r($data);die;
             $data['customers']=$this->ACM->companyList();
             $data['category'] =  $this->MM->fetchCategory(); 
              $this->load->view("admin/complaints/complaints", $data); 
        }

             // Validate for Branch
      public function select_Branch($selectValue){
        if($selectValue == 'none'){
            $this->form_validation->set_message('select_Branch', 'The Branch field is required Please Select one.');
            return false;
        }else{
            return true;
        }
     }

         // Validate for Role
      public function select_Role($selectValue){
        if($selectValue == 'none'){
            $this->form_validation->set_message('select_Role', 'The Role field is required Please Select one.');
            return false;
        }else{
            return true;
        }
     }

     

        public function viewAllComplaintsList()
        {  
          if ($this->input->is_ajax_request()) {
 
                 $data = $this->ACM->adminGetAllComplaintsAjax($_POST,false);
                  $totalData = $this->ACM->adminGetAllComplaintsAjax($_POST,true);


                    $response=array();
                    $complaints='';
                    $status='';
                    $assign = '';
                    if(count($data)>0){
                        foreach($data as $row){                         
                      
                              if ($row['bus_que_score']=='1'){
                                  $complaints = '<i class="fa fa-times-circle-o fa-1x text-danger"> Very Poor</i>';
                                }
                                  elseif ($row['bus_que_score']=='2'){
                                    $complaints ='<i class="fa fa-times-circle-o fa-1x text-warning"> Poor</i>';
                                  }
                                    elseif ($row['bus_que_score']=='3'){
                                      $complaints ='<i class="fa fa-check-circle-o fa-1x text-primary"> Average</i>';
                                    }
                                      elseif ($row['bus_que_score']=='4'){
                                        $complaints ='<i class="fa fa-check-circle-o fa-1x text-success"> Good</i>';
                                      }
                                      elseif ($row['bus_que_score']=='5'){
                                        $complaints ='<i class="fa fa-check-circle-o fa-1x text-success"> Very Good </i>';                            
                                      } 
                                else{
                                   $complaints ='<i class="fa fa-check-circle-o fa-1x text-success"> No Complaints</i>';
                                }


                                /* Status */ 

                                if ($row['cf_status']=='1'){
                                  $status= '<i class="fa-1x text-danger"> Pending</i>';
                                }
                                  elseif ($row['cf_status']=='2'){
                                    $status=  '<i class="fa-1x text-warning"> Ongoing</i>';
                                  }
                                  elseif ($row['cf_status']=='3'){
                                    $status= '<i class="fa-1x text-success"> Issue has been addressed</i>';
                                  }
                                else{
                                   $status= '<i class="fa-1x text-danger">Pending</i>';
                                }

                                /* Assign */

                                $past_time =  date('Y-m-d H:i',strtotime($row['cf_time']));                                
                                $current_time = time();
                                $difference = $current_time - strtotime($past_time);
                                $difference_minute =  $difference/60;
                                $d= intval($difference_minute);
                                
                                if ( intval($d) < '1440') {
                                  $assign =  "Superviser";
                                } 
                                  elseif ( intval($d) < '2880') {
                                  $assign = "Manger";
                                 }
                                 else {
                                  $assign =  "G Manger";
                                }



                        $post_date = strtotime($row['cf_time']);
                        $now = time();
                        $units = 5;
                        $complaintstime= timespan($post_date, $now, $units);  
                        
                            $response[]=array(
                                'id'=>$row['cf_id'],
                                'name'=>ucfirst($row['user_name']),
                                'category_name'=>ucfirst($row['category_name']),
                                'question'=>$row['bus_q_que'],
                                'busid'=>ucfirst($row['fname']),
                                'status'=>$status,
                                'complaints'=>$complaints,
                                'assign'=>$assign,
                                'complaintstime'=>'<i class="fa fa-clock-o fa-1x">  ' .$complaintstime. " Ago </i>",
                                'all'=>$row
                             );
                        }
                    }

                    $json_data = array(
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalData,
                    "data"            => $response 
            );
            echo json_encode($json_data);
             }           
 
           
 
                       
        }

        public function viewAllCompanyList()
        {  
          if ($this->input->is_ajax_request()) {
          

                 $data = $this->ACM->companyListAjax($_POST,false);
                  $totalData = $this->ACM->companyListAjax($_POST,true);


                    $response=array();
                    if(count($data)>0){
                        foreach($data as $row){     
                        $score = number_format($row['score'], 2);
                       
                            $response[]=array(
                                'id'=>$row['sub_login_id'],
                                'name'=>ucfirst($row['fname'].' '.$row['lname']),
                                'category_name'=>ucfirst($row['category_name']),
                                'sub_name'=>ucfirst($row['sub_name']),
                                'package_name'=>ucfirst($row['package_name']),
                                //'price'=> $row['package_price'].' '.$this->config->item('krw'),
                                'price'=> $row['package_price'],
                                'score'=> $row['score'] ? number_format($row['score'], 2) : 0.0,
                                'expiredate'=>ucfirst($row['subscriber_end_date']),
                                'email'=>$row['sub_email'],
                                'status'=>$row['sub_isActive'],
                                'image'=>$row['userfile'],
                                'all'=>$row,
                                'company_url'=>$row['company_url']
                            );
                        }
                    }

                    $json_data = array(
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalData,
                    "data"            => $response 
            );
            echo json_encode($json_data);
             }           
 
           
 
                       
        }




  public function changeCompanyStatus(){
    if ($this->input->is_ajax_request()) {

      $id=$this->input->post('id');
      $status=$this->input->post('status');
      if($id>0){
        $update_data=array('id'=>$id,'status'=>$status);
        //$response['status']=$this->ACM->changeDataStatus('subscriberlogin',$update_data);
        if($response['status']){
          $response['message']="Company Status Updated Successfully!";
      
         }
        $data=array('status'=>1);
        echo json_encode($data);
      }
    }
  }





  public function adminCompanyUsersDetailsAjax(){
   if ($this->input->is_ajax_request()) {
     
      $id=$this->input->post('id');
      if($id>0){
          $data['companyListAdmin'] = $this->ACM->getCompanyListAdmin($id);
          if(count($data)>0){
             $response['data'] =$this->load->view("admin/allusers/all_company_users", $data,true);      
           echo json_encode($response);   
          }
      }
    }
  }

  //editSubscriber/26




  public function adminUpdateCompanyAjax($id){
    $this->isSuperAdmin();
    $data['companydata']= $this->ACM->fetchCompanyBYId($id);
    $data['role'] = $this->ACM->fetchSubscribersRole();                     
    $data['package'] = $this->PM->fetchPackage(); 
    $data['category'] = $this->MM->fetchCategory(); 
    $this->load->view("admin/allusers/update_company", $data);         
     
  }
  /* public function updateCompany($id){

      $data['companydata'] = $this->ACM->fetchCompanyBYId($id);
       $this->load->view("admin/allusers/update_company", $data);  

   }*/






    }