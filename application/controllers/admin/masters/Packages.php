<?php defined('BASEPATH') or die('not allow access to script');
   
   require APPPATH . '/libraries/BaseController.php';

   /**

    * Class : Packages (UserController)   
    * Packages Class to control all Packages related operations.   
    * @author : Esther Praveen   
    * @version : 1.1   
    * @since : 28 August 2018   
    */
   
   class Packages extends BaseController   
   {
    function __construct()
    {
        parent::__construct();
        $this->isSuperAdmin();

    }

    public function index()
    {
        //$Package_desc = array();
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('package_name', 'Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('package_price', 'Price', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('allowed_user', 'Allowed User', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('package_validity', 'Validity', 'required|strip_tags|xss_clean|callback_select_Package');
        //$this->form_validation->set_rules('package_desc[]', 'Description', 'required|strip_tags|xss_clean');
           
           if ($this->form_validation->run() == true){
        /*    foreach ($this->input->post('package_desc') as $package_desc)
            {
                $Package_desc[] = $package_desc;

            }         
            $packagedesc  = implode(',',$Package_desc);*/  
            $packagesdata = array('package_name'=>html_escape(trim($this->input->post('package_name', TRUE))),
                'package_price'=>html_escape(trim($this->input->post('package_price', TRUE))), 
                'package_unit'=>html_escape(trim($this->input->post('allowed_user', TRUE))), 
                'package_valid'=>html_escape(trim($this->input->post('package_validity', TRUE))) 
                //'package_desc'=>$packagedesc        
            );
            //echo "<pre/>";print_r($packagesdata);die;
            $packagesresult= $this->MastersModel->insertPackage($packagesdata = $this->security->xss_clean($packagesdata)); 
            if($packagesresult ==TRUE )
                {                       
                   $this->session->set_flashdata('success','Packages Added Successfully.');
                   redirect('view-packages');
                }  
            }
            else {
                     $error = validation_errors();
                     $this->session->set_flashdata('validationerrormsg',$error);
                    // $this->global['pageTitle'] = 'wageniCRM :Add Packages'; die;
                     $this->global['Package'] = $this->MastersModel->fetchPackage(); 
                     $this->load->view("admin/packages/viewallpackage", $this->global);        
            }             
    }

     function select_Package($selectValue)
    {
        if($selectValue == 'none')
        {
            $this->form_validation->set_message('select_Package', 'The Validity field is required Please Select one.');
            return false;
        }
        else 
        {
            return true;
        }
    }
    //Fetch  Subscribers By id
    public function getPackagerById($id)
    {
        $this->global['editpackage']= $this->MastersModel->fetchPackageId($id);
        $this->load->view("admin/packages/edit-package", $this->global);    
    }
    //Update Subscribers
    public function updatePackages($id)
    {
        $this->global['pageTitle'] = 'wageniCRM : Update packages';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('package_name', 'Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('package_price', 'Price', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('allowed_user', 'Allowed User', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('package_validity', 'Validity', 'required|strip_tags|xss_clean');
           
            if ($this->form_validation->run() == true){
              
                $data = array('package_id'=>$id,
                'package_name'=>html_escape(trim($this->input->post('package_name', TRUE))),
                'package_price'=>html_escape(trim($this->input->post('package_price', TRUE))), 
                'package_unit'=>html_escape(trim($this->input->post('allowed_user', TRUE))), 
                'package_valid'=>html_escape(trim($this->input->post('package_validity', TRUE))) 
                //'package_desc'=>$packagedesc        
            );
            $dataresult= $this->MastersModel->updatePackages($data = $this->security->xss_clean($data)); 
            if($dataresult ==TRUE )
            {                       
                $this->session->set_flashdata('success','Packages Updated Successfully.');
                redirect('view-packages');
            }else{
                //$error = validation_errors();
                //$this->session->set_flashdata('failed',$error);  
                $this->session->set_flashdata('failed','Failed.');
                redirect('view-packages');
            }  
        }
    }
    //Delete Subscribers
    public function deletePackages($id)
    {
        $usersresult= $this->MastersModel->deletePackages($id); 
        if ($usersresult == true) {
             $this->session->set_flashdata('success','Packages Deleted Successfully.');
            redirect('view-packages','refresh');
        }else{
            redirect('view-packages','refresh');
        }
    }



}

?>