<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Home (UserController)
 * Home Class to control Home Pages related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 28 August 2018
 */
class Blog extends BaseController
{ 
    public function __construct()
    { 
        parent::__construct(); 
    } 
    
    // This function used to load the first screen of the user

    public function index()
    {

     $this->isSuperAdmin();
     $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
     $this->form_validation->set_rules('title', 'title', 'required|strip_tags|xss_clean');       
     $this->form_validation->set_rules('content', 'content', 'required|strip_tags|xss_clean');
     $this->form_validation->set_rules('created_date', 'created_date', 'required|strip_tags|xss_clean');
     $this->form_validation->set_rules('userfile', 'userfile');      


     if ($this->form_validation->run() == true){
        $config['upload_path']     =  './uploads/';
        $config['allowed_types']   =  'jpeg|jpg|png'; 
        $config['max_size']        =  1024 * 10;
        $config['encrypt_name']    =  TRUE;

        $config['file_name']=$_FILES['userfile']['name'];

        $this->load->library('upload', $config);   
        $this->upload->initialize($config);
        $this->upload->do_upload('userfile');
        $imagedetails= $this->upload->data();
        $updImage=$imagedetails['file_name'];

        $data=array(
            'title'        => trim($this->input->post('title')),
            'content'      => trim($this->input->post('content')),
            'created_date' => trim($this->input->post('created_date')),
            'created_by'   => trim($this->input->post('created_by')),
            'userfile'     => $updImage);

        $res = $this->MastersModel->blogInsert($data); 

        if($res ==TRUE )
        {   

            $this->session->set_flashdata('success',' Inserted Successfully.');
            redirect('view-blog', 'refresh');
        }  
    }
    else 
    {
        $error = validation_errors();
        $this->session->set_flashdata('validationerrormsg',$error);
        $data['pageTitle'] = 'Blog';   
        $data['alldata']= $this->MastersModel->fetchBlog();                           
        $this->load->view('admin/blog/add-blog',$data);
    } 
}


       /* public function getdata()
        {

        $data['alldata']= $this->MastersModel->fetchBlog();
        
        $this->load->view('admin/blog/add-blog',$data);

    }*/



    public function getbyid($id)
    { 
        $this->isSuperAdmin();
        $data['records'] = $this->MastersModel->getblogdatabyid($id);
            //print_r($data);die;
        $this->load->view('admin/blog/edit-blog',$data);

    }

    public function editdata($id)
    {


       /* $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('title', 'title', 'required|strip_tags|xss_clean');       
        $this->form_validation->set_rules('content', 'content', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('created_date', 'created_date', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('userfile', 'userfile');      
          

        if ($this->form_validation->run() == true){
            $config['upload_path']     =  './uploads/';
            $config['allowed_types']   =  'jpeg|jpg|png'; 
            $config['max_size']        =  1024 * 10;
            $config['encrypt_name']    =  TRUE;

            $config['file_name']=$_FILES['userfile']['name'];
            
            $this->load->library('upload', $config);   
            $this->upload->initialize($config);
            $this->upload->do_upload('userfile');
            $imagedetails= $this->upload->data();
            $updImage=$imagedetails['file_name'];
 
            $data=array(
                'id'       => $id,
            'title'        => trim($this->input->post('title')),
            'content'      => trim($this->input->post('content')),
            'created_date' => trim($this->input->post('created_date')),
            'created_by'   => trim($this->input->post('created_by')),
            'userfile'     => $updImage);
           
            $res = $this->MastersModel->updateblogdata($data,$id); 
        //echo "</pre>"; print_r($data);die;
            if($res ==TRUE )
                {   

                $this->session->set_flashdata('success',' Updated Successfully.');
                redirect('view-blog', 'refresh');
                }  
            }
            else 
            {
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error);
                $data['pageTitle'] = 'Blog';   
                $data['alldata']= $this->MastersModel->fetchBlog();                           
                $this->load->view('admin/blog/edit-blog',$data);
            } */

           // echo $id; die;
            $this->isSuperAdmin();
            $setimage= $this->input->post('hiddenimg');
            $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
            $this->form_validation->set_rules('title', 'title', 'required|strip_tags|xss_clean');       
            $this->form_validation->set_rules('content', 'content', 'required|strip_tags|xss_clean');
            $this->form_validation->set_rules('created_date', 'created_date', 'required|strip_tags|xss_clean');
       // $this->form_validation->set_rules('userfile', 'userfile');


            if ($this->form_validation->run() == true){

               if(empty($_FILES['userfile']['name'])) 
               {                
                $updImage=$setimage;
            }
            else{

                $config['upload_path']     =  './uploads/';
                $config['allowed_types']   =  'jpeg|jpg|png'; 
                $config['max_size']        =  1024 * 10;
                $config['encrypt_name']    =  TRUE;

                $config['file_name']=$_FILES['userfile']['name'];

                $this->load->library('upload', $config);   
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $imagedetails= $this->upload->data();
                $updImage=$imagedetails['file_name'];
            } 
            $data=array(
               'id'          => $id,
               'title'        => $this->input->post('title'),
               'content'      => $this->input->post('content'),
               'created_date' => $this->input->post('created_date'),
               'created_by'   => $this->input->post('created_by'),
               'userfile'     => $updImage);

            $res = $this->MastersModel->updateblogdata($data,$id); 
            //print_r($data);die;
            if($res ==TRUE )
            {   

                $this->session->set_flashdata('success',' Updated Successfully.');
                redirect('view-blog', 'refresh');
            }  
        }
        else 
        {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);
            $data['pageTitle'] = 'Blog';   
            $data['alldata']= $this->MastersModel->fetchBlog();                           
            $this->load->view('admin/blog/edit-blog',$data);
        } 

    }


    public function delete($id)
    {
        $this->isSuperAdmin();
        $this->MastersModel->deleteBlog($id);
        
        $this->session->set_flashdata('success', 'Successfully Deleted!');
        
        redirect('view-blog');
    }    
}

?>