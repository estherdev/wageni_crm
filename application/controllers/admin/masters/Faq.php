<?php
   defined('BASEPATH') or die('not allow to access script');
require APPPATH . '/libraries/BaseController.php';

/**
 * Class : FAQ (UserController)
 * FAQ Class to control all FAQ related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 28 August 2018
 */
   class Faq extends BaseController
   {   	
   	function __construct()
   	{
   		parent::__construct();
   		$this->load->model('MastersModel');
   		$this->isSuperAdmin();

   	}
   	// Add FAQ
	public function index()
	{
		$data['title'] = "Add Faq";
		$this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');

		$this->form_validation->set_rules('question', 'question', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('answer', 'answer', 'required|strip_tags|xss_clean');

      if ($this->form_validation->run() == true)
      {
            $data=array(
            'faq_que' => trim($this->input->post('question')),
            'faq_ans'  => trim($this->input->post('answer'))
        );

            $res = $this->MastersModel->faqInsert($data); 
          	//print_r($data);
            if($res ==TRUE )
            {     
	            $this->session->set_flashdata('success',' Inserted Successfully.');
	            redirect('view-faq', 'refresh');
            } 
            }
            else 
            {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);
            $data['alldata']= $this->MastersModel->getdata();            
            $this->load->view('admin/faq/viewallfaq',$data);
            } 
	}

	public function getdata()
	{
		$data['alldata']= $this->MastersModel->getdata();
		//print_r($data);die;
		$this->load->view('admin/faq/viewallfaq',$data);

	}


	public function getbyid($id)
	{ 
		
		$data['records'] = $this->MastersModel->getdatabyid($id);
		//print_r($data);
		$this->load->view('admin/faq/viewfaqedit',$data);

	}

		public function editdata($id)
	{
 //print_r($id);
			$this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');

			  $this->form_validation->set_rules('question', 'question', 'required|strip_tags|xss_clean');
    		  $this->form_validation->set_rules('answer', 'answer', 'required|strip_tags|xss_clean');
	
				if ($this->form_validation->run() == true){

				$data=array(
					//'faq_id'   => $id,
				    'faq_que'  => trim($this->input->post('question')),
                    'faq_ans'  => trim($this->input->post('answer')));

				//print_r($data);die;
				$res = $this->MastersModel->faqUpdate($data,$id);
				//print_r($data);die;
			//echo $this->db->last_query();die; 

				 if ($res==true) 
	              { 
				     $this->session->set_flashdata('success', 'Updated Successfuly!');
	                redirect('view-faq');
	              }
	              else
	              {
	                 $this->session->set_flashdata("danger","You are not able to update");
	                 redirect('view-faq');
	              }
			}
	}

	public function deleteFaq($id)
	{
		$retsult= $this->MastersModel->deleteFaq($id);
		if ($retsult == true) {
			$this->session->set_flashdata('success', 'Successfully Deleted!');
			redirect('view-faq', 'refresh');
		}else{
			$this->session->set_flashdata('success', 'Failed!');
			redirect('view-faq', 'refresh');
		}
	}

}