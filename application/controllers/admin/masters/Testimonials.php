<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
Class Testimonials extends BaseController
{

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('MastersModel');
		$this->load->library('form_validation');
		$this->isSuperAdmin();
	}

	public function index()
	{
		$this->isSuperAdmin();

		$data = '';
		$this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');

		$this->form_validation->set_rules('title', 'title', 'required|strip_tags|xss_clean');
				//$this->form_validation->set_rules('userfile', 'Document', 'callback_file_check_pp3');
		$this->form_validation->set_rules('title_text', 'title_text', 'required|strip_tags|xss_clean');


  			//print_r($_FILES['userfile']['name']);

		if ($this->form_validation->run() == true){ 

			$config['upload_path']     =  './uploads/';
			$config['allowed_types']   =  'jpeg|jpg|png'; 
			$config['max_size']        =  1024 * 10;
			$config['encrypt_name']    =  TRUE;					
			$config['file_name'] = $_FILES['userfile']['name'];

			$this->load->library('upload', $config);   
			$this->upload->initialize($config);
			$this->upload->do_upload('userfile');
			$imagedetails = $this->upload->data();
			$updImage = $imagedetails['file_name'];

			$data=array(
				'title'       => trim($this->input->post('title')),
				'content'     => trim($this->input->post('title_text')),
				'image' 	  => $updImage);

			//echo "/<pre>";	print_r($data); die;
			$res = $this->MastersModel->testInsert($data); 
          		// print_r($data); die;
			if($res ==TRUE )
			{  	
				$this->session->set_flashdata('success',' Inserted Successfully.');
				redirect('view-testimonials', 'refresh');
			}  
		}
		else 
		{
			$error = validation_errors();
			$this->session->set_flashdata('validationerrormsg',$error);
			$data['pageTitle'] = 'Testimonials';
			$data['alldata'] = $this->MastersModel->getTest();
			$this->load->view('admin/testimonials/view-test',$data);
		} 
		
	}
	/*	public function file_check_pp3($str){
		
		$allowed_mime_type_arr = array('image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
       	$mime = get_mime_by_extension($_FILES['userfile']['name']);
       	if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['name']!=""){
           if(in_array($mime, $allowed_mime_type_arr)){
               return true;
           }else{
               $this->form_validation->set_message('file_check_pp', 'Please select only jpeg, pjpeg, png, x-png, gif');
               return false;
           }
       	}else{
           $this->form_validation->set_message('file_check_pp', 'Please choose a file to upload.');
           return false;
       	}
       } */

   		/*public function getTest()
		{

		$data['alldata']= $this->MastersModel->getTest();
		
		$this->load->view('test_view',$data);

		}
*/
		public function gettesttbyid($id)
		{ 
			$this->isSuperAdmin();

			$data['records'] = $this->MastersModel->getTestDataById($id);
			$this->load->view('admin/testimonials/edit-test',$data);

		}

		public function editTest($id)
		{	
			$this->isSuperAdmin();
			$setimghidden1=  $this->input->post('hiddenimg');
			$this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');

			$this->form_validation->set_rules('title', 'title', 'required|strip_tags|xss_clean');
				//$this->form_validation->set_rules('userfile', 'Document', 'callback_file_check_pp3');
			$this->form_validation->set_rules('title_text', 'title_text', 'required|strip_tags|xss_clean');


			if(empty($_FILES['userfile']['name'])) 
			{                
				$updImage=$setimghidden1;
			}
			else{
				$config['upload_path']          = './uploads/';
				$config['allowed_types']        = 'jpeg|jpg|png'; 
				$config['max_size']     		= 1024 * 10;
				$config['encrypt_name'] 		= TRUE; 
				//$config['max_width'] 			= '1024';
				//$config['max_height'] 			= '768';
				$config['file_name']=$_FILES['userfile']['name'];
				$this->load->library('upload', $config);   
				$this->upload->initialize($config);
				if($this->upload->do_upload('userfile'))
				{
					$imagedetails= $this->upload->data();
					$updImage = $imagedetails['file_name'];					
				}
				else{
				}

			}

			if ($this->form_validation->run() == true){ 

				$data=array(
					'id'	  => $id,
					'title'       => trim($this->input->post('title')),
					'content'      => trim($this->input->post('title_text')),
					'image' 	  => $updImage);

			//echo "/<pre>";	print_r($data); die;
				$res = $this->MastersModel->testUpdate($id,$data); 
          		// print_r($data); die;
				if($res ==TRUE )
				{  	
					$this->session->set_flashdata('success',' Updated Successfully.');
					redirect('view-testimonials', 'refresh');
				} 
				else 
				{
					redirect('view-testimonials', 'refresh');
				} 
			}

		}
		public function delete($id)
		{
			$this->isSuperAdmin();

			$this->MastersModel->deleteTesti($id);

			$this->session->set_flashdata('success', 'Successfully Deleted!');

			redirect('view-testimonials');
		}
	}