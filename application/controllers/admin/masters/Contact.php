<?php
   defined('BASEPATH') or die('not allow to access script');
require APPPATH . '/libraries/BaseController.php';
   /**
   Contact 
   */
   class Contact extends BaseController
   {
   	
   	function __construct()
   	{
   		parent::__construct();
         $this->load->model('MastersModel');
         $this->isSuperAdmin();
               
   	}
	public function index1()
	{
      $data = '';

      $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');

      $this->form_validation->set_rules('address', 'address', 'required|strip_tags|xss_clean');
      $this->form_validation->set_rules('phone', 'phone', 'required|strip_tags|xss_clean');
      $this->form_validation->set_rules('email', 'email', 'required|strip_tags|xss_clean');

      if ($this->form_validation->run() == true)
      { 
            $data=array(
            'address' => trim($this->input->post('address')),
            'phone1'   => trim($this->input->post('phone')),
            'phone2'   => trim($this->input->post('phone2')),
            'phone3'   => trim($this->input->post('phone3')),
            'contact_email'   => trim($this->input->post('email')));

          // echo "<pre/>"; print_r($data);die;
            $res = $this->MastersModel->insertContact($data);            
            if($res ==TRUE )
            {     
            $this->session->set_flashdata('success',' Inserted Successfully.');
            redirect('view-contact', 'refresh');
            } 
            else
            {
               echo"error";
            } 
            }
            else 
            {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);
            $data['pageTitle'] = 'Contact';   
            $data['alldata']= $this->MastersModel->getContact();
            $this->load->view('admin/contact/add-contact',$data);
            } 
	}

      public function getContactById($id)
      { 
         $data['records'] = $this->MastersModel->getContactById($id);
         //echo "<pre/>";print_r($data);die;
         $this->load->view('admin/contact/edit-contact',$data);
      }

      public function index()
      {
         $data = "";
       $id = $this->input->post('id');  
      $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
      $this->form_validation->set_rules('address', 'address', 'required|strip_tags|xss_clean');
      $this->form_validation->set_rules('phone', 'phone', 'required|strip_tags|xss_clean');
      $this->form_validation->set_rules('email', 'email', 'required|strip_tags|xss_clean');
      $this->form_validation->set_rules('fphone', 'fphone', 'required|strip_tags|xss_clean');
      $this->form_validation->set_rules('faddress', 'faddress', 'required|strip_tags|xss_clean');
      $this->form_validation->set_rules('femail', 'femail', 'required|strip_tags|xss_clean');
      $this->form_validation->set_rules('fdescription', 'fdescription', 'required|strip_tags|xss_clean');

      if ($this->form_validation->run() == true)
      {
            $data=array(
               'id'   => $id,
            'address'       =>   trim($this->input->post('address')),
            'phone1'        =>   trim($this->input->post('phone')),
            'phone2'        =>   trim($this->input->post('phone2')),
            'phone3'        =>   trim($this->input->post('phone3')),
            'contact_email' =>   trim($this->input->post('email')),
            'fphone'        =>   trim($this->input->post('fphone')),
            'faddress'      =>   trim($this->input->post('faddress')),
            'femail'        =>   trim($this->input->post('femail')),
            'fdescription'  =>   trim($this->input->post('fdescription')));

           // echo "<pre/>"; print_r($data);die;
            $res = $this->MastersModel->updateContact($data,$id);             
            if($res ==TRUE ) 
            {     
                $this->session->set_flashdata('success',' Update Successfully.');
                redirect('view-contact', 'refresh');
            } 
          }
            else 
            {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);           
             $data['records']= $this->MastersModel->getContact();
            $this->load->view('admin/contact/edit-contact',$data);
            } 
      }

      public function delete($id)
      {
               $this->MastersModel->deleteContact($id);
               $this->session->set_flashdata('success', 'Successfully Deleted!'); 
               redirect('view-contact');
               
      }
}