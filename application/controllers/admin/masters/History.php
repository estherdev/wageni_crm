<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
Class History extends BaseController
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('MastersModel');
    }
    
    public function index()
    {
      /*  if (isset($this->input->post('sub'))) {
          
        } else {
          
        }*/
        
        $this->isSuperAdmin();
        $setimghidden1 = $this->input->post('imghidden');
        $a             = $this->input->post('hc_id');
        $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('title', 'title', 'required|strip_tags|xss_clean');
        //$this->form_validation->set_rules('userfile', 'Document', 'callback_file_check_pp3');
        $this->form_validation->set_rules('content', 'content', 'required|strip_tags|xss_clean');
        
        if (empty($_FILES['userfile']['name'])) {
            $updImage = $setimghidden1;
        } else {
            $config['upload_path']   = './uploads/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']      = 1024 * 10;
            $config['encrypt_name']  = TRUE;
            //$config['max_width']             = '1024';
            //$config['max_height']         = '768';
            $config['file_name']     = $_FILES['userfile']['name'];
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('userfile')) {
                $imagedetails = $this->upload->data();
                $updImage     = $imagedetails['file_name'];
            } else {
            }
            
        }
        if ($this->form_validation->run() == true) {
            $data = array(
                'hc_id' => $a,
                'title' => trim($this->input->post('title')),
                'content' => trim($this->input->post('content')),
                'image' => $updImage
            );
            
            $res = $this->MastersModel->historyUpdate($data, $a);
            if ($res == TRUE) {
                $this->session->set_flashdata('success', ' Updated Successfully.');
                redirect('admin/masters/History', 'refresh');
            }
        } else {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg', $error);
            $data = "";
            $data['records'] = $this->MastersModel->getHistoryDataId();
            $data['alldata']= $this->MastersModel->getHistory();
            $this->load->view('admin/history/History_View', $data);
        }
        
    }
    

    public function getHistoryDataId($id)
    {
       $this->isSuperAdmin();
       $data['records'] = $this->MastersModel->getHistoryDataId($id);
      	//echo "<pre/>";print_r($data);die;
       $this->load->view('admin/history/edit_history', $data);
   }

   public function insertHistory()
   {
       $this->isSuperAdmin();
       $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
      // $this->form_validation->set_rules('title', 'title', 'required|strip_tags|xss_clean');
       $this->form_validation->set_rules('date', 'date', 'required|strip_tags|xss_clean');
       $this->form_validation->set_rules('content', 'content', 'required|strip_tags|xss_clean');

       if ($this->form_validation->run() == true) {
        $data = array(
            'history_date' => trim($this->input->post('date')),
            'title' => trim($this->input->post('title')),
            'content' => trim($this->input->post('content'))
        );

            //echo "<pre/>";print_r($data);die;
        $res = $this->MastersModel->historyInsert($data);
            //print_r($data); die;
        if ($res == TRUE) {
            $this->session->set_flashdata('success', ' Insert Successfully.');
            redirect('admin/masters/History', 'refresh');
        } else {
            echo "error";
        }
    } else {
        $error = validation_errors();
        $this->session->set_flashdata('validationerrormsg', $error);
        $data = "";
           // $data['alldata']= $this->MastersModel->getHistory();
                // echo("<pre/>");print_r($data);die;
        $this->load->view('admin/history/History_View', $data);
    }

}

public function getHistory()
{
   $this->isSuperAdmin();
   $data['alldata'] = $this->MastersModel->getHistory();
   //echo("<pre/>");print_r($data);die;
   $this->load->view('admin/masters/History/get', $data);

}

public function getHistoryId($id)
{
   $this->isSuperAdmin();

   $data['records'] = $this->MastersModel->getHistoryId($id);
   $this->load->view('admin/history/edit_history', $data);
}

public function editHistory($id)
{
   $this->isSuperAdmin();

   $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');

   $this->form_validation->set_rules('date', 'date', 'required|strip_tags|xss_clean');
   //$this->form_validation->set_rules('title', 'title', 'required|strip_tags|xss_clean');
   $this->form_validation->set_rules('content', 'content', 'required|strip_tags|xss_clean');

   if ($this->form_validation->run() == true) {
    $data = array(
      'h_id' => $id,
      'history_date' => trim($this->input->post('date')),
      'title' => trim($this->input->post('title')),
      'content' => trim($this->input->post('content'))
    );

    $res = $this->MastersModel->updateHistory($data, $id);
    if ($res == TRUE) {
      $this->session->set_flashdata('success', ' Updated Successfully.');
      redirect('view-history', 'refresh');
    } else {
      echo "error";
    }
  } else {
    $error = validation_errors();
    $this->session->set_flashdata('validationerrormsg', $error);
    $this->load->view('admin/history/edit_history', $data);
  }

}

public function delete($id)
{
   $this->isSuperAdmin();
   $this->MastersModel->deleteHistory($id);
   $this->session->set_flashdata('success', 'Successfully Deleted!');
   redirect('admin/masters/History', 'refresh');
   
}
}