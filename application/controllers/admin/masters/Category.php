<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : SubsControler (UserController)
 * SubsControler Class to control all Website SubsControler related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 14 August 2018
 */
class Category extends BaseController
{ 
    public function __construct()
    {
        parent::__construct();  
    }



    private $category_counter=2;


    public function addUpdateCategory()
    {
        $data=array();
        $id=$this->uri->segment(2);
        $where=array('c.parent_id'=>0,'c.status'=>1);
        $data['categories']=$this->MastersModel->categoryList($where);
        if($id>0){
            $select=array('id'=>$id);
            $data['category']=$this->MastersModel->CategoryData($select);
             $data['heading']="Update Category";
             $data['button']="Update";
        }else{
            $data['category']['id']='';
            $data['category']['parent_id']='';
            $data['category']['name']='';
            $data['category']['image']='';
            $data['category']['status']=1;
            $data['category']['meta_title']='';
            $data['category']['meta_keywords']='';
            $data['category']['meta_description']='';
            $data['heading']="Add Category";
            $data['button']="Add";
        }
        $this->load->view('admin/category/manage-category',$data);
    }

    public function categoryHierarchy(){
       ///$this->isAjaxRequest();
        $parent_id=$this->input->post('parent_id');
        $where=array('c.parent_id'=>$parent_id);
        $response['categories']=$this->MastersModel->categoryList($where);
        $where=array('c.id'=>$parent_id);
        $this->recursive($where);
        $response['counter']=$this->category_counter;
        echo json_encode($response);
    }   
    public function categoryView(){
        $this->load->view('admin/category/view-caregory');
    }

    function recursive($where){
      if(count($where)>0){
          $level=$this->MastersModel->getCategoryLevel($where);
          $parent_id=$level['parent_id'];
          if($parent_id>0){
              $where=array('c.id'=>$parent_id);
               $this->recursive($where);
              $this->category_counter++;
          }
          return $this->category_counter;
      }
  }

    public function categoryLevel(){
   // $this->isAjaxRequest();
    $id=$this->input->post('id');
    $where=array('c.id'=>$id);
    $response['counter']=$this->recursive($where);
    echo json_encode($response);
}


public function categoryParentDom(){
    //$this->isAjaxRequest();
    $id=$this->input->post('id');
    $where=array('id'=>$id);
    $response=$where;
    $data=$this->MastersModel->categoryData($where);
    if($data){
        $response=$data;
    }
    echo json_encode($response);
}

    public function saveCategory(){
       // $this->isAjaxRequest();
        $response['status']=true;
        $image_error='';
        $name_error='';
        $image='';
        $old_image=$this->input->post('old_image');
        $this->form_validation->set_rules('name', 'Category Name', 'trim|required');
        $name=$this->input->post('name');
        $id=$this->input->post('id') ? $this->input->post('id'):0;
        if(!empty($name)){
            $where=array('name'=>$name);
            if($id>0){
                $where['id <>']=$id;
            }
            $response['status']=$this->MastersModel->checkExists('allcategory',$where);
            if(!$response['status']){
                $name_error="Category Name already exists!";
            }
        }



      

        if ($this->form_validation->run() == FALSE || $response['status']==FALSE) {
            $response=$this->form_validation->error_array();
            $response['status']=false;
            if(!empty($image_error)){
                $response['category_image']=$image_error;
            }
            if(!empty($name_error)){
                $response['name']=$name_error;
            }
            $response['message']='There is error in submitting form!';
        }else{
            $save_data=array(
                'id'=>$this->input->post('id')? $this->input->post('id'):0,
                'name'=>$this->input->post('name'),
                'parent_id'=>$this->input->post('parent_id'),
                'status'=>$this->input->post('status'),
                'meta_title'=>$this->input->post('meta_title'),
                'meta_keywords'=>$this->input->post('meta_keywords'),
                'meta_description'=>$this->input->post('meta_description'),
            );
            $save_data['image']=$image ? $image:'';
            $id=$this->MastersModel->AddUpdateData('allcategory',$save_data);
            $slug=makeSlug($save_data['name'].'-'.date('dmymis'));
            $update_data=array('id'=>$id,'slug'=>$slug,'modified_date'=>date('d-m-y m:i:s A'));
            $this->MastersModel->AddUpdateData('allcategory',$update_data);
            if($save_data['id']>0){
                $response['message']="Category Updated Successfully!";
            }else{
                $response['message']="Category Added Successfully!";
            }
            $this->session->set_flashdata('success_message',$response['message']);
            
        }
        echo json_encode($response);
    }


    public function manageCategories(){
       // $this->isAjaxRequest();
        $response['data']=$this->MastersModel->manageCategoriesAjax($_POST,false);
        $response['total']=$this->MastersModel->manageCategoriesAjax($_POST,true);
        $json_data = array(
            "recordsTotal"    => $response['total'],  
            "recordsFiltered" => $response['total'],
            "data"            => $response['data'] 
        );
        echo json_encode($json_data);
    }


    public function changeStatus(){
        //$this->isAjaxRequest();
        $id=$this->input->post('id');
        $status=$this->input->post('status');
        if($id>0){
            if($status==1){
                $update_data=array('id'=>$id,'status'=>$status);
                $response['status']=$this->MastersModel->changeDataStatus('allcategory',$update_data);
            }else{
                $response['status']=$this->categoryRecursiveChangeStatus($id);
            }
            
            if($response['status']){
                $response['message']="Category Status Updated Successfully!";
                
            }
            $data=array('status'=>1);
            echo json_encode($data);
        }
    }
    
    public function categoryRecursive($id){
        $where=array('c.parent_id'=>$id);
        $update_data=array('id'=>$id,'is_deleted'=>1);
        $this->MastersModel->AddUpdateData('allcategory',$update_data);
        $response=$this->MastersModel->categoryList($where,true);
        if(count($response)>0){
            foreach($response as $row){
                $this->categoryRecursive($row['id']);
            }
        }
        return true;
    }

    public function categoryRecursiveChangeStatus($id){
        $where=array('c.parent_id'=>$id);
        $update_data=array('id'=>$id,'status'=>0);
        $this->MastersModel->AddUpdateData('allcategory',$update_data);
        $response=$this->MastersModel->categoryList($where,true);
        if(count($response)>0){
            foreach($response as $row){
                $this->categoryRecursiveChangeStatus($row['id']);
            }
        }
        return true;
    }
     
 
 


    function deleteCategoryNew(){
        //$this->isAjaxRequest();
        $action_id=$this->input->post('action_id');
        if(!empty($action_id)){
            $select=array('id'=>$action_id);
            //$category_info=$this->Admin_model->categoryData($select);
            //$image=$category_info['image']; 
            //$response['status']=$this->Admin_model->deleteRecord($select,'category');
            $category_data=array('id'=>$action_id);
            //$customer_data['is_deleted']=1;
            $response['status']=$this->categoryRecursive($action_id);
            //$this->Admin_model->AddUpdateData('category',$customer_data);
            if($response['status']){
                $response['message']="Category Deleted Successfully!";
               
            }
            //unlinkImage('category',$image);
            echo json_encode($response);
        }
    }
   











   // Add Category
    public function addCategory()
    {
    	$this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : ADD Category ';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('category_name', 'Category', 'required|strip_tags|xss_clean|is_unique[category.category_name]');
       
        if ($this->form_validation->run() == true){
          
            $data = array('category_name'=>$this->input->post('category_name'));
            $result= $this->MastersModel->insertCategory($data = $this->security->xss_clean($data)); 
            if($result ==TRUE )
            {                       
                $this->session->set_flashdata('success','Category Added Successfully.');
                redirect('view-category');
            }  
        }else{
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error);          
                $this->global['category'] = $this->MastersModel->fetchCategory(); 
                //print_r( $this->global['category'] );die;
                $this->load->view("admin/category/addNewCategory", $this->global);      
            } 
    }
    
    //Get Category Id Data
    public function editCategory($id)
    {
        $this->isSuperAdmin();
        $this->global['category']= $this->MastersModel->fetchCategoryById($id);
       //  print_r( $this->global['category'] );die;
        $this->load->view("admin/category/editCategory", $this->global);           
    }
    //Update Category Id Data
    public function updateCategory($id)
    {
        $this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : Update Category';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('category_name', 'Category', 'required|strip_tags|xss_clean|is_unique[category.category_name]');
       
        if ($this->form_validation->run() == true){
            $data = array('cat_id'=>$id,'category_name'=>$this->input->post('category_name'));
            $result= $this->MastersModel->updateCategory($data = $this->security->xss_clean($data)); 
            if($result ==TRUE ){                       
                $this->session->set_flashdata('success','Category Update Successfully.');
                redirect('view-category');
            }  
        }else{ 
                 $this->session->set_flashdata('failed','Category Already Exist.');
                redirect('view-category');        
        }      
    }

    //Delete Category
    public function deleteCategory($id)
    {
        $this->isSuperAdmin();
        $result= $this->MastersModel->deleteCategory($id); 
        if ($result == true) {
             $this->session->set_flashdata('success','Category Deleted Successfully.');
            redirect('view-category','refresh');
        }else{
            redirect('view-category','refresh');
        }
    }

    /******************************************** Sub Category    ***********************************************/

   // Add Category
    public function addSubCategory()
    {
        $this->isSuperAdmin();
        //print_r($this->input->post());
        $this->global['pageTitle'] = 'wageniCRM : ADD Category ';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('main_cat_id', 'Category', 'required|strip_tags|xss_clean|callback_select_Category');
        $this->form_validation->set_rules('sub_cat_name', 'Sub Category', 'required|strip_tags|xss_clean');
       
        if ($this->form_validation->run() == true){
          
            $data = array('main_cat_id'=>$this->input->post('main_cat_id'),'sub_cat_name'=>$this->input->post('sub_cat_name'));
            $result= $this->MastersModel->insertSubCategory($data = $this->security->xss_clean($data)); 
            if($result ==TRUE )
            {                       
                $this->session->set_flashdata('success','Sub Category Added Successfully.');
                redirect('view-sub-category');
            }  
        }else{  
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error); 
                $this->global['category'] = $this->MastersModel->fetchCategory();          
                $this->global['subcategory'] = $this->MastersModel->fetchSubCategory(); 
                //print_r( $this->global['category'] );die;
                $this->load->view("admin/category/addNewSubCategory", $this->global);      
            } 
    }
    
    public function select_Category($selectValue)
    {
        if($selectValue == 'none')
        {
            $this->form_validation->set_message('select_Category', 'The Category field is required Please Select one.');
            return false;
        }
        else 
        {
            return true;
        }
    }

    public function sub_select_Category($selectValue)
    {
        if($selectValue == '0')
        {
            $this->form_validation->set_message('select_Category', 'The Category field is required Please Select one.');
            return false;
        }
        else 
        {
            return true;
        }
    }
    
    //Get Category Id Data
    public function editSubCategory($id)
    {
        $this->isSuperAdmin();
        $this->global['subcategory']= $this->MastersModel->fetchSubCategoryById($id);
        $this->global['category'] = $this->MastersModel->fetchCategory();  
       //  print_r( $this->global['category'] );die;
        $this->load->view("admin/category/editSubCategory", $this->global);           
    }
    //Update Category Id Data
    public function updateSubCategory($id)
    {
        $this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : Update Sub Category';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
                $this->form_validation->set_rules('main_cat_id', 'Category', 'required|strip_tags|xss_clean|callback_sub_select_Category');
        $this->form_validation->set_rules('sub_cat_name', 'Sub Category', 'required|strip_tags|xss_clean');
       
        if ($this->form_validation->run() == true){
            $data = array('sub_cat_id'=>$id,'main_cat_id'=>$this->input->post('main_cat_id'),'sub_cat_name'=>$this->input->post('sub_cat_name'));
            $result= $this->MastersModel->updateSubCategory($data = $this->security->xss_clean($data)); 
            if($result ==TRUE ){                       
                $this->session->set_flashdata('success','Sub Category Update Successfully.');
                redirect('view-sub-category');
            }  
        }else{ 
                redirect('view-sub-category');        
        }      
    }

    //Delete Category
    public function deleteSubCategory($id)
    {
        $this->isSuperAdmin();
        $result= $this->MastersModel->deleteSubCategory($id); 
        if ($result == true) {
             $this->session->set_flashdata('success','Sub Category Deleted Successfully.');
            redirect('view-sub-category','refresh');
        }else{
            redirect('view-sub-category','refresh');
        }
    }


 /******************************************** Category Process ***********************************************/
    public function addCategoryProcess()
    {
        $this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : ADD Process ';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('main_cat_id', 'Category', 'required|strip_tags|xss_clean|callback_select_Category');
        $this->form_validation->set_rules('subcat', 'Sub Category', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('process_name', 'Category Process', 'required|strip_tags|xss_clean');
       
        if ($this->form_validation->run() == true){
          
            $data = array(
            	'main_cat_id'     => $this->input->post('main_cat_id'),
                'sub_category_id' => $this->input->post('subcat'),
                'sub_sub_cat_name'=> $this->input->post('process_name')
            );
           // echo "<pre/>"; print_r($data);die;
            $result= $this->MastersModel->insertCategoryProcess($data = $this->security->xss_clean($data)); 
            if($result ==TRUE )
            {                       
                $this->session->set_flashdata('success','Category Process Added Successfully.');
                redirect('view-category-process');
            }  
        }else{
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error);          
                $this->global['category'] = $this->MastersModel->fetchCategory(); 
                $this->global['process'] = $this->MastersModel->fetchCategoryProcess(); 
                //print_r( $this->global['process'] );die;
                $this->load->view("admin/category/addNewSubSubCategory", $this->global);      
            } 
    }

    function select_Sub_Category($selectValue)
    {
        if($selectValue == 'none')
        {
            $this->form_validation->set_message('select_Sub_Category', 'The Sub Category field is required Please Select one.');
            return false;
        }
        else 
        {
            return true;
        }
    }

    //Get Category Id Data
    public function editCategoryProcess($id)
    {
        $this->isSuperAdmin();
        $this->global['subcategory']= $this->MastersModel->fetchSubCategoryById($id);
        $this->global['category'] = $this->MastersModel->fetchCategory(); 
        $this->global['records'] = $this->MastersModel->fetchCatProcessById($id); 
        //echo("<pre/>");print_r($this->global['records']);die;
        $this->load->view("admin/category/editSubSubCategory", $this->global);           
    }


        /* Update the Category Process */
     public function updateCategoryProcess($id)
    {
        $this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : ADD Process ';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('main_cat_id', 'Category', 'required|strip_tags|xss_clean|callback_select_Category');
        $this->form_validation->set_rules('subcat', 'Sub Category', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('process_name', 'Category Process', 'required|strip_tags|xss_clean');
       
        if ($this->form_validation->run() == true){
          
            $data = array(
                'sub_sub_cat_id' => $id,
                'main_cat_id'     => $this->input->post('main_cat_id'),
                'sub_category_id' => $this->input->post('subcat'),
                'sub_sub_cat_name'=> $this->input->post('process_name')
            );
           //echo "<pre/>"; print_r($data);die;
            $result= $this->MastersModel->updateCategoryProcess($data,$id); 
            if($result ==TRUE )
            {                       
                $this->session->set_flashdata('success','Category Process Updated Successfully.');
                redirect('view-category-process');
            }  
        }else{
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error);          
                $this->global['category'] = $this->MastersModel->fetchCategory(); 
                $this->global['process'] = $this->MastersModel->fetchCategoryProcess(); 
                //print_r( $this->global['process'] );die;
                //$this->load->view("admin/category/editSubSubCategory", $this->global);      
            } 
    }

    //Delete Category
    public function deleteCategoryProcess($id)
    {
        $this->isSuperAdmin();
        $result= $this->MastersModel->deleteCategoryProcess($id); 
        if ($result == true) {
             $this->session->set_flashdata('success','Category Process Deleted Successfully.');
            redirect('view-category-process','refresh');
        }else{
            redirect('view-category-process','refresh');
        }
    }


    
   
}

?>