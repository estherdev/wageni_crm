<?php


class CategoryDrop extends CI_Controller {


   public function __construct() { 
      parent::__construct();
      $this->load->database();
   }


   public function index() {
      $categories = $this->db->get("category")->result();
     // echo("<pre/>");print_r($categories);die;
      $this->load->view('admin/catdrop/catdrop', array('categories' => $categories )); 
   } 


   public function catDropAjax($id) { 
       $result = $this->db->where("main_cat_id",$id)->get("sub_category")->result();
       echo json_encode($result);
   }
    

} 


?>