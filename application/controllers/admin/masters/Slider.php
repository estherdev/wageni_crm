<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Slider (BaseController)
 * User Class to control all user related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 28 August 2018
 */
class Slider extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();  
        $this->load->model('MastersModel');
        $this->isSuperAdmin();
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('slider_title', 'Slider Title', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('slider_sub_title', 'Slider Sub Title', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('slider_link_title', 'Slider Link Title', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('slider_link', 'Slider Link', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('slider_image', 'Slider Image', 'callback_file_check_hsp');

        
        if ($this->form_validation->run() == true){

             //====******   Slider Images   *****====//
                                    
            $config['upload_path']          = 'uploads/Slider/';
            $config['allowed_types']        = 'jpeg|jpg|png'; 
            $config['max_size']             = 1024 * 10;
            $config['encrypt_name']         = TRUE;
            $config['remove_spaces']        = TRUE;
            //$config['max_width']          = '1024';
            //$config['max_height']             = '768';
            $config['file_name']=$_FILES['slider_image']['name'];
            $this->load->library('upload', $config);   
            $this->upload->initialize($config);
            $this->upload->do_upload('slider_image');
            $imagedetails= $this->upload->data();
            $homeSliderImage='uploads/Slider/'.$imagedetails['file_name'];
          
            $data = array('title'=>html_escape(trim($this->input->post('slider_title', TRUE))),
                            'sub_title'=>html_escape(trim($this->input->post('slider_sub_title', TRUE))),      
                            'url_link_text'=>html_escape(trim($this->input->post('slider_link_title', TRUE))),      
                            'url_link'=>html_escape(trim($this->input->post('slider_link', TRUE))),      
                            'slider_image'=> $homeSliderImage     
                        );
            $dataresult= $this->MastersModel->insertSlider($data = $this->security->xss_clean($data)); 
            if($dataresult ==TRUE )
            {                       
                $this->session->set_flashdata('success','Slider Added Successfully.');
                redirect('view-slider');
            }  
        }
        else {
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error);                    
                $this->global['homeslider'] = $this->MastersModel->fetchSlider();  
                $this->global['pageTitle'] = 'wageniCRM : Home Page Slider';
                $this->load->view("admin/slider/viewallslider", $this->global); 
        } 
    }
    public function deleteSlider($id)
    {
        $result= $this->MastersModel->deleteSlider($id); 
        if ($result == true) {
            $this->session->set_flashdata('success','Slider Deleted Successfully.');
            redirect('view-slider','refresh');
        }else{
            redirect('view-slider','refresh');
        }
    }
    
    //  Start Home Sage Slider Photo Validation 
    public function file_check_hsp($str){
         $allowed_mime_type_arr = array('image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
        $mime = get_mime_by_extension($_FILES['slider_image']['name']);
        if(isset($_FILES['slider_image']['name']) && $_FILES['slider_image']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            }else{
                $this->form_validation->set_message('file_check_hsp', 'Please select only jpg, jpeg, pjpeg, png');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check_hsp', 'Please choose a file to upload.');
            return false;
        }
    } 

    public function getSliderById($id)
    {
        $this->global['sliderById']= $this->MastersModel->gethomepage_slider($id);
        $this->load->view("admin/slider/editslider", $this->global); 
    }  
    public function updateSlider($id)
    {
       
        $setimage=  $this->input->post('imghidden');
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('slider_title', 'Slider Title', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('slider_sub_title', 'Slider Sub Title', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('slider_link_title', 'Slider Link Title', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('slider_link', 'Slider Link', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('slider_image', 'Slider Image', 'callback_file_check_uhsp');

            //====******   Slider Photo   *****====//

            if(empty($_FILES['slider_image']['name']))
                {                
                    $homeSliderImage=$setimage;
                }
                else
                {
                    $config['upload_path']          = 'uploads/Slider/';
                    $config['allowed_types']        = 'jpeg|jpg|png'; 
                    $config['max_size']             = 1024 * 10;
                    $config['encrypt_name']         = TRUE;
                    $config['remove_spaces']        = TRUE;
                    //$config['max_width']          = '1024';
                    //$config['max_height']         = '768';
                    $config['file_name']=$_FILES['slider_image']['name'];
                    $this->load->library('upload', $config);   
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('slider_image'))
                    {
                       $imagedetails= $this->upload->data();
                       $homeSliderImage='uploads/Slider/'.$imagedetails['file_name'];                      
                    }else{
                        
                    }
                }               
            //====******    End Slider Photo   *****====//
            if ($this->form_validation->run() == true){
             
            $homepage_sliderData = array('slider_id'=>$id,
                'title'=>html_escape(trim($this->input->post('slider_title', TRUE))),
                'sub_title'=>html_escape(trim($this->input->post('slider_sub_title', TRUE))),      
                'url_link_text'=>html_escape(trim($this->input->post('slider_link_title', TRUE))),      
                'url_link'=>html_escape(trim($this->input->post('slider_link', TRUE))),      
                'slider_image'=> $homeSliderImage     
            );
            //echo "<pre/>"; print_r($homepage_sliderData);die;
            $dataresult= $this->MastersModel->updateSlider($homepage_sliderData = $this->security->xss_clean($homepage_sliderData)); 
            if($dataresult ==TRUE )
            {                       
                $this->session->set_flashdata('success','Slider Updated Successfully.');
                redirect('view-slider');
            }else{
                //$error = validation_errors();
                //$this->session->set_flashdata('failed',$error);  
                $this->session->set_flashdata('failed','Failed.');
                redirect('view-slider');
            }  
        }
    }
     public function file_check_uhsp($str){
         $allowed_mime_type_arr = array('image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
        $mime = get_mime_by_extension($_FILES['slider_image']['name']);
        if(isset($_FILES['slider_image']['name']) && $_FILES['slider_image']['name']!=""){
            if(in_array($mime, $allowed_mime_type_arr)){
                return true;
            }else{
                $this->form_validation->set_message('file_check_uhsp', 'Please select only jpg, jpeg, pjpeg, png');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check_uhsp', 'Please choose a file to upload.');
            return true;
        }
    }   
    
    
}

?>