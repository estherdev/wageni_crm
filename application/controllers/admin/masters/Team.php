<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
Class Team extends BaseController
{

	function __construct()
	{ 
		parent::__construct();		
		$this->load->model('MastersModel');
		$this->load->library('form_validation');
		//$this->isSuperAdmin();

	}
			/* Insert Team Data */
	public function index()
	{	
		$this->isSuperAdmin();
		$setimghidden1=  $this->input->post('imghidden');
		$a =$this->input->post('id');
		$this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
		$this->form_validation->set_rules('name','name','required|strip_tags|xss_clean');
		$this->form_validation->set_rules('post','post','required|strip_tags|xss_clean');
		//$this->form_validation->set_rules('userfile', 'Document', 'callback_file_check_pp3');
		$this->form_validation->set_rules('content', 'content', 'required|strip_tags|xss_clean');

		if(empty($_FILES['userfile']['name'])) 
		{                
			$updImage=$setimghidden1;
		}
		else{
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'jpeg|jpg|png'; 
			$config['max_size']     		= 1024 * 10;
			$config['encrypt_name'] 		= TRUE; 
					//$config['max_width'] 			= '1024';
					//$config['max_height'] 		= '768';
			$config['file_name']=$_FILES['userfile']['name'];
			$this->load->library('upload', $config);   
			$this->upload->initialize($config);
			if($this->upload->do_upload('userfile'))
			{
				$imagedetails= $this->upload->data();
				$updImage=$imagedetails['file_name'];						
			}
			else{
			}

		}
	if ($this->form_validation->run() == true){ 

		$data=array(
			'name'           => trim($this->input->post('name')),
			'designation'    => trim($this->input->post('post')),
			'content'        => trim($this->input->post('content')),
			'image' 	     => $updImage);

		//echo "/<pre>";	print_r($data); die;
		$res = $this->MastersModel->teamInsert($data); 
      		//echo("<pre/>"); print_r($data); die;
		if($res ==TRUE )
		{  	
			$this->session->set_flashdata('success',' Inserted Successfully.');
			redirect('view-team', 'refresh');
		} 
		else
		{
			echo "error";
		} 
	}
	else 
	{
		$error = validation_errors();
		$this->session->set_flashdata('validationerrormsg',$error);
		$data="";   
		$data['teamdata']= $this->MastersModel->fetchTeam(); 
		//echo("<pre/>");print_r($data);die;
		$this->load->view('admin/team/add-team',$data);
	} 

}
		/* Fetch Team data by Id  */
	public function getTeambyid($id)
	{ 
	    $this->isSuperAdmin();
		$data['records'] = $this->MastersModel->getTeamdatabyid($id);
	    //echo("<pre/>");print_r($data);die;
		$this->load->view('admin/team/edit-team',$data);

	}
	
	/* Update Team Data */
	public function editTeam($id)
	{

	    $this->isSuperAdmin();
		$setimage= $this->input->post('hiddenimg');
		$this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
		$this->form_validation->set_rules('name', 'name', 'required|strip_tags|xss_clean');
		$this->form_validation->set_rules('post','post','required|strip_tags|xss_clean');       
		$this->form_validation->set_rules('content', 'content', 'required|strip_tags|xss_clean');

		if ($this->form_validation->run() == true){

			if(empty($_FILES['userfile']['name'])) 
			{                
				$updImage=$setimage;
			}
			else{

				$config['upload_path']     =  './uploads/';
				$config['allowed_types']   =  'jpeg|jpg|png'; 
				$config['max_size']        =  1024 * 10;
				$config['encrypt_name']    =  TRUE;

				$config['file_name']=$_FILES['userfile']['name'];

				$this->load->library('upload', $config);   
				$this->upload->initialize($config);
				$this->upload->do_upload('userfile');
				$imagedetails= $this->upload->data();
				$updImage=$imagedetails['file_name'];
			} 
			$data=array(
				'id'          => $id,
				'image'       => $updImage,
				'name'        => $this->input->post('name'),
				'designation' => $this->input->post('post'),
				'content'     => $this->input->post('content'));

	            //echo("<pre/>");print_r($data);die;
			$res = $this->MastersModel->updateTeam($data,$id); 
	            //echo("<pre/>");print_r($data);die;
			if($res ==TRUE )
			{   

				$this->session->set_flashdata('success',' Updated Successfully.');
				redirect('view-team', 'refresh');
			}  
		}
		else 
		{
			$error = validation_errors();
			$this->session->set_flashdata('validationerrormsg',$error);                         
			$this->load->view('admin/practice/edit-team',$data);
		} 

	}

		/* Delete Team Data  */
	public function delete($id)
	{
	    $this->isSuperAdmin();
		$this->MastersModel->deleteTeam($id);       
		$this->session->set_flashdata('success', 'Successfully Deleted!');       
		redirect('view-team');
	}    
}