<?php defined('BASEPATH') or die('not allow access to script');
  require APPPATH . '/libraries/BaseController.php';
   class Company extends BaseController{
        function __construct(){
        parent::__construct();
     }

    public function index(){
          
          $this->isSuperAdmin();
          $subscriber_end_date= date('m-d-Y', strtotime('+364 days') );
          $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
          $this->form_validation->set_rules('subscribername', 'Name', 'required|strip_tags|xss_clean|callback_select_subscriber');
          $this->form_validation->set_rules('packagename', 'Price', 'required|strip_tags|xss_clean|callback_select_package');
          if ($this->form_validation->run() == true){
               $data = array('subscriber_id'=>html_escape(trim($this->input->post('subscribername', TRUE))),
                    'subscriber_package_id'=>html_escape(trim($this->input->post('packagename', TRUE))),    
                    'subscriber_start_date'=>date('m-d-Y'),
                    'subscriber_end_date'=>$subscriber_end_date,
                    /*'package_desc'=>$packagedesc   */
               );
               $packagesresult= $this->MastersModel->insertCompany($data = $this->security->xss_clean($data)); 
               if($packagesresult ==TRUE )
               {              
                    $this->session->set_flashdata('success','Company Added Successfully.');
                    redirect('add-company', 'refresh');
               }  
          }
      else
      {
       $error = validation_errors();
       $this->session->set_flashdata('validationerrormsg',$error);
       $this->global['company'] = $this->MastersModel->fetchCompanyUser();  
       $this->global['package'] = $this->MastersModel->fetchPackage();  
       $this->global['viewcompany'] = $this->MastersModel->fetchCompany();  
       $this->load->view("admin/company/add-company", $this->global);   
     }      
   }
     // Validation dropdown company
   public function select_subscriber($selectValue)
   {
     if($selectValue == 'none'){
       $this->form_validation->set_message('select_subscriber', 'The company is required Please Select one.');
       return false;
     }else{
       return true;
     }
   } 
     // Validation dropdown package
   public function select_package($selectValue)
   {
     if($selectValue == 'none'){
       $this->form_validation->set_message('select_package', 'The Package is required Please Select one.');
       return false;
     }else{
       return true;
     }
   } 

   /*New Company */
   public function newCompany(){
      $this->isSuperAdmin();
     $data['viewnewcompany'] = $this->MastersModel->fetchNewCompany();  
     $this->load->view("admin/company/view-new-company", $data);    
   }

  /*ADMIN MANAGE HOTEL STATUS */

   public function companyStatus1($Id,$Status) {
     $NewStatus;
     if($Status=='1'){
       $NewStatus='0';
     }else{
       $NewStatus='1';     
     }
     $result=$this->MastersModel->statusCompany($Id,$NewStatus);
     if($result>0)   {
       $this->session->set_flashdata('success','Status Change Successfully');

        $this->companyAdminSurveyStatus($Id,$Category);

       redirect('admin-add-company');
     }else{
       $this->session->set_flashdata('lock','Status Change Failed');
       redirect('admin-add-company');
     }
   }

   public function companyAdminSurveyStatus($Id,$Category){
      $result=$this->MastersModel->statusSurveyCompany($Id,$Category);

    // prd($result);
      for($i=0; $i<count($result);$i++)
        {

          /*$questions = array('businessId'=>$Id,
                    'bus_q_cat_id'=>$Category,
                    'bus_q_sub_cat_id'=>$result[$i][4],
                    'bus_cat_process'=>$result[$i][5],
                    'bus_q_que'=>$result[$i][1],
                    'bus_q_type'=>$result[$i][2]
                  );

                 // $questionnaire=$this->MastersModel->masterinsert('questionnaire',$arraydata);
                  $this->db->insert('questionnaire',$questions);
                  $que_last_insert_id= $this->db->insert_id();

                for($j=0;$j<count($result[$i][3]); $j++){

                   $answer = array('bus_que_id'=>$insert_id,
                                   'bus_que_type'=>$result[$i][3][$j]->que_type,
                                   'bus_que_score'=>$result[$i][3][$j]->que_score
                            );

                  $this->db->insert('business_questionnaire_answer',$answer);
                  $answer_last_insert_id= $this->db->insert_id();

                  }
*/
           $que = "INSERT INTO questionnaire(businessId,bus_q_cat_id,bus_q_sub_cat_id,bus_cat_process,bus_q_que,bus_q_type) VALUES ('".$Id."','".$Category."','".$result[$i][4]."','".$result[$i][5]."','".$result[$i][1]."','".$result[$i][2]."')";

                $result_que= $this->db->query($que); 
                $lastID= $this->db->insert_id();

                for($j=0;$j<count($result[$i][3]); $j++)
                {
                    $que_options = "INSERT INTO business_questionnaire_answer(bus_que_id,bus_que_type,bus_que_score) VALUES ('".$lastID."','".$result[$i][3][$j]->que_type."','".$result[$i][3][$j]->que_score."')";
                    $result_que_options= $this->db->query($que_options);                      
                }             
        }
       // return true;        
        //$this->session->set_flashdata('success','Import Successfully');
        //redirect('admin-add-company');
    }

    //This function is  (Only Admin)  Status  */
   public function companyStatus($Id,$Status,$Category)
   {

      $Ques=$this->MastersModel->CheckQuestionStatusCompany($Id);

        if ($Ques==true)
        {
              $NewStatus; 

              if($Status=='1'){
                    $NewStatus='0';
                    $NewStatusString="InActive";
              }else{
                  $NewStatus='1'; 
                  $NewStatusString="Active";      
              }

              $res=$this->MastersModel->statusCompany($Id,$NewStatus);

              if($res == true){
                 /* $this->db->select('sub_email,fname,lname');
                  $this->db->where('sub_login_id',$Id);
                  $company=$this->db->get('subscriberlogin')->result();

                  $email=$company[0]->sub_email;
                  $name=$company[0]->fname;
                  $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'mastcrew22@gmail.com',
                    'smtp_pass' => 'crew@1234',
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                  );    

                $mailMsg= '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>WageniCRM Newsletter</title><link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet"></head><body style="background-color: #f1f1f1; font-family: raleway, sans-serif; font-size: 15px; margin: 0; padding: 0;"><table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f1f1f1; font-family: "raleway", sans-serif; font-size: 15px;"><tr><td align="center"><table border="0" cellpadding="10" cellspacing="0" width="600" align="center"><tr><td align="center"><table border="0" cellpadding="15" cellspacing="0" width="600" style="background: #fff; border-bottom: 1px solid #ccc;"><tr><td align="center"><a href=""><img src="http://wageni.us.tempcloudsite.com/globalassets/admin/logo1.png"></a></td></tr></table><table border="0" cellpadding="10" cellspacing="10" width="600" style="background: #fff;"><tr><td align="center" style="font-size: 30px; font-weight: bold; color: #10a1da; letter-spacing: 1px;">Dear '.$name.'</td></tr><tr><td> Your Status has Been changed to '.$NewStatusString.'</td></tr><tr></tr><tr><td><p>Regards,</p><p style="font-style: italic;">The WageniCRM team</p><p>This is an automatically generated email, please do not reply</p></td></tr></table></td></tr></table></td></tr></table></body></html>';
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");      
                $this->email->from('mastcrew22@gmail.com', 'Status Changed');             
                $this->email->to($email);
                $this->email->subject('WageniCRM');
                $this->email->message($mailMsg);
                $sent = $this->email->send();   */                       
          } 

          $result=$this->MastersModel->statusSurveyCompany($Id,$Category);
          //echo $this->db->last_query();
        // echo "<pre/>"."=====================";print_r($result); die;
          for($i=0; $i<count($result);$i++)

            {

/*
               $questions = array('businessId'=>$Id,
                    'bus_q_cat_id'=>$Category,
                    'bus_q_sub_cat_id'=>$result[$i][4],
                    'bus_cat_process'=>$result[$i][5],
                    'bus_q_que'=>$result[$i][1],
                    'bus_q_type'=>$result[$i][2]
                  );

                 // $questionnaire=$this->MastersModel->masterinsert('questionnaire',$arraydata);
                  $this->db->insert('questionnaire',$questions);
                  $que_last_insert_id= $this->db->insert_id();

                for($j=0;$j<count($result[$i][3]); $j++){

                   $answer = array('bus_que_id'=>$insert_id,
                                   'bus_que_type'=>$result[$i][3][$j]->que_type,
                                   'bus_que_score'=>$result[$i][3][$j]->que_score
                            );

                  $this->db->insert('business_questionnaire_answer',$answer);
                  $answer_last_insert_id= $this->db->insert_id();

                  }*/

               $que = "INSERT INTO questionnaire(businessId,bus_q_cat_id,bus_q_sub_cat_id,bus_cat_process,bus_q_que,bus_q_type) VALUES ('".$Id."','".$Category."','".$result[$i][4]."','".$result[$i][5]."','".$result[$i][1]."','".$result[$i][2]."')";

                    $result_que= $this->db->query($que); 
                    $lastID= $this->db->insert_id();

                    for($j=0;$j<count($result[$i][3]); $j++)
                    {
                        $que_options = "INSERT INTO business_questionnaire_answer(bus_que_id,bus_que_type,bus_que_score) VALUES ('".$lastID."','".$result[$i][3][$j]->que_type."','".$result[$i][3][$j]->que_score."')";
                        $result_que_options= $this->db->query($que_options);         
                  }             
          }

        //redirect('admin-view-customer');
         // die;
        } 
        else
        {

        $NewStatus;
            if($Status=='1'){
              $NewStatus='0';
              $NewStatusString="InActive";
            }else{
              $NewStatus='1'; 
              $NewStatusString="Active";      
            }       

          $result=$this->MastersModel->statusCompany($Id,$NewStatus);
           // print_r($result);
          if($result == true) {
             // $company=$this->CrewModel->getcompany_emailid($Id);

            $this->db->select('sub_email,fname,lname');
            $this->db->where('sub_login_id',$Id);
            $company=$this->db->get('subscriberlogin')->result();

            $email=$company[0]->sub_email;
            $name=$company[0]->fname;
            $config = Array(
              'protocol' => 'smtp',
              'smtp_host' => 'ssl://smtp.googlemail.com',
              'smtp_port' => 465,
              'smtp_user' => 'mastcrew22@gmail.com',
              'smtp_pass' => 'crew@1234',
              'mailtype'  => 'html', 
              'charset'   => 'iso-8859-1'
            );              
            $mailMsg= '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>WageniCRM Newsletter</title><link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet"></head><body style="background-color: #f1f1f1; font-family: raleway, sans-serif; font-size: 15px; margin: 0; padding: 0;"><table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f1f1f1; font-family: "raleway", sans-serif; font-size: 15px;"><tr><td align="center"><table border="0" cellpadding="10" cellspacing="0" width="600" align="center"><tr><td align="center"><table border="0" cellpadding="15" cellspacing="0" width="600" style="background: #fff; border-bottom: 1px solid #ccc;"><tr><td align="center"><a href=""><img src="http://wageni.us.tempcloudsite.com/globalassets/admin/logo1.png"></a></td></tr></table><table border="0" cellpadding="10" cellspacing="10" width="600" style="background: #fff;"><tr><td align="center" style="font-size: 30px; font-weight: bold; color: #10a1da; letter-spacing: 1px;">Dear '.$name.'</td></tr><tr><td> Your Status has Been changed to '.$NewStatusString.'</td></tr><tr></tr><tr><td><p>Regards,</p><p style="font-style: italic;">The WageniCRM team</p><p>This is an automatically generated email, please do not reply</p></td></tr></table></td></tr></table></td></tr></table></body></html>';
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");      
            $this->email->from('mastcrew22@gmail.com', 'Status Changed');             
            $this->email->to($email);
            $this->email->subject('WageniCRM');
            $this->email->message($mailMsg);
            $sent = $this->email->send();

            redirect('admin-add-company');
        }
    }
  }  
  public function companyAdminStatus($Id,$Status)
   {
    //echo "string"; die();
    $NewStatus;
    if($Status=='1'){
      $NewStatus='0';
      $NewStatusString="InActive";
    }else{
      $NewStatus='1'; 
      $NewStatusString="Active";      
    }

    $result=$this->MastersModel->statusCompany($Id,$NewStatus);
     // print_r($result);
    if($result == true) {
       // $company=$this->CrewModel->getcompany_emailid($Id);

      $this->db->select('sub_email,fname,lname');
      $this->db->where('sub_login_id',$Id);
      $company=$this->db->get('subscriberlogin')->result();

      $email=$company[0]->sub_email;
      $name=$company[0]->fname;
      $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => 'mastcrew22@gmail.com',
        'smtp_pass' => 'crew@1234',
        'mailtype'  => 'html', 
        'charset'   => 'iso-8859-1'
      );              
      $mailMsg= '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>WageniCRM Newsletter</title><link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet"></head><body style="background-color: #f1f1f1; font-family: raleway, sans-serif; font-size: 15px; margin: 0; padding: 0;"><table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f1f1f1; font-family: "raleway", sans-serif; font-size: 15px;"><tr><td align="center"><table border="0" cellpadding="10" cellspacing="0" width="600" align="center"><tr><td align="center"><table border="0" cellpadding="15" cellspacing="0" width="600" style="background: #fff; border-bottom: 1px solid #ccc;"><tr><td align="center"><a href=""><img src="http://wageni.us.tempcloudsite.com/globalassets/admin/logo1.png"></a></td></tr></table><table border="0" cellpadding="10" cellspacing="10" width="600" style="background: #fff;"><tr><td align="center" style="font-size: 30px; font-weight: bold; color: #10a1da; letter-spacing: 1px;">Dear '.$name.'</td></tr><tr><td> Your Status has Been changed to '.$NewStatusString.'</td></tr>  
      <tr></tr><tr><td><p>Regards,</p><p style="font-style: italic;">The WageniCRM team</p><p>This is an automatically generated email, please do not reply</p></td></tr></table></td></tr></table></td></tr></table></body></html>';
      $this->load->library('email', $config);
      $this->email->set_newline("\r\n");      
      $this->email->from('mastcrew22@gmail.com', 'Status Changed');             
      $this->email->to($email);
      $this->email->subject('WageniCRM');
      $this->email->message($mailMsg);
      $result = $this->email->send();
       $this->session->set_flashdata('success','Status Change Successfully');
      redirect('add-subscriber-employee');
    }else{
       $this->session->set_flashdata('failed','Status Change failed');
      redirect('add-subscriber-employee');
    }
  }  

  }

  ?>