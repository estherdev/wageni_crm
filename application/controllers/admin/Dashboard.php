<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Dashboard (UserController)
 * User Class to control all user related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 28 August 2018
 */
class Dashboard extends BaseController
{
    public function __construct()    {
        parent::__construct();  
        $this->isSuperAdmin();
        $this->load->model(array('All_company_model' => 'ACM','AuthModel' => 'AUTHM', 'AuthModel' => 'AM', 'PackageModel' => 'PM', 'MastersModel' => 'MM'));
    }
    
    // Admin Dashboard
    public function index()    {
      //
        //if ($this->session->userdata('admin_login_id')==true) {
            $this->global['pageTitle'] = 'wageniCRM : Dashboard';
            $this->global['totalCompany'] = $this->common->totalCompany();
            $this->global['totalSubscriber'] = $this->common->totalSubscriber();
            $this->global['complaints']=$this->MM->adminGetAllComplaintsAjaxforgraph();
            //echo "<pre>";print_r($this->global['complaints']);die;
            $this->load->view("admin/index", $this->global);
        /*}else{
            redirect('loginMe');
        }*/
    }

    public function subscriber_dashboard()    {
        //if ($this->session->userdata('sub_login_id')==true) {
            $this->global['pageTitle'] = 'wageniCRM : Subscribe Dashboard';
            $this->global['totalCompany'] = $this->common->totalCompany();
            $this->global['totalSubscriber'] = $this->common->totalSubscriber();
            $this->load->view("admin/index", $this->global);
        /*}else{
        	redirect('subloginMe');
        }*/
    }

    public function catid()     {
        $categories = $this->db->get("category")->result();
        // echo("<pre/>");print_r($categories);die;
        //$this->load->view('admin/serve/serve-questions', array('categories' => $categories )); 
    } 

    public function subcatid($id)     { 
        $result = $this->db->where("main_cat_id",$id)->get("sub_category")->result();
        echo json_encode($result);
    }

    // Insert Questionnaire
    public function questionSubmit()     {

        $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
        //$this->form_validation->set_rules('category1', 'Category', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('cat', 'Category', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('subcat', 'Sub Category', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('subsubcat', 'Process', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('surveyQue', 'Question', 'required|strip_tags|xss_clean');
        
        if($this->form_validation->run() == true){            
              $data= array('q_cat_id'=>$this->input->post('cat'),
                            'q_sub_cat_id'=>$this->input->post('subcat'),
                            'cat_process'=>$this->input->post('subsubcat'),
                            'q_que'=>$this->input->post('surveyQue'),
                            'q_type'=>$this->input->post('q_type')
                );  

               /*  $data= array('q_cat_id'=>$this->input->post('parent_id'),
                            'q_que'=>$this->input->post('surveyQue'),
                            'q_type'=>$this->input->post('q_type')
                ); */

                $result = $this->MastersModel->superAdminInserQuestionnaire($data);   

                if ($result >0){
                $qansoptions = array(
                    'id' =>$result,
                    'qtype' =>$this->input->post($data['q_type']),
                    'que_score'=>$this->input->post('q_score[]') 
                );   
                        
                $resultrespose = $this->MastersModel->superAdminInserQuestionnaireAnswer($qansoptions); 
                if ($resultrespose >0){
                    $response['message'] = 'Inserted Successfuly!';
                    $response['status'] = 1;
                    $response['values'] = array();
                }else{
                    $response['message'] = 'Sorry! Please try again.';
                    $response['status'] = 0;
                    $response['values'] =array();
                }
            }

        }else{
            $response['message'] = 'error';
            $response['status'] = 0;
            $response['values'] = $this->form_validation->error_array();
        }
        echo json_encode($response);                      
     } 
    // Insert Questionnaire
    public function questionSubmitNew()     {

        $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('category1', 'Category', 'required|strip_tags|xss_clean');
       // $this->form_validation->set_rules('subcat', 'Sub Category', 'required|strip_tags|xss_clean');
       // $this->form_validation->set_rules('subsubcat', 'Process', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('surveyQue', 'Question', 'required|strip_tags|xss_clean');
        
        if($this->form_validation->run() == true){            
        /*        $data= array('q_cat_id'=>$this->input->post('cat'),
                            'q_sub_cat_id'=>$this->input->post('subcat'),
                            'cat_process'=>$this->input->post('subsubcat'),
                            'q_que'=>$this->input->post('surveyQue'),
                            'q_type'=>$this->input->post('q_type')
                );  */ 

                 $data= array('q_cat_id'=>$this->input->post('parent_id'),
                            'q_que'=>$this->input->post('surveyQue'),
                            'q_type'=>$this->input->post('q_type')
                );     

                $result = $this->MastersModel->superAdminInserQuestionnaire($data);   
                if ($result >0){
                $qansoptions = array(
                    'id' =>$result,
                    'qtype' =>$this->input->post($data['q_type']),
                    'que_score'=>$this->input->post('q_score[]') 
                );                 
                $resultrespose = $this->MastersModel->superAdminInserQuestionnaireAnswer($qansoptions); 
                if ($resultrespose >0){
                    $response['message'] = 'Inserted Successfuly!';
                    $response['status'] = 1;
                    $response['values'] = array();
                }else{
                    $response['message'] = 'Sorry! Please try again.';
                    $response['status'] = 0;
                    $response['values'] =array();
                }
            }

        }else{
            $response['message'] = 'error';
            $response['status'] = 0;
            $response['values'] = $this->form_validation->error_array();
        }
        echo json_encode($response);                      
     } 

    //Serch All Questionnaire By Category & Sub Category 
    public function allQueByCat()    {
      	//$categories = $this->db->get("category")->result();
		//$this->load->view('admin/serve/serve-questions', array('categories' => $categories ));
        $data['category'] = $this->MastersModel->fetch_category();
        $this->load->view('admin/serve/serve-questions', $data);
    }

    //Serch All Questionnaire By Category & Sub Category Result
    public function fetchQuestionnaireById()    {
        $categoryid             =   $this->input->post('cat');
        $subcategoryid          =   $this->input->post('subcat');
        $cat_process          	=   $this->input->post('subsubcat');      
        $data['allquestions']   =   $this->MastersModel->getQue($categoryid,$subcategoryid,$cat_process);      
        $this->load->view('admin/serve/serve-questions-data', $data); 
    }

    // Get Questionnaire  and answer By Id for Update
    public function getQuestionnaireById()    {  
        $id = $this->uri->segment('4');   
        $data['allquestions'] = $this->MastersModel->getQuestionnaireById($id);
        $data['allanswer'] = $this->MastersModel->getAnswerById($id); 
        //echo "<pre/>";print_r($data['allanswer']);die;     
        $this->load->view('admin/serve/new-edit-serve-form', $data);      
    }

     // Update The Questionnaire
    public function updateQuestionnaire($id)    {
        $this->form_validation->set_error_delimiters('<span class="error"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('surveyQue', 'surveyQue', 'required|strip_tags|xss_clean');
        
        if($this->form_validation->run() == true){          
            $data= array('qid'=>$id,
                         'q_que'=>$this->input->post('surveyQue')
         );  
               //echo"<pre/>"; print_r($data);die;
        $result = $this->MastersModel->updateQuestionnaire($data,$id);   
        //echo"<pre/>"; print_r($data);die;
            if ($result==true){ 
                $this->session->set_flashdata('success', 'Updated Successfuly!');
                redirect('allQueByCat','refresh');
            }else{
                $this->session->set_flashdata("danger","You are not able to update");
                redirect('allQueByCat','refresh');
            }
        }
    }

    // Delete The Questionnaire
    public function deleteQuestionnaire(){
        $result =  $this->MastersModel->deleteQuestionnaire($_POST["qid"]);  
        if ($result == true) {
            echo "1";
            redirect ('allQueByCat');
        }else{
            echo "2";
        }    
     }

    // Sub Sub category 
    public function survey(){
        $data['category'] = $this->MastersModel->fetch_category();
        $where=array('c.parent_id'=>0,'c.status'=>1);
        $data['categories']=$this->MastersModel->categoryList($where);
        $data['fetchScore'] = $this->common->fetchScore();
        $this->load->view('admin/serve/serve-form', $data);
    }

    // Fetch Sub Category
    public function fetch_subcategory(){
        if($this->input->post('cat_id')){
            echo $this->MastersModel->fetch_subcategory($this->input->post('cat_id'));
        }
    }

    // Fetch Sub Sub Category (Process)
    public function fetch_subsubcat()    {
        if($this->input->post('sub_cat_id'))        {
            echo $this->MastersModel->fetch_subsubcat($this->input->post('sub_cat_id'));
        }
    }
    // Fetch Sub Sub Category (Process) Super admin
    public function fetch_feedbck_result()    {
        $data='';
        $data['Complaints']= $this->MastersModel->fetchSurveyFeedback();
        $this->load->view('admin/serve/userfeedbackresult', $data);     
    }
}

?>