<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : AdminLogin (UserController)
 * User Class to control Admin Login related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 28 August 2018
 */
class AdminLogin extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();  
         $this->load->model('AdminLoginModel');        
    }
    
    public function index()
    {
        $data['pageTitle'] = 'wageniCRM : AdminLogin';
        
        $this->load->view("admin/adminlogin", $data);
    }
    public function login(){
        $loginData=array(
            'admin_email'=>html_escape(trim($this->input->post('adminEmail', TRUE))),
            'admin_pass'=>html_escape(trim($this->input->post('adminPass', TRUE)))
          );
       
        $loginresult= $this->AdminLoginModel->checkAdminLogin($loginData);   
         if($loginresult['status'] =='true' ){
            $this->session->set_userdata('is_admin_login',TRUE);
            $this->session->set_userdata('admin_login_id',$loginresult['data']->admin_login_id);
            $this->session->set_userdata('admin_role_id',$loginresult['data']->admin_role_id);
            $this->session->set_userdata('admin_name',$loginresult['data']->admin_name);
            $this->session->set_userdata('admin_username',$loginresult['data']->admin_username);
            $this->session->set_userdata('admin_email',$loginresult['data']->admin_email);
            $this->session->set_flashdata('loginmsg','Error !');

        }
        echo json_encode($loginresult);         
    } 

    public function SubscriberLoginView()
    {
        $data['pageTitle'] = 'wageniCRM : Subscriber Login';        
        $this->load->view("admin/subscriberlogin", $data);
    }
    public function SubscriberDashboardLogin()
    {
        $loginData=array('sub_email'=>html_escape(trim($this->input->post('adminEmail', TRUE))),
         'sub_pass'=>html_escape(trim($this->input->post('adminPass', TRUE)))
     );
       // print_r($loginData);die;
        $this->load->model('AdminLoginModel');
        $loginresult= $this->AdminLoginModel->checkSubscriberLogin($loginData);   
        //print_r($loginresult); die;
        if($loginresult['status'] =='true' ){
            $this->session->set_userdata('sub_login_id',$loginresult['data']->sub_login_id);
            $this->session->set_userdata('sub_role_id',$loginresult['data']->sub_login_role);
            $this->session->set_userdata('sub_name',$loginresult['data']->sub_name);
            $this->session->set_userdata('sub_username',$loginresult['data']->sub_username);
            $this->session->set_userdata('sub_email',$loginresult['data']->sub_email);
            $this->session->set_flashdata('loginmsg','Error !');

        }
        echo json_encode($loginresult);         
    } 


    public function Forgot(){
        $this->load->view('admin/forgotPassword');
    }

    public function ForgotPassword() {      
      $email = $this->input->post('adminEmail', TRUE);
      $this->load->model('AdminLoginModel');     
      $findemail = $this->AdminLoginModel->ForgotPassword($email);  
    
      if($findemail){
        $this->AdminLoginModel->sendpassword($findemail->admin_email);
        $this->session->set_flashdata('success',' Password Send to your Email Id !');
        redirect('admin/AdminLogin/Forgot','refresh');
      }else{
        $this->session->set_flashdata('success',' No record found !');
        redirect('admin/AdminLogin/Forgot','refresh');
      }


}


}

?>