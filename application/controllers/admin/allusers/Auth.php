    <?php if(!defined('BASEPATH')) exit('No direct script access allowed');

    require APPPATH . '/libraries/BaseController.php';

    /**
     * Class : Auth (UserController)
     * Auth Class to control Auth related operations.
     * @author : Esther Praveen
     * @version : 1.1
     * @since : 28 August 2018
     */
    class Auth extends BaseController
    {
        /**
         * This is default constructor of the class
         */
        public function __construct()
        {
            parent::__construct(); 
            $this->load->model('AuthModel'); 

        }

        public function customerExport(){ 
           // file name 
           $filename = 'users_'.date('Ymd').'.csv'; 
           header("Content-Description: File Transfer"); 
           header("Content-Disposition: attachment; filename=$filename"); 
           header("Content-Type: application/csv; ");           
           // get data 
           $usersData=$this->AuthModel->getcustomerExportData();
           // file creation 
           $file = fopen('php://output', 'w');
     

           $header = array(
                    "Category",
                    "Total Rooms",
                    "First Name",
                    "Last Name",
                    "Company Name",
                    "Company No",
                    "Url",
                    "Email",
                    "Country",
                    "Telephone",
                    "Contact Person No.",
                    "Account No.",
                    "Contact Address",
                    "Contact Person Name",
                    "Contact Person Email",
                    "Contact Person Title",
                    "Contact Person Mobile",
                    "Contact Person Address",
                    "Contact Person Office",
                    "Role"
                ); 
           fputcsv($file, $header);
           foreach ($usersData as $key=>$line){ 
             fputcsv($file,$line); 
           }
           fclose($file); 
           exit; 
          }

        public function viewCustomer() {
           $this->isSuperAdmin();
            $this->global['subscriber'] = $this->AuthModel->fetchSubscriber();  
            $this->global['subscriberrole'] = $this->AuthModel->fetchSubscribersRole();  
            $this->global['package'] = $this->PackageModel->fetchPackage(); 
            $this->global['category'] = $this->MastersModel->fetchCategory();
            $this->load->view("admin/allusers/view-customers", $this->global);             
        }
        public function viewCompanyStatus() {   
            $this->isSuperAdmin();
            $this->global['subscriber'] = $this->AuthModel->fetchSubscriber();  
            $this->global['subscriberrole'] = $this->AuthModel->fetchSubscribersRole();  
            $this->global['package'] = $this->PackageModel->fetchPackage(); 
            $this->global['category'] = $this->MastersModel->fetchCategory();
            $this->load->view("admin/allusers/view-company-status", $this->global);             
        }

        // Fetch All data
        public function fetchcommonalldata() {
            $this->isSuperAdmin();
             $this->global['role'] = $this->AuthModel->fetchAdminRole(); 
        }

        //Admin Add Employee
        public function index()        {
            $this->isSuperAdmin();
            $this->global['pageTitle'] = 'wageniCRM : ADD Users';
            $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
            $this->form_validation->set_rules('uname', 'Name', 'required|strip_tags|xss_clean');
            $this->form_validation->set_rules('username', 'User Name', 'required|strip_tags|xss_clean');
            $this->form_validation->set_rules('uemail', 'Email', 'required|strip_tags|xss_clean');
            $this->form_validation->set_rules('upass', 'Password', 'required|strip_tags|xss_clean');
            $this->form_validation->set_rules('uphone', 'Phone', 'required|strip_tags|xss_clean');
            $this->form_validation->set_rules('userrole', 'Role', 'required|strip_tags|xss_clean|callback_select_Role');

            if ($this->form_validation->run() == true){

                $usersdata = array('admin_name'=>html_escape(trim($this->input->post('uname', TRUE))),
                    'admin_username'=>html_escape(trim($this->input->post('username', TRUE))),      
                    'admin_email'=>html_escape(trim($this->input->post('uemail', TRUE))),      
                    'admin_pass'=>html_escape(trim($this->input->post('upass', TRUE))),      
                    'admin_phone'=>html_escape(trim($this->input->post('uphone', TRUE))),        
                    'admin_role_id'=>$this->input->post('userrole')       
                );
                //echo "<pre/>";print_r($usersdata);die;
                $usersresult= $this->AuthModel->insertUsers($usersdata = $this->security->xss_clean($usersdata)); 
                if($usersresult ==TRUE )
                {                       
                    $this->session->set_flashdata('success','User Added Successfully.');
                    redirect('add-user');
                }  
            }
            else {
                $error = validation_errors();
                $this->session->set_flashdata('validationerrormsg',$error);                    
                $this->global['users'] = $this->AuthModel->fetchUsers();  
                $this->global['role'] = $this->AuthModel->fetchAdminRole();  
                //echo("<pre/>");print_r($this->global['role']);die;              
                $this->load->view("admin/allusers/add-users", $this->global);      
            } 

        }

        public function select_Role($selectValue){
            if($selectValue == 'none'){
                $this->form_validation->set_message('select_Role', 'The Role field is required Please Select one.');
                return false;
            }else{
                return true;
            }
        }

        public function deleteUsers($id)
        {
             $this->isSuperAdmin();
            $usersresult= $this->AuthModel->deleteUsers($id); 
            if ($usersresult == true) {
               $this->session->set_flashdata('success','User Deleted Successfully.');
               redirect('add-user','refresh');
           }else{
            redirect('add-user','refresh');
        }
    }
    public function getUserById($id)
    {
        $this->isSuperAdmin();
        $this->global['editadmin']= $this->AuthModel->fetchUsersById($id);
        $this->global['role'] = $this->AuthModel->fetchAdminRole(); 
        $this->load->view("admin/allusers/edit-users", $this->global);    
    }
    public function updateUser($id)
    {
        $this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : Update Users';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('uname', 'Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('username', 'User Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('uemail', 'Email', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('upass', 'Password', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('uphone', 'Phone', 'required|strip_tags|xss_clean');
        //$this->form_validation->set_rules('userrole', 'Role', 'required|strip_tags|xss_clean');

        if ($this->form_validation->run() == true){

            $data = array('admin_login_id'=>$id,
                'admin_name'=>html_escape(trim($this->input->post('uname', TRUE))),
                'admin_username'=>html_escape(trim($this->input->post('username', TRUE))),      
                'admin_email'=>html_escape(trim($this->input->post('uemail', TRUE))),      
                'admin_pass'=>html_escape(trim($this->input->post('upass', TRUE))),      
                'admin_phone'=>html_escape(trim($this->input->post('uphone', TRUE))),        
                'admin_role_id'=>$this->input->post('userrole')       
            );
           //echo "<pre/>"; print_r($data);die;
            $dataresult= $this->AuthModel->updateUsers($data = $this->security->xss_clean($data)); 
            if($dataresult ==TRUE )
            {                       
                $this->session->set_flashdata('success','User Updated Successfully.');
                redirect('add-user');
            }else{
                //$error = validation_errors();
                //$this->session->set_flashdata('failed',$error);  
                $this->session->set_flashdata('failed','Failed.');
                redirect('add-user');
            }  
        }
    }
    /***********************************************    Subscriber    *****************************************************/
    /* Add Subscriber  */
    public function adminAddCompany(){
       // echo "<pre/>";print_r($_POST);die;
        $this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : ADD Company';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('fname', 'First Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('lname', 'Last Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('uemail', 'Email', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('upass', 'Password', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('uphone', 'Phone', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('userrole', 'Role', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('userpackage', 'Package', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('usercategory', 'Category', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('cname', 'Company Name', 'required|strip_tags|xss_clean');
        //$this->form_validation->set_rules('contacttel', 'Contact Telephone', 'required|strip_tags|xss_clean');
        //$this->form_validation->set_rules('contactno', 'Contact No', 'required|strip_tags|xss_clean');

        if ($this->form_validation->run() == true){

            $config['upload_path']     =  './uploads/';
            $config['allowed_types']   =  'jpeg|jpg|png'; 
            $config['max_size']        =  1024 * 10;
            $config['encrypt_name']    =  TRUE;                 
            $config['file_name'] = $_FILES['userfile']['name'];

            $this->load->library('upload', $config);   
            $this->upload->initialize($config);
            $this->upload->do_upload('userfile');
            $imagedetails = $this->upload->data();
            $companylogo = $imagedetails['file_name'];

            $usersdata = array(
                'category'      =>  html_escape(trim($this->input->post('usercategory', TRUE))),
                'fname'         =>  html_escape(trim($this->input->post('fname', TRUE))),
                'lname'         =>  html_escape(trim($this->input->post('lname', TRUE))),
                'sub_email'     =>  html_escape(trim($this->input->post('uemail', TRUE))),      
                'sub_pass'      =>  html_escape(trim($this->input->post('upass', TRUE))),      
                'sub_phone'     =>  html_escape(trim($this->input->post('uphone', TRUE))),        
                'userfile'      =>  $companylogo,        
                'added_by'      =>  0,        
                'company_url'   =>  html_escape(trim($this->input->post('weburl', TRUE))),        
                'accno'         =>  html_escape(trim($this->input->post('accno', TRUE))),        
                'contactaddress'=>  html_escape(trim($this->input->post('contactaddress', TRUE))),        
                'cpname'        =>  html_escape(trim($this->input->post('cpname', TRUE))),        
                'contacttel'    =>  html_escape(trim($this->input->post('contacttel', TRUE))),        
                'contactno'     =>  html_escape(trim($this->input->post('contactno', TRUE))),        
                'sub_name'      =>  html_escape(trim($this->input->post('cname', TRUE))),        
                'cpemail'       =>  html_escape(trim($this->input->post('cpemail', TRUE))),        
                'cptitle'       =>  html_escape(trim($this->input->post('cptitle', TRUE))),        
                'cpmobile'      =>  html_escape(trim($this->input->post('cpmobile', TRUE))),        
                'cpaddress'     =>  html_escape(trim($this->input->post('cpaddress', TRUE))),        
                'cpoffice'      =>  html_escape(trim($this->input->post('cpoffice', TRUE))),        
                'sub_login_role'=>$this->input->post('userrole')                                    
            );

            //echo "<pre/>";print_r($usersdata);die;
            $usersresult= $this->AuthModel->insertSubscriber($usersdata = $this->security->xss_clean($usersdata)); 
               // print_r($usersresult);die;
            if($usersresult ==TRUE )
            {                       
                $this->session->set_flashdata('success','Company Added Successfully.');
                redirect('admin-add-company');
            }  
        }
        else {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);                    
            $this->global['subscriber'] = $this->AuthModel->fetchSubscriber();  
            $this->global['subscriberrole'] = $this->AuthModel->fetchSubscribersRole();  
            $this->global['package'] = $this->PackageModel->fetchPackage(); 
            $this->global['category'] = $this->MastersModel->fetchCategory(); 
             $this->load->view("admin/allusers/add-subscribers", $this->global);      
        } 
    }
        //Fetch  Subscribers By id
    public function getSubscriberById($id)    {
        $this->isSuperAdmin();
        $this->global['editsubscriber']= $this->AuthModel->fetchSubscribersId($id);
        $this->global['role'] = $this->AuthModel->fetchSubscribersRole();                     
        $this->global['package'] = $this->PackageModel->fetchPackage(); 
        $this->global['category'] = $this->MastersModel->fetchCategory(); 
         $this->load->view("admin/allusers/edit-subscribers", $this->global);    
    }
        //Update Subscribers
    public function updateSubscriber($id)   {
        $this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : ADD Subscriber';
        $companylogohiddin=  $this->input->post('hiddenimg');
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('fname', 'First Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('lname', 'Last Name', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('upass', 'Password', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('uphone', 'Phone', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('userrole', 'Role', 'required|strip_tags|xss_clean');
        $this->form_validation->set_rules('usercategory', 'Role', 'required|strip_tags|xss_clean');

        if ($this->form_validation->run() == true){

            if(empty($_FILES['userfile']['name'])){                
                $companylogo=$companylogohiddin;
            }else{
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'jpeg|jpg|png'; 
                $config['max_size']             = 1024 * 10;
                $config['encrypt_name']         = TRUE; 
                $config['file_name']=$_FILES['userfile']['name'];
                $this->load->library('upload', $config);   
                $this->upload->initialize($config);
                if($this->upload->do_upload('userfile')){
                    $imagedetails= $this->upload->data();
                    $companylogo = $imagedetails['file_name'];                 
                }

            }

            $data = array('sub_login_id'=>$id,
                'category'      =>  html_escape(trim($this->input->post('usercategory', TRUE))),
                'fname'     =>  html_escape(trim($this->input->post('fname', TRUE))),
                'lname'     =>  html_escape(trim($this->input->post('lname', TRUE))),
                'sub_pass'  =>  html_escape(trim($this->input->post('upass', TRUE))),      
                'sub_phone' =>  html_escape(trim($this->input->post('uphone', TRUE))), 
                'userfile'  =>  $companylogo, 
                'added_by'  =>0,                          
                'contactaddress'=>  html_escape(trim($this->input->post('contactaddress', TRUE))),        
                'cpname'        =>  html_escape(trim($this->input->post('cpname', TRUE))),        
                'contacttel'    =>  html_escape(trim($this->input->post('contacttel', TRUE))),        
                'contactno'     =>  html_escape(trim($this->input->post('contactno', TRUE))),        
                'cpname'        =>  html_escape(trim($this->input->post('cpname', TRUE))),        
                'cpemail'       =>  html_escape(trim($this->input->post('cpemail', TRUE))),        
                'cptitle'       =>  html_escape(trim($this->input->post('cptitle', TRUE))),        
                'cpmobile'      =>  html_escape(trim($this->input->post('cpmobile', TRUE))),        
                'cpaddress'     =>  html_escape(trim($this->input->post('cpaddress', TRUE))),        
                'cpoffice'      =>  html_escape(trim($this->input->post('cpoffice', TRUE))),
                'sub_login_role'=>$this->input->post('userrole')      
            );
             // echo "<pre/>";  print_r($data); die;
            $dataresult= $this->AuthModel->updateSubscribers($data = $this->security->xss_clean($data)); 
            if($dataresult ==TRUE ){                       
                $this->session->set_flashdata('success','Company Updated Successfully.');
                redirect('admin-view-all-company', 'refresh');
            }else{
                //$error = validation_errors();
                //$this->session->set_flashdata('failed',$error);  
                $this->session->set_flashdata('failed','Failed.');
                redirect('admin-view-all-company', 'refresh');
            }  
        }
    }
        //Delete Subscribers
    public function adminDeleteCompany($id)   {
        $this->isSuperAdmin();
        $usersresult= $this->AuthModel->deleteAllCompanyDataById($id); 
        if ($usersresult == true){
            $this->session->set_flashdata('success','Company Deleted Successfully.');
            redirect('admin-add-company','refresh');
        }else{
            redirect('admin-add-company','refresh');
        }
    }

    /* Add Admin  Role */
    public function addAdminRole()  {
        $this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : ADD Admin Role';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('rolename', 'Role', 'required|strip_tags|is_unique[adminemprole.admin_emp_role_name]|xss_clean');

        if ($this->form_validation->run() == true){
            $data = array('admin_emp_role_name'=>$this->input->post('rolename'));
            $result= $this->AuthModel->insertAdminRole($data = $this->security->xss_clean($data)); 
            if($result == TRUE ){                       
                $this->session->set_flashdata('success','Admin Role Added Successfully.');
                redirect('add-admin-role');
            }  
        }else{
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);          
            $this->global['adminrole'] = $this->common->fetchAdminRoles(); 
            $this->load->view("admin/allusers/add-admin-role", $this->global);      
        } 
    }

    /* get Admin Role By Id*/
    public function getAdminRole($id)    { 
        $this->isSuperAdmin();
        $data['records'] = $this->AuthModel->getAdminRoleById($id);
        $this->load->view('admin/allusers/edit-admin-role',$data);
    }

    /* Update Admin Role*/
    public function updateAdminRole($id)  {
        $this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : ADD Admin Role';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('rolename', 'Role', 'required|strip_tags|is_unique[adminemprole.admin_emp_role_name]|xss_clean');

        if ($this->form_validation->run() == true){
            $data = array(
                'admin_role_id'=> $id,
                'admin_emp_role_name'=>$this->input->post('rolename'));
            $result= $this->AuthModel->updateAdminRole($data = $this->security->xss_clean($data));
                   // echo ("<pre/>");print_r($result);die;
            if($result ==TRUE ){                       
                $this->session->set_flashdata('success','Admin Role Updated Successfully.');
                redirect('add-admin-role');
            }

        }else {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);
            $this->session->set_flashdata('success','The Role field must contain a unique value..'); 
            redirect('add-admin-role');          
            //$this->load->view("admin/allusers/edit-admin-role", $this->global);   
        } 
    }


    //Delete Subscribers
    public function deleteAdminRole($id)    {
        $this->isSuperAdmin();
        $usersresult= $this->AuthModel->deleteAdminRole($id); 
        if ($usersresult == true) {
           $this->session->set_flashdata('success','Admin Role Deleted Successfully.');
            redirect('add-admin-role','refresh');
       }else{
            redirect('add-admin-role','refresh');
        }
    }

    /* Add Subscriber Role */
    public function addSubscriberRole()    {
        $this->isSuperAdmin();
        $this->global['pageTitle'] = 'wageniCRM : ADD Subscriber Role';
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('rolename', 'Role', 'required|strip_tags|xss_clean');

        if ($this->form_validation->run() == true){

            $data = array('subscriber_role_name'=>$this->input->post('rolename'));
            $result= $this->AuthModel->insertSubscriberRole($data = $this->security->xss_clean($data)); 
                   // print_r($result);die;
            if($result ==TRUE )
            {                       
                $this->session->set_flashdata('success','Subscriber Role Added Successfully.');
                redirect('add-subscriber-role');
            }  
        }
        else {
            $error = validation_errors();
            $this->session->set_flashdata('validationerrormsg',$error);          
            $this->global['subscriberrole'] = $this->common->fetchSubscriberRoles(); 
            $this->load->view("admin/allusers/add-subscribers-role", $this->global);      
        } 
    }
             //Delete Subscribers
    public function deleteSubscribersRole($id)    {
        $this->isSuperAdmin();
        $usersresult= $this->AuthModel->deleteSubscribersRole($id); 
        if ($usersresult == true) {
            $this->session->set_flashdata('success','Subscriber Role Deleted Successfully.');
            redirect('add-subscriber-role','refresh');
        }else{
            redirect('add-subscriber-role','refresh');
        }
    }

    public function viewSubscriberAdmin()    {
        $this->isSuperAdmin();
        $this->global['subscriberAdmin'] = $this->AuthModel->fetchSubscriberAdmin(); 
        $this->load->view("admin/allusers/add-subscribers-admin", $this->global);      

    }
    public function viewSubscriberSubAdmin()    {
        $this->isSuperAdmin();
        $this->global['subscriberSubAdmin'] = $this->AuthModel->fetchSubscriberSubAdmin(); 
        $this->load->view("admin/allusers/add-subscribers-sub-admin", $this->global);      

    }
    /* Delete Sub Admin Company */
    public function deletesubscribers($id)    {
        $this->isSuperAdmin();
        $result=$this->AuthModel->deletesubscriberSubAdmin($id);
        if($result == TRUE){
            $this->session->set_flashdata('success','Subscriber Deleted Successfully.');
            redirect('view-company-admin','refresh');
        }else{
            redirect('view-company-admin','refresh');
        }
    }

    /*******************************************Edit Profile For Super Admin**************************************************/

    /* Fetch Admin  Profile Data By id */
    public function fetchAdminProfile()    {   
        $id = $this->session->userdata('admin_login_id');
        $data['Aprofile'] = $this->MastersModel->fetchAdminProfileData($id);
        //echo"<pre/>";print_r($data['Aprofile']);die;
        $this->load->view('admin/allusers/editAdminProfile');
    }

    public function editAdminProfile($id)    {   
        $this->isSuperAdmin();
        $setimage= $this->input->post('hiddenimg');
        $this->form_validation->set_error_delimiters('<span class="error alert"  style="color: red";>', '</span>');
        $this->form_validation->set_rules('name','name','required|strip_tags|xss_clean');
        $this->form_validation->set_rules('username','username','required|strip_tags|xss_clean');
        $this->form_validation->set_rules('uphone','uphone','required|strip_tags|xss_clean');
        $this->form_validation->set_rules('upass','upass','required|strip_tags|xss_clean');
        
        if ($this->form_validation->run()==TRUE) {       
            if(empty($_FILES['userfile']['name'])){
                $updImage=$setimage;
            }else{
                $config['upload_path']     =  './uploads/';
                $config['allowed_types']   =  'jpeg|jpg|png'; 
                $config['max_size']        =  1024 * 10;
                $config['encrypt_name']    =  TRUE;
                $config['file_name']=$_FILES['userfile']['name'];
                $this->load->library('upload', $config);   
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $imagedetails= $this->upload->data();
                $updImage=$imagedetails['file_name'];
                //echo("<pre/>");print_r('$imagedetails');die;
            } 
            
                $data = array(
                    'admin_login_id' => $id,
                    'admin_name'     => $this->input->post('name'),
                    'admin_username' => $this->input->post('username'),
                    'admin_phone'    => $this->input->post('uphone'),
                    'admin_pass'     => $this->input->post('upass'),
                    'userfile'       => $updImage 
                );
                          
            $result = $this->MastersModel->UpdateAdminProfile($data,$id);
            if ($result == TRUE) {
                $this->session->set_flashdata('success','Admin Profile  Updated Successfully.');
                redirect('edit-admin-profile');
            } 
            else {
                $this->session->set_flashdata('failed','Failed.');
                redirect('edit-admin-profile');
            }            
        }else{
                $error = validation_errors();
                $this->session->set_flashdata('failed',$error);  
                //$this->session->set_flashdata('failed','Failed.');
                redirect('edit-admin-profile','refresh');
        }    

    }


    }

    ?>