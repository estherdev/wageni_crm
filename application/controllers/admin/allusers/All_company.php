<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

    require APPPATH . '/libraries/BaseController.php';

    /**
     * Class : Auth (UserController)
     * Auth Class to control Auth related operations.
     * @author : Esther Praveen
     * @version : 1.1
     * @since : 28 August 2018
     */
    class All_company extends BaseController {        
      public function __construct() {
          parent::__construct(); 
            $this->load->model(array('All_company_model' => 'ACM','AuthModel' => 'AUTHM', 'PackageModel' => 'PM', 'MastersModel' => 'MM'));
      }

/*      public function panindia()
      {
        $to='praveen@panindia.in';
        $mail_data = "Wageni CRM <a href='http://wageni.us.tempcloudsite.com/beta/pie'>click</a>";
        $subject = 'Wageni CRM status';
        $sent = sendEmail($to,'',$subject,$mail_data); 
      }

      public function panindiaexpire()
      {

        echo $date = '2011-04-8 08:29:49';
         echo "<br/>";
       echo  $newDate = date("Y-m-d H:i:s",strtotime($date." +5 minutes"));
         echo "<br/>";
        
         if($date <= $newDate){
           echo "<br> VALID TIME : ".date('Y-m-d H:i:s').'<br/>';
         }else{

         echo "<br>INVALID TIME :".date('Y-m-d H:i:s').'<br/>';
        }

        $date= '2019-08-09 07:23:43';
        $currentDate = strtotime($date);
        $futureDate = $currentDate+(60*2);
        echo $formatDate = date("Y-m-d H:i:s", $futureDate);
        echo "<br/>";
        if($currentDate <= $formatDate){
          echo "<br> VALID TIME : ".date('Y-m-d H:i:s').'<br/>';
        }else{

         echo "<br>INVALID TIME :".date('Y-m-d H:i:s').'<br/>';
        }

      }*/
    
    public function saveQuestionOptionsAjax(){
       if ($this->input->is_ajax_request()) {
           $sr = $this->input->post('sr');
            $response=array();
            if(count($sr)>0){            
              foreach($sr as $key){
                $option_data=array(
                  'que_type'=>$this->input->post('optionid'.$key),
                  'que_id'=>$this->input->post('queid'.$key),
                  'que_score'=>$this->input->post('score_name'.$key)
                );
                 $response['id']=$this->MM->quesOptionUpdate('questionnaire_answer',$option_data);
              }
              if($response['id']>0){ 
                echo json_encode($response);
              }
            }
          }
      }

       public function allQuestionsList(){
          $id=$this->session->userdata('sub_login_id')?$this->session->userdata('sub_login_id'):0;
          $category=$this->session->userdata('category')?$this->session->userdata('category'):0;
          $data['questions']=$this->MM->allQuestionsList($id,$category);
          $this->load->view("admin/serve/survey-questions-list", $data); 
        } 

       public function allQuestionsDetailsAjax(){            
          if ($this->input->is_ajax_request()) {
               $data = $this->MM->allAdminQuestionsListAjax($_POST,false);// prd($data);
                $totalData = $this->MM->allAdminQuestionsListAjax($_POST,true);
                  $response=array();
                  if(count($data)>0){
                      foreach($data as $row){   
                            $response[]=array(
                              'ownerid'=>1014446,
                              'owner'=>10,
                              'id'=> $row['qid'],
                              'cat_id'=>$row['q_cat_id'],
                              'category_name'=>ucfirst($row['category_name']),
                              'SUB_cat_id'=>$row['q_sub_cat_id'],
                              'sub_cat_name'=>ucfirst($row['sub_cat_name']),
                              'process_cat_id'=>$row['cat_process'],
                              'sub_sub_cat_name'=>ucfirst($row['sub_sub_cat_name']),
                              'que'=>$row['q_que'],
                              'q_type'=>$row['q_type'],
                              'all'=>$row
                           );
                      }
                  }
                  $json_data = array(
                  "recordsTotal"    => $totalData,  
                  "recordsFiltered" => $totalData,
                  "data"            => $response 
          );
              echo json_encode($json_data);
         }     
      }
      //adminDeleteQuestion
  function adminDeleteQuestion(){
    if ($this->input->is_ajax_request()) {
      $id=$this->input->post('action_id');
      if(!empty($id)){
         $response['status']=$this->MM->deleteRecord($id,'questionnaire_masters');
        if($response['status']){
          if($response['status']){
            $response['message']="Question Deleted Successfully!";            
           }        
        }
         echo json_encode($response);
      }
    }
  }


      public function viewallQuestionsDetailsAjax(){
       if ($this->input->is_ajax_request()) {         
          $id=$this->input->post('id');
          if($id>0){
              $data['questionsDetails'] = $this->MM->getquestionnaireOptionListAdmin($id);
              if(count($data)>0){         
                  $response['data'] =$this->load->view("admin/serve/survey-questions-details-list", $data,true);      
                  echo json_encode($response);   
              }
          }
        }
      }


      public function admincomplaintsListNetPromterScore(){
          $data['complaints']=$this->MM->complaintsListNetPromterScore();
          $this->load->view("admin/complaints/complaints-list", $data); 
      } 

        public function index(){
           $this->isSuperAdmin();
          $data['customers']=$this->ACM->companyList();
          $data['category'] =  $this->MM->fetchCategory(); 
          $this->load->view("admin/allusers/all_company", $data); 
        }

        public function complaints(){
           $this->isSuperAdmin();
          $data['complaints']=$this->MM->adminGetAllComplaintsAjax();     
          //echo "<pre/>"; print_r($data);die;
          $data['customers']=$this->ACM->companyList();
          $data['category'] =  $this->MM->fetchCategory(); 
          $this->load->view("admin/complaints/complaints", $data); 
        }

        public function sendNotification(){
            if ($this->input->is_ajax_request()) {
              $response=array();
              $id=$this->input->post('id');
              $status=$this->input->post('status');
              $updatestatus='';
              
              if ($status==1) {
               $updatestatus=2;
              }
              if ($status==2) {
                $updatestatus=3;
              } 


              $data=array();
              $response['status']=false;
              $response['message']="There are an error in sending notification";
              if($id>0){
                $where=array('cf.cf_id'=>$id);
                $data['complaintinfo']=$this->ACM->complaintsDetails($where);
                if ($data['complaintinfo']>0) {                   
                    $update_data=array('id'=>$id,'cf_status'=>$updatestatus);
                    $result= $this->ACM->AddUpdateData('customer_feedback',$update_data);

                    if ($result) {
                      $where=array('cf.cf_id'=>$id);
                      $data['info']=$this->ACM->complaintsDetails($where);                    
                      $to= $data['info']['user_email_id'];
                      $mail_message=$this->load->view('email_templates/survey-status',$data,true); 
                      $sent=sendEmail($to,'','Feedback Stutus ',$mail_message);
                      if ($sent) {
                          $response['status']=true;
                          $response['message']="Notification successfully sent to the customer";
                      }
                    }
                  }               
              }
              echo json_encode($response);
            }
        } 
 
        public function viewAllComplaintsList() {  
          if ($this->input->is_ajax_request()) { 
                 $data = $this->ACM->adminGetAllComplaintsAjax($_POST,false);
                  $totalData = $this->ACM->adminGetAllComplaintsAjax($_POST,true);
                    $response=array();
                    $complaints='';
                    $status='';
                    $assign = '';
                    if(count($data)>0){
                        foreach($data as $row){                        
                              if ($row['bus_que_score']=='1'){
                                  $complaints = '<i class="fa fa-times-circle-o fa-1x text-danger"> Poor </i>';
                                }elseif ($row['bus_que_score']=='2'){
                                    $complaints ='<i class="fa fa-times-circle-o fa-1x text-warning"> Fair </i>';
                                  }elseif ($row['bus_que_score']=='3'){
                                      $complaints ='<i class="fa fa-check-circle-o fa-1x text-primary"> Good </i>';
                                    }elseif ($row['bus_que_score']=='4'){
                                        $complaints ='<i class="fa fa-check-circle-o fa-1x text-success"> Good</i>';
                                      }
                                else{
                                   $complaints ='<i class="fa fa-check-circle-o fa-1x text-success"> No Excellent</i>';
                                }


                                /* Status */ 

                                if ($row['cf_status']=='1'){
                                  $status= "<span class='btn btn-sm btn-danger'>Pending</span>";
                                } elseif ($row['cf_status']=='2'){
                                    $status= "<span class='btn btn-sm btn-warning'>Ongoing</span>";
                                  } elseif ($row['cf_status']=='3'){
                                    $status= "<span class='btn btn-sm btn-success'>Issue has been addressed</span>";
                                  }
                                else{
                                   $status= "<span class='btn btn-sm btn-danger'>Pending</span>";
                                }

                                /* Assign */

                                $past_time =  date('Y-m-d H:i',strtotime($row['cf_time']));                                
                                $current_time = time();
                                $difference = $current_time - strtotime($past_time);
                                $difference_minute =  $difference/60;
                                $d= intval($difference_minute);
                                
                                if ( intval($d) < '1440') {
                                  $assign =  "Superviser";
                                } 
                                  elseif ( intval($d) < '2880') {
                                  $assign = "Manger";
                                 }
                                 else {
                                  $assign =  "G Manger";
                                }

                        $post_date = strtotime($row['cf_time']);
                        $now = time();
                        $units = 5;
                        $complaintstime= timespan($post_date, $now, $units);  
                        
                        /*    $response[]=array(
                                'id'=>$row['cf_id'],
                                'name'=>ucfirst($row['user_name']),
                                'category_name'=>ucfirst($row['category_name']),
                                'question'=>$row['bus_q_que'],
                                'answer'=>$row['bus_que_type'],
                                'cf_status'=>$row['cf_status'],
                                'frsid'=>$row['frsid'],
                                'frsname'=>$row['frsname'],
                                'busid'=>ucfirst($row['fname'].' '. $row['lname']),
                                'status'=>$status,
                                'complaints'=>$complaints,
                                'assign'=>$assign,
                                'complaintstime'=>'<i class="fa fa-clock-o fa-1x">  ' .$complaintstime. " Ago </i>",
                                'all'=>$row
                             );*/

                            $response[]=array(
                                  'id'=>$row['cf_id'],
                                  'name'=>ucfirst($row['user_name']),
                                  'category_name'=>ucfirst($row['category_name']),
                                  'question'=>$row['bus_q_que'],
                                  'answer'=>$row['bus_que_type'],
                                  'cf_status'=>$row['cf_status'],
                                  'frsid'=>$row['frsid'],
                                  'frsname'=>$row['frsname'],
                                  'busid'=>ucfirst($row['fname']),
                                  'status'=>$status,
                                  'complaints'=>$complaints,
                                  'assign'=>$assign,
                                  'nps_main_id'=>$row['nps_main_id'],
                                  'npscatid'=>$row['npscatid'],
                                  'npssubcatid'=>$row['npssubcatid'],
                                  'npsprocessid'=>$row['npsprocessid'],
                                  'npsbusid'=>$row['npsbusid'],
                                  'npsscore'=>$row['npsscore'],
                                  'npsadded_time'=>$row['npsadded_time'],
                                  'complaintstime'=>'<i class="fa fa-clock-o fa-1x">  ' .$complaintstime. " Ago </i>",
                                  'all'=>$row
                               );





                        }
                    }

                    $json_data = array(
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalData,
                    "data"            => $response 
            );
            echo json_encode($json_data);
             }        
        }

        public function viewAllCompanyList(){  
          if ($this->input->is_ajax_request()) { 
                 $data = $this->ACM->companyListAjax($_POST,false);
                  $totalData = $this->ACM->companyListAjax($_POST,true);
                    $response=array();
                    if(count($data)>0){
                        foreach($data as $row){     
                        $score = number_format($row['score'], 2);
                       
                            $response[]=array(
                                'id'=>$row['sub_login_id'],
                                'name'=>ucfirst($row['fname'].' '.$row['lname']),
                                'category_name'=>ucfirst($row['category_name']),
                                'sub_name'=>ucfirst($row['sub_name']),
                                'sub_phone'=>ucfirst($row['sub_phone']),
                                'package_name'=>ucfirst($row['package_name']),
                                //'price'=> $row['package_price'].' '.$this->config->item('krw'),
                                'price'=> $row['package_price'],
                                'score'=> $row['score'] ? number_format($row['score'], 2) : 0.0,
                                'expiredate'=>ucfirst($row['subscriber_end_date']),
                                'email'=>$row['sub_email'],
                                'status'=>$row['sub_isActive'],                                
                                'cpemail'=>$row['cpemail'],
                                'cpmobile'=>$row['cpmobile'],
                                'image'=>$row['userfile'],
                                'all'=>$row,
                                'company_url'=>$row['company_url']
                            );
                        }
                    }

                    $json_data = array(
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalData,
                    "data"            => $response 
            );
            echo json_encode($json_data);
             }                                  
        }

  public function changeCompanyStatus(){
    if ($this->input->is_ajax_request()) {
      $id=$this->input->post('id');
      $status=$this->input->post('status');
      if($id>0){   
        $update_data=array('sub_login_id'=>$id,'sub_isActive'=>$status);
        $response['status']=$this->ACM->changeDataStatus('subscriberlogin',$update_data);
        if($response['status']){
            $data=array();
            $NewStatusString;
            $data['user']=$this->ACM->fetchUserRecord($id);
            $data['user_name'] = $data['user']['fname'];
            $to = $data['user']['sub_email'];
            $status = $data['user']['sub_isActive']; 
             $NewStatusString="Active";   
            if($status=='0'){ 
                  $NewStatusString="In-Active";
            } 
            $data['status']= $NewStatusString;
            $mail_data = $this->load->view("email_templates/user-status" ,$data, true);
            $subject = 'Wageni CRM status';
            $sent = sendEmail($to,'',$subject,$mail_data); 
            $survey_que_status = $data['user']['survey_que_status'];            
            if($survey_que_status=='0'){
              $category = $data['user']['category'];
                $allquestions=$this->MM->surveyQueByCat($id,$category);
                if ($allquestions>0) {
                  foreach ($allquestions as $key => $question) {
                      $questions = array(
                                  'businessId'        =>  $id,
                                  'bus_q_cat_id'      =>  $question['cat_id'],
                                  'bus_q_sub_cat_id'  =>  $question['SUB_cat_id'],
                                  'bus_cat_process'   =>  $question['process_cat_id'],
                                  'bus_q_que'         =>  $question['que'],
                                  'bus_q_type'        =>  $question['q_type']
                                );
                      $this->db->insert('questionnaire',$questions);
                      $que_last_insert_id= $this->db->insert_id();

                      foreach ($question['allresult'] as $key => $answer) {              

                        $answers = array(
                                  'bus_que_id'      => $que_last_insert_id,
                                  'businessId'      => $id,
                                  'bus_que_type'    =>$answer['que_type'],
                                  'bus_que_score'   =>$answer['que_score']
                                );
                         $this->db->insert('business_questionnaire_answer',$answers);
                     }
                  }
                      
                    $update_data=array('sub_login_id'=>$id,'survey_que_status'=>1);
                    $response['status']=$this->ACM->changeDataStatus('subscriberlogin',$update_data);
              }                
           }
          $response['message']="Company Status Updated Successfully!";      
         }
        $data=array('status'=>1);
        echo json_encode($data);
      }
    }
  }

  public function adminCompanyUsersDetailsAjax(){
   if ($this->input->is_ajax_request()) {
     
      $id=$this->input->post('id');
      if($id>0){
          $data['companyListAdmin'] = $this->ACM->getCompanyListAdmin($id);
          if(count($data)>0){
             $response['data'] =$this->load->view("admin/allusers/all_company_users", $data,true);      
           echo json_encode($response);   
          }
      }
    }
  }

  //editSubscriber/26
  public function adminUpdateCompanyAjax($id){
    $this->isSuperAdmin();
    $data['companydata']= $this->ACM->fetchCompanyBYId($id);
    $data['role'] = $this->ACM->fetchSubscribersRole();                     
    $data['package'] = $this->PM->fetchPackage(); 
    $data['category'] = $this->MM->fetchCategory(); 
    $this->load->view("admin/allusers/update_company", $data);         
     
  }
  /* public function updateCompany($id){

      $data['companydata'] = $this->ACM->fetchCompanyBYId($id);
       $this->load->view("admin/allusers/update_company", $data);  

   }*/

}