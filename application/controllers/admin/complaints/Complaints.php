<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

    require APPPATH . '/libraries/BaseController.php';

    class Complaints extends BaseController
    {
        
        public function __construct()
        {
            parent::__construct(); 
           // $this->load->model('All_company_model' , 'ACM'); 
            $this->load->model(array('All_company_model' => 'ACM','AuthModel' => 'AUTHM', 'AuthModel' => 'AM', 'PackageModel' => 'PM', 'MastersModel' => 'MM'));


        }

        public function index(){
             $this->isSuperAdmin();
            $data['customers']=$this->ACM->companyList();
            $data['category'] =  $this->MM->fetchCategory(); 
            $this->load->view("admin/complaints/complaints", $data); 
        }
     

        public function viewAllCompanyList()
        {  
          if ($this->input->is_ajax_request()) {
          

                 $data = $this->ACM->companyListAjax($_POST,false);
                  $totalData = $this->ACM->companyListAjax($_POST,true);


                    $response=array();
                    if(count($data)>0){
                        foreach($data as $row){     
                        $score = number_format($row['score'], 2);
                       
                            $response[]=array(
                                'id'=>$row['sub_login_id'],
                                'name'=>ucfirst($row['fname'].' '.$row['lname']),
                                'category_name'=>ucfirst($row['category_name']),
                                'sub_name'=>ucfirst($row['sub_name']),
                                'package_name'=>ucfirst($row['package_name']),
                                //'price'=> $row['package_price'].' '.$this->config->item('krw'),
                                'price'=> $row['package_price'],
                                'score'=> $row['score'] ? number_format($row['score'], 2) : 0,
                                'expiredate'=>ucfirst($row['subscriber_end_date']),
                                'email'=>$row['sub_email'],
                                'status'=>$row['sub_isActive'],
                                'image'=>$row['userfile'],
                                'all'=>$row,
                                'company_url'=>$row['company_url']
                            );
                        }
                    }

                    $json_data = array(
                    "recordsTotal"    => $totalData,  
                    "recordsFiltered" => $totalData,
                    "data"            => $response 
            );
            echo json_encode($json_data);
             }           
 
           
 
                       
        }




  public function changeCompanyStatus(){
    if ($this->input->is_ajax_request()) {

      $id=$this->input->post('id');
      $status=$this->input->post('status');
      if($id>0){
        $update_data=array('id'=>$id,'status'=>$status);
        //$response['status']=$this->ACM->changeDataStatus('subscriberlogin',$update_data);
        if($response['status']){
          $response['message']="Company Status Updated Successfully!";
      
         }
        $data=array('status'=>1);
        echo json_encode($data);
      }
    }
  }





  public function adminCompanyUsersDetailsAjax(){
   if ($this->input->is_ajax_request()) {
     
      $id=$this->input->post('id');
      if($id>0){
          $data['companyListAdmin'] = $this->ACM->getCompanyListAdmin($id);
          if(count($data)>0){
             $response['data'] =$this->load->view("admin/allusers/all_company_users", $data,true);      
           echo json_encode($response);   
          }
      }
    }
  }

  //editSubscriber/26




  public function adminUpdateCompanyAjax($id){
    $this->isSuperAdmin();
    $data['companydata']= $this->ACM->fetchCompanyBYId($id);
    $data['role'] = $this->ACM->fetchSubscribersRole();                     
    $data['package'] = $this->PM->fetchPackage(); 
    $data['category'] = $this->MM->fetchCategory(); 
    $this->load->view("admin/allusers/update_company", $data);         
     
  }
  /* public function updateCompany($id){

      $data['companydata'] = $this->ACM->fetchCompanyBYId($id);
       $this->load->view("admin/allusers/update_company", $data);  

   }*/






    }