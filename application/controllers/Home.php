<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Home (UserController)
 * Home Class to control Home Pages related operations.
 * @author : Esther Praveen
 * @version : 1.1
 * @since : 28 August 2018
 */
class Home extends BaseController
{

    //  This is default constructor of the class

    public function __construct()
    {
        parent::__construct();  
        $this->load->model(array('LoginModel', 'All_company_model' => 'ACM','MastersModel' => 'MM'));
        $this->load->helper('wageni');
    }

    public function logout(){
        $this->session->unset_userdata('useremail');
        $this->session->sess_destroy();
        redirect(base_url());
    }
    
    // This function used to load the first screen of the user

    public function index()
    {
        $data['pageTitle'] = 'Home';    
        $data['homeslider'] = $this->MastersModel->fetchSlider();  
        $data['alldata'] = $this->MastersModel->FetchClientLogo();
        $data['alltestimonials'] = $this->MastersModel->getTest();        
        $data['packge_demo'] = $this->LoginModel->fetch_packge_demo();
        $data['practice'] = $this->MastersModel->fetchPractice();
        $data['teamdata'] = $this->MastersModel->fetchTeam();    

        $this->load->view("website/home", $data);
    }

    public function hotellogin(){
        $this->load->view("website/login");
    }

    public function hotelForgetPassword(){
        $this->load->view("website/forget-password");
    }

    public function login() {

       

        $response=array();

        $loginData = array(
                'uEmail'=>html_escape(trim($this->input->post('lemail', TRUE))),
                'uPass'=>html_escape(trim($this->input->post('lpass', TRUE)))
            );

        $response= $this->LoginModel->checkfrontLogin($loginData);   
         //pr($_POST);
         //prd($response);
        if($response['status'] ==true ){
            $this->session->set_userdata('sub_login_id',$response['data']->sub_login_id);
            $this->session->set_userdata('added_by',$response['data']->added_by);
            $this->session->set_userdata('useremail',$response['data']->sub_email);
            $this->session->set_userdata('username',$response['data']->sub_name);
            $this->session->set_userdata('fname',$response['data']->fname);
            $this->session->set_userdata('lname',$response['data']->lname);
            $this->session->set_userdata('userrole',$response['data']->sub_login_role);
            $this->session->set_userdata('rolename',$response['data']->subscriber_role_name);
            $this->session->set_userdata('member_since',$response['data']->subscriber_created);
            $this->session->set_userdata('userfile',$response['data']->userfile);
            $this->session->set_userdata('category',$response['data']->category);
            $this->session->set_userdata('category_name',$response['data']->category_name);
        }
       // prd($response);
        echo json_encode($response);         
    }
     // Company Dashboard  
    public function web_admin_dashboard(){ 
        //$this->isLogin();
        $data['pageTitle'] = 'Web Admin Dashboard';   
        $data['totalBranch'] = $this->MastersModel->totalBranch();    
        $data['complaintsgraph'] = $this->MM->adminGetAllComplaintsAjaxforgraph(); 
        $data['complaints'] =   $this->ACM->subadminGetAllComplaintsAjax($id=null); 
        $this->load->view("webadmin/index", $data);
    } 

    public function viewAllComplaintsGtaphFilterList(){
        if ($this->input->is_ajax_request()) { 
            echo "string";
        }
    }

    public function sendSurvey(){
        $this->load->view("website/emailsurvey");
    }

    public function company_register()    {
        //echo "<pre/>"; print_r($this->input->post()); die;
        $this->form_validation->set_rules('fname', ' First Name', 'required|min_length[1]|max_length[15]|trim');  
        $this->form_validation->set_rules('lname', 'Last Name', 'required|min_length[1]|max_length[15]|trim');  
        $this->form_validation->set_rules('company_name', 'Company Name', 'required|trim');  
        //$this->form_validation->set_rules('sub_phone', '', 'required|min_length[9]|max_length[13]|trim'); 
        $this->form_validation->set_rules('company_email', 'Email', 'required|valid_email|is_unique[subscriberlogin.sub_email]|trim');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('company_url', 'Company Url', 'required');
        $this->form_validation->set_rules('hvroom', 'total Rooms', 'required');
        /*echo $p=random_string('alnum', 16);
        echo "</br>";
        echo $pa=random_string('sha1', 16);*/
        $pass="123456";
        if($this->form_validation->run()){
            $company_registerData=array('fname'=>html_escape(trim($this->input->post('fname', TRUE))),
                'lname'=>html_escape(trim($this->input->post('lname', TRUE))),
                'sub_phone'=>html_escape(trim($this->input->post('company_phone', TRUE))),
                'sub_name'=>html_escape(trim($this->input->post('company_name', TRUE))),
                'sub_email'=>html_escape(trim($this->input->post('company_email', TRUE))),
                'company_url'=>html_escape(trim($this->input->post('company_url', TRUE))),
                'country'=>html_escape(trim($this->input->post('country', TRUE))),
                'category'=>html_escape(trim($this->input->post('category', TRUE))),
                'sub_login_role'=>1,
                'added_by'=>0,
                'sub_pass'=>$pass,
                'hvroom'=>html_escape(trim($this->input->post('hvroom', TRUE)))
            );

            $this->session->set_userdata('reg_name',$company_registerData['fname']);
            $this->session->set_userdata('reg_email',$company_registerData['sub_email']);
            $this->session->set_userdata('reg_phone',$company_registerData['sub_phone']);

            $loginresult= $this->LoginModel->companyRegister($company_registerData);  
            
            if($loginresult ==TRUE ){  
                echo "registered";


                $data['name']=$this->input->post('fname'). " ".$this->input->post('lname');
                $data['content']='Thank You for registering on Wageni CRM. Your account is waiting for admin approval.';
                $data['mailtitle']='Welcome to Wageni CRM.';
                $mail_data=  $this->load->view('email_templates/register-mail', $data,true);

                $to=$this->input->post('company_email');             
                $subject='Wageni CRM : Registered';

                $sent= sendEmail($to,'',$subject,$mail_data); 

              /*  $msg = 'Thank You for registering on Wageni CRM. Your account is waiting for admin approval.';
                $sub = 'Registered';
                $name=$this->session->userdata('reg_name');
                $email=$this->session->userdata('reg_email');

                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'mastcrew22@gmail.com',
                    'smtp_pass' => 'crew@1234',
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );
                 $this->load->library('email', $config);
                 $this->email->initialize($config);*/  


             }else{
                echo "err";
            }                   
        }
        else
        {
            echo "1";
        }
    }

    public function testsms(){
                /*$username = "moses";
                $password = "workstuff18";
                $shortcode = "INFOTEXT";

                $mobile = "+254722453777";
                //$mobile=$this->session->userdata('reg_phone');
                $message = " This is a test message ";

                $finalURL = "https://isms.advantasms.com:1112/sendsms.php?username=" . urlencode($username) . "&password=" . urlencode($password) . "&message=" . urlencode($message) . "&shortcode=$shortcode&mobile=$mobile";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $finalURL);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec($ch);
                curl_close($ch);
               // print_r($response);
                echo "Response: $response";*/


           /*     $username="moses";
                $Key="m6Z0mSfzivUKCujaQsvfdqnecYhjiTVhRyCqdj85sCPfAHqk61";
                $senderId="Smartlink";
                $tophonenumber="+254722453777";
                $finalmessage=urlencode("This is For Testing Purpose Only");
                $finalURL="https://sms.movesms.co.ke/api/compose?username=".$username."&api_key=".$Key."&sender=".$senderId."&to=".$tophonenumber."&message=".$finalmessage."&msgtype=5&dlr=0";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $finalURL);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $response = curl_exec($ch);
                curl_close($ch);*/
               // print_r($response);
                $username="moses";
                $Key="m6Z0mSfzivUKCujaQsvfdqnecYhjiTVhRyCqdj85sCPfAHqk61";
                $senderId="SMARTLINK";
                $tophonenumber="+254705666677";
                $finalmessage=urlencode("This is For Wageni Testing Only");         

                //"https://sms.movesms.co.ke/api/compose?username=moses&api_key=m6Z0mSfzivUKCujaQsvfdqnecYhjiTVhRyCqdj85sCPfAHqk61&sender=SMARTLINK&to=[Your+Recipients]&message=[Your message]&msgtype=[Type of the message]&dlr=[Type of Delivery Report] ";  

                $Curl = curl_init();
                $CurlOptions = array(
                    CURLOPT_URL => "https://sms.movesms.co.ke/api/compose?username=".$username."&api_key=".$Key."&sender=".$senderId."&to=".$tophonenumber."&message=".$finalmessage."&msgtype=5&dlr=0",    
                    CURLOPT_FOLLOWLOCATION => false,
                    CURLOPT_POST => false,
                    CURLOPT_HEADER => false,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CONNECTTIMEOUT => 15,
                    CURLOPT_TIMEOUT => 100,
                );
                curl_setopt_array($Curl, $CurlOptions);
                $data = curl_exec($Curl);   
                //$array_data = explode("|",$data);

                print_r($data);



               /* $parse_url=file($live_url);

                 $output1= $parse_url[0];
              */

             }

             /* ************************************************** Forgot password ****************************************************** */

             public function ForgotPassword1(){  
                $reponse = array();
                $email = $this->input->post('email');
                $this->load->model('LoginModel');     
                $findemail = $this->LoginModel->ForgotPassword($email);  
                $reponse = array('success' => isset($findemail));
                if($findemail){
        //if(isset($reponse)){
                  $this->LoginModel->sendpassword($findemail);  
        //echo $this->email->print_debugger();die;
                  $this->session->set_flashdata('success',' Password Send to your Email Id !');
              }else{
                $this->session->set_flashdata('success',' No record found!');
            }
            header('Content-type: application/json');
            echo json_encode($reponse);

        }



        public function ForgotPassword(){

            $this->form_validation->set_rules('lost_email', 'Email', 'required|valid_email|trim');
        //echo $frogetemail = $this->input->post('lost_email'); die;
            if ($this->form_validation->run()){
                $forgotemail = $this->input->post('lost_email');
                $forgot = $this->LoginModel->filename_exists($forgotemail);
        //print_r($forgot); die;
                if($forgot ==true ){  
                    echo "Password successfuly send to your mail id. Please check your Mail !";
                    $this->session->set_userdata('name',$forgot[0]->fname);
                    $this->session->set_userdata('forgot_email',$forgot[0]->sub_email);
                    $this->session->set_userdata('forgot_pass',$forgot[0]->sub_pass);
                    $name=$this->session->userdata('name');
                    $pass=$this->session->userdata('forgot_pass');
                    $email=$this->session->userdata('forgot_email');
                    
                    $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://smtp.googlemail.com',
                        'smtp_port' => 465,
                        'smtp_user' => 'mastcrew22@gmail.com',
                        'smtp_pass' => 'crew@1234',
                        'mailtype'  => 'html', 
                        'charset'   => 'iso-8859-1'
                    );

                 $baseurl= base_url();          //$mailMsg= 'Dear '.$name.' Your Email Id = '.$email.' ,Password = '.$pass.''; 
                 $mailMsg='<!DOCTYPE html>
                 <html>
                 <head>
                 <meta name="viewport" content="width=device-width, initial-scale=1.0">
                 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                 <title>Wageni Newsletter</title>
                 <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,700,800,900" rel="stylesheet">
                 </head>
                 <body style="background-color: #f1f1f1; font-family: raleway, sans-serif; font-size: 15px; margin: 0; padding: 0;">
                 <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="background: #f1f1f1; font-family: "raleway", sans-serif; font-size: 15px;">
                 <tr>
                 <td align="center"><table border="0" cellpadding="10" cellspacing="0" width="600" align="center"><tr>
                 <td align="center"><table border="0" cellpadding="15" cellspacing="0" width="600" style="background: transparent;">
                 <tr>
                 <td align="center"><a href=""><img src="'.$baseurl.'globalassets/admin/logo1.png"></a></td>
                 </tr>
                 </table>
                 <table border="0" cellpadding="10" cellspacing="10" width="600" style="background: #fff;">
                 <tr>
                 <td align="center" style="font-size: 36px; font-weight: bold; color: #555; letter-spacing: 1px;">Welcome to Wageni CRM.</td>
                 </tr>
                 <tr><td align="center" style="font-size: 30px; font-weight: bold; color: #10a1da; letter-spacing: 1px;">Dear '.$name.' </td></tr>
                 <tr>
                 <td>Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>'.$pass.'</b></td>
                 </tr>
                 <tr><td>Login for please click on the following link <a href="'.$baseurl.'">Login</a></td></tr>
                 <tr><td>Thanks & Regards</td>
                 </tr>
                 <tr><td><b>Wageni CRM Team</b></td>
                 </tr>
                 </table>
                 <table border="0" cellpadding="15" cellspacing="0" width="600" style="background: transparent;">
                 <tr>
                 <td align="center">Copyright &copy; 2018 <a href="'.$baseurl.'">wagenicrm.com</a>. All rights reserved.</td>
                 </tr>
                 </table>
                 </td>
                 </tr>
                 </table>
                 </td>
                 </tr>
                 </table>
                 </body>
                 </html>'; 
                 $this->load->library('email', $config);
                 $this->email->set_newline("\r\n");          
                 $this->email->from('praveen@panindia.in', 'Forgot Password');
                 $this->email->to($email);
                 $this->email->subject('Wageni CRM');
                 $this->email->message($mailMsg);
                 $result = $this->email->send();
             }else{
                echo "err";
            }
        }
        else{
            echo "err";
        }

    }

    /* ********************************************** Forgot password ************************************************* */

}

?>