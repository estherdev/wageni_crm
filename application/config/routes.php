<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/*	Static Pages	*/
$route['default_controller'] = 'Home';
$route['logout'] = 'Home/logout';
$route['user-login'] = 'Home/hotellogin';
$route['user-forget-password'] = 'Home/hotelForgetPassword';
$route['dashboard'] = 'Home/web_admin_dashboard';
$route['about-us'] = 'website/Pages/about_us';
$route['clients'] = 'website/Pages/clients';
$route['history'] = 'website/Pages/history';
$route['testimonials'] = 'website/Pages/testimonials';
$route['contact-us'] = 'website/Pages/contact_us';
$route['package'] = 'website/Pages/package';
$route['faq'] = 'website/Pages/faq';
$route['blog'] = 'website/Pages/blog';
$route['user-dashboard'] = 'website/Pages/user_dashboard';
$route['404_override'] = '';
$route['success'] = 'website/Pages/success';
$route['payment'] = 'website/Pages/payment';
$route['checkout'] = 'website/Pages/checkout';
$route['translate_uri_dashes'] = FALSE;

/* Category  */
 



/* Add User  */
$route['add-user'] = 'admin/allusers/Auth';
$route['company-export'] = 'admin/allusers/Auth/customerExport';
$route['view-user'] = 'admin/allusers/Auth/viewUsers';
$route['editUser/(:num)'] = 'admin/allusers/Auth/getUserById/$1';
$route['updateUser/(:num)'] = 'admin/allusers/Auth/updateUser/$1';
$route['deleteusers/(:num)'] = 'admin/allusers/Auth/deleteUsers/$1';

/* Add New Company  */
$route['admin-add-company'] = 'admin/allusers/Auth/adminAddCompany';
$route['admin-view-customer'] = 'admin/allusers/Auth/viewCustomer';
$route['admin-view-company-status'] = 'admin/allusers/Auth/viewCompanyStatus';
$route['view-subscriber'] = 'admin/allusers/Auth/viewSubscriber';
$route['editSubscriber/(:num)'] = 'admin/allusers/Auth/getSubscriberById/$1';
$route['updateSubscriber/(:num)'] = 'admin/allusers/Auth/updateSubscriber/$1';
$route['admin-delete-company/(:num)'] = 'admin/allusers/Auth/adminDeleteCompany/$1';

/* Main Adminn view Company*/
$route['view-company-admin'] = 'admin/allusers/Auth/viewSubscriberAdmin';
$route['view-company-sub-admin'] = 'admin/allusers/Auth/viewSubscriberSubAdmin';
$route['delete-company-sub-admin/(:num)'] = 'admin/allusers/Auth/deletesubscribers/$1';



$route['add-admin-role'] = 'admin/allusers/Auth/addAdminRole';
$route['edit-admin-role/(:num)'] = 'admin/allusers/Auth/getAdminRole/$1';
$route['update-admin-role/(:num)'] = 'admin/allusers/Auth/updateAdminRole/$1';
$route['deleteadminrole/(:num)'] = 'admin/allusers/Auth/deleteAdminRole/$1';

$route['add-user-role'] = 'website/SubsControler/addUserRole';
$route['edit-user-role/(:num)'] = 'website/SubsControler/editUserRole/$1';
$route['update-user-role/(:num)'] = 'website/SubsControler/updateUserRole/$1';
$route['deletesubscribersrole/(:num)'] = 'website/SubsControler/deleteSubscribersRole/$1';
/* All Masters  */
$route['view-packages'] = 'admin/masters/Packages';
$route['editPackages/(:num)'] = 'admin/masters/Packages/getPackagerById/$1';
$route['updatePackages/(:num)'] = 'admin/masters/Packages/updatePackages/$1';
$route['deletepackages/(:num)'] = 'admin/masters/Packages/deletePackages/$1';

$route['new-company'] = 'admin/masters/Company/newCompany';
$route['company-status/(:num)/(:num)/(:num)'] = 'admin/masters/Company/companyStatus/$1/$2/$3';
$route['company-admin-status/(:num)/(:num)'] = 'admin/masters/Company/companyAdminStatus/$1/$2';
$route['add-company'] = 'admin/masters/Company';
$route['company-survey-status/(:num)/(:num)'] = 'admin/masters/Company/companyAdminSurveyStatus/$1/$2';

// Home Page Slider
$route['view-slider'] = 'admin/masters/Slider';
$route['editSlider/(:num)'] = 'admin/masters/Slider/getSliderById/$1';
$route['updateSlider/(:num)'] = 'admin/masters/Slider/updateSlider/$1';
$route['deleteSlider/(:num)'] = 'admin/masters/Slider/deleteSlider/$1';

/*	FAQ  */
$route['view-faq'] = 'admin/masters/Faq';
$route['editFaq/(:num)'] = 'admin/masters/Faq/getbyid/$1/';
$route['deleteFaq/(:num)'] = 'admin/masters/Faq/deleteFaq/$1';

/*   BLOG   */
$route['view-blog'] = 'admin/masters/Blog';
$route['edit-blog/(:num)'] = 'admin/masters/Blog/getbyid/$1';
$route['update-blog/(:num)'] = 'admin/masters/Blog/editdata/$1';
$route['delete-blog/(:num)'] = 'admin/masters/Blog/delete/$1';

/* Pages  */
$route['loginMe'] = 'admin/AdminLogin';
$route['login'] = 'admin/AdminLogin/login';
$route['adminLogout'] = 'admin/AdminLogin/logout';

/* Subscriber Login */
$route['subloginMe'] = 'admin/AdminLogin/SubscriberLoginView';
$route['subscriberLogin'] = 'admin/AdminLogin/SubscriberDashboardLogin';
$route['subscriber-dashboard'] = 'admin/Dashboard/subscriber_dashboard';
$route['SubscriberLogout'] = 'admin/AdminLogin/logout';


/* User Branch*/
$route['view-branch'] = 'website/SubsControler';
$route['edit-branch/(:num)'] = 'website/SubsControler';
$route['delete-branch/(:num)'] = 'website/SubsControler/deleteBranch/$1';

/* Main subscriber Add Own Employee*/
$route['add-subscriber-employee'] = 'website/SubsControler/addSubscriber';
$route['view-subscriber-employee'] = 'website/SubsControler/viewSubscriber';
$route['edit-subscriber-employee/(:num)'] = 'website/SubsControler/getSubscriberById/$1';
$route['update-subscriber-employee/(:num)'] = 'website/SubsControler/updateSubscriber/$1';
$route['delete-subscriber-employee/(:num)'] = 'website/SubsControler/deleteSubscribers/$1';


$route['add-about'] = 'admin/masters/About';
//$route['edit-about'] = 'admin/masters/About/';
$route['view-subscriber-employee'] = 'website/SubsControler/viewSubscriber';
$route['edit-subscriber-employee/(:num)'] = 'website/SubsControler/getSubscriberById/$1';
$route['update-subscriber-employee/(:num)'] = 'website/SubsControler/updateSubscriber/$1';
$route['delete-subscriber-employee/(:num)'] = 'website/SubsControler/deleteSubscribers/$1';


/* Testimonials */
$route['view-testimonials'] = 'admin/masters/Testimonials';
$route['edit-testimonials/(:num)'] = 'admin/masters/Testimonials/gettesttbyid/$1';
$route['update-testimonials/(:num)'] = 'admin/masters/Testimonials/editTest/$1';
$route['delete-testimonials/(:num)'] = 'admin/masters/Testimonials/delete/$1';


/* Update Profile For Super Admin */
$route['edit-admin-profile'] = 'admin/allusers/Auth/fetchAdminProfile';

/* ClientLogo */
$route['view-clientlogo'] = 'admin/masters/ClientLogo';
$route['edit-clientlogo/(:num)']   = 'admin/masters/ClientLogo/getClientLogo/$1';
$route['update-clientlogo/(:num)'] = 'admin/masters/ClientLogo/editClientLogo/$1';
$route['delete-clientlogo/(:num)'] = 'admin/masters/ClientLogo/delete/$1';

/*Contact*/
$route['view-contact'] = 'admin/masters/Contact';
$route['edit-contact/(:num)']   = 'admin/masters/Contact/getContactById/$1';
$route['update-contact/(:num)'] = 'admin/masters/Contact/editContact/$1';
$route['delete-contact/(:num)'] = 'admin/masters/Contact/delete/$1';

/* History */
$route['view-history'] = 'admin/masters/History';
$route['edit-history/(:num)'] = 'admin/masters/History/getHistoryId/$1';
$route['eddit-history/(:num)'] = 'admin/masters/History/getHistoryDataId/$1';
$route['update-history/(:num)'] = 'admin/masters/History/editHistory/$1';
$route['delete-history/(:num)'] = 'admin/masters/History/delete/$1';

/*   Practice Area   */
$route['view-practice'] = 'admin/masters/Practice';
$route['edit-practice/(:num)'] = 'admin/masters/Practice/getbyid/$1';
$route['update-practice/(:num)'] = 'admin/masters/practice/editPractice/$1';
$route['delete-practice/(:num)'] = 'admin/masters/Practice/delete/$1';

/*   Team    */
$route['view-team'] = 'admin/masters/Team';
$route['edit-team/(:num)'] = 'admin/masters/Team/getTeambyid/$1';
$route['update-team/(:num)'] = 'admin/masters/Team/editTeam/$1';
$route['delete-team/(:num)'] = 'admin/masters/Team/delete/$1';
//$route['delete-history/(:num)'] = 'admin/masters/History';

/* Category */
$route['admin-manage-category'] = 'admin/masters/Category/addUpdateCategory';
$route['admin-view-category'] = 'admin/masters/Category/categoryView';
$route['admin-category-hierarchy'] = "admin/masters/Category/categoryHierarchy";
$route['admin-add-update-category'] = "admin/masters/Category/saveCategory";
$route['admin-manage-category-ajax'] = "admin/masters/Category/manageCategories";
$route['admin-change-category-status'] = "admin/masters/Category/changeStatus";
$route['admin-delete-category'] = "admin/masters/Category/deleteCategoryNew";
$route['admin-update-category/(:num)'] = "admin/masters/Category/addUpdateCategory/$1";
$route['admin-get-parent-category'] = "admin/masters/Category/categoryParentDom";
$route['admin-get-category-level'] = "admin/masters/Category/categoryLevel";


$route['view-category'] = 'admin/masters/Category/addCategory';
$route['edit-category/(:num)'] = 'admin/masters/Category/editCategory/$1';
$route['update-category/(:num)'] = 'admin/masters/Category/updateCategory/$1';
$route['delete-category/(:num)'] = 'admin/masters/Category/deleteCategory/$1';


/* Sub Category */
$route['view-sub-category'] = 'admin/masters/Category/addSubCategory';
$route['edit-sub-category/(:num)'] = 'admin/masters/Category/editSubCategory/$1';
$route['update-sub-category/(:num)'] = 'admin/masters/Category/updateSubCategory/$1';
$route['delete-sub-category/(:num)'] = 'admin/masters/Category/deleteSubCategory/$1';


/* Sub Category */
$route['view-category-process'] = 'admin/masters/Category/addCategoryProcess';
$route['edit-category-process/(:num)'] = 'admin/masters/Category/editCategoryProcess/$1';
$route['update-category-process/(:num)'] = 'admin/masters/Category/updateCategoryProcess/$1';
$route['delete-category-process/(:num)'] = 'admin/masters/Category/deleteCategoryProcess/$1';


/* 	Survey For Super Admin 	*/
$route['survey'] = 'admin/Dashboard/survey/';
$route['questionSubmit'] = 'admin/Dashboard/questionSubmit';
$route['allQueByCat'] = 'admin/Dashboard/allQueByCat';
$route['catid'] = 'admin/Dashboard/catid/';
$route['subcatid/(:num)'] = 'admin/Dashboard/subcatid/$1';
$route['QuesAns'] = 'admin/Dashboard/fetchQuestionnaireById';


/* 	Create Survey For Admin 	*/
$route['create-survey'] = 'website/SubsControler/addQue';
$route['admin-questionSubmit'] = 'website/SubsControler/adminquestionSubmit';
$route['admin-allQueByCat'] = 'website/SubsControler/allQueByCat';
$route['admin-catid'] = 'website/SubsControler/catid/';
$route['admin-subcatid/(:num)'] = 'website/SubsControler/subcatid/$1';
$route['admin-QuesAns'] = 'website/SubsControler/fetchQuestionnaireById';


/* Update Profile */
$route['edit-profile'] = 'website/SubsControler/fetchProfileData';
//$route['update-profile'] = '';




/*Guest User */
$route['guestuser'] = 'website/Feedback/gusestUserList';
$route['sub-admin-send-notification-to-guest'] = 'website/Feedback/sendNotification';
$route['sub-admin-view-all-guest-user-ajax'] = 'website/Feedback/viewAllGusestUserList';
$route['save-guest-user'] = 'website/Feedback/saveGuest';
$route['editGuest/(:any)'] = 'website/Feedback/getGuestData/$1';
$route['update-guestuser/(:num)'] = 'website/Feedback/editGuestUser/$1';
$route['delete-guestuser/(:num)'] = 'website/Feedback/deleteGuestUser/$1';





/* Survey */

$route['survey-form/(:any)/(:any)'] = 'website/Pages/getsurvey/$1/$2';
$route['guest-save-survey-question'] = 'website/Pages/guestSaveSurveyQuestion';
//$route['survey-form/([\w+-]+)(\.[\w+-]+)*@([a-zA-Z\d-]+\.)+[a-zA-Z]{2,6}'] = 'website/Pages/dummy/$1';



$route['complaintsRafer/(:any)'] = 'website/Feedback/complaintsRafer/$1';
$route['complaintsRafer1/(:any)'] = 'website/Feedback/complaintsRafer1/$1';


/******************************************** 	WEBADMIN 	****************************************************/

$route['user-complaints'] = 'website/Feedback/fetch_feedbck_result';

/* All Complaints role wise	*/
$route['admin-guest-user-feedback'] = 'website/Feedback/fetch_feedbck_result_admin/';
$route['supervisior-guest-user-feedback'] = 'website/Feedback/fetch_feedback_supervisior';
$route['gernal-manger-guest-user-feedback'] = 'website/Feedback/fetch_feedback_gernalmanger';
$route['manger-guest-user-feedback'] = 'website/Feedback/fetch_feedback_manger';
$route['fetch-feedback'] = 'website/Feedback/fetchFeedback';


//$route['admin-view-all-complaints'] = 'admin/feedback/Complaints';

$route['sub-admin-change-question-options'] = 'website/Company/saveQuestionOptionsAjax';
$route['sub-admin-view-all-questions-list'] = 'website/Company/allQuestionsList';
$route['sub-admin-view-all-questions-list-ajax'] = 'website/Company/allQuestionsDetailsAjax';
$route['sub-admin-all-questions-details-ajax'] = 'website/Company/viewallQuestionsDetailsAjax';

$route['sub-admin-view-feedback'] = 'website/Company/complaintsListNetPromterScore';
$route['sub-admin-view-net-promter-score-survey-ajax'] = 'website/Company/complaintsListNetPromterScoreAjax';
$route['sub-admin-view-survey-user/(:num)'] = 'website/Company/complaintsListByUser/$1';

$route['sub-admin-view-all-feedback/(:num)'] = 'website/Company/complaints/$1';
$route['sub-admin-view-all-complaints-ajax'] = 'website/Company/viewAllComplaintsList';
$route['sub-admin-complaints-graph-filter-ajax'] = 'website/Home/viewAllComplaintsGtaphFilterList';

$route['sub-admin-delete-question'] = 'website/Company/subadminDeleteQuestion';


$route['sub-admin-view-all-users'] = 'subadmin/Users';
$route['sub-admin-add-sub-users'] = 'subadmin/Users';

/******  	ADMIN 	*******/
$route['pi'] = 'admin/allusers/All_company/panindia';
$route['pie'] = 'admin/allusers/All_company/panindiaexpire';
$route['admin-change-question-options'] = 'admin/allusers/All_company/saveQuestionOptionsAjax';
$route['admin-view-all-questions-list'] = 'admin/allusers/All_company/allQuestionsList';
$route['admin-view-all-questions-list-ajax'] = 'admin/allusers/All_company/allQuestionsDetailsAjax';
$route['admin-all-questions-details-ajax'] = 'admin/allusers/All_company/viewallQuestionsDetailsAjax';
$route['admin-delete-question'] = 'admin/allusers/All_company/adminDeleteQuestion';

$route['admin-view-feedback'] = 'admin/allusers/All_company/admincomplaintsListNetPromterScore';
$route['admin-view-all-feedback/(:num)'] = 'admin/allusers/All_company/complaints/$1';
$route['admin-view-all-complaints-ajax'] = 'admin/allusers/All_company/viewAllComplaintsList';
$route['admin-view-all-company'] = 'admin/allusers/All_company';
$route['admin-view-all-company-ajax'] = 'admin/allusers/All_company/viewAllCompanyList';
$route['admin-change-company-users-status'] = 'admin/allusers/All_company/changeCompanyStatus';
$route['admin-company-users-details-ajax'] = 'admin/allusers/All_company/adminCompanyUsersDetailsAjax';
$route['admin-update-company/(:any)'] = 'admin/allusers/All_company/adminUpdateCompanyAjax/$1';
$route['admin-send-notification-to-customer'] = 'admin/allusers/All_company/sendNotification';

//////DEEKSHA
$route['Cart/buypackage/(:num)'] = "website/Cart/buypackage/$1";
