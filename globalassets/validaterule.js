


// validate for Client logo 
$('form[id="client"]').validate({

 rules: {
   name:{
          required: true,
          minlength: 3,
          maxlength:15
            },
   userfile: 'required',
  
 },
 messages: {
   name: 'Please Enter Name !',
  userfile: 'Please Upload an Image !',  
 },

});

// validate for Super Admin Profile 
$('form[id="profile"]').validate({

 rules: {
   name:{
          required: true,
          minlength: 3,
          maxlength:15
            },
  username:{
          required: true
            },
  uphone:{
          required: true,
          minlength: 3,
          maxlength:15,
          number:true
            },
  upass: 'required'
  
 },
 messages: {
   name: 'Please Enter Name !',
   username: 'Please Enter Username !',
   uphone: 'Please Enter Phone Number !',
   upass: 'Please Enter Password !'
 },

});

//validate for edit client logo
$('form[id="editclient"]').validate({

 rules: {
     name:{
          required: true,
          minlength: 3,
          maxlength:15
            },
   //userfile: 'required',
 },
 messages: {
   name: 'Please Enter Name !',
  //userfile: 'Please Upload an Image !',  
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

// validate for add history
$('form[id="history"]').validate({

 rules: {
   date: 'required',
   content:'required'
  
 },
 messages: {
  date: 'Please Enter Date !',  
  content: 'Please Write The Content !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


// validate for Update history Only
$('form[id="historyupdate"]').validate({

 rules: {
   content:'required'
 },
 messages: {
  content: 'Please Write The Content !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


// validate for edit history
$('form[id="edithistory"]').validate({

 rules: {
   date: 'required',
   content:'required'
 },
 messages: {
   date: 'Please Enter Date !',
  content: 'Please Write The Content !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


/*
 uname:{
          required: true,
          minlength: 3,
          maxlength:15
            },
    uemail:{
            required: true,
            email: true
          },
   uphone:{
            required:true,
            minlength:9,
            maxlength:13,
            number: true
      },*/

// Validate for Edit contact
$('form[id="contact"]').validate({

 rules: {
   address:{required: true,
   },
   phone:{
          required: true,
          minlength:9,
          maxlength:13,
          number:true
         },
   phone:{
          required: true,
          minlength:9,
          maxlength:13,
          number:true
        },
   phone:{
          required: true,
          minlength:9,
          maxlength:13,
          number:true
        },
   email:{
            required: true,
            email: true
          },

   fphone:{
          required: true,
          minlength:9,
          maxlength:13
          /*number:true*/
        },

   femail:{
            required: true,
            email: true
          },
   faddress:{
    required: true,
   },

   fdescription:{
    required: true,
   }

 },
 messages: {
  address: 'Please Enter Address !',  
  phone:   'Please Enter Phone !',
  phone2:  'Please Enter Phone !',
  phone3:  'Please Enter Phone !',
  email:   'Please Enter Email !',
  fphone:  'Please Enter Phone  !',
  femail:  'Please Enter Email !',
  faddress:'Please Enter Address !',
  fdescription:'Please Enter Description !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

// Validate for Add Blog
$('form[id="addblog"]').validate({

 rules: {
   title: 'required',
   userfile: 'required',
   content:'required',
   created_date:'required',
   created_by:'required',
  
 },
 messages: {
  title: 'Please Enter Title!',  
  userfile: 'please Upload an Image!',
  content: 'Please Write The Content!',
  created_date: 'Please Enter Date !',
  created_by:'Please Enter Created By Field !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

// Validate for Edit Blog
$('form[id="editblog"]').validate({

 rules: {
   title: 'required',
   content:'required',
   created_date:'required',
   created_by:'required',
  
 },
 messages: {
  title: 'Please Enter Title!',  
  content: 'Please Write The Content!',
  created_date: 'Please Enter Date !',
  created_by:'Please Enter Created By Field!'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


// Validate for AddSlider
$('form[id="viewslider"]').validate({

 rules: {
   slider_title: 'required',
   slider_sub_title: 'required',
   slider_link_title:'required',
   slider_link:'required',
   slider_image:'required'
   
  
 },
 messages: {
  slider_title: 'Please Enter Title !',  
  slider_sub_title: 'please Enter Sub Title !',
  slider_link_title: 'Please Enter Link Title !',
  slider_link: 'Please Enter Link !',
  slider_image:'Please Upload an Image !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


// Validate for Edit Slider
$('form[id="editslider"]').validate({

 rules: {
   slider_title: 'required',
   slider_sub_title: 'required',
   slider_link_title:'required',
   slider_link:'required' 
 },
 messages: {
  slider_title: 'Please Enter Title !',  
  slider_sub_title: 'please Enter Sub Title !',
  slider_link_title: 'Please Enter Link Title !',
  slider_link: 'Please Enter Link !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

// Validate for Add Testimonials
$('form[id="test"]').validate({

 rules: {
   title: 'required',
   userfile:'required',
   title_text:'required'
 },
 messages: {
  title: 'Please Enter Title !',
  userfile:'Please Upload Image !',
  title_text:'Please Enter Content !' 
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


// Validate for Edit Testimonials
$('form[id="edittest"]').validate({

 rules: {
   title: 'required',
   title_text:'required'
 },
 messages: {
  title: 'Please Enter Title !',
  title_text:'Please Enter Content !' 
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


// Validate For Add Packages
$('form[id="package"]').validate({
	rules:{
		package_name: 'required',
		package_price: 'required',
		allowed_user: 'required',
		package_validity: 'required'
	},

	messages:{
		package_name: 'Please Enter Package Name !',
		package_price: 'Please Enter Package Price !',
		allowed_user: 'Please Enter No. Of Branch !',
		package_validity: 'Please Select Package Validity !'
	},
	submitHandler: function(form){
		form.submit();
	}
});

	//validate for edit Package
$('form[id="editpack"]').validate({
	rules:{
		package_name: 'required',
		package_price: 'required',
		allowed_user: 'required',
    package_validity: 'required'
	},

	messages:{
		package_name: 'Please Enter Package Name !',
		package_price: 'Please Enter Package Price !',
		allowed_user: 'Please Enter No. Of Branch !',
    package_validity: 'Please Select Package Validity !'
	},
	submitHandler: function(form){
		form.submit();
	}
});


// Validate for Add Practice Area
$('form[id="practice"]').validate({

 rules: {
   title: 'required',
   userfile:'required',
   content:'required'
 },
 messages: {
  title: 'Please Enter Title !',
  userfile:'Please Upload an Image !',
  content:'Please Enter Content !' 
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

// Validate for Edit Practice Area
$('form[id="editpractice"]').validate({

 rules: {
   title: 'required',
   content:'required'
 },
 messages: {
  title: 'Please Enter Title !',
  content:'Please Enter Content !' 
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

//Validate for FAQ
$('form[id="faq"]').validate({

 rules: {
   question: 'required',
   answer:'required'
 },
 messages: {
  question: 'Please Enter Question  !',
  answer:'Please Enter Answer !' 
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

// Validate for About_Us
$('form[id="about"]').validate({

 rules: {
   title: 'required',
   title_text:'required'
 },
 messages: {
  title: 'Please Enter Name !',
  title_text:'Please Enter Content !' 
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

	// Validate for Sub_Admin_Company
$('form[id="sub_admin_company"]').validate({

 rules: {
    fname:{
          required: true,
          minlength: 3,
          maxlength:15
                //alphanumeric : true,
                //noSpace: true
            },
      lname:{
              required: true,
              minlength: 3,
              maxlength:15
                //alphanumeric : true,
                //noSpace: true
            },
   uphone:{
              required:true,
              minlength:9,
              maxlength:13,
        //customphone:true,
        number: true
      },
          uemail:{
            required: true,
            email: true
          },
   upass: 'required',
   userrole: 'required',
   userpackage: 'required'
 },
 messages: {
  fname: 'Please Enter First Name !',
  lname:'Please Enter Last Name !' ,
  uemail: 'Please Enter Email Id !',
  uphone: 'Please Enter Mobile Number !',
  upass: 'Please Enter Password !',
  userrole: 'Please Select Role !',
  userpackage:'Please Select Package'  
 },
 submitHandler: function(form) {
   form.submit(); 
 }
});

 // Validate for Category
$('form[id="category"]').validate({

 rules: {
   category_name: 'required'
 },
 messages: {
  category_name: 'Please Enter  Category !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

//Validate for Sub Category
$('form[id="sub_cat"]').validate({

 rules: {
   main_cat_id: 'required',
   sub_cat_name: 'required'
 },
 messages: {
  main_cat_id: 'First Select Category !',
  sub_cat_name:'Please Enter Sub Category !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

//Validate for Sub Sub Category
$('form[id="sub_sub_cat"]').validate({

 rules: {
   main_cat_id: 'required',
   subcat: 'required',
   process_name: 'required'
 },
 messages: {
  main_cat_id: 'First Select Category !',
  subcat:'Please Select Sub Category !',
  process_name: 'Please Enter Process !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


// Validate for Add Team
$('form[id="add_team"]').validate({

 rules: {
   userfile:'required',
   name: 'required',
   post:'required',
   content:'required'
 },
 messages: {
  userfile:'Please Upload an Image !',
  name: 'Please Enter Name !',
  post: 'Please Enter Designation !',
  content:'Please Write The Content !' 
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


// Validate for Edit Team
$('form[id="edit_team"]').validate({

 rules: {
   name: 'required',
   post:'required',
   content:'required'
 },
 messages: {
  name: 'Please Enter Name !',
  post: 'Please Enter Designation !',
  content:'Please Write The Content !' 
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


   //Validate for Role
$('form[id="role"]').validate({

 rules: {
   rolename: 'required'
 },
 messages: {
  rolename: 'Please Enter Role Name !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


// Validate for users
$('form[id="user"]').validate({

 rules: {
    uname:{
          required: true,
          minlength: 3,
          maxlength:15
                //alphanumeric : true,
                //noSpace: true
            },
    uemail:{
            required: true,
            email: true
          },
   uphone:{
            required:true,
            minlength:9,
            maxlength:13,
        //customphone:true,
        number: true
      },
   upass:'required',
   username: 'required',
   userrole:'required'
 },
 messages: {
  uname: 'Please Enter Name !',
  username: 'Please Enter User Name !',
  uemail:'Please Enter E-mail Id !',
  upass: 'Please Enter Password !',
  uphone: 'Please Enter Valid Phone Number !',
  userrole:'Please Select Role !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

// Validate for edit users
$('form[id="edituser"]').validate({
 rules: {
     uname:{
          required: true,
          minlength: 3,
          maxlength:15
            },
    uemail:{
            required: true,
            email: true
          },
   uphone:{
            required:true,
            minlength:9,
            maxlength:13,
            number: true
      },
   username: 'required',
   upass:'required',
   userrole:'required'
 },
 messages: {
  uname: 'Please Enter Name!',
  username: 'Please Enter User Name !',
  uemail:'Please Enter E-mail Id !',
  upass: 'Please Enter Password !',
  uphone: 'Please Enter Valid Phone Number !',
  userrole:'Please Select Role !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

// Validate for edit subscriber
$("#editsubs").validate({
      rules:
      {
        fname:{
          required: true,
          minlength: 3,
          maxlength:15
                //alphanumeric : true,
                //noSpace: true
            },
            lname:{
              required: true,
              minlength: 3,
              maxlength:15
                //alphanumeric : true,
                //noSpace: true
            },
            uphone:{
              required:true,
              minlength:9,
              maxlength:13,
        //customphone:true,
        number: true
      },
          uemail:{
            required: true,
            email: true
          },
          upass:{ required: true },
          userrole:{ required: true },
          userpackage:{ required: true }
      },


 messages: {
  fname: 'Please Enter First Name !',
  lname: 'Please Enter Last Name !',
  uemail:'Please Enter E-mail Id !',
  upass: 'Please Enter Password !',
  uphone: 'Please Enter phone Number !',
  userrole: 'Please Select Role !',
  userpackage: 'Please Select Package !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});



/****************************************** Validation For Sub Admin Module Start************************************************/
//validate for Role
$('form[id="role"]').validate({

 rules: {
     rolename:{
          required: true,
          minlength: 3,
          maxlength: 15
            },  
 },
 messages: {
   rolename: 'Please Enter Role Name !',
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

//validate for Branch
$('form[id="branch"]').validate({

 rules: {
     branch_name:{
          required: true,
          minlength: 3,
          maxlength: 15
            },  
 },
 messages: {
   branch_name: 'Please Enter Branch Name !',
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


// Validate for Subscriber
$('form[id="subs"]').validate({

 rules: {
    fname:{
          required: true,
          minlength: 3,
          maxlength:15
                //alphanumeric : true,
                //noSpace: true
            },
   lname:{
          required: true,
          minlength: 3,
          maxlength:15
                //alphanumeric : true,
                //noSpace: true
            },
    uemail:{
            required: true,
            email: true
          },
   uphone:{
            required:true,
            minlength:9,
            maxlength:13,
        //customphone:true,
        number: true
      },
   userbranch:'required',
   upass:'required',
   username: 'required',
   userrole:'required'
 },
 messages: {
  userbranch: 'Please Select User Branch !',
  fname: 'Please Enter Your First Name !',
  lname: 'Please Enter Your Last Name !',
  username: 'Please Enter User Name !',
  uemail:'Please Enter E-mail Id !',
  upass: 'Please Enter Password !',
  uphone: 'Please Enter Valid Phone Number !',
  userrole:'Please Select Role !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

//validate for Guest User
$('form[id="guest"]').validate({

 rules: {
     user_name:{
          required: true,
          minlength: 3,
          maxlength: 15
            },  
    subcat:'required',
    user_id:'required',
    no_of_adult:'required',
    no_of_children: 'required',
    no_of_room: 'required',
    check_in:'required',
    check_out:'required'    
    },   
 messages: {
   subcat:'Please Select Hotel Name !',
   user_name: 'Please Enter User Name !',  
   user_id:'Please Enter E-mail Id !',
   no_of_adult:'Please Enter No.Of Adults !',
   no_of_children: 'Please Enter No. Of Children !',
   no_of_room: 'Please Enter No. Of Room Needed !',
   check_in:'Please Choose Check In Date !',
   check_out:'Please Choose Check Out Date !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

// validate for  Admin Profile 
$('form[id="editProfile"]').validate({

 rules: {
   fname:{
          required: true,
          minlength: 3,
          maxlength: 15
            },
    lname:{
          required: true,
          minlength: 3,
          maxlength: 15
            },
  uphone:{
           required:true,
           minlength:9,
           maxlength:13,
           number: true
          },
  upass: 'required'
  
 },
 messages: {
   fname: 'Please Enter First Name !',
   lname: 'Please Enter Last Name !',
   uphone:'Please Enter Phone Number !',
   upass: 'Please Enter Password !'
 },

});


  // validate for Sub Admin For Survey Question
$('form[id="editsurvey"]').validate({

 rules: {
   surveyQue: 'required'
 },
 messages: { 
  surveyQue: 'Please Enter The Question First !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});


  // validate for Admin For Update Survey Question
$('form[id="updateqstn"]').validate({

 rules: {
   surveyQue: 'required'
 },
 messages: { 
  surveyQue: 'Please Enter The Question First !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});

  // validate for Admin For Insert Survey Question
/*$('form[id="nextQues"]').validate({

 rules: {
   surveyQue: 'required'
 },
 messages: { 
  surveyQue: 'Please Enter The Question First !'
 },
 submitHandler: function(form) {
   form.submit();
  
 }
});*/

